/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     periph
 * @{
 *
 * @file        wdt.h
 * @brief       Implements a simple to use Watchdog driver
 * @details     Simple Watchdog execution, inicialization and reload registers
 *
 * @author      Rui Pires
 *
 * @}
 */

 #ifndef PERIPH_WDT_H
 #define PERIPH_WDT_H

#include "nrf51.h"
#include "nrf51_bitfields.h"


/* WDT timeout in seconds = (CRV + 1)/ 32768 */

/**
 * @brief       Defines the the watchdog register R0 timeout
 */
#define WDT_R0_TIMEOUT          (59)	/* (60) seconds */

/**
 * @brief       Initializes watchdog
 * @details     Setup watchdog execution, initializes registers and start timer.
 */
void wdt_init();

/**
 * @brief       Reload a specific watchdog register
 * @param[in]   reg
 *              The specific register
 */
void wdt_reload_register(int reg);

 #endif /* PERIPH_WDT_H */
