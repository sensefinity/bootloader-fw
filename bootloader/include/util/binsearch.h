/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @file        binsearch.h
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 * @date        8 Oct 2015
 * @brief       Implements a simple to use generic binary search algorithm.
 * @see         http://www.sensefinity.com
 */
#ifndef UTIL_BINSEARCH_H
#define UTIL_BINSEARCH_H

/* C standard library */
#include <assert.h>
#include <stddef.h>

/* Defines a new binary search implementation for a specific type */
#define define_binsearch(type) \
static int binsearch_##type(const type *array, size_t length, const type *key, \
        int compare(const type *, const type *)) __attribute__((unused)); { \
    int lft, rgt, mid, cmp; \
    assert(array != NULL); \
    if (length == 0) return -1; \
    lft = 0; rgt = length - 1; \
    \
    do { \
        mid = (lft + rgt) / 2; \
        cmp = compare(key, array + mid); \
        if (cmp < 0) { \
            rgt = mid - 1; \
        } else if (cmp > 0) { \
            lft = mid + 1; \
        } else { \
            return mid; \
        } \
    } while (lft <= rgt); \
    \
    return -lft - 1; \
}

#endif /* UTIL_BINSEARCH_H */
