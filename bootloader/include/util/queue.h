/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @file        vector.h
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 * @date        5 Oct 2015
 * @brief       Implements a generic queue, using pre-processor "hacking".
 * @details     Qeueues are FIFO (first-in, first-out) containers represented
 *              by circular arrays. This queue is written in a minimalistic
 *              style designed to reduce lines of code to the minimum. The
 *              implemented queue is interrupt-safe when used with a single
 *              producer and single consumer.
 * @see         http://www.sensefinity.com
 */
#ifndef UTIL_QUEUE_H
#define UTIL_QUEUE_H

/* C standard library */
#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>

/**
 * @brief       Tests whether a given number is a power of two.
 */
#define queue_ispow2(x)     (((x) != 0) && !((x) & ((x) - 1)))

/**
 * @brief       Applies the modulus operator to a dividend (a) and a divisor
 *              (b). This macro also optimizes for powers of two.
 */
#define queue_mod(a, b)     (queue_ispow2(a) ? ((a) & ((b) - 1)) : ((a) % (b)))

/* Defines a new queue implementation for a specific type */
#define define_queue(type) \
typedef struct queue_##type { \
    type    *array; \
    size_t  reads; \
    size_t  writes; \
    size_t  capacity; \
} *queue_##type##_t; \
\
static queue_##type##_t queue_##type##_new(size_t capacity) __attribute__((unused)); \
static void queue_##type##_delete(queue_##type##_t *pqueue) __attribute__((unused)); \
static void queue_##type##_put(queue_##type##_t queue, type element) __attribute__((unused)); \
static type queue_##type##_get(queue_##type##_t queue) __attribute__((unused)); \
static bool queue_##type##_empty(queue_##type##_t queue) __attribute__((unused)); \
static bool queue_##type##_full(queue_##type##_t queue) __attribute__((unused)); \
\
static queue_##type##_t queue_##type##_new(size_t capacity) { \
    queue_##type##_t queue = malloc(sizeof(struct queue_##type)); \
    if (!queue) return NULL; \
    queue->array = malloc(capacity * sizeof(type)); \
    if (!queue->array) { free(queue); return NULL; } \
    queue->reads = queue->writes = 0; queue->capacity = capacity; \
    return queue; \
} \
\
static void queue_##type##_delete(queue_##type##_t *pqueue) { \
    if (!pqueue || !(*pqueue)) return; \
    free((*pqueue)->array); free(*pqueue); \
    *pqueue = NULL; \
} \
\
static void queue_##type##_put(queue_##type##_t queue, type element) { \
    if (!queue_##type##_full(queue)) { \
        queue->array[queue_mod(queue->writes, queue->capacity)] = element; \
        ++queue->writes; \
    } \
} \
\
static type queue_##type##_get(queue_##type##_t queue) { \
    assert(!queue_##type##_empty(queue)); \
    ++queue->reads; \
    return queue->array[queue_mod(queue->reads - 1, queue->capacity)]; \
} \
\
static bool queue_##type##_empty(queue_##type##_t queue) { \
    return queue->reads == queue->writes; \
} \
\
static bool queue_##type##_full(queue_##type##_t queue) { \
    return (queue->writes - queue->reads) == queue->capacity; \
}

#endif /* UTIL_QUEUE_H */
