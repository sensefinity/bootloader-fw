/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @file        atomic.h
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 * @date        5 Oct 2015
 * @brief       Implements a simple (platform-specific) mechanism for handling
 *              critical code sections, through the use of atomic code blocks.
 * @see         http://www.sensefinity.com
 */
#ifndef UTIL_ATOMIC_H
#define UTIL_ATOMIC_H

/* C standard library */
#include <stdint.h>

/* nRF51 */
#include "nrf51.h"
#include "nrf51_bitfields.h"

/**
 * @brief       Implements a function-like macro that executes code atomically.
 *              For this, we start by reading the value of the PRIMASK register,
 *              which stands for "Priority Mask Register". The PRIMASK is a
 *              32-bit register holding a single bit. If that bit is set, the
 *              CPU prevents the activation of all exceptions with configurable
 *              priority and if that bit is cleared, it has no effect. As such,
 *              in order to support nesting of atomic code blocks, we only
 *              re-enable interrupts if they were previously enabled, otherwise
 *              unexpected behavior may happen.
 */
#define atomic(code) { \
    uint32_t primask = __get_PRIMASK(); \
    __disable_irq(); \
    code \
    ; \
    if (!primask) __enable_irq(); \
}

#endif /* UTIL_ATOMIC_H */
