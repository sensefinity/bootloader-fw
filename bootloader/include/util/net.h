/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     util
 * @{
 *
 * @file        net.h
 * @brief       Implements helper functions for comm related tasks
 *
 * @author      Carlos Filipe Costa <carlos.costa@sensefinity.com>
 *
 * @}
 */

#ifndef __UTIL_NET_H__
#define __UTIL_NET_H__

#include <stdint.h>
#include <stdbool.h>

#define CONN_TIMEOUT    (10000);
#define REPLY_TIMEOUT   (10000);

/**
* @brief Enum describing transport-layer types.
*/
enum transport_type {
  TCP, /**< TCP type*/
  UDP  /**< UDP type*/
};

/**
* @brief Strategy-type interface to retain POST
*        and GET specific to the available comm device.
*/
struct rest_interface{
  bool (*post)(const char*, const char*);              /**< POST interface.*/
  bool (*get) (const char*, const char*, char *, int); /**< GET interface. */
};

#ifdef DEBUG
/**
* @brief     Prints ipv4 address.
* @param[in] address
*            IPv4 Address to print.
*/
void print_ipv4_address(uint32_t address);

/**
* @brief prints ipv4 address with port while specifying if port is UDP/TCP.
*/
void print_ipv4_address_with_port(uint32_t address,
                                         uint16_t port,
                                         enum transport_type type);

/**
* @brief     Prints MAC address.
* @param[in] hw_address
*            Pointer to MAC address to be printed.
*/
void print_mac_address(uint8_t *hw_address);
#endif

/**
* @brief Sets implementation-specific functions to REST interface.
* @param[in] rest
*            Pointer to struct containing implementation-specific REST.
*/
void set_rest_comm(struct rest_interface *rest);

/**
* @brief POST general interface.
* @param[in] url
*            URL containing server address and service path.
* @param[in] data
*            Data to be included in the POST message.
* @returns   True if the the POST is successfully received by the server,
*            false otherwise.
*/
bool post(const char *url, const char *data);

/**
* @brief  GET general interface.
* @param[in] url
*            URL containing server address and service path.
* @param[in] data
*            Data to be included in the POST message.
* @returns   True if the the POST is successfully received by the server,
*            false otherwise.
*/
bool get (const char *url, const char *data,
          char *response_data, int response_data_size);

/**
* @brief Test function of debug output.
*/
void print_count(void);
#endif
