/**
 * Copyright (c) 2016 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     target
 * @{
 *
 * @file        svc_handlers.h
 * @brief       Startup code and SVC routines definition
 *
 * @author      Carlos Filipe Costa <carlos.costa@sensefinity.com>
 *
 * @}
 */

#ifndef __SVC_HANDLERS_H__
#define __SVC_HANDLERS_H__

/* C standard library */
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

/* Crunchie library */
//#include "crunchie.h"

/* Platform */
#include "nrf51.h"

#include "dev/svc_handlers.h"
#include "dev/boot.h"
#include "dev/nrf_nvmc.h"

/* Utilities */
#include "util/debug.h"

typedef enum _test_state_t{
  TEST_PROCEED,
  TEST_FAIL,
  TEST_OK
}test_state_t;

#define SVC_NEW_FW            (0x00)
#define SVC_TEST_QUERY        (0x01)
#define SVC_TEST_RESULT       (0x02)
#define SVC_WRITE_DATA        (0x03)
#define SVC_RESET             (0x04)

#endif
