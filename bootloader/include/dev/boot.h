/**
 * Copyright (c) 2016 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     target
 * @{
 *
 * @file        boot.h
 * @brief       Startup code and interrupt vector definition
 *
 * @author      Carlos Filipe Costa <carlos.costa@sensefinity.com>
 *
 * @}
 */

#ifndef __BOOT_H__
#define __BOOT_H__

typedef enum __attribute__((__packed__)) _boot_state_t {
  BOOT_FW_OK = 0xFF,
  BOOT_FW_SWAP = 0,
  BOOT_SWAP_PENDING =1,
  BOOT_TEST = 2,
  BOOT_ROLLBACK = 3,
  BOOT_ROLLBACK_SWAP_PENDING = 4,
  BOOT_ROLLBACK_TEST = 5,
  BOOT_HALT = 6,
}boot_state_t;

void boot_change_state(boot_state_t new_state);

/**
 * @brief       Prototype for reset's interrupt service routine
 * @details     Serves for documentation purposes only.
 */
void isr_reset(void);

#endif
