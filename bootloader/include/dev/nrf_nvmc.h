#ifndef __NRF_NVMC_H__
#define __NRF_NVMC_H__

#define K                 (1024)
#define PAGE_SIZE         K
#define WORD_SIZE         (sizeof(uint32_t))
#define CLEAR_BYTE        (0xFF)
#define CLEAR_WORD        (0xFFFFFFFF)
#define SVC_VECTOR_ADDR   (0x2C)
#define VECTOR_TABLE_SIZE (192)

#include "util/atomic.h"

/**
 * @brief  erases flash page if address points to first word.
 * @param  page_address
 *         address of flash page's first word.
 *  @param data
 *         Pointer to the data to be copied.
 *  @param length
 *         Size of data to be copied.
 * */
void nrf_nvmc_erase_page(uint32_t *addr);

/**
 * @brief  Writes data to flash page.
 * @param  page_address
 *         address of flash page's first word.
 *  @param data
 *         Pointer to the data to be copied.
 *  @param length
 *         Size of data to be copied.
 * */
void nrf_nvmc_write_page(uint32_t *page_address, uint32_t *data, size_t length);

/**
 * @brief   Copies data to flash, if address is located in backup or configuration zone.
 * @param   address
 *          The adress where to copy the data.
 * @param   data
 *          Pointer to the data to be copied.
 * @param   length
 *          The size of the data to be copied.
 * @returns Size of written data,
 *          0 if address is not ready for writing,
 *          -1 if address is outside of backup or configuration regions.
 * */
int nrf_nvmc_write_data(char *address, char *data, size_t length);


/**
 * @brief   Swaps application with backup zones.
 * */
void nrf_nvmc_fw_swap();

//void nrf_nvmc_copy_vector_table(char *src);
#endif
