
       ___                      _     _
      / __\ __ _   _ _ __   ___| |__ (_) ___
     / / | '__| | | | '_ \ / __| '_ \| |/ _ \
    / /__| |  | |_| | | | | (__| | | | |  __/
    \____/_|   \__,_|_| |_|\___|_| |_|_|\___|

    Copyright (c) 2015 Sensefinity


Crunchie, The next-generation platform for the IoT!

# FEATURES

The Crunchie project is a platform made of custom hardware and software suited
for the IoT (Internet of Things). The features are outlined below:

    * Complete platform combining hardware and software tools;
    * Bluetooth low energy communication;
    * Mesh network support;
    * Sensor expansion to support virtually an unlimited number of sensors;
    * Connection to the cloud;
    * Ultra low power consumption;

# TOOLCHAIN

The Crunchie project is built on top of the ARM architecture using the GNU
toolchain for bare-metal development.

What do you need:
    - GNU environment
    - GCC targeted for Embedded ARM Processors
    - Firmware programmer and debugger (J-Link tools)

Follow these steps to install a build environment:

    1. Install a minimal GNU environment:
        * If you're using Windows, use MinGW or Cygwin and make sure the
          binary folders are added to the environment variables;
        * If you're using a Linux-based system or a Mac, chances are you
          already have a minimal GNU environment installed;

    2. Install the GNU ARM toolchain for bare-metal development (arm-none-eabi)
       using the following link: https://launchpad.net/gcc-arm-embedded; Make
       sure the binary folders are added to the environment variables;

    3. Build project invoking `make MODE=DEBUG` on the top-level directory;

    4. Ok at this point you can edit and compile the firmware. Let's run it
       on the hardware!

    4. Install J-Link tools (from https://www.segger.com);
        * From https://www.segger.com/jlink-software.html download the software
          and documentation pack specific for your system.

    5. Go to the 'tools' directory of the project;

    6. Flash device configuration;
        * Type "sh jlink-config.sh *.cfg", where the *.cfg files can be one of
          the tree different operation modes:
            - gateway.cfg
            - beacon.cfg
            - mote.cfg

    7. Flash device Firmware;
        * Type "sh jlink-flash.sh" and the firmware build will be flashed
          to the device.

    8. See the DEBUG prints;
        * Open the J-Link RTT Viewer application.
          RTT Viewer configuration parameters:
            - Target device: nRF51422_xxAA.
            - Target interface: SWD at 4000 kHz.

# VERSIONING

The Crunchie project uses a simple versioning system which fully integrates with
the git VCS. Each version follows the template:

{MAJOR}.{MINOR}.{PATCH}-{STAGE}

The MAJOR, MINOR and PATCH components are non-negative non-zero numbers.
The STAGE holds the current development stage.

On a version change, increment the:

    * MAJOR version when incompatible changes are performed;
    * MINOR version when functionality is added which is backwards compatible;
    * PATCH version when performing backwards-compatible bug fixes;

The STAGE indicates the current development stage and it must be a tag with one
of the following values:

    * pre, for pre-alpha stage; In pre-alpha stage, we're considering software
      that has yet to undergo the testing phase;
    * alpha, for alpha stage; In alpha stage, we begin testing using any
      available techniques; New patches can be added in this phase until it
      becomes feature-complete;
    * beta, for beta stage; In beta stage we begin with a feature-complete
      software with known and unknown bugs; Testing is performed targeting
      usability;
    * rc, for release candidate stage; In release candidate stage we begin
      with a beta version which has the potential to be a final product unless
      significant bugs emerge; Typically in this stage there are no known
      bugs;
    * release, for the release stage;
