#include <stdint.h>
#include <string.h>

#include "dev/boot.h"
#include "dev/nrf_nvmc.h"
#include "util/debug.h"

/**
 * @brief       Memory markers as defined in the linker script
 */
extern uint32_t _sfixed;
extern uint32_t _efixed;
extern uint32_t _etext;
extern uint32_t _srelocate;
extern uint32_t _erelocate;
extern uint32_t _sram_table;
extern uint32_t _eram_table;
extern uint32_t _szero;
extern uint32_t _ezero;
extern uint32_t _sstack;
extern uint32_t _estack;
extern uint32_t _sapp;
//extern char _ram_vector_table;
extern char _test_state;


const boot_state_t __attribute__((section(".state_section")))
_state = BOOT_FW_OK;

void boot_change_state(boot_state_t new_state){
  char page[PAGE_SIZE];
  memcpy(page, &_test_state, PAGE_SIZE);
  boot_state_t *state = (boot_state_t *)page;

  *state = new_state;
  nrf_nvmc_erase_page((uint32_t *)&_test_state);
  nrf_nvmc_write_page((uint32_t *)&_test_state,
                      (uint32_t *)page, PAGE_SIZE);
}

/**
 * @brief       Non-maskable interrupt handler
 * @details     This interrupt is unused, as such the Crunchie board will die
 *              if control reaches this point.
 */
//__attribute__((section(".ramfunc")))
void isr_nmi(void) {
  //crunchie_die();
}

/**
 * @brief       Prototype for the hard fault debugging function
 */
//__attribute__((section(".ramfunc")))
void hard_fault_debug(unsigned long *sp) asm("_hard_fault_debug");

/**
 * @brief       Hard fault interrupt handler
 * @details     This interrupt is generated when something happened that wasn't
 *              supposed to, such as unaligned memory access. If control reaches
 *              this point, we should do a short stack dump in order to easily
 *              determine the cause.
 *              Note, the 'naked' attribute prevents GCC from generating the
 *              prologue and epilogue for C functions. As such, only assembly
 *              code shall be used.
 */
void __attribute__((naked/*,section(".ramfunc")*/)) isr_hard_fault(void) {
    /* Procedure:
     *      1. Determine if the processor is using the PSP or MSP stack pointers
     *         by checking bit 2 from LR register;
     *      2. Copy wither PSP or MSP conditionally to R0 register
     *         (first C function argument);
     *      3. Give up control to the debug function;
     */

    asm volatile (
        "movs   r0, #4\n\t"
        "mov    r1, lr\n\t"
        "tst    r0, r1\n\t"
        "beq    _is_msp\n\t"
        "mrs    r0, psp\n\t"
        "b      _hard_fault_debug\n\t"
    "_is_msp:\n\t"
        "mrs    r0, msp\n\t"
        "b      _hard_fault_debug\n\t"
    );
}

/**
 * @brief       Hard fault debugging function
 * @details     This function performs a short stack dump that will be useful
 *              to debug hard faults.
 * @param[in]   sp
 *              The stack pointer of the last stacked frame.
 */
void hard_fault_debug(unsigned long *sp) {
#ifndef DEBUG
    (void) sp;
#else
    volatile unsigned long stacked_r0;
    volatile unsigned long stacked_r1;
    volatile unsigned long stacked_r2;
    volatile unsigned long stacked_r3;
    volatile unsigned long stacked_r12;
    volatile unsigned long stacked_lr;
    volatile unsigned long stacked_pc;
    volatile unsigned long stacked_psr;
    volatile unsigned long _CFSR;
    volatile unsigned long _HFSR;
    volatile unsigned long _DFSR;
    volatile unsigned long _AFSR;
    volatile unsigned long _BFAR;
    volatile unsigned long _MMAR;

    /* Retrieve stacked registers */
    stacked_r0  = sp[0];
    stacked_r1  = sp[1];
    stacked_r2  = sp[2];
    stacked_r3  = sp[3];
    stacked_r12 = sp[4];
    stacked_lr  = sp[5];
    stacked_pc  = sp[6];
    stacked_psr = sp[7];

    /* Retrieve Configurable Fault Status Register */
    /* Consists of MMSR, BFSR, UFSR */
    _CFSR = *((volatile unsigned long *) (0xE000ED28));

    /* Retrieve Hard Fault Status Register */
    _HFSR = *((volatile unsigned long *) (0xE000ED2C));

    /* Retrieve Debug Fault Status Register */
    _DFSR = *((volatile unsigned long *) (0xE000ED30));

    /* Retrieve Auxiliary Fault Status Register */
    _AFSR = *((volatile unsigned long *) (0xE000ED3C));

    /* Read the Fault Address Registers. These may not contain valid values */
    /* Check BFARVALID/MMARVALID to see if they are valid values */

    /* MemManage Fault Address Register */
    _MMAR = *((volatile unsigned long *) (0xE000ED34));

    /* Bus Fault Address Register */
    _BFAR = *((volatile unsigned long *) (0xE000ED38));

    /* Print debug information */
    dprintf("Crunchie Hard Fault Handler\r\n");
    dprintf("Stacked Registers:\r\n");
    dprintf(" r0: 0x%lx\r\n", stacked_r0);
    dprintf(" r1: 0x%lx\r\n", stacked_r1);
    dprintf(" r2: 0x%lx\r\n", stacked_r2);
    dprintf(" r3: 0x%lx\r\n", stacked_r3);
    dprintf("r12: 0x%lx\r\n", stacked_r12);
    dprintf(" lr: 0x%lx\r\n", stacked_lr);
    dprintf(" pc: 0x%lx\r\n", stacked_pc);
    dprintf("psr: 0x%lx\r\n", stacked_psr);
    dprintf("Fault Registers:\r\n");
    dprintf("_CFSR: 0x%lx\r\n", _CFSR);
    dprintf("_HFSR: 0x%lx\r\n", _HFSR);
    dprintf("_DFSR: 0x%lx\r\n", _DFSR);
    dprintf("_AFSR: 0x%lx\r\n", _AFSR);
    dprintf("_MMAR: 0x%lx\r\n", _MMAR);
    dprintf("_BFAR: 0x%lx\r\n", _BFAR);
#endif

    /* Die */
    //crunchie_die();
}

/**
 * @brief       External standard C library initialization function
 */
extern void __libc_init_array(void);

/**
 * @brief       Application entry point after reset
 * @details     After a system reset, the following steps are required and
 *              carried out:
 *                  1. Load data section from flash to ram;
 *                  2. Default bss section to zero;
 *                  3. Initialize the crunchie board;
 *                  4. Calling main function;
 */
void isr_reset(void){
  /* Make sure all RAM blocks are turned on */
  /* See nrf51822 Product Anomaly Notice (PAN) #16 for more details */
  NRF_POWER->RAMON = 0xF;

  /*
   * Handle PAN 42 (See Product Anomaly Notice v2.4 - Page 29)
   * "System: Writing to RAM right after reset or turning it ON fails."
   *
   * Solution:
   * Do not access RAM for 0.5 us after a reset or enabling a RAM block.
   * Note that function calls normally make use of the RAM.
   */

  /* Delay for 8 NOP instructions (= 0.5 us) */
  __NOP();
  __NOP();
  __NOP();
  __NOP();
  __NOP();
  __NOP();
  __NOP();
  __NOP();

  uint32_t *dst;
  uint32_t *src = &_etext;
  uint32_t *fw_vector_table = (uint32_t *)&_sapp;
  void (* const app_reset)(void) = (void (*)(void))fw_vector_table[1];

  /*load boot ram vector*/
  /* for(dst = &_sram_table; dst < &_eram_table; ){ */
  /*   *(dst++) = *(src++); */
  /* } */

  /* Load data section from flash to ram */
  for (dst = &_srelocate; dst < &_erelocate; ) {
    *(dst++) = *(src++);
  }

  /* Default bss section to zero */
  for (dst = &_szero; dst < &_ezero; ) {
    *(dst++) = 0;
  }


  /* Initialize Crunchie board */
  //crunchie_init();

  /* Initialize standard library */
  __libc_init_array();

#ifdef DEBUG
  /* Configure up buffer */
  SEGGER_RTT_ConfigUpBuffer(0, NULL, NULL, 0,
                            SEGGER_RTT_MODE_BLOCK_IF_FIFO_FULL);
#endif

/*bool test = sv_call_is_test_required();
  dprintf("Result %hhu\r\n", test);*/

  boot_state_t *state = (boot_state_t *)&_test_state;
  dprintf("State = %u\r\n", (uint8_t)*state);

  switch(*state){
  case BOOT_FW_OK:
    /* Call firmware */
    if (fw_vector_table[1] != 0xFFFFFFFF){
      /*memcpy(&(boot_ram_isr_table[ISR_POWER_CLOCK_INDEX]),
             &(fw_vector_table[ISR_POWER_CLOCK_INDEX]),
             NRF_ISR_SIZE);*/ //copy NRF51 handlers to ram vector table
      /* Run Firmware from reset_handler*/
      dprintf("Reset address: 0x%08x\r\n",
              fw_vector_table[1]);
      dprintf("PSP address: 0x%08x\r\n",
              fw_vector_table[0]);
      __set_PSP((uint32_t)fw_vector_table[0]); // Set PSP to top of the firmware stack
      __set_MSP(__get_MSP());
      /* Run Firmware from reset_handler*/
      app_reset();
    }else{
      dputs("No firmware image!");
      boot_change_state(BOOT_HALT);
      NVIC_SystemReset();
    }
    break;
  case BOOT_FW_SWAP:
        boot_change_state(BOOT_SWAP_PENDING);
  case BOOT_SWAP_PENDING:
        nrf_nvmc_fw_swap();
        boot_change_state(BOOT_TEST);
        /* Call firmware */
        if (fw_vector_table[1] != 0xFFFFFFFF){
          /*memcpy(&(boot_ram_isr_table[ISR_POWER_CLOCK_INDEX]),
                 &(fw_vector_table[ISR_POWER_CLOCK_INDEX]),
                 NRF_ISR_SIZE);*/ //copy NRF51 handlers to ram vector tabl
          dprintf("Reset address: 0x%08x\r\n",
                  fw_vector_table[1]);
          dprintf("Reset address: 0x%08x\r\n",
                  fw_vector_table[1]);
          dprintf("PSP address: 0x%08x\r\n",
                  fw_vector_table[0]);
          __set_PSP((uint32_t)fw_vector_table[0]); // Set PSP to top of the firmware stack
          __set_MSP(__get_MSP());
          /* Run Firmware from reset_handler*/
          app_reset();
        }else{
          dputs("No firmware image!");
        }
        break;
  case BOOT_ROLLBACK:
    boot_change_state(BOOT_ROLLBACK_SWAP_PENDING);
  case BOOT_ROLLBACK_SWAP_PENDING:
    nrf_nvmc_fw_swap();
    boot_change_state(BOOT_ROLLBACK_TEST);
    NVIC_SystemReset();
    break;
  case BOOT_HALT:
    dputs("No valid firmware!");
    while(1);
    break;
  case BOOT_TEST:
    dputs("Something went wrong with the test!");
    boot_change_state(BOOT_ROLLBACK);
    NVIC_SystemReset();
    break;
  case BOOT_ROLLBACK_TEST:
    dputs("Something went wrong with the test after the rollback!");
    boot_change_state(BOOT_HALT);
    NVIC_SystemReset();
    break;
  }
}
