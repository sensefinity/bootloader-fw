#include <string.h>
#include <stdbool.h>

#include "dev/nrf_nvmc.h"
#include "util/debug.h"
#include "periph/wdt.h"

#define PAGE_INDEX(address) (((uint32_t)address >> 10) & 0xFF)

#define MIN(x,y) (((x) > (y))? (y) : (x))

extern char _sapp, _eapp, _sbackup, _ebackup, _sdata, _edata, _configuration;

extern inline void __enable_irq();
extern inline void __disable_irq();
extern inline uint32_t __get_PRIMASK();

void nrf_nvmc_erase_page(uint32_t *addr){
  uint32_t k_mask = K - 1;
  if(!((uint32_t)addr & k_mask)){

    /* Enable erase mode */
    NRF_NVMC->CONFIG = (NVMC_CONFIG_WEN_Een << NVMC_CONFIG_WEN_Pos);

    /* Wait for erase mode ready */
    while (NRF_NVMC->READY == NVMC_READY_READY_Busy);

    /* Erase page */
    NRF_NVMC->ERASEPAGE = (uint32_t) addr;

    /* Wait for erase */
    while (NRF_NVMC->READY == NVMC_READY_READY_Busy);

    /* Turn off erase mode */
    NRF_NVMC->CONFIG = (NVMC_CONFIG_WEN_Ren << NVMC_CONFIG_WEN_Pos);

    while (NRF_NVMC->READY == NVMC_READY_READY_Busy);
  }else{
    dprintf("[erase_page] Illegal page address 0x%08p\r\n", addr);
  }
}

int nrf_nvmc_write_data(char *address, char *data, size_t length){
  uint32_t k_mask = K - 1;
  uint32_t word_mask = sizeof(uint32_t) - 1;
  uint32_t *addr_iter = NULL;
  char *data_iter = data;
  uint32_t last_addr = (uint32_t)address + length;
  uint32_t data_word = CLEAR_WORD;
  uint32_t byte_pos =  (uint32_t)address & word_mask;
  bool first_word = true;
  int data_batch_size = 0;

  /* Checking if address is in data-writable region (Backup or Configuration)*/
  if(((address < &_sbackup) &&
      (last_addr > (uint32_t)&_ebackup)) ||
      ((address < &_configuration) &&
       (last_addr > (uint32_t)&_configuration + K))){
    return -1;
  /* If address is NOT a page flash init*/
  }else if ((uint32_t)address & k_mask){
  /*If the address isn't aligned, align word to write it*/
    if(byte_pos){
      uint32_t aligned_addr = (uint32_t)address & ~word_mask;
      addr_iter = (uint32_t *)aligned_addr;
    }else{
      addr_iter = (uint32_t *)address;
    }
    /* If the position isn't clear. abort */
    if(*addr_iter != CLEAR_WORD){
      return 0;
    }
  }else{
  /*If address is a flash page init, it's guaranteed to be aligned*/
    addr_iter = (uint32_t *)address;
  }
  data_word = *addr_iter;

  memcpy((uint8_t *)&data_word + byte_pos, data_iter, MIN(WORD_SIZE - byte_pos, length));
  //dprintf("[write_data] Data_word: 0x%08x\r\n", data_word);
  data_batch_size = WORD_SIZE - byte_pos;

  int result = 0;
  int init_page_index = PAGE_INDEX(address);
  int end_page_index = PAGE_INDEX((address + length));
  uint8_t spanning_pages = (end_page_index - init_page_index) + 1;

  dprintf("[write_data] Spanning pages %d\r\n", spanning_pages);

  size_t i, j;
  dprintf("[write_data] Initial length: %d\r\n", length);
  for (i = 0; i < spanning_pages; ++i){
    bool pass_erased = false;
    /*if addr_iter is the beginning of flash page*/
    if (!((uint32_t)addr_iter & k_mask)){
      //dputs("[write_data] Page address!");
      //dprintf("[write_data] write address: 0x%08p\r\n", addr_iter);
      //dprintf("[write_data] write address content: 0x%08x\r\n", *addr_iter);
      /* If flash page address contains data, erase*/
      if(*addr_iter != CLEAR_WORD){
        //dprintf("[write_data] ERASE page @ 0x%08p\r\n", addr_iter);
        nrf_nvmc_erase_page(addr_iter);
      }
      pass_erased = true;
    }
    /* Turn on write mode */
    NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Wen;

    /* Wait until ready */
    while (NRF_NVMC->READY == NVMC_READY_READY_Busy);

    for(j=result; j < length; j+=data_batch_size, addr_iter++, data_iter+=data_batch_size){

      /*if addr_iter is the beginning of flash page, break for eventual erase*/
      if(!((uint32_t)addr_iter & k_mask) & !pass_erased){

        dputs("[write_data] Break!");
        dprintf("[write_data] Length: %d\r\n", length);
        break;
      }else{
        pass_erased = false;
      }

      if(*addr_iter != CLEAR_WORD){
        /* Turn off write mode */
        NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Ren;

        /* Wait until ready */
        while (NRF_NVMC->READY == NVMC_READY_READY_Busy);
        return result;
      }

      if(!first_word){
        if(length - j < WORD_SIZE){
          //dputs("here");
          data_word = *addr_iter;
          memcpy((uint8_t *)&data_word, data_iter, length - j);
          data_batch_size = length - j;
        }else {
          //dputs("there");
          memcpy((uint8_t *)&data_word, data_iter, WORD_SIZE);
          data_batch_size = WORD_SIZE;
        }
      }else{
        first_word = false;
      }
      //dprintf("[write_data] Data_iter 0x%08p\r\n", data_iter);
      //dprintf("[write_data] Data_word: 0x%02x 0x%02x 0x%02x 0x%02x\r\n",
      //    ((uint8_t *)&data_word)[0], ((uint8_t *)&data_word)[1], ((uint8_t *)&data_word)[2], ((uint8_t *)&data_word)[3]);
      //dprintf("[write_data] write address: 0x%08p\r\n", addr_iter);
      //dprintf("[write_data] Data batch size %d\r\n", data_batch_size);

      *addr_iter = data_word;

      /* Wait until ready */
      while (NRF_NVMC->READY == NVMC_READY_READY_Busy);
      result+=data_batch_size;
    }
    //dprintf("[write_data] Out of loop! length: %d, j %d, addr_iter 0x0%8p\r\n", length, j, addr_iter);
    /* Turn off write mode */
    NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Ren;

    /* Wait until ready */
    while (NRF_NVMC->READY == NVMC_READY_READY_Busy);
  }
  return result;
}


void nrf_nvmc_write_page(uint32_t *page_addr, uint32_t *src, size_t length){
  size_t words = (length < PAGE_SIZE? length: PAGE_SIZE) >> 2;
  uint32_t *dst = page_addr;
  /* Turn on write mode */
  NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Wen;

  /* Wait until ready */
while (NRF_NVMC->READY == NVMC_READY_READY_Busy);

  while (words--) {

    /* Perform write */
    *dst++ = *src++;

/* Wait until ready */
while (NRF_NVMC->READY == NVMC_READY_READY_Busy);
  }

  /* Turn off write mode */
  NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Ren;

/* Wait until ready */
while (NRF_NVMC->READY == NVMC_READY_READY_Busy);
}

void nrf_nvmc_fw_swap(){
  bool clean = true;
  char *app_ptr = &_sapp,       *end_app_ptr = &_eapp,
       *data_ptr = &_sdata,     *end_data_ptr = &_edata,
       *backup_ptr = &_sbackup, *end_data_page = &_sdata + K;

  while(app_ptr < end_app_ptr){
    char *temp = data_ptr;

    while (data_ptr < end_data_page){
      if(*data_ptr != CLEAR_BYTE){
        clean = false;
        data_ptr = temp + K;
        break;
      }
      data_ptr ++;
    }

    if(!clean){
      nrf_nvmc_erase_page((uint32_t *)temp);
      clean = true;
    }

    nrf_nvmc_write_page((uint32_t *)temp, (uint32_t *)app_ptr, K);
    nrf_nvmc_erase_page((uint32_t *)app_ptr);
    nrf_nvmc_write_page((uint32_t *)app_ptr, (uint32_t *)backup_ptr, K);
    nrf_nvmc_erase_page((uint32_t *)backup_ptr);
    nrf_nvmc_write_page((uint32_t *)backup_ptr, (uint32_t *)temp, K);
    app_ptr += K;
    backup_ptr += K;
    /* moves one page untill the last page of data,
       then loops to the beginning until copy ends*/
    if(data_ptr == (end_data_ptr - K)){
      data_ptr = &_sdata;
      end_data_page = data_ptr + K;
    }else{
      end_data_page += K;
    }
  }
}

/*inline void nrf_nvmc_copy_vector_table(char *src){
  atomic(memcpy(&_ram_vector_table, src, VECTOR_TABLE_SIZE));
}*/
