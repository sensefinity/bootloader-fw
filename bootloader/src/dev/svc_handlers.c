/**
 * Copyright (c) 2016 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     target
 * @{
 *
 * @file        boot.h
 * @brief       Startup code and interrupt vector definition
 *
 * @author      Carlos Filipe Costa <carlos.costa@sensefinity.com>
 *
 * @}
 */

/* C standard library */
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

/* Crunchie library */
//#include "crunchie.h"

/* Platform */
#include "nrf51.h"
#include "dev/svc_handlers.h"
#include "dev/boot.h"
#include "dev/nrf_nvmc.h"

/* Utilities */
#include "util/debug.h"

/**
 * @brief       Memory markers as defined in the linker script
 */
extern uint32_t _sfixed;
extern uint32_t _efixed;
extern uint32_t _etext;
extern uint32_t _srelocate;
extern uint32_t _erelocate;
extern uint32_t _szero;
extern uint32_t _ezero;
extern uint32_t _sstack;
extern uint32_t _estack;
extern uint32_t _sapp;
extern char _test_state;

typedef struct _svc_stack_t{
  union {
    uint32_t r0;
    uint32_t arg0;
    uint32_t return_val;
  };
  union {
    uint32_t r1;
    uint32_t arg1;
  };
  union {
    uint32_t r2;
    uint32_t arg2;
  };
  union {
    uint32_t r3;
    uint32_t arg3;
  };
  uint32_t r12;
  union {
    uint32_t r14;
    uint32_t lr;
  };
  uint32_t return_addr;
  uint32_t xPSR;
}svc_stack_t;

/* void _svc_set_fw_test_state_handler(boot_state_t state); */

void _sv_call_new_fw_handler(void);

bool _sv_call_test_query_handler(void);

void _sv_call_test_result_handler(test_state_t result);

int _sv_call_write_data_handler(char *address, char *data, size_t length);

void _sv_call_reset_handler(void);

void _svc_handler_main(svc_stack_t * stack_frame);

/* void _svc_set_fw_test_state_handler(boot_state_t state){ */
/*   char page[PAGE_SIZE]; */
/*   memcpy(page, &_test_state, PAGE_SIZE); */
/*   page[0] = state; */
/*   nrf_nvmc_erase_page((uint32_t *)&_test_state); */
/*   nrf_nvmc_write_page((uint32_t *)&_test_state, (uint32_t *)page, PAGE_SIZE); */
/*   dputs("Supervisor set TEST state!"); */
/*   switch(state){ */
/*     case TEST_PROCEED: */
/*       dputs("TEST_PROCEED"); */
/*       break; */
/*     case TEST_FAIL: */
/*       dputs("TEST_FAIL"); */
/*       break; */
/*     case TEST_OK: */
/*       dputs("TEST_OK"); */
/*       break; */
/*   } */
/*   return; */
/* }//; */

void _sv_call_new_fw_handler(){
  dputs("Supervisor new firmware!");
  boot_change_state(BOOT_FW_SWAP);
  NVIC_SystemReset();
}

bool _sv_call_test_query_handler(){
  dputs("Supervisor firmware test query!");
  boot_state_t *state = (boot_state_t *)&_test_state;
  if (*state == BOOT_TEST ||
      *state == BOOT_ROLLBACK_TEST){
    return true;
  }else{
    return false;
  }
}

void _sv_call_test_result_handler(test_state_t result){
  dputs("Supervisor check test result!");
  boot_state_t *state = ((boot_state_t *)&_test_state);

  if(result == TEST_OK){
    boot_change_state(BOOT_FW_OK);
  }else{
    if(*state == BOOT_TEST){
      boot_change_state(BOOT_ROLLBACK);
    }else {
      boot_change_state(BOOT_HALT);
    }
  }
  NVIC_SystemReset();
}

int _sv_call_write_data_handler(char *address, char * data, size_t length){
  dputs("Supervisor write data!");
  return nrf_nvmc_write_data(address, data, length);
}

void _sv_call_reset_handler(void){
  dputs("Supervisor Reset!");
  NVIC_SystemReset();
}

// Function declarations
// SVC handler - Assembly wrapper to extract
// stack frame starting address
void __attribute__ ((naked/*,section(".ramfunc")*/)) isr_svc(void)
{
  //dputs("svc!");
  asm volatile (
      "movs r0, #4\r\n\t"
      "mov r1, lr\r\n\t"
      "tst r0, r1\r\n\t"    //BEQ stacking_used_MSP
      "beq stacking_used_msp\r\n\t"
      "mrs r0, psp\r\n\t"  //first parameter - stacking was using PSP
      "ldr r1,=_svc_handler_main\r\n\t"
      "bx r1\r\n"
      "stacking_used_msp:\r\n\t"
      "mrs r0, msp\r\n\t" //first parameter - stacking was using MSP
      "ldr r1,=_svc_handler_main\r\n\t"
      "bx r1"
  );
}

// SVC handler - main code to handle processing
// Input parameter is stack frame starting address
// obtained from assembly wrapper.
void _svc_handler_main(svc_stack_t * stack_frame)
{
  dputs("aqui");
  // Stack frame contains:
  // r0, r1, r2, r3, r12, r14, the return address and xPSR
  // - Stacked R0 = svc_args[0]
  // - Stacked R1 = svc_args[1]
  // - Stacked R2 = svc_args[2]
  // - Stacked R3 = svc_args[3]
  // - Stacked R12 = svc_args[4]
  // - Stacked LR = svc_args[5]
  // - Stacked PC = svc_args[6]
  // - Stacked xPSR= svc_args[7]
  unsigned int svc_number;
  svc_number = ((unsigned char *)stack_frame->return_addr)[-2];
  switch(svc_number)
  {
    case SVC_NEW_FW:
      _sv_call_new_fw_handler();
      break;
    case SVC_TEST_QUERY:
      stack_frame->return_val = _sv_call_test_query_handler();
      break;
    case SVC_TEST_RESULT:
      _sv_call_test_result_handler((test_state_t)stack_frame->arg0);
      break;
    case SVC_WRITE_DATA:
      stack_frame->return_val =
          _sv_call_write_data_handler((char *)stack_frame->arg0,
                                      (char *)stack_frame->arg1,
                                      (size_t)stack_frame->arg2);
      break;
    case SVC_RESET:
      _sv_call_reset_handler();
      break;
  }
}
