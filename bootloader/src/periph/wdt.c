/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     periph
 * @{
 *
 * @file        wdt.h
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Rui Pires
 *
 * @}
 */
#include "periph/wdt.h"

void wdt_init(){

  /* Configure Watchdog.
  a) Pause watchdog while the CPU is halted by the debugger.
  b) Pause watchdog while the CPU is sleeping */
    NRF_WDT->CONFIG = (WDT_CONFIG_HALT_Pause << WDT_CONFIG_HALT_Pos) | ( WDT_CONFIG_SLEEP_Pause << WDT_CONFIG_SLEEP_Pos);

  NRF_WDT->CRV = (WDT_R0_TIMEOUT * 32768);   /* set register timeout */
  NRF_WDT->RREN |= WDT_RREN_RR0_Msk;  /* Enable reload register 0 */

  NRF_WDT->TASKS_START = 1;
}

void wdt_reload_register(int reg){
  NRF_WDT->RR[reg] = WDT_RR_RR_Reload;  /* Reload watchdog register */
}
