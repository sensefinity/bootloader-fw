/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @file        syscalls.c
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 * @date        2 Oct 2015
 * @brief       System calls implementation for newlib.
 * @details     Implements the newlib system calls following the stubs provided
 *              in the newlib documentation. The system calls are sorted by name
 *              is ascending order.
 * @see         http://www.sensefinity.com
 * @see         https://sourceware.org/newlib/
 */

/* Standard C library */
#include <errno.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/times.h>
#include <sys/unistd.h>

/* Crunchie library */
//#include "crunchie.h"

/* Heap markers as defined in the linkerscript */
extern char _sheap;
extern char _eheap;

/* Re-definition of the global errno variable */
#undef errno
extern int errno;

/**
 * @brief       Closes a file.
 * @details     We have no file system as such, this function is unsused.
 * @param       file
 *              The file descriptor of the file to be closed.
 * @return      Always (-1).
 */
int _close(int file) {
    (void) file;

    return -1;
}

/**
 * @brief       Exits a program without cleaning up.
 * @details     For now, this function does nothing. In the future, we may need
 *              to include some kind of warning system.
 * @param       status
 *              The exit status.
 */
void _exit(int status) {
    (void) status;

    if (status < 0) {
        /* Die */
        //crunchie_die();
    }

    while (1) {
        ;
    }
}

/* Holds the system's environment variables */
char *__env[1] = { 0 };
char **environ = __env;

/**
 * @brief       Transfers control to a new process.
 * @details     We don't use processes, as such this function is unused.
 * @param       name
 *              The name of the process.
 * @param       argv
 *              The array of arguments for the new process.
 * @param       env
 *              The array of environment variables for the new process.
 * @return      Always (-1).
 */
int _execve(char *name, char **argv, char **env) {
    (void) name;
    (void) argv;
    (void) env;

    errno = ENOMEM;
    return -1;
}

/**
 * @brief       Creates a new process.
 * @details     We don't use processes, as such this function is unsused.
 * @return      Always (-1).
 */
int _fork(void) {
    errno = EAGAIN;
    return -1;
}

/**
 * @brief       Returns the status of an open file.
 * @details     We have no file system as such, this function is unsused.
 * @param       file
 *              The file descriptor of the file.
 * @param       st
 *              The status structure for the information output.
 * @return      Always (0).
 */
int _fstat(int file, struct stat *st) {
    (void) file;

    st->st_mode = S_IFCHR;
    return 0;
}

/**
 * @brief       Returns the current process ID.
 * @details     We don't use processes, as such this function is unsused.
 * @return      Always (1), as if there is only one process executing.
 */
int _getpid(void) {
    return 1;
}

/**
 * @brief       Provides support for obtaining the system time.
 * @details     For now, we won't integrate timing with the standard library.
 * @param       tv
 *              The timeval which holds times in both seconds and microseconds.
 * @param       tz
 *              The timezone information.
 * @return      Always (0).
 */
int _gettimeofday(struct timeval *tv, struct timezone *tz) {
    (void) tv;
    (void) tz;

    return 0;
}

/**
 * @brief       Initialization function.
 */
void _init(void) {
    /* Nothing to do here... */
}

/**
 * @brief       Tests whether the provided file descriptor is a terminal.
 * @param       file
 *              The file descriptor.
 * @return      A boolean value indicating if the file descriptor is a terminal.
 */
int _isatty(int file) {
    (void) file;
    return 0;
}

/**
 * @brief       Send a signal (kill).
 * @param       pid
 *              The process ID.
 * @param       sig
 *              The signal.
 * @return      Always (-1).
 */
int _kill(int pid, int sig) {
    (void) pid;
    (void) sig;

    errno = EINVAL;
    return -1;
}

/**
 * @brief       Establish a new name for an existing file.
 * @details     We have no file system as such, this function is unsused.
 * @param       old
 *              The old file name.
 * @param       new
 *              The new file name.
 * @return      Always (-1).
 */
int _link(char *old, char *new) {
    (void) old;
    (void) new;

    errno = EMLINK;
    return -1;
}

/**
 * @brief       Set position in a file.
 * @details     We have no file system as such, this function is unsused.
 * @param       file
 *              The file descriptor.
 * @param       ptr
 *              Seek start point.
 * @param       dir
 *              Seek direction/bytes.
 * @return      Always (0).
 */
int _lseek(int file, int ptr, int dir) {
    (void) file;
    (void) ptr;
    (void) dir;

    return 0;
}

/**
 * @brief       Open a file.
 * @details     We have no file system as such, this function is unsused.
 * @param       name
 *              The full name of the file.
 * @param       flags
 *              The flags for reading the file.
 * @param       mode
 *              The mode.
 * @return      Always (-1).
 */
int _open(const char *name, int flags, int mode) {
    (void) name;
    (void) flags;
    (void) mode;

    return -1;
}

/**
 * @brief       Reads from a file.
 * @param       fd
 *              File descriptor.
 * @param       buf
 *              Pointer to an input buffer.
 * @param       nbyte
 *              Capacity of the input buffer in number of bytes.
 * @return      The number of read bytes.
 */
int _read(int fd, char *buf, int nbyte) {
    (void) fd;
    (void) buf;
    (void) nbyte;
    return 0;
}

/**
 * @brief       Increase program data space.
 * @details     This function is used for supporting dynamic data allocation.
 * @param       incr
 *              The increase the program data space.
 * @return      The address of the new heap break.
 */
caddr_t _sbrk(int incr) {
    /* Holds the current heap break (marking the end of the heap) */
    static char *pheap = NULL;

    /* Heap markers */
    const char *sheap = &_sheap;
    const char *eheap = &_eheap;

    if (pheap == NULL) {
        /* Heap is empty, initialize it */
        pheap = &_sheap;
    }

    if ((pheap + incr) < sheap || (pheap + incr) > eheap) {
        /* No memory available */
        errno = ENOMEM;
        return (caddr_t) -1;
    }

    /* Increase/decrease heap break */
    pheap += incr;

    /* Return previous heap break */
    return (caddr_t) (pheap - incr);
}

/**
 * @brief       Provides support for setting the system time.
 * @details     For now, we won't integrate timing with the standard library.
 * @param       tv
 *              The timeval which holds times in both seconds and microseconds.
 * @param       tz
 *              The timezone information.
 * @return      Always (0).
 */
int _settimeofday(const struct timeval *tv, const struct timezone *tz) {
    (void) tv;
    (void) tz;

    return 0;
}

/**
 * @brief       Status of a file by name.
 * @details     We have no file system as such, this function is unsused.
 * @param       file
 *              The name of the file.
 * @param       st
 *              Pointer to the status structure.
 * @return      Always (0).
 */
int _stat(const char *file, struct stat *st) {
    (void) file;

    st->st_mode = S_IFCHR;
    return 0;
}

/**
 * @brief       Timing information for current process.
 * @details     This function is unused.
 * @param       buf
 *              Structure for time-accounting information.
 * @return      Always (-1).
 */
clock_t _times(struct tms *buf) {
    (void) buf;

    /* No measurement available */
    return (clock_t) -1;
}

/**
 * @brief       Remove a file’s directory entry.
 * @details     We have no file system as such, this function is unsused.
 * @param       name
 *              The name of the file.
 * @return      Always (-1).
 */
int _unlink(char *name) {
    (void) name;

    errno = ENOENT;
    return -1;
}

/**
 * @brief       Wait for a child process.
 * @details     We don't use processes, as such this function is unused.
 * @param       status
 *              Pointer to where the status is written.
 * @return      Always (-1).
 */
int _wait(int *status) {
    (void) status;

    errno = ECHILD;
    return -1;
}

/**
 * @brief       Writes to a file.
 * @param       fd
 *              File descriptor.
 * @param       buf
 *              Pointer to an output buffer.
 * @param       nbyte
 *              Number of bytes in the output buffer.
 * @return      The number of written bytes.
 */
int _write(int fd, char *buf, int nbyte) {
    (void) fd;
    (void) buf;
    (void) nbyte;
    return 0;
}
