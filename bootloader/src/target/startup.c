/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     target
 * @{
 *
 * @file        startup.c
 * @brief       Startup code and interrupt vector definition
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#define ISR_POWER_CLOCK_INDEX  (16)
#define ISR_RADIO_INDEX        (17)
#define ISR_UART0_INDEX        (18)
#define ISR_SPI0_TWI0_INDEX    (19)
#define ISR_SPI1_TWI1_INDEX    (20)
#define ISR_GPIOTE_INDEX       (22)
#define ISR_ADC_INDEX          (23)
#define ISR_TIMER0_INDEX       (24)
#define ISR_TIMER1_INDEX       (25)
#define ISR_TIMER2_INDEX       (26)
#define ISR_RTC0_INDEX         (27)
#define ISR_TEMP_INDEX         (28)
#define ISR_RNG_INDEX          (29)
#define ISR_ECB_INDEX          (30)
#define ISR_CCM_AAR_INDEX      (31)
#define ISR_WDT_INDEX          (32)
#define ISR_RTC1_INDEX         (33)
#define ISR_QDEC_INDEX         (34)
#define ISR_LPCOMP_INDEX       (35)
#define ISR_SWI0_INDEX         (36)
#define ISR_SWI1_INDEX         (37)
#define ISR_SWI2_INDEX         (38)
#define ISR_SWI3_INDEX         (39)
#define ISR_SWI4_INDEX         (40)
#define ISR_SWI5_INDEX         (41)

/* nRF51822qfaa specific redirection interrupt vector */
#define ISR_POWER_CLOCK_REDIR (void (*)(void))((&_ram_vector_table + 1) + (ISR_POWER_CLOCK_INDEX << 2))
#define ISR_RADIO_REDIR       (void (*)(void))((&_ram_vector_table + 1) + (ISR_RADIO_INDEX << 2))
#define ISR_UART0_REDIR       (void (*)(void))((&_ram_vector_table + 1) + (ISR_UART0_INDEX << 2))
#define ISR_SPI0_TWI0_REDIR   (void (*)(void))((&_ram_vector_table + 1) + (ISR_SPI0_TWI0_INDEX << 2))
#define ISR_SPI1_TWI1_REDIR   (void (*)(void))((&_ram_vector_table + 1) + (ISR_SPI1_TWI1_INDEX << 2))
#define ISR_GPIOTE_REDIR      (void (*)(void))((&_ram_vector_table + 1) + (ISR_GPIOTE_INDEX << 2))
#define ISR_ADC_REDIR         (void (*)(void))((&_ram_vector_table + 1) + (ISR_ADC_INDEX << 2))
#define ISR_TIMER0_REDIR      (void (*)(void))((&_ram_vector_table + 1) + (ISR_TIMER0_INDEX << 2))
#define ISR_TIMER1_REDIR      (void (*)(void))((&_ram_vector_table + 1) + (ISR_TIMER1_INDEX << 2))
#define ISR_TIMER2_REDIR      (void (*)(void))((&_ram_vector_table + 1) + (ISR_TIMER2_INDEX << 2))
#define ISR_RTC0_REDIR        (void (*)(void))((&_ram_vector_table + 1) + (ISR_RTC0_INDEX << 2))
#define ISR_TEMP_REDIR        (void (*)(void))((&_ram_vector_table + 1) + (ISR_RTC1_INDEX << 2))
#define ISR_RNG_REDIR         (void (*)(void))((&_ram_vector_table + 1) + (ISR_RNG_INDEX << 2))
#define ISR_ECB_REDIR         (void (*)(void))((&_ram_vector_table + 1) + (ISR_ECB_INDEX << 2))
#define ISR_CCM_AAR_REDIR     (void (*)(void))((&_ram_vector_table + 1) + (ISR_CCM_AAR_INDEX << 2))
#define ISR_WDT_REDIR         (void (*)(void))((&_ram_vector_table + 1) + (ISR_WDT_INDEX << 2))
#define ISR_RTC1_REDIR        (void (*)(void))((&_ram_vector_table + 1) + (ISR_RTC1_INDEX << 2))
#define ISR_QDEC_REDIR        (void (*)(void))((&_ram_vector_table + 1) + (ISR_QDEC_INDEX << 2))
#define ISR_LPCOMP_REDIR      (void (*)(void))((&_ram_vector_table + 1) + (ISR_LPCOMP_INDEX << 2))
#define ISR_SWI0_REDIR        (void (*)(void))((&_ram_vector_table + 1) + (ISR_SWI0_INDEX << 2))
#define ISR_SWI1_REDIR        (void (*)(void))((&_ram_vector_table + 1) + (ISR_SWI1_INDEX << 2))
#define ISR_SWI2_REDIR        (void (*)(void))((&_ram_vector_table + 1) + (ISR_SWI2_INDEX << 2))
#define ISR_SWI3_REDIR        (void (*)(void))((&_ram_vector_table + 1) + (ISR_SWI3_INDEX << 2))
#define ISR_SWI4_REDIR        (void (*)(void))((&_ram_vector_table + 1) + (ISR_SWI4_INDEX << 2))
#define ISR_SWI5_REDIR        (void (*)(void))((&_ram_vector_table + 1) + (ISR_SWI5_INDEX << 2))
#define RESERVED              (void (*)(void)) 0UL
#define NRF_ISR_SIZE          (25 * sizeof(void *))

/* C standard library */
//#include <stdio.h>
#include <stdint.h>
//#include <stdbool.h>
//#include <string.h>

/* Crunchie library */
//#include "crunchie.h"

/* Platform */
//#include "nrf51.h"

#include "dev/svc_handlers.h"


/* Utilities */
#include "util/debug.h"

/**
 * @brief       Memory markers as defined in the linker script
 */
extern uint32_t _estack;
extern uint32_t _sapp;

/**
 * @brief       Default interrupt handler
 * @details     The default interrupt handler is called when no other interrupt
 *              handler is defined.
 */
//__attribute__((section(".ramfunc")))
void __attribute__ ((noinline)) default_handler(void) {
     /* Note: Only use scratch registers: r0, r1, r2, r3
     */
  uint32_t app_offset = (uint32_t) &_sapp;
  (void)app_offset;
    asm (
            "mov r2, %[value]\n\t"//%[value]\n\t"
            /* Get current IRQ no from PSR */
            "mrs    r0, psr     \n\t"
            /* Mask the interrupt number only */
            "mov   r1, #0x3F   \n\t"
            "and   r0, r1      \n\t"
            /* Irq address position = IRQ No * 4 */
            "lsl   r0, r0, #2  \n\t"
            /* Fetch the user vector offset */
            "ldr    r0, [r0, r2]\n\t"
            /* Jump to user interrupt vector */
            "bx     r0          \n\t"
            : /* output */
            : /* input */[value]"r"(app_offset)
            : /* clobbered reg */"r0","r1","r2"
    );
}

/* Cortex-M specific interrupt vectors */
void isr_reset(void)            __attribute__ ((weak, alias("default_handler")));
void isr_nmi(void)              __attribute__ ((weak, alias("default_handler")));
void isr_hard_fault(void)       __attribute__ ((weak, alias("default_handler")));
void isr_svc(void)              __attribute__ ((weak, alias("default_handler")));
void isr_pendsv(void)           __attribute__ ((weak, alias("default_handler")));
void isr_systick(void)          __attribute__ ((weak, alias("default_handler")));

/* nRF51822qfaa specific interrupt vector */
void isr_power_clock(void)      __attribute__ ((weak, alias("default_handler")));
void isr_radio(void)            __attribute__ ((weak, alias("default_handler")));
void isr_uart0(void)            __attribute__ ((weak, alias("default_handler")));
void isr_spi0_twi0(void)        __attribute__ ((weak, alias("default_handler")));
void isr_spi1_twi1(void)        __attribute__ ((weak, alias("default_handler")));
void isr_gpiote(void)           __attribute__ ((weak, alias("default_handler")));
void isr_adc(void)              __attribute__ ((weak, alias("default_handler")));
void isr_timer0(void)           __attribute__ ((weak, alias("default_handler")));
void isr_timer1(void)           __attribute__ ((weak, alias("default_handler")));
void isr_timer2(void)           __attribute__ ((weak, alias("default_handler")));
void isr_rtc0(void)             __attribute__ ((weak, alias("default_handler")));
void isr_temp(void)             __attribute__ ((weak, alias("default_handler")));
void isr_rng(void)              __attribute__ ((weak, alias("default_handler")));
void isr_ecb(void)              __attribute__ ((weak, alias("default_handler")));
void isr_ccm_aar(void)          __attribute__ ((weak, alias("default_handler")));
void isr_wdt(void)              __attribute__ ((weak, alias("default_handler")));
void isr_rtc1(void)             __attribute__ ((weak, alias("default_handler")));
void isr_qdec(void)             __attribute__ ((weak, alias("default_handler")));
void isr_lpcomp(void)           __attribute__ ((weak, alias("default_handler")));
void isr_swi0(void)             __attribute__ ((weak, alias("default_handler")));
void isr_swi1(void)             __attribute__ ((weak, alias("default_handler")));
void isr_swi2(void)             __attribute__ ((weak, alias("default_handler")));
void isr_swi3(void)             __attribute__ ((weak, alias("default_handler")));
void isr_swi4(void)             __attribute__ ((weak, alias("default_handler")));
void isr_swi5(void)             __attribute__ ((weak, alias("default_handler")));

/* void (* boot_ram_isr_table[])(void) __attribute__ ((section(".ram_isr_table"))) = { */
/*     /\* stack pointer *\/ */
/*     (void (*)(void)) ((unsigned long) &_estack), */

/*     /\* Cortex-M handlers *\/ */
/*     &isr_reset,                 /\* Entry point after reset *\/ */
/*     &isr_nmi,                   /\* Non-maskable interrupt *\/ */
/*     &isr_hard_fault,            /\* Hard fault *\/ */
/*     RESERVED,                   /\* Reserved *\/ */
/*     RESERVED,                   /\* Reserved *\/ */
/*     RESERVED,                   /\* Reserved *\/ */
/*     RESERVED,                   /\* Reserved *\/ */
/*     RESERVED,                   /\* Reserved *\/ */
/*     RESERVED,                   /\* Reserved *\/ */
/*     RESERVED,                   /\* Reserved *\/ */
/*     &isr_svc,                   /\* Supervisor call *\/ */
/*     RESERVED,                   /\* Reserved *\/ */
/*     RESERVED,                   /\* Reserved *\/ */
/*     &isr_pendsv,                /\* Pendsv *\/ */
/*     &isr_systick,               /\* System tick *\/ */

/*     /\* nRF51 specific peripheral handlers *\/ */
/*     &isr_power_clock,           /\* Power clock *\/ */
/*     &isr_radio,                 /\* Radio *\/ */
/*     &isr_uart0,                 /\* UART0 *\/ */
/*     &isr_spi0_twi0,             /\* SPI0 TWI0 *\/ */
/*     &isr_spi1_twi1,             /\* SPI1 TWI1 *\/ */
/*     RESERVED,                   /\* Reserved *\/ */
/*     &isr_gpiote,                /\* GPIOTE *\/ */
/*     &isr_adc,                   /\* ADC *\/ */
/*     &isr_timer0,                /\* Timer0 *\/ */
/*     &isr_timer1,                /\* Timer1 *\/ */
/*     &isr_timer2,                /\* Timer2 *\/ */
/*     &isr_rtc0,                  /\* RTC0 *\/ */
/*     &isr_temp,                  /\* TEMP *\/ */
/*     &isr_rng,                   /\* RNG *\/ */
/*     &isr_ecb,                   /\* ECB *\/ */
/*     &isr_ccm_aar,               /\* CCM AAR *\/ */
/*     &isr_wdt,                   /\* WDT *\/ */
/*     &isr_rtc1,                  /\* RTC1 *\/ */
/*     &isr_qdec,                  /\* QDEC *\/ */
/*     &isr_lpcomp,                /\* LPCOMP *\/ */
/*     &isr_swi0,                  /\* SWI0 *\/ */
/*     &isr_swi1,                  /\* SWI1 *\/ */
/*     &isr_swi2,                  /\* SWI2 *\/ */
/*     &isr_swi3,                  /\* SWI3 *\/ */
/*     &isr_swi4,                  /\* SWI4 *\/ */
/*     &isr_swi5,                  /\* SWI5 *\/ */
/*     RESERVED,                   /\* Reserved *\/ */
/*     RESERVED,                   /\* Reserved *\/ */
/*     RESERVED,                   /\* Reserved *\/ */
/*     RESERVED,                   /\* Reserved *\/ */
/*     RESERVED,                   /\* Reserved *\/ */
/*     RESERVED                    /\* Reserved *\/ */
/* }; */

/**
 * @brief       Interrupt vector table
 */
__attribute__((section(".vectors")))
void (* /*const*/ interrupt_vector[])(void) = {
    /* stack pointer */
  (void (*)(void)) ((unsigned long) &_estack),

    /* Cortex-M handlers */
    &isr_reset,                 /* Entry point after reset */
    &isr_nmi,                   /* Non-maskable interrupt */
    &isr_hard_fault,            /* Hard fault */
    RESERVED,                   /* Reserved */
    RESERVED,                   /* Reserved */
    RESERVED,                   /* Reserved */
    RESERVED,                   /* Reserved */
    RESERVED,                   /* Reserved */
    RESERVED,                   /* Reserved */
    RESERVED,                   /* Reserved */
    &isr_svc,                   /* Supervisor call */
    RESERVED,                   /* Reserved */
    RESERVED,                   /* Reserved */
    &isr_pendsv,                /* Pendsv */
    &isr_systick,               /* System tick */

    /* nRF51 specific peripheral handlers */
    &isr_power_clock,           /* Power clock */
    &isr_radio,                 /* Radio */
    &isr_uart0,                 /* UART0 */
    &isr_spi0_twi0,             /* SPI0 TWI0 */
    &isr_spi1_twi1,             /* SPI1 TWI1 */
    RESERVED,                   /* Reserved */
    &isr_gpiote,                /* GPIOTE */
    &isr_adc,                   /* ADC */
    &isr_timer0,                /* Timer0 */
    &isr_timer1,                /* Timer1 */
    &isr_timer2,                /* Timer2 */
    &isr_rtc0,                  /* RTC0 */
    &isr_temp,                  /* TEMP */
    &isr_rng,                   /* RNG */
    &isr_ecb,                   /* ECB */
    &isr_ccm_aar,               /* CCM AAR */
    &isr_wdt,                   /* WDT */
    &isr_rtc1,                  /* RTC1 */
    &isr_qdec,                  /* QDEC */
    &isr_lpcomp,                /* LPCOMP */
    &isr_swi0,                  /* SWI0 */
    &isr_swi1,                  /* SWI1 */
    &isr_swi2,                  /* SWI2 */
    &isr_swi3,                  /* SWI3 */
    &isr_swi4,                  /* SWI4 */
    &isr_swi5,                  /* SWI5 */
    RESERVED,                   /* Reserved */
    RESERVED,                   /* Reserved */
    RESERVED,                   /* Reserved */
    RESERVED,                   /* Reserved */
    RESERVED,                   /* Reserved */
    RESERVED                    /* Reserved */
};

