/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     util
 * @{
 *
 * @file        net.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Carlos Filipe Costa <carlos.costa@sensefinity.com>
 *
 * @}
 */

#include "util/debug.h"
#include "util/net.h"

struct rest_interface _rest;

#ifdef DEBUG
inline void print_ipv4_address(uint32_t address){
  dprintf("IP address %lu"
          "."
          "%lu"
          "."
          "%lu"
          "."
          "%lu\r\n",
          ( address >> 24),
          ((address >> 16) & 0xFF),
          ((address >>  8) & 0xFF),
          ( address        & 0xFF));
}

inline void print_ipv4_address_with_port(uint32_t address,
                                         uint16_t port,
                                         enum transport_type type){
  dprintf("IP/%s address %lu"
          "."
          "%lu"
          "."
          "%lu"
          "."
          "%lu"
          ":"
          "%d\r\n",
         (type == UDP ? "UDP" : "TCP"),
         ( address >> 24),
         ((address >> 16) & 0xFF),
         ((address >>  8) & 0xFF),
         ( address        & 0xFF),
         port);
}

inline void print_mac_address(uint8_t *hw_address){
    dprintf("MAC %02x:%02x:%02x:%02x:%02x:%02x\r\n", hw_address[0],
                                                    hw_address[1],
                                                    hw_address[2],
                                                    hw_address[3],
                                                    hw_address[4],
                                                    hw_address[5]);
}
#endif

void  set_rest_comm(struct rest_interface *interface){
  _rest.post =  interface->post;
  _rest.get  =  interface->get;
}

bool post(const char *url, const char *data){
  dputs("[post] sending POST...");
  return _rest.post(url, data);
}
bool get (const char *url,
          const char *data,
          char *response_data,
          int response_data_size){
  dputs("[get] sending GET...");
  return _rest.get(url, data, response_data, response_data_size);
}

void print_count(void){
  uint16_t i, j;
  for(i = 0; i<1000; ++i){
    for(j = 0; j < 256; ++j){
      if(j>0 && !(j%16)){
        dputs("");
      }
      dprintf("%02x ", j);
    }
    dputs("");
  }
  dputs("");
}
