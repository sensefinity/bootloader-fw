# Sensoroid Tools

Holds tools for the flashing and configuration of Sensoroid devices.
The usage of these tools is **mandatory**. The usage of other tools or methods
not included in this folder for the same purposes may destroy sections of the
Sensoroid configuration block.

### Usage

1. 'cd' into the tools directory;
2. Execute the required command, using a shell script interpreter,
e.g., in Windows, execute 'sh <file>.sh';

### Details

##### Flashing Tools

The following tools are available for firmware flashing:

- jlink-flash.sh
- openocd-flash.sh

The usage of **jlink-flash.sh** is recommended.

##### Configuration Tools

The following tools are available for device configuration:

- jlink-config.sh <file.json>

The tool **jlink-config.sh** flashes the device configuration to a special
block of memory inside the device, at the end of the flash. For proper
configuration, follow the "config-%.json template files.

##### Configuration Setup
Device configuration:
```sh
$ jlink-config.sh <file.cfg>
```
The tool jlink-config.sh flashes the device configuration to a special block of memory inside the device. For proper configuration, follow the .cfg template files.

#### beacon.cfg

```sh
type=BEACON
serial=0
```
* type -> Type of device (GATEWAY || MOTE || BEACON ).
* serial -> Device serial, 0 for using their "own" serial.

#### gateway.cfg

```sh
type=GATEWAY
serial=0
interface=BUTTERFINGER
```
* type -> Type of device (GATEWAY || MOTE || BEACON ).
* serial -> Device serial, 0 for using their "own" serial.
* interface -> defines the gateway interface to forward messages to machinates, Two options, "BUTTERFINGER" or "ETHERNET".

#### mote.cfg
```sh
type=MOTE
serial=0
service.mesh=0,5
service.battery=1,900000
service.temperature=1,300000
service.ping=1,60000
service.position=0
service.irds=0
```
* type -> Type of device (GATEWAY || MOTE || BEACON ).
* serial -> Device serial, 0 for using their "own" serial.
* service.xxxx=1 -> indicates that a specific service is active.
* service.mesh=1,xxxxx -> mesh entries initial life time.
* mesh period -> mesh decrease life time entries period.
* battery, temperature, ping, position, irds -> sending messages/reading period, in miliseconds.
