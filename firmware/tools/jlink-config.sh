#!/bin/bash

# Minify JSON configuration file
#tr -d '\040\011\012\015' < $1 > .config.min.json

# Apply padding to extend file to 1k bytes
# Also, rename file to .bin, so that J-Link doesn't complain
dd if=$1 ibs=1k count=1 of=.config.bin conv=sync

# Flash configuration file
JLink -If SWD -Speed 4000 -Device nRF51822_xxAA -CommandFile ./jlink-config.commands

# Remove temporary files
rm .config.bin
