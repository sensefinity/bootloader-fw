/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     application
 * @{
 *
 * @file        dandelion.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#include "dandelion.h"
#include "expansion/expansion_protocol.h"
#include "periph/uart0.h"

/* SEGGER */
#include "SEGGER_RTT.h"

/**
 * @brief       Holds the Bluetooth device address
 */
static struct bdaddr _bdaddr;


/* -------------------------------------------------------------------------- */
/*          Private API                                                       */
/* -------------------------------------------------------------------------- */
void _rx_handler(void);
void expansion_protocol_sync_int(void);
/**
 * @brief       Initializes each LED pin individually
 */
static void _init_led_pin(void) {
//    enum pin_name i;

    gpio_output_init(DANDELION_LED);
    gpio_output_clear(DANDELION_LED);
//    for (i = DANDELION_LED_START; i <= DANDELION_LED_STOP; ++i) {
//        /* Initialize pin */
//        gpio_output_init(i);
//
//        /* Set pin, so that the LED is OFF */
//        gpio_output_set(i);
//    }
}

/**
 * @brief       Initializes each user button pin individually
 */
static void _init_user_button_pin(void) {
//    enum pin_name i;
//
//    for (i = DANDELION_BUTTONS_START; i <= DANDELION_BUTTONS_STOP; ++i) {
//        /* Initialize pin */
//        gpio_input_init(i, GPIO_PULL_UP);
//    }
    gpio_input_init(DANDELION_BUTTON, GPIO_PULL_UP);
}

/**
 * @brief       Initializes accelerometer and barometer/altimeter pins
 */
static void _init_accel_hum_pins(void) {
    gpio_output_init(P0_00); /* SCL */
    gpio_input_init(P0_01, GPIO_PULL_UP); /* SDA */
    gpio_input_init(P0_03, GPIO_PULL_UP); /* int2 accel */
    gpio_input_init(P0_04, GPIO_PULL_UP); /* int1 accel */
    gpio_input_init(P0_05, GPIO_PULL_UP); /* int2 alt */
    gpio_input_init(P0_06, GPIO_PULL_UP); /* int1 alt */

    gpio_output_set(P0_00); /* set to 1 to disable */
}

/**
 * @brief       Initializes flash memory pins
 */
static void _init_flash_pins(void) {
    gpio_output_init(P0_12); /* SCLK */
    gpio_output_init(P0_13); /* MOSI */
    gpio_output_init(P0_15); /* nCS */

    gpio_output_set(P0_12); /* set to 1 to disable */
    gpio_output_set(P0_13); /* set to 1 to disable */
    gpio_output_set(P0_15); /* set to 1 to disable */

    gpio_output_clear(P0_12); /* set to 1 to disable */
    gpio_output_clear(P0_13); /* set to 1 to disable */
    gpio_output_clear(P0_15); /* set to 1 to disable */
//
    gpio_input_init(P0_14, GPIO_PULL_DOWN); /* MISO */
//
//    gpio_output_init(P0_16); /* nWP */
//    gpio_output_init(P0_17); /* nHOLD */
//    gpio_output_set(P0_16); /* set to 1 to disable */
//    gpio_output_set(P0_17); /* set to 1 to disable */
}

/* -------------------------------------------------------------------------- */
/*          Public API                                                        */
/* -------------------------------------------------------------------------- */

void dandelion_init(void) {
#ifdef DEBUG
    /* Configure up buffer */
    SEGGER_RTT_ConfigUpBuffer(0, NULL, NULL, 0,
        SEGGER_RTT_MODE_BLOCK_IF_FIFO_FULL);
#endif
    /* Initialize system clocks */
    clock_init();

    /* Initialize system power */
    power_init();

    /* Enter system in low power mode */
    power_set_low_power_mode();

    /* Initialize system pins */
    _init_led_pin();
    _init_user_button_pin();

    _init_accel_hum_pins();
    _init_flash_pins();

    /* Initialize UART pins */
    gpio_input_init(EXPANSION_UART_TXD, GPIO_PULL_UP); /* set default state when no uart is used */
    gpio_input_init(EXPANSION_UART_RXD, GPIO_PULL_UP);
    //uart0_init(EXPANSION_UART_TXD, EXPANSION_UART_RXD);

    /* Attach a new UART handler */
   // uart0_attach(&_rx_handler);
    /* The remaining UART parameters are set to the defaults */

    gpio_input_init(EX_SYNC_PIN, GPIO_PULL_UP);

    /* Initialize task scheduler and system clock */
    sched_init();
    sysclk_init();

    /* Initialize Bluetooth stack */
    _bdaddr = ficr_device_address();
    ll_init(&_bdaddr);
}

//enum dandelion_color dandelion_get_color(void) {
//    enum dandelion_color color = 0;
//    enum pin_name i;
//
//    for (i = DANDELION_LED_START; i <= DANDELION_LED_STOP; ++i) {
//        if (!gpio_output_read(i)) {
//            color |= 0x1;
//        }
//
//        color <<= 1;
//    }
//
//    return color;
//}

//void dandelion_set_color(enum dandelion_color color) {
//    enum pin_name i;
//
//    for (i = DANDELION_LED_START; i <= DANDELION_LED_STOP; ++i) {
//        gpio_output_set(i);
//
//        if (color & 0x1) {
//            gpio_output_clear(i);
//        }
//
//        color >>= 1;
//    }
//}

void dandelion_led_power(uint8_t on) {
    if (on != 0) {
        gpio_output_set(DANDELION_LED);
    } else {
        gpio_output_clear(DANDELION_LED);
    }
}

uint32_t dandelion_read_button_state(void) {
    uint32_t state = 0;
    state = gpio_input_read(DANDELION_BUTTON);
    return state;
}

void dandelion_die(void) {
    /* Blink red color */
    while (1) {
        //dandelion_set_color(DANDELION_COLOR_RED);
        delay_us(100000);
        //dandelion_set_color(DANDELION_COLOR_NONE);
        delay_us(100000);
    }
}



void dandelion_sleep(void) {

    /* Enter System ON sleep mode */
    __WFE();

    /* Make sure any pending events are cleared */
    __SEV();
    __WFE();

}

void dandelion_sleep_force(void) {

    clock_hfclk_stop();

    /* Enter System ON sleep mode */
    __WFI();

    /* Make sure any pending events are cleared */
    __SEV();
    __WFI();

}

void dandelion_deep_sleep(void) {
    /* In deep sleep mode, the system shall enter power off mode */
    power_off();
}
