/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     util
 * @{
 *
 * @file        base64.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#include "util/base64.h"

/**
 * @brief       Holds the Base64 encoding table
 */
static const char *_BASE64_TABLE =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";


/* -------------------------------------------------------------------------- */
/*          Public API                                                        */
/* -------------------------------------------------------------------------- */

void base64_encode(const void *data, size_t length, char *output) {
    int ch;
    unsigned int i;
    const unsigned char *in = (const unsigned char *) data;

    for (i = 0; i < length; i += 3) {
        ch = (in[i] & 0xFC) >> 2;
        *output++ = _BASE64_TABLE[ch];

        ch = (in[i] & 0x03) << 4;

        if ((i + 1) < length) {
            ch |= ((in[i + 1] & 0xF0) >> 4);
            *output++ = _BASE64_TABLE[ch];
            ch = (in[i + 1] & 0x0F) << 2;

            if ((i + 2) < length) {
                ch |= ((in[i + 2] & 0xC0) >> 6);
                *output++ = _BASE64_TABLE[ch];
                ch = in[i + 2] & 0x3F;
                *output++ = _BASE64_TABLE[ch];

            } else {
                *output++ = _BASE64_TABLE[ch];
                *output++ = '=';
            }

        } else {
            *output++ = _BASE64_TABLE[ch];
            *output++ = '=';
            *output++ = '=';
        }
    }

    /* Terminate string */
    *output = '\0';
}
