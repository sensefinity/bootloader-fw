/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup
 * @{
 *
 * @file        crc_ops.c
 * @brief       Use CRC driver to compute and verify messages crc.
 *
 * @}
 */

 #include "util/crc_ops.h"
 #include "util/lib_crc.h"



bool check_crc(uint8_t * messageSource, size_t size, uint16_t crc_cmp) {
    /* Compute crc and compare it */
    return (crc_cmp==compute_crc(messageSource,size));
}

uint16_t compute_crc(uint8_t * messageSource, size_t size) {

    /* Inicializes CRC Seed - 0xffff */
    uint16_t crc = 0xffff;

    size_t i;
    for(i = 0; i < size ; i++){
        crc         = update_crc_ccitt(crc, (char) messageSource[i]);
    }

    return crc;
}
