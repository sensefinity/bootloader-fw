
#include "util/bitmap.h"
#include "util/debug.h"

/* Reset bitmap */
void bitmap_clean(uint8_t * bitmap, size_t size) {
  memset(bitmap,0x00,size);
}

/* Complete bitmap */
/*void bitmap_complete(uint8_t * bitmap, size_t size) {
  memset(bitmap,0xff,size);
}
*/
/* True if value is already present, false otherwise  */
bool bitmap_is_value_present(uint8_t * bitmap, uint16_t segment_number) {

  /* Select specific byte */
  size_t bitmap_offset= segment_number / BIT_WORD;
  /* Select specific bit */
  size_t byte_mask  = segment_number % BIT_WORD;

  /* Check bitmap */
  if(bitmap[bitmap_offset] & (1 << byte_mask))
    return true;
  else return false;
}

/* True when new value updated */
void bitmap_update(uint8_t * bitmap, uint16_t segment_number) {

  /* Select specific byte */
  size_t bitmap_offset= segment_number / BIT_WORD;

  /* Select specific bit */
  size_t byte_mask  = segment_number % BIT_WORD;

  /* Update bitmap, Set to 1 */
  bitmap[bitmap_offset] |= 1 << byte_mask;

  return;
}


/* True, bitmap is complete false otherwise */
bool bitmap_is_complete(uint8_t * bitmap, uint16_t segment_count) {

  int total_offset = segment_count/BIT_WORD;
  int byte_mask  = segment_count % BIT_WORD;

  //dprintf("total_offset%d\n",total_offset );
  //dprintf("byte_mask%d\n",byte_mask );
  int i;
  /* Check all bytes that must be full */
  for (i=0; i <= total_offset-1 && total_offset > 0; i++) {
    if(bitmap[i] !=0xff) {
      /* Fail return false */
      return false;
    }
  }

  /* Check Last byte if needed */
  for (i=0; i < byte_mask+1; i++) {
    if(!bitmap_is_value_present(bitmap,(total_offset*BIT_WORD)+ i)) {
      /* Fail bit */
      return false;
    }
  }
  /* No fails, bitmap is completed */
  return true;
}


void bip_buffer_print(uint8_t * bitmap,size_t size) {
  uint8_t e;
  dprintf("BitMAP print[");
  for(e = 0; e < size ; e++)
    dprintf("%02x",bitmap[e]);
  dprintf("]\n");
}
