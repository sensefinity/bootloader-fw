#include <string.h>

#include "util/reset_reason.h"
#include "util/debug.h"

#define PIN "PIN"
#define WDT "WDT"
#define SOFT "SOFT"
#define CPU_LOCKUP "CPU LOCKUP"
#define OFF "OFF"
#define LPCOMP "LPCOMP"
#define DIF "DIF"
#define SEPARATOR ','

/* NRF_POWER->RESETREAS fields */
#define RESET_FROM_PIN                  0x00000001
#define RESET_FROM_WDT                  0x00000002
#define RESET_FROM_SOFTWARE             0x00000004
#define RESET_FROM_CPU_LOCKUP           0x00000008
#define RESET_FROM_OFF                  0x00010000
#define RESET_FROM_LPCOMP               0x00020000
#define RESET_FROM_DIF                  0x00040000

/*sizeof accounts for trailing null,
  so it indirectly accounts for all the separators*/
#define MAX_RESET_TEXT_LEN     (/*1 + */sizeof(PIN)/* + 1 */+\
                                sizeof(WDT)/* + 1  */+\
                                sizeof(SOFT)/* + 1  */+\
                                sizeof(CPU_LOCKUP)/* + 1 */+\
                                sizeof(OFF)/* + 1  */+\
                                sizeof(LPCOMP)/* + 1  */+\
                                sizeof(DIF))

static struct _reset_reason_t {
  char text[MAX_RESET_TEXT_LEN];
  size_t text_len;
} _reset = {.text ={0},
	    .text_len = 0};


void reset_reason_set(unsigned int reason){
  char *ptr = _reset.text;

  if(!(reason)){
    dputs("INITIAL_POWER_ON");
    return;
  }

  if(reason & RESET_FROM_PIN){
    dputs("RESET_FROM_PIN");
    *ptr = SEPARATOR;
    ptr++;
    memcpy(ptr, PIN, strlen(PIN));
    ptr += strlen(PIN);
  }
  if(reason &  RESET_FROM_WDT ){
    dputs("RESET_FROM_WDT");
    *ptr = SEPARATOR;
    ptr++;
    memcpy(ptr, WDT, strlen(WDT));
    ptr += strlen(WDT);
  }
  if(reason & RESET_FROM_SOFTWARE){
    dputs("RESET_FROM_SOFTWARE");
    *ptr = SEPARATOR;
    ptr++;
    memcpy(ptr, SOFT, strlen(SOFT));
    ptr += strlen(SOFT);
  }
  if(reason & RESET_FROM_CPU_LOCKUP){
    dputs("RESET_FROM_CPU_LOCKUP");
    *ptr = SEPARATOR;
    ptr++;
    memcpy(ptr, CPU_LOCKUP, strlen(CPU_LOCKUP));
    ptr += strlen(CPU_LOCKUP);
  }
  if(reason & RESET_FROM_OFF){
    dputs("RESET_FROM_OFF");
      *ptr = SEPARATOR;
      ptr++;
    memcpy(ptr, OFF, strlen(OFF));
    ptr += strlen(OFF);
  }
  if(reason & RESET_FROM_LPCOMP){
    dputs("RESET_FROM_LPCOMP");
    *ptr = SEPARATOR;
    ptr++;
    memcpy(ptr, LPCOMP, strlen(LPCOMP));
    ptr += strlen(LPCOMP);

  }
  if(reason & RESET_FROM_DIF){
    dputs("RESET_FROM_DIF");
    *ptr = SEPARATOR;
    ptr++;
    memcpy(ptr, DIF, strlen(DIF));
    ptr += strlen(DIF);
  }
  _reset.text_len = ptr - _reset.text;
}

char *reset_reason_get_text(size_t *len){
  *len = _reset.text_len;
  if (!(*len)){
    return NULL;
  }else{
    return _reset.text;
  }
}
