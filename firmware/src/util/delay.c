/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @file        delay.c
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 * @date        5 Oct 2015
 * @brief       Implementation of the interface in the header file of same name.
 * @see         http://www.sensefinity.com
 */

/* Interface */
#include "util/delay.h"

/********************************** Public ************************************/

void delay_ms(uint32_t millis) {
    while (millis != 0) {
        --millis;
        delay_us(999);
    }
}

void delay_us(uint32_t micros) {
    asm volatile (
        "start: nop\n\t"
        "nop\n\t"
        "nop\n\t"
        "nop\n\t"
        "nop\n\t"
        "nop\n\t"
        "nop\n\t"
        "nop\n\t"
        "nop\n\t"
        "nop\n\t"
        "nop\n\t"
        "nop\n\t"
        "nop\n\t"
        "nop\n\t"
        "sub %[cycles], #1\n\t"
        "bne start\n\t"
        : /* empty */ : [cycles] "l" (micros)
    );
}
