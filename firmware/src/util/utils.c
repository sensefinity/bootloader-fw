


#include "util/utils.h"
 #include <stdlib.h>

int cmp_64_with_array(uint64_t u64, uint8_t * array) {

    uint64_t dst_64 = 0;

    dst_64  = ((uint64_t) array[0])  << 56;
    dst_64 |= ((uint64_t) array[1]) << 48;
    dst_64 |= ((uint64_t) array[2]) << 40;
    dst_64 |= ((uint64_t) array[3]) << 32;
    dst_64 |= ((uint64_t) array[4]) << 24;
    dst_64 |= ((uint64_t) array[5]) << 16;
    dst_64 |= ((uint64_t) array[6]) << 8;
    dst_64 |= ((uint64_t) array[7]);

     return ((dst_64 - u64)==0);

}


uint64_t get_serial_from_array(uint8_t * array) {
    /*Convert to serial number */
	uint64_t serial = 0 ;
    serial  = ((uint64_t) array[0]) << 56;
	serial |= ((uint64_t) array[1]) << 48;
	serial |= ((uint64_t) array[2]) << 40;
	serial |= ((uint64_t) array[3]) << 32;
    serial |= ((uint64_t) array[4]) << 24;
    serial |= ((uint64_t) array[5]) << 16;
    serial |= ((uint64_t) array[6]) << 8;
    serial |= ((uint64_t) array[7]);
    return serial;
}


void save_uint64_t_to_array(uint8_t *message, uint64_t u64) {
    size_t i;
    uint8_t *ptr = (uint8_t *) &u64;

    /* The u64 always starts @ offset = 9 */
    for (i=0 ; i < (sizeof(uint64_t) - 1); i++) {
        message[i] = *ptr++;
    }
}

uint32_t get_uint32_from_array(uint8_t * array) {
	uint32_t u32 = 0 ;
    u32 |= ((uint32_t) array[0]) << 24;
    u32 |= ((uint32_t) array[1]) << 16;
    u32 |= ((uint32_t) array[2]) << 8;
    u32 |= ((uint32_t) array[3]);
    return u32;
}


void save_uint32_t_to_array(uint8_t * message, uint32_t u32) {
    int i;
    uint8_t *ptr = (uint8_t *) &u32;
    for (i=(sizeof(uint32_t)-1) ; i >=0; i--) {
        message[i] = *ptr++;
    }
}


uint16_t get_uint16_from_array(uint8_t * array) {
	uint16_t u16 = 0 ;
    u16 |= ((uint16_t) array[0]) << 8;
    u16 |= ((uint16_t) array[1]);
    return u16;
}

/**
 * @brief       Converts a string a unsigned integer.
 * @param       value
 *              The value to be converted.
 * @return      The resulting value as a uint64_t.
 */
uint64_t get_uint64_from_string(const char *value) {
    return strtoull(value, NULL, 10);
}
