/*
 * write_fw.c
 *
 *  Created on: 27/09/2016
 *      Author: Carlos Filipe Costa <carlos.costa@sensefinity.com>
 */

#include "util/write_fw.h"
#include "util/debug.h"
#include "dev/svc.h"

extern uint32_t _sbackup;


int write_fw(uint32_t offset, uint8_t *message, size_t data_size){
  char *pointer = (char *)&_sbackup + offset;
  dprintf("[write_fw] Address 0x%08p\r\n", pointer);
  return sv_call_write_data(pointer, (char *)message, data_size);
}

void fw_done(uint8_t* checksum, int size){
    (void)checksum;
    (void)size;
  sv_call_new_fw();
}
