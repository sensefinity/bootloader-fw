/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     ble
 * @{
 *
 * @file        ll_buffer.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

/* Bluetooth stack */
#include "ble/ll_buffer.h"

/* -------------------------------------------------------------------------- */
/*          Public API                                                        */
/* -------------------------------------------------------------------------- */

void ll_buffer_init(struct ll_buffer *buffer) {
    buffer->head = buffer->tail = 0;
}

void ll_buffer_put(struct ll_buffer *buffer, const struct ll_pdu_data *pdu) {
    /* Compute next head */
    uint32_t next_head = (buffer->head + 1) % BUFFER_CAPACITY;

    if (next_head != buffer->tail) {
        /* There is enough room in the buffer */

        /* Copy data */
        buffer->bank[buffer->head] = *pdu;

        /* Set next head */
        buffer->head = next_head;
    }
}

void ll_buffer_get(struct ll_buffer *buffer, struct ll_pdu_data *pdu) {
    if (buffer->head != buffer->tail) {
        /* There is available data in the buffer */

        /* Copy data */
        *pdu = buffer->bank[buffer->tail];

        /* Set next tail */
        buffer->tail = (buffer->tail + 1) % BUFFER_CAPACITY;
    }
}

bool ll_buffer_space_available(struct ll_buffer *buffer) {
    return ((buffer->head + 1) % BUFFER_CAPACITY) != buffer->tail;
}

bool ll_buffer_data_available(struct ll_buffer *buffer) {
    return buffer->head != buffer->tail;
}
