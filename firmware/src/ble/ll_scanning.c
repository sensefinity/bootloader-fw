/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     ble
 * @{
 *
 * @file        ll_scanning.c
 * @brief       Implementation of the scanning state part of the link layer
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

/* Bluetooth stack */
#include "ble/ll.h"

/**
 * @brief       Holds the size of the advertising report queue
 */
#define ADVERTISING_REPORT_QUEUE_SIZE       (16)

/**
 * @brief       Holds the timer channels to be used for scheduling
 */
#define TIMER_CHANNEL_SCANNING_START        (0)
#define TIMER_CHANNEL_SCANNING_STOP         (1)
#define TIMER_CHANNEL_RECEIVE_TIMEOUT       (2)

/**
 * @brief       Holds the link layer scanning parameters
 * @details     This structure is statically initialized with the default
 *              parameters provided in the Bluetooth Core Specification Version
 *              4.1 [Vol 2] 7.8.10 - LE Set Scan Parameters Command.
 */
static struct ll_scan_params _params = {
    .type       = LL_SCAN_TYPE_PASSIVE,
    .interval   = 0x0010,
    .window     = 0x0010
};

/**
 * @brief       Holds the advertising report queue
 * @details     The advertising report queue is designed to be an interrupt-safe
 *              ring buffer.
 */
static struct {
    struct ll_advertising_report reports[ADVERTISING_REPORT_QUEUE_SIZE];
    uint32_t head;
    uint32_t tail;

} _report_queue;

/**
 * @brief       Holds the handler function for advertising reports
 */
static void (*_handler)(struct ll_advertising_report) = NULL;

/**
 * @brief       Holds the PDUs to be used for scanning
 */
static struct ll_pdu_adv _recv_pdu;
static struct ll_pdu_adv _send_req_pdu;
static struct ll_pdu_adv _recv_rsp_pdu;

/**
 * @brief       Holds the variables for the backoff procedure
 */
static uint32_t _upper_limit = 1;
static uint32_t _backoff_count = 1;
static uint32_t _failures = 0;
static uint32_t _successes = 0;


/* -------------------------------------------------------------------------- */
/*          Private API                                                       */
/* -------------------------------------------------------------------------- */

/**
 * @brief       Performs the pre-initialization of the PDU that will be used
 *              to send scan requests
 * @details     This is the first phase when initializing the send request PDU
 *              and must performed before the scanning state is entered.
 *              Initializes the static data with the current link layer
 *              information.
 */
static void _ll_pre_init_send_req_pdu(void) {
    struct ll_pdu_payload_scan_req *payload;

    /* Initialize header */
    _send_req_pdu.pdu_type  = LL_PDU_ADV_TYPE_SCAN_REQ;
    _send_req_pdu.tx_add    = ll_get_bdaddr()->type;
    _send_req_pdu.length    = 2 * BDADDR_LEN;

    /* Initialize payload */
    payload = (struct ll_pdu_payload_scan_req *) _send_req_pdu.payload;

    memcpy(payload->scan_a, ll_get_bdaddr()->data, BDADDR_LEN);
}

/**
 * @brief       Performs the post-initialization of the PDU that will be used
 *              to send scan requests
 * @details     This is the second phase when initializing the send request PDU
 *              and must be performed before the request is actually sent.
 */
static void _ll_post_init_send_req_pdu(void) {
    struct ll_pdu_payload_scan_req *payload;

    /* Initialize header */
    _send_req_pdu.rx_add = _recv_pdu.tx_add;

    /* Initialize payload */
    payload = (struct ll_pdu_payload_scan_req *) _send_req_pdu.payload;

    memcpy(payload->adv_a, _recv_pdu.payload, BDADDR_LEN);
}

/**
 * @brief       Sends an advertising report to the host
 * @param[in]   pdu
 *              The PDU from where the advertising report is to be built from.
 */
static void _ll_send_advertising_report(const struct ll_pdu_adv *pdu) {
    /* Create new advertising report */
    struct ll_advertising_report report;

    /* Holds the payload pointer for each type */
    struct ll_pdu_payload_adv_ind *p0;
    struct ll_pdu_payload_adv_direct_ind *p1;
    struct ll_pdu_payload_adv_nonconn_ind *p2;
    struct ll_pdu_payload_adv_scan_ind *p3;
    struct ll_pdu_payload_scan_rsp *p4;

    switch (pdu->pdu_type) {
    case LL_PDU_ADV_TYPE_ADV_IND:
        /* Report connectable undirected advertising */
        p0 = (struct ll_pdu_payload_adv_ind *) pdu->payload;

        report.type = LL_ADVERTISING_REPORT_TYPE_CONNECTABLE_UNDIRECTED;
        report.addr.type = pdu->tx_add;
        memcpy(report.addr.data, p0->adv_a, BDADDR_LEN);
        memcpy(report.data, p0->adv_data, pdu->length - BDADDR_LEN);
        report.size = pdu->length - BDADDR_LEN;
        report.rssi = radio_read_rssi();

        break;

    case LL_PDU_ADV_TYPE_ADV_DIRECT_IND:
        /* Report connectable directed advertising */
        p1 = (struct ll_pdu_payload_adv_direct_ind *) pdu->payload;

        if (memcmp(p1->init_a, ll_get_bdaddr()->data, BDADDR_LEN) != 0
                || pdu->rx_add != ll_get_bdaddr()->type) {
            /* This directed advertising report is not directed at us */
            return;
        }

        report.type = LL_ADVERTISING_REPORT_TYPE_CONNECTABLE_DIRECTED;
        report.addr.type = pdu->tx_add;
        memcpy(report.addr.data, p1->adv_a, BDADDR_LEN);
        report.size = 0;
        report.rssi = radio_read_rssi();

        break;

    case LL_PDU_ADV_TYPE_ADV_NONCONN_IND:
        /* Report non-connectable undirected advertising */
        p2 = (struct ll_pdu_payload_adv_nonconn_ind *) pdu->payload;

        report.type = LL_ADVERTISING_REPORT_TYPE_NONCONNECTABLE_UNDIRECTED;
        report.addr.type = pdu->tx_add;
        memcpy(report.addr.data, p2->adv_a, BDADDR_LEN);
        memcpy(report.data, p2->adv_data, pdu->length - BDADDR_LEN);
        report.size = pdu->length - BDADDR_LEN;
        report.rssi = radio_read_rssi();

        break;

    case LL_PDU_ADV_TYPE_ADV_SCAN_IND:
        /* Report scannable undirected advertising */
        p3 = (struct ll_pdu_payload_adv_scan_ind *) pdu->payload;

        report.type = LL_ADVERTISING_REPORT_TYPE_SCANNABLE_UNDIRECTED;
        report.addr.type = pdu->tx_add;
        memcpy(report.addr.data, p3->adv_a, BDADDR_LEN);
        memcpy(report.data, p3->adv_data, pdu->length - BDADDR_LEN);
        report.size = pdu->length - BDADDR_LEN;
        report.rssi = radio_read_rssi();

        break;

    case LL_PDU_ADV_TYPE_SCAN_RSP:
        /* Report scan response */
        p4 = (struct ll_pdu_payload_scan_rsp *) pdu->payload;

        report.type = LL_ADVERTISING_REPORT_TYPE_SCAN_RESPONSE;
        report.addr.type = pdu->tx_add;
        memcpy(report.addr.data, p4->adv_a, BDADDR_LEN);
        memcpy(report.data, p4->scan_rsp_data, pdu->length - BDADDR_LEN);
        report.size = pdu->length - BDADDR_LEN;
        report.rssi = radio_read_rssi();

        break;

    default:
        /* We cannot generate a report for other types */
        return;
    }

    /* Put report in the queue */
    uint32_t next_head = (_report_queue.head + 1)
            % ADVERTISING_REPORT_QUEUE_SIZE;

    if (next_head != _report_queue.tail) {
        /* There is room for an advertising report, put copy */
        _report_queue.reports[_report_queue.head] = report;

        /* Update head */
        _report_queue.head = next_head;

        /* Signal that a new report is present */
        NVIC_SetPendingIRQ(SWI0_IRQn);
    }
}

/**
 * @brief       Software interrupt SWI0 handler
 * @details     Handles reporting to the host of advertising reports.
 */
void isr_swi0(void) {
    struct ll_advertising_report report;

    while (_report_queue.head != _report_queue.tail && _handler != NULL) {
        /* Get copy of report */
        report = _report_queue.reports[_report_queue.tail];

        /* Update tail */
        _report_queue.tail = (_report_queue.tail + 1)
                % ADVERTISING_REPORT_QUEUE_SIZE;

        /* Send report */
        _handler(report);
    }
}

/**
 * @brief       Initializes the required data for the backoff procedure
 */
static void _ll_backoff_procedure_init(void) {
    _upper_limit = 1;
    _backoff_count = 1;
    _failures = 0;
    _successes = 0;
}

/**
 * @brief       Logs one successfull attempt to receive a scan response
 */
static void _ll_backoff_procedure_log_success(void) {
    if (++_successes == 2) {
        /* Reset number of consecutive successes */
        _successes = 0;

        /* Halve upper limit */
        if (_upper_limit > 1) {
            _upper_limit >>= 1;
        }
    }

    /* Reset number of consecutive failures, since we got a success */
    _failures = 0;

    /* Compute new value for the backoff count */
    _backoff_count = 1 + (random_next() & (_upper_limit - 1));
}

/**
 * @brief       Logs one failed attempt to receive a scan response
 */
static void _ll_backoff_procedure_log_failure(void) {
    if (++_failures == 2) {
        /* Reset number of consecutive failures */
        _failures = 0;

        /* Double upper limit */
        if (_upper_limit < 256) {
            _upper_limit <<= 1;
        }
    }

    /* Reset number of consecutive successes, since we got a failure */
    _successes = 0;

    /* Compute new value for the backoff count */
    _backoff_count = 1 + (random_next() & (_upper_limit - 1));
}

/**
 * @brief       Decides whether the link layer should restrict one scan
 *              request from being sent
 * @returns     True if the sending of a scan request should be restricted
 *              or false, otherwise.
 */
static bool _ll_backoff_procedure_requests_restricted(void) {
    if (_backoff_count == 0 || --_backoff_count == 0) {
        /* Backoff count is zero or reached zero, as such, requests should
           NOT be restricted */
        return false;
    }

    /* Backoff count is not zero, as such, requests SHOULD be restricted */
    return true;
}

/**
 * @brief       Prototype for the function that handles the case when an
 *              advertising packet has been received
 * @param[in]   event
 *              The radio event to be handled.
 */
static void _ll_advertising_packet_received(enum radio_event event);

/**
 * @brief       Handles the receive timeout
 */
static void _ll_handle_receive_timeout(void) {
    /* Disable radio */
    radio_disable();

    /* Cancel receive timeout channel */
    timer_cancel(TIMER_CHANNEL_RECEIVE_TIMEOUT);

    /* Since the radio timed-out, it means we were unable to receive a scan
       response, log failure */
    _ll_backoff_procedure_log_failure();

    /* Restart radio with initial configurations */
    radio_set_pdu(&_recv_pdu);
    radio_set_event_handler(&_ll_advertising_packet_received);
    radio_interleaved_rx_tx();
}

/**
 * @brief       Handles the end of the scanning window
 */
static void _ll_handle_scanning_window(void) {
    /* Disable radio */
    radio_disable();

    /* Cancel scanning stop channel */
    timer_cancel(TIMER_CHANNEL_SCANNING_STOP);
}

/**
 * @brief       Handles the case when a scan response has been received
 * @param[in]   event
 *              The radio event to be handled.
 */
static void _ll_scan_response_received(enum radio_event event) {
    struct ll_pdu_payload_scan_rsp *payload;

    if (event == RADIO_EVENT_RX_STARTED) {
        /* Cancel receive timeout */
        timer_cancel(TIMER_CHANNEL_RECEIVE_TIMEOUT);
    }

    if (event == RADIO_EVENT_RX_DONE) {
        /* The reception of a packet (hopefully a scan response) is done; This
           means that we can report the previously received packet since we
           couldn't do it on the previous state due to time constraints */
        _ll_send_advertising_report(&_recv_pdu);

        if (radio_crc_match()) {
            /* CRC matches; It's time to verify that we indeed received a scan
               response */
            if (_recv_rsp_pdu.pdu_type == LL_PDU_ADV_TYPE_SCAN_RSP) {
                /* Retrieve pointer to the payload */
                payload = (struct ll_pdu_payload_scan_rsp *) &_recv_rsp_pdu.payload;

                /* We have received a scan response; Yet we still don't know
                   if it was addressed at us */
               if (memcmp(_recv_pdu.payload, payload->adv_a, BDADDR_LEN) == 0
                    && _recv_pdu.tx_add == _recv_rsp_pdu.tx_add) {
                    /* We received the scan response we were looking for; As
                       such, we can report it to the host and log one success */
                    _ll_send_advertising_report(&_recv_rsp_pdu);
                    _ll_backoff_procedure_log_success();

                } else {
                    /* The scan response was not addressed at us; This means
                       we have a failure */
                    _ll_backoff_procedure_log_failure();
                }

            } else {
                /* We did not receive a scan response; It's o failure */
                _ll_backoff_procedure_log_failure();
            }

        } else {
            /* CRC does not match; Log one failure */
            _ll_backoff_procedure_log_failure();
        }

        /* Restart radio with base configurations in order to receive more
           advertising packets */
        radio_set_pdu(&_recv_pdu);
        radio_set_event_handler(&_ll_advertising_packet_received);
        radio_interleaved_rx_tx();
    }
}

/**
 * @brief       Handles the case when a scan request was sent to a device
 * @param[in]   event
 *              The radio event to be handled.
 */
static void _ll_scan_request_sent(enum radio_event event) {
    if (event == RADIO_EVENT_TX_DONE) {
        /* A scan request has been sent; Quickly re-start radio in receive
           mode in order to receive the scan response */
        radio_rx();
        radio_set_pdu(&_recv_rsp_pdu);
        radio_set_event_handler(&_ll_scan_response_received);

        /* Configure radio receive timeout */
        timer_configure(
                TIMER_CHANNEL_RECEIVE_TIMEOUT,
                &_ll_handle_receive_timeout,
                timer_read() + RADIO_TIFS + 50);
    }
}

/**
 * @brief       Handles the case when an advertising packet has been received
 * @param[in]   event
 *              The radio event to be handled.
 */
static void _ll_advertising_packet_received(enum radio_event event) {
    if (event == RADIO_EVENT_RX_DONE) {
        if (radio_crc_match()) {
            if (_params.type == LL_SCAN_TYPE_PASSIVE) {
                /* We're using passive scanning, as such we can log the received
                   packet right now and restart radio */
                _ll_send_advertising_report(&_recv_pdu);
                radio_start();

            } else {
                if ((_recv_pdu.pdu_type == LL_PDU_ADV_TYPE_ADV_IND
                    || _recv_pdu.pdu_type == LL_PDU_ADV_TYPE_ADV_SCAN_IND)
                    && !_ll_backoff_procedure_requests_restricted()) {
                    /* The received packet identifies the device as scannable
                       and the backoff procedure allows us to send a scan
                       request */
                   _ll_post_init_send_req_pdu();
                   radio_set_pdu(&_send_req_pdu);
                   radio_set_event_handler(&_ll_scan_request_sent);

                } else {
                    /* The received packet identifies the remote device as
                       not scannable, as such, simply disable radio and report
                       the received packet, restarting the radio afterwards */
                    radio_disable();
                    _ll_send_advertising_report(&_recv_pdu);
                    radio_interleaved_rx_tx();
                }
            }

        } else {
            if (_params.type == LL_SCAN_TYPE_PASSIVE) {
                /* CRC does not match and we're using passive scanning; Simply
                   trigger start again */
                radio_start();

            } else {
                /* We're using active scanning; We need to disable the radio
                   and re-enable it */
                radio_disable();
                radio_interleaved_rx_tx();
            }
        }

    } else if(event == RADIO_EVENT_RX_READY) {
        /* We're using passive scanning, which uses the continuous reception
           radio mode; This will happen only once; Trigger start */
        radio_start();
    }
}

/**
 * @brief       Starts a new scan interval
 * @details     Performs the start of a new scan interval while also scheduling
 *              the end of a scanning window.
 */
static void _ll_scan_start(void) {
    /* Make sure radio is disabled (useful for continuous scanning mode) */
    radio_disable();

    /* Setup advertising channel */
    radio_setup_advertising(ll_get_advertising_channel_index());

    if (!ll_set_next_advertising_channel()) {
        /* Setup next advertising channel */
        ll_set_initial_advertising_channel();
    }

    /* Set radio PDU */
    radio_set_pdu(&_recv_pdu);

    /* Set radio event handler */
    radio_set_event_handler(&_ll_advertising_packet_received);

    if (_params.interval != _params.window) {
        /* Since the interval is not equal to the window, the host is not
           using continuous scanning mode; As such, we need to schedule the
           end of the scanning window */
        timer_configure(
            TIMER_CHANNEL_SCANNING_STOP,
            &_ll_handle_scanning_window,
            timer_read() + _params.window * 625);
    }

    /* Schedule the start of the next scanning interval */
    timer_update(
        TIMER_CHANNEL_SCANNING_START,
        timer_read() + _params.interval * 625);

    if (_params.type == LL_SCAN_TYPE_PASSIVE) {
        /* In passive scanning, we receive only */
        radio_continuous_rx();

    } else {
        /* In active scanning, we may need to send requests */
        radio_interleaved_rx_tx();
    }
}

/**
 * @brief       Enables the scanning state
 */
static void _ll_scan_enable(void) {
    if (_params.type == LL_SCAN_TYPE_ACTIVE) {
        /* We're enabling active scanning, as such we'll be sending scan
           requests to scannable devices; Pre-initialize the send request PDU */
        _ll_pre_init_send_req_pdu();

        /* Initialize backoff procedure, since we will be sending scan
           requests */
        _ll_backoff_procedure_init();
    }

    /* Clear report queue */
    _report_queue.head = _report_queue.tail = 0;

    /* Set advertising channel map */
    ll_set_advertising_channel_map(_params.channel_map);

    /* Initialize the advertising channel index */
    ll_set_initial_advertising_channel();

    /* Enable RSSI reading */
    radio_set_rssi_enable(true);

    /* Enable software interrupt */
    NVIC_ClearPendingIRQ(SWI0_IRQn);
    NVIC_SetPriority(SWI0_IRQn, DANDELION_IRQ_PRIORITY_LOW);
    NVIC_EnableIRQ(SWI0_IRQn);

    /* Schedule the start of the scanning window */
    timer_stop();
    timer_clear();
    timer_configure(
            TIMER_CHANNEL_SCANNING_START,
            &_ll_scan_start,
            1);

    timer_start();
}

/**
 * @brief       Disables the scanning state
 */
static void _ll_scan_disable(void) {
    /* Stop timer */
    timer_stop();

    /* Disable radio (in case it is currently enabled) */
    radio_disable();

    /* Disable RSSI reading */
    radio_set_rssi_enable(false);

    /* Disable software interrupt */
    NVIC_DisableIRQ(SWI0_IRQn);

    /* Cancel configured timers */
    timer_cancel(TIMER_CHANNEL_SCANNING_START);
    timer_cancel(TIMER_CHANNEL_SCANNING_STOP);
    timer_cancel(TIMER_CHANNEL_RECEIVE_TIMEOUT);
}

/* -------------------------------------------------------------------------- */
/*          Public API                                                        */
/* -------------------------------------------------------------------------- */

bool ll_set_scan_params(struct ll_scan_params params) {
    /* Validate parameters */
    if (params.window > params.interval) {
        /* The window is larger than the interval */
        return false;
    }

    if (params.interval < LL_SCAN_INTERVAL_MIN
            || params.interval > LL_SCAN_INTERVAL_MAX) {
        /* The scan interval is not within valid range */
        return false;
    }

    if (params.window < LL_SCAN_WINDOW_MIN
            || params.window > LL_SCAN_WINDOW_MAX) {
        /* The scan window is not within valid range */
        return false;
    }

    /* Filter out unwanted bits in the channel map */
    params.channel_map &= LL_ADVERTISING_CHANNEL_MAP_ALL;

    if (params.channel_map == 0) {
        /* There are no enabled advertising channels */
        return false;
    }

    /* Parameters are valid */
    _params = params;

    return true;
}

struct ll_scan_params ll_get_scan_params(void) {
    return _params;
}

bool ll_set_scan_enable(bool enable) {
    if (enable) {
        if (!ll_set_state(LL_STATE_SCANNING)) {
            /* Can't perform state transition */
            return false;
        }

        /* Enable scanning */
        _ll_scan_enable();

    } else {
        if (!ll_set_state(LL_STATE_STANDBY)) {
            /* Can't perform state transition */
            return false;
        }

        /* Disable advertising */
        _ll_scan_disable();
    }

    return true;
}

void ll_set_advertising_report_handler(
        void (*handler)(struct ll_advertising_report)) {
    /* Store handler */
    _handler = handler;
}
