/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     ble
 * @{
 *
 * @file        ll_hop_data.c
 * @brief       Implementation of the hop sequence for data channels
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

/* Bluetooth stack */
#include "ble/ll.h"

/**
 * @brief       Holds the channel classification which holds the used and
 *              unused channels
 */
static uint64_t _channel_classification = LL_DATA_CHANNEL_MAP_ALL;

/**
 * @brief       Holds the number of used channels
 */
static uint32_t _used_channel_count = 37;

/**
 * @brief       Holds the hop increment
 */
static uint32_t _hop_increment;

/**
 * @brief       Holds the last unmapped channel
 */
static uint32_t _last_unmapped_channel;

/**
 * @brief       Holds the current unmapped channel
 */
static uint32_t _unmapped_channel;

/**
 * @brief       Holds the current data channel index
 */
static uint32_t _data_channel_index;


/* -------------------------------------------------------------------------- */
/*          Private API                                                       */
/* -------------------------------------------------------------------------- */

/**
 * @brief       Tests whether a channel index corresponds to a used channel
 * @param[in]   index
 *              The index of the channel to be tested.
 * @returns     True if the channel index corresponds to a used channel or
 *              false, otherwise.
 */
static inline bool _ll_is_used_channel(uint32_t index) {
    return (LL_DATA_CHANNEL_MAP_BIT(index) & _channel_classification) != 0;
}

/**
 * @brief       Performs remapping of the current unmapped channel
 * @returns     The data channel index after remapping.
 */
static uint32_t _ll_data_channel_remap(void) {
    uint32_t i;

    /* Compute remapping index */
    uint32_t remapping_index = _unmapped_channel % _used_channel_count;

    /* Holds the index of the current used channel */
    int32_t used_channel_index = -1;

    for (i = 0; i < 37; ++i) {
        if (_ll_is_used_channel(i)) {
            ++used_channel_index;

            if (((uint32_t) used_channel_index) == remapping_index) {
                return i;
            }
        }
    }

    return 0;
}

/**
 * @brief       Checks whether a value is a power of two
 * @param[in]   value
 *              The value to be checked.
 * @returns     True if the value is a power of two or false, otherwise.
 */
static bool _ll_is_pow2(uint32_t value) {
    while ((value & 1) == 0 && value > 1) {
        /* While the value is even and greater than (1) */
        value >>= 1;
    }

    return value == 1;
}


/* -------------------------------------------------------------------------- */
/*          Public API                                                        */
/* -------------------------------------------------------------------------- */

bool ll_configure_data_channel_hopping(uint64_t map, uint32_t hop) {
    uint32_t i;

    /* Remove unwanted bits from channel map */
    map &= LL_DATA_CHANNEL_MAP_ALL;

    if (map == 0 || _ll_is_pow2(map)) {
        /* There are no used channels or only a single channel is used */
        return false;
    }

    if (hop < 5 || hop > 16) {
        /* The hop value is invalid */
        return false;
    }

    /* Store new channel classification */
    _channel_classification = map;

    /* Compute new number of used channels */
    _used_channel_count = 0;

    for (i = 0; i < 37; ++i) {
        if (_ll_is_used_channel(i)) {
            ++_used_channel_count;
        }
    }

    /* Store new hop increment */
    _hop_increment = hop;

    /* Reset unmapped channel */
    _unmapped_channel = 0;

    return true;
}

void ll_set_next_data_channel(void) {
    /* We'll assume we're starting a new connection event; This means that
       we first have to set the value of the last unmapped channel */
    _last_unmapped_channel = _unmapped_channel;

    /* Compute new unmapped channel */
    _unmapped_channel = (_last_unmapped_channel + _hop_increment);

    if (_unmapped_channel >= 37) {
        /* Apply simplified modulus operator (avoids division) */
        _unmapped_channel -= 37;
    }

    if (_ll_is_used_channel(_unmapped_channel)) {
        /* We hit an used channel */
        _data_channel_index = _unmapped_channel;

    } else {
        /* We hit an unused channel, we need remapping */
        _data_channel_index = _ll_data_channel_remap();
    }
}

uint32_t ll_get_data_channel_index(void) {
    return _data_channel_index;
}
