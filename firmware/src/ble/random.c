/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     ble
 * @{
 *
 * @file        random.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#include "ble/random.h"

/**
 * @brief       Holds the random number pool
 */
static uint8_t _pool[RANDOM_POOL_SIZE];

/**
 * @brief       Holds of the next element in the random number pool
 */
static uint32_t _pool_index = 0;

void random_init(void) {
    /* Enable digital correction algorithm */
    NRF_RNG->CONFIG = RNG_CONFIG_DERCEN_Enabled << RNG_CONFIG_DERCEN_Pos;

    /* Disable random number generator after each value */
    NRF_RNG->SHORTS =
            RNG_SHORTS_VALRDY_STOP_Enabled << RNG_SHORTS_VALRDY_STOP_Pos;

    /* Refresh pool */
    random_refresh();
}

uint8_t random_next(void) {
    uint8_t value = _pool[_pool_index];

    if (++_pool_index == RANDOM_POOL_SIZE) {
        /* Wrap around */
        _pool_index = 0;
    }

    return value;
}

/**
 * @brief       Internal function that generates a new random value
 * @returns     A new random number
 */
uint8_t random_generate(void) {
    /* Clear VALRDY event */
    NRF_RNG->EVENTS_VALRDY = 0;

    /* Start generation of new random number */
    NRF_RNG->TASKS_START = 1;

    /* Wait for random number to be available */
    while (NRF_RNG->EVENTS_VALRDY == 0);

    /* Return random number */
    return (uint8_t) (NRF_RNG->VALUE);
}

void random_refresh(void) {
    uint32_t i;

    for (i = 0; i < RANDOM_POOL_SIZE; ++i) {
        /* Generate new random value */
        _pool[i] = random_generate();
    }

    _pool_index = 0;
}
