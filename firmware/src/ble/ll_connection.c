/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     ble
 * @{
 *
 * @file        ll_connection.c
 * @brief       Implementation of the connection state part of the link layer
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

/* Bluetooth stack */
#include "ble/ll.h"
#include <stdio.h>

/**
 * @brief       Definition of a macro that computes the maximum of two values
 */
#ifndef MAX
#define MAX(a, b) (((a) > (b)) ? (a) : (b))
#endif

/**
 * @brief       Holds the timer channels to be used for scheduling during the
 *              connection phase
 */
#define TIMER_CHANNEL_CONNECTION_INTERVAL   (0)
#define TIMER_CHANNEL_SUPERVISION_TIMEOUT   (1)
#define TIMER_CHANNEL_CAPTURE_ADDRESS       (2)
#define TIMER_CHANNEL_CAPTURE_END           (2)
#define TIMER_CHANNEL_RECEIVE_TIMEOUT       (3)

/**
 * @brief       Holds the PPI channels that we'll use to control the various
 *              connection procedures
 */
#define PPI_CHANNEL_CONNECTION_INTERVAL     (0)
#define PPI_CHANNEL_ADDRESS_RECEIVED        (1)
#define PPI_CHANNEL_PACKET_SENT             (1)

/**
 * @brief       Holds the maximum number of events that may be in the queue
 *              at any given time
 */
#define EVENT_QUEUE_CAPACITY                (16)

/**
 * @brief       Holds the current link layer connection role
 */
static enum ll_connection_role _role = LL_CONNECTION_ROLE_NONE;

/**
 * @brief       Holds the connection event handler
 */
static void (*_handler)(enum ll_connection_event) = NULL;

/**
 * @brief       Holds the access address to be used for the whole connection
 */
static uint32_t _access_address;

/**
 * @brief       Holds the CRC initialization value to be used for the whole
 *              connection
 */
static uint32_t _crc_init;

/**
 * @brief       Holds the window offset value to be used when establishing
 *              a connection
 */
static uint32_t _window_offset;

/**
 * @brief       Holds the window size value to be used when establishing
 *              a connection
 */
static uint32_t _window_size;

/**
 * @brief       Holds the value for the connection interval
 */
static uint32_t _interval;

/**
 * @brief       Holds the value to be used for the supervision timeout
 */
static uint32_t _supervision_timeout;

/* ===== */
/* Additional variables for slave role, not documented for now */
static uint32_t accum_ppm;
static uint32_t t_last_anchor;
static uint32_t t_last_slave_expected_anchor;
/* ===== */

/**
 * @brief       Holds whether the connection is currently established
 */
static bool _conn_established;

/**
 * @brief       Holds the sequence number for the transmit packet
 */
static uint32_t _transmit_seq_num;

/**
 * @brief       Holds the next expected sequence number
 */
static uint32_t _next_expected_seq_num;

/**
 * @brief       Holds the transmit buffer
 */
static struct ll_buffer _tx_buffer = BUFFER_STATIC_INIT;

/**
 * @brief       Holds the receive buffer
 */
static struct ll_buffer _rx_buffer = BUFFER_STATIC_INIT;

/**
 * @brief       Holds the transmit PDU
 */
static struct ll_pdu_data _tx_pdu;

/**
 * @brief       Holds the receive PDU
 */
static struct ll_pdu_data _rx_pdu;

/**
 * @brief       Holds the event queue that will be used to notify the host
 */
static struct {
    enum ll_connection_event events[EVENT_QUEUE_CAPACITY];
    uint32_t head;
    uint32_t tail;

} _event_queue;

/**
 * @brief       Holds the device address of the peer device.
 */
static struct bdaddr _peer_addr;

/* -------------------------------------------------------------------------- */
/*          Private API                                                       */
/* -------------------------------------------------------------------------- */

/**
 * @brief       Dispatches a connection event to the configured handler
 * @param[in]   event
 *              The connection event to be dispatched.
 */
static void _ll_dispatch_connection_event(enum ll_connection_event event) {
    uint32_t next_head = (_event_queue.head + 1) % EVENT_QUEUE_CAPACITY;

    if (next_head != _event_queue.tail) {
        /* Put new connection event */
        _event_queue.events[_event_queue.head] = event;

        /* Update head */
        _event_queue.head = next_head;

        /* Signal that a new event is present */
        NVIC_SetPendingIRQ(SWI1_IRQn);
    }
}

/**
 * @brief       Handles the notifications to the host by reading from the
 *              event queue
 */
void isr_swi1(void) {
    enum ll_connection_event event;

    while (_event_queue.head != _event_queue.tail && _handler != NULL) {
        /* Get copy of event */
        event = _event_queue.events[_event_queue.tail];

        /* Update tail */
        _event_queue.tail = (_event_queue.tail + 1) % EVENT_QUEUE_CAPACITY;

        /* Send report */
        _handler(event);
    }
}

/**
 * @brief       Initializes the provided PDU as an empty PDU
 * @param[in]   pdu
 *              The PDU to be initialized as an empty PDU.
 */
static void _ll_init_empty_pdu(struct ll_pdu_data *pdu) {
    /* Initialize only mandatory fields */
    pdu->llid   = LL_PDU_DATA_LLID_CONTINUATION;
    pdu->length = 0;
}

/**
 * @brief       Tests whether a data channel PDU is empty
 * @param[in]   pdu
 *              Pointer to the PDU to be tested.
 * @returns     True if the PDU is empty or false, otherwise.
 */
static bool _ll_is_empty_pdu(const struct ll_pdu_data *pdu) {
    return pdu->llid == LL_PDU_DATA_LLID_CONTINUATION
            && pdu->length == 0;
}

/**
 * @brief       Exits the current connection by disabling all resources
 */
static void _ll_exit_connection(void) {
    /* Stop timer */
    timer_stop();

    /* Cancel all used timers */
    timer_cancel(TIMER_CHANNEL_CONNECTION_INTERVAL);
    timer_cancel(TIMER_CHANNEL_SUPERVISION_TIMEOUT);
    timer_cancel(TIMER_CHANNEL_CAPTURE_ADDRESS);
    timer_cancel(TIMER_CHANNEL_RECEIVE_TIMEOUT);

    /* Disable all PPI channels */
    ppi_channel_disable(PPI_CHANNEL_CONNECTION_INTERVAL);
    ppi_channel_disable(PPI_CHANNEL_ADDRESS_RECEIVED);

    /* Disable radio, just in case it was enabled */
    radio_disable();

    /* Exit link layer connection state */
    ll_set_state(LL_STATE_STANDBY);
}

/**
 * @brief       Handles the supervision timeout
 * @details     When the supervision timeout happens, the current link layer
 *              connection is considered lost. As such, we must exit the
 *              connection state, clearing any lingering data and notify the
 *              host that the connection was lost.
 */
static void _ll_handle_supervision_timeout(void) {
    /* Exit the current connection */
    _ll_exit_connection();

    /* Notify host */
    _ll_dispatch_connection_event(LL_CONNECTION_EVENT_CONNECTION_LOST);
}

/**
 * @brief       Handles the connection termination procedure
 * @param[in]   error_code
 *              The error code.
 */
static void _ll_terminate_connection(uint8_t error_code) {
    /* Exit the current connection */
    _ll_exit_connection();

    /* Notify host */
    if (error_code == 0) {
        _ll_dispatch_connection_event(
                LL_CONNECTION_EVENT_CONNECTION_TERMINATED_OK);

    } else {
        _ll_dispatch_connection_event(
                LL_CONNECTION_EVENT_CONNECTION_TERMINATED_ERROR);
    }
}

/**
 * @brief       Handles control PDUs received from the peer device
 * @details     Control PDUs are used to negotiate the aspects of the current
 *              connection. This function will handle control PDUs received
 *              from the peer device.
 */
static void _ll_handle_rx_control_pdu(void) {
    struct ll_pdu_data reply_pdu;
    struct ll_pdu_payload_control *payload;
    enum ll_pdu_control_opcode opcode;

    /* Get payload */
    payload = (struct ll_pdu_payload_control *) _rx_pdu.payload;

    /* Get opcode */
    opcode = payload->opcode;

    switch (opcode) {
    case LL_PDU_CONTROL_OPCODE_TERMINATE_IND:
        /* We've been asked to terminate the current connection */
        _ll_terminate_connection(payload->ctr_data[0]);
        break;

    default:
        /* We can't handle the provided opcode, as such we'll reply with a
           LL_PDU_CONTROL_OPCODE_UNKNOWN_RSP control PDU */

        /* Initialize header */
        reply_pdu.llid = LL_PDU_DATA_LLID_CONTROL;
        reply_pdu.length = 2;

        /* Initialize payload */
        payload = (struct ll_pdu_payload_control *) reply_pdu.payload;

        payload->opcode = LL_PDU_CONTROL_OPCODE_UNKNOWN_RSP;
        payload->ctr_data[0] = opcode;

        /* Insert into transmit buffer */
        ll_buffer_put(&_tx_buffer, &reply_pdu);

        break;
    }
}

/**
 * @brief       Handles control PDUs sent by this device
 * @details     Control PDUs are used to negotiate the aspects of the current
 *              connection. This function will handle control PDUs sent by
 *              this device. As such, this function shall only be called upon
 *              acknowledgement from the peer.
 */
static void _ll_handle_tx_control_pdu(void) {
    struct ll_pdu_payload_control *payload;

    /* Get payload */
    payload = (struct ll_pdu_payload_control *) _tx_pdu.payload;

    switch (payload->opcode) {
    case LL_PDU_CONTROL_OPCODE_TERMINATE_IND:
        /* We told the peer device that we're terminating the connection, as
           such we can terminate it on our end */
        _ll_terminate_connection(payload->ctr_data[0]);
        break;

    default:
        /* We cannot handle other opcodes */
        return;
    }
}

/**
 * @brief       Prepares the transmit PDU
 * @details     This function will take into account whether retransmission
 *              is needed or not and also handle the more data field in all
 *              the transmit PDUs.
 */
static void _ll_prepare_tx_pdu(void) {
    if (_rx_pdu.nesn != _transmit_seq_num) {
        /* The last transmitted packet was acknowledged by the peer; This means
           we can increment the transmit sequence number */
        _transmit_seq_num = (_transmit_seq_num + 1) & 1;

        if (_tx_pdu.llid == LL_PDU_DATA_LLID_CONTROL) {
            /* The peer has just acknowledged a control PDU, as such, we'll
               first handle the control PDU */
            _ll_handle_tx_control_pdu();

        } else {
            /* The peer has just acknowledged a data PDU */
            if (!_ll_is_empty_pdu(&_tx_pdu)) {
                /* Since the PDU was not empty, we can notify the host that
                   we've sent data */
                _ll_dispatch_connection_event(LL_CONNECTION_EVENT_DATA_SENT);
            }
        }

        /* Prepare next PDU */

        if (ll_buffer_data_available(&_tx_buffer)) {
            /* There is more data in the buffer, obtain next packet */
            ll_buffer_get(&_tx_buffer, &_tx_pdu);

        } else {
            /* There is no more data in the buffer; This means that we have
               to use the empty PDU in order to keep the connection alive */
            _ll_init_empty_pdu(&_tx_pdu);
        }

        /* Set new sequence number */
        _tx_pdu.sn = _transmit_seq_num;
    }

    /* Set new next expected sequence number */
    _tx_pdu.nesn = _next_expected_seq_num;

    /* Tell peer that we never have more data to be sent, since we'll use
       connection events exclusively */
    _tx_pdu.md = 0;
}

/**
 * @brief       Handles the received PDU
 * @details     This function will handle the received PDU and also handle
 *              acknowledgement and flow control. This function must only be
 *              called when a PDU with a valid CRC was received.
 */
static void _ll_handle_rx_pdu(void) {
    if (_rx_pdu.sn == _next_expected_seq_num) {
        /* We have just received a new data channel PDU */
        switch (_rx_pdu.llid) {
        case LL_PDU_DATA_LLID_CONTINUATION:
        case LL_PDU_DATA_LLID_START:
            /* We have a L2CAP PDU, as such we'll let the host handle it */
            if (ll_buffer_space_available(&_rx_buffer)) {
                /* There is enough space in the buffer to store the received PDU;
                   As such, we'll acknowledge it by incrementing the next expected
                   sequence number */
                _next_expected_seq_num = (_next_expected_seq_num + 1) & 1;

                if (!_ll_is_empty_pdu(&_rx_pdu)) {
                    /* Put the received PDU in the buffer */
                    ll_buffer_put(&_rx_buffer, &_rx_pdu);

                    /* Notify host */
                    _ll_dispatch_connection_event(LL_CONNECTION_EVENT_DATA_RECEIVED);
                }
            }

            break;

        case LL_PDU_DATA_LLID_CONTROL:
            /* This is a control PDU, as such we'll handle it; Since we don't
               need to store this PDU in the receive buffer, we can simply
               acknowledge the packet by incrementing the sequence number */
            _next_expected_seq_num = (_next_expected_seq_num + 1) & 1;

            /* Handle control PDU */
            _ll_handle_rx_control_pdu();

            break;

        default:
            /* Unknown PDU Type */
            return;
        }
    }
}

/* === */
/* Some function prototypes. Nothing to do here... */
static void _ll_handle_master_packet_sent(enum radio_event event);
static void _ll_handle_slave_packet_received(enum radio_event event);
/* === */

/**
 * @brief       Computes the window widening, taking into account the last
 *              anchor point.
 */
static uint32_t _window_widening(uint32_t t_slave_expected_anchor) {
    uint32_t t_since_last_anchor;

    /* Compute time since the last anchor */
    t_since_last_anchor = t_slave_expected_anchor - t_last_anchor;

    /* Compute and return window widening */
    /* NOTE: We add 1us to account for non-divisible numbers */
    return ((accum_ppm * t_since_last_anchor) / 1000000) + 1;
}

/**
 * @brief       Handles the master receive timeout, which happens when we
 *              have received no packet from the slave.
 */
static void _ll_handle_master_rx_timeout(void) {
    uint32_t t_last_anchor;

    if (NRF_RADIO->EVENTS_ADDRESS == 0) {
        /* We did not receive an address match from the radio, as such we'll
           consider that we've timed-out; Disable the radio */
        radio_disable();

        /* Get last anchor time */
        t_last_anchor = NRF_TIMER0->CC[TIMER_CHANNEL_CONNECTION_INTERVAL];

        /* Schedule the next connection interval */
        NRF_TIMER0->CC[TIMER_CHANNEL_CONNECTION_INTERVAL] =
                t_last_anchor + (_interval * 1250);

        /* Configure radio for PPI triggering */
        radio_interleaved_tx_rx_ppi();

        /* Setup data channel */
        ll_set_next_data_channel();
        radio_setup_data(ll_get_data_channel_index(), _access_address, _crc_init);

        /* Setup radio PDU */
        _ll_prepare_tx_pdu();
        radio_set_pdu(&_tx_pdu);

        /* Setup radio event handler */
        radio_set_event_handler(&_ll_handle_master_packet_sent);
    }
}

/**
 * @brief       Handles the slave receive timeout, which happens when we
 *              haven't received any packet from the master.
 */
static void _ll_handle_slave_rx_timeout(void) {
    uint32_t t_slave_expected_anchor;
    uint32_t t_window_widening;

    /* We timed-out before we could receive a packet; Disable radio */
    radio_disable();

    /* Compute the next expected anchor point */
    t_slave_expected_anchor = t_last_slave_expected_anchor + (_interval * 1250);
    t_last_slave_expected_anchor = t_slave_expected_anchor;

    /* Compute window widening */
    t_window_widening = _window_widening(t_slave_expected_anchor);

    /* Shedule the next receive window */
    NRF_TIMER0->CC[TIMER_CHANNEL_CONNECTION_INTERVAL]
        = t_slave_expected_anchor
        - RADIO_RAMP_TIME_RX
        - RADIO_TIFS_JITTER
        - t_window_widening;

    if (!_conn_established) {
        /* The connection is not yet established; As such we need to listen
           to a whole new master transmit window */
        timer_configure(
            TIMER_CHANNEL_RECEIVE_TIMEOUT,
            &_ll_handle_slave_rx_timeout,
            t_slave_expected_anchor
            + (_window_size * 1250)
            + ((1 + 4) * 8)
            + RADIO_TIFS_JITTER
            + t_window_widening
            + LL_ISR_LATENCY);

    } else {
        /* The connection is established; Schedule standard receive timeout */
        timer_configure(
            TIMER_CHANNEL_RECEIVE_TIMEOUT,
            &_ll_handle_slave_rx_timeout,
            t_slave_expected_anchor
            + ((1 + 4) * 8)
            + RADIO_TIFS_JITTER
            + t_window_widening
            + LL_ISR_LATENCY);
    }

    /* FROM HERE DOWN: BOILER PLATE CODE TO CONFIGURE RADIO */

    /* Configure radio for PPI triggering */
    radio_interleaved_rx_tx_ppi();

    /* Setup data channel */
    ll_set_next_data_channel();
    radio_setup_data(ll_get_data_channel_index(), _access_address, _crc_init);

    /* Setup radio PDU */
    radio_set_pdu(&_rx_pdu);

    /* Setup radio event handler */
    radio_set_event_handler(&_ll_handle_slave_packet_received);
}

/**
 * @brief       Handles the case when the master has received a packet
 * @param[in]   event
 *              The radio event to be handled.
 */
static void _ll_handle_master_packet_received(enum radio_event event) {
    if (event == RADIO_EVENT_RX_DONE) {
        /* Get last anchor time */
        t_last_anchor = NRF_TIMER0->CC[TIMER_CHANNEL_CONNECTION_INTERVAL];

        /* Scheudle the next connection interval */
        NRF_TIMER0->CC[TIMER_CHANNEL_CONNECTION_INTERVAL] =
                t_last_anchor + (_interval * 1250);

        if (!_conn_established) {
            /* We've received the first packet from the slave, as such we can
               consider the connection established, regardless of the CRC */
            _conn_established = true;

            /* Notify host */
            _ll_dispatch_connection_event(LL_CONNECTION_EVENT_CONNECTION_ESTABLISHED);
        }

        if (radio_crc_match()) {
            /* The radio CRC matches, as such we can reset the link layer
               supervision timeout */
            timer_update(
                    TIMER_CHANNEL_SUPERVISION_TIMEOUT,
                    t_last_anchor + _supervision_timeout * 10000);

            /* Handle the received PDU */
            _ll_handle_rx_pdu();
        }

        /* Configure radio for PPI triggering */
        radio_interleaved_tx_rx_ppi();

        /* Setup data channel */
        ll_set_next_data_channel();
        radio_setup_data(ll_get_data_channel_index(), _access_address, _crc_init);

        /* Setup radio PDU */
        _ll_prepare_tx_pdu();
        radio_set_pdu(&_tx_pdu);

        /* Setup radio event handler */
        radio_set_event_handler(&_ll_handle_master_packet_sent);
    }
}

/**
 * @brief       Handles the case when the master has sent a packet
 * @param[in]   event
 *              The radio event to be handled.
 */
static void _ll_handle_master_packet_sent(enum radio_event event) {
    if (event == RADIO_EVENT_TX_DONE) {
        /* Radio is already preparing for receiving, prepare receive PDU */
        radio_set_pdu(&_rx_pdu);
        radio_set_event_handler(&_ll_handle_master_packet_received);

        // Prepare receive timeout
        NRF_RADIO->EVENTS_ADDRESS = 0;
        timer_configure(
            TIMER_CHANNEL_RECEIVE_TIMEOUT,
            &_ll_handle_master_rx_timeout,
            NRF_TIMER0->CC[TIMER_CHANNEL_CAPTURE_END]
                + RADIO_TIFS
                + RADIO_TIFS_JITTER
                + ((1 + 4) * 8));
    }
}

/**
 * @brief       Handles the case when the slave has sent a packet and the
 *              connection event was closed, configuring the radio for the
 *              next data channel
 */
static void _ll_handle_slave_packet_sent(enum radio_event event) {
    if (event == RADIO_EVENT_TX_DONE) {
        /* Configure radio for PPI triggering */
        radio_interleaved_rx_tx_ppi();

        /* Setup data channel */
        ll_set_next_data_channel();
        radio_setup_data(ll_get_data_channel_index(), _access_address, _crc_init);

        /* Setup radio PDU */
        radio_set_pdu(&_rx_pdu);

        /* Setup radio event handler */
        radio_set_event_handler(&_ll_handle_slave_packet_received);
    }
}

/**
 * @brief       Handles the case when the slave has received a packet
 * @param[in]   event
 *              The radio event to be handled.
 */
static void _ll_handle_slave_packet_received(enum radio_event event) {
    uint32_t t_slave_expected_anchor;
    uint32_t t_window_widening;

    if (event == RADIO_EVENT_RX_STARTED) {
        /* Cancel receive timeout */
        timer_cancel(TIMER_CHANNEL_RECEIVE_TIMEOUT);
    }

    if (event == RADIO_EVENT_RX_DONE) {
        /* Compute the last anchor time */
        t_last_anchor = NRF_TIMER0->CC[TIMER_CHANNEL_CAPTURE_ADDRESS] - ((1 + 4) * 8);

        /* Compute the time at which we expect another packet from the master */
        t_slave_expected_anchor = t_last_anchor + (_interval * 1250);
        t_last_slave_expected_anchor = t_slave_expected_anchor;

        /* Compute window widening */
        t_window_widening = _window_widening(t_slave_expected_anchor);

        /* Shedule the next receive window */
        NRF_TIMER0->CC[TIMER_CHANNEL_CONNECTION_INTERVAL]
            = t_slave_expected_anchor
            - RADIO_RAMP_TIME_RX
            - RADIO_TIFS_JITTER
            - t_window_widening;

        /* Schedule next receive timeout */
        timer_configure(
            TIMER_CHANNEL_RECEIVE_TIMEOUT,
            &_ll_handle_slave_rx_timeout,
            t_slave_expected_anchor
            + ((1 + 4) * 8)
            + RADIO_TIFS_JITTER
            + t_window_widening
            + LL_ISR_LATENCY);

        if (!_conn_established) {
            /* We've received the first packet from the master, as such we
               can consider the connection established, regardless of the CRC */
            _conn_established = true;

            /* Notify host */
            _ll_dispatch_connection_event(LL_CONNECTION_EVENT_CONNECTION_ESTABLISHED);
        }

        if (radio_crc_match()) {
            /* The radio CRC matches, as such we can reset the link layer
               supervision timeout */
            timer_update(
                    TIMER_CHANNEL_SUPERVISION_TIMEOUT,
                    t_last_anchor + _supervision_timeout * 10000);

            /* Handle the received PDU */
            _ll_handle_rx_pdu();
        }

        /* Regardless of the the received CRC, we're always required to answer
           the master when it sends us a packet */
        _ll_prepare_tx_pdu();
        radio_set_pdu(&_tx_pdu);
        radio_set_event_handler(&_ll_handle_slave_packet_sent);
    }
}

/**
 * @brief       Attempts to establish a connection to the peer device
 * @details     Specifically, this function performs the scheduling of the
 *              first connection interval for both roles, and schedules the
 *              supervision timeout.
 * @param[in]   t_reference
 *              The reference time in microseconds that marks the end of
 *              a received/sent connect requests. All future times will be
 *              computed using this value as reference.
 */
static void _ll_establish_connection(uint32_t t_reference) {
    /* Compute time of the initial anchor point:
     *      When the connection state is entered, both devices must respect
     *      a time of 1.25 ms after the connection request is sent/received
     *      before the first connection event starts. The master may extend
     *      that time using the window offset parameter (which is a multiple
     *      of 1.25 ms)
     */
    uint32_t t_anchor_point = t_reference + ((1 + _window_offset) * 1250);

    /* Holds the window widening value */
    uint32_t t_window_widening;

    if (_role == LL_CONNECTION_ROLE_MASTER) {
        /* We are in the master role, meaning we send first; Since we also
           define a transmit window, we shall transmit right in the middle
           of it, giving enough time for the slave to receive the first packet */
        NRF_TIMER0->CC[TIMER_CHANNEL_CONNECTION_INTERVAL] =
                t_anchor_point - RADIO_RAMP_TIME_TX
                        + ((_window_size * 1250) / 2);

        /* Configure PPI channel to trigger the TXEN task when the timer
           reaches the configured time */
        ppi_channel_enable(
                PPI_CHANNEL_CONNECTION_INTERVAL,
                (uint32_t) (&NRF_TIMER0->EVENTS_COMPARE[TIMER_CHANNEL_CONNECTION_INTERVAL]),
                (uint32_t) (&NRF_RADIO->TASKS_TXEN));

        /* Configure PPI channel to capture the end of a transmitted packet,
           which we'll use to compute receive time-outs */
        ppi_channel_enable(
            PPI_CHANNEL_PACKET_SENT,
            (uint32_t) (&NRF_RADIO->EVENTS_END),
            (uint32_t) (&NRF_TIMER0->TASKS_CAPTURE[TIMER_CHANNEL_CAPTURE_END]));

        /* Configure radio for PPI triggering */
        radio_interleaved_tx_rx_ppi();

        /* Setup data channel */
        ll_set_next_data_channel();
        radio_setup_data(ll_get_data_channel_index(), _access_address, _crc_init);

        /* Setup radio PDU */
        _ll_prepare_tx_pdu();
        radio_set_pdu(&_tx_pdu);

        /* Setup radio event handler */
        radio_set_event_handler(&_ll_handle_master_packet_sent);

    } else {
        /* Compute window widening value */
        t_last_slave_expected_anchor = t_anchor_point;
        t_window_widening = _window_widening(t_anchor_point);

        /* Schedule the slave to wake-up before the anchor point */
        NRF_TIMER0->CC[TIMER_CHANNEL_CONNECTION_INTERVAL] =
            t_anchor_point
            - RADIO_RAMP_TIME_RX
            - RADIO_TIFS_JITTER
            - t_window_widening;

        /* Configure receive timeout to occur after the master's transmit
           window */
        timer_configure(
            TIMER_CHANNEL_RECEIVE_TIMEOUT,
            &_ll_handle_slave_rx_timeout,
            t_anchor_point
            + (_window_size * 1250)
            + ((1 + 4) * 8)
            + RADIO_TIFS_JITTER
            + t_window_widening
            + LL_ISR_LATENCY);

        /* Configure PPI channel to trigger the RXEN task when the timer
           reaches the configured time */
        ppi_channel_enable(
                PPI_CHANNEL_CONNECTION_INTERVAL,
                (uint32_t) (&NRF_TIMER0->EVENTS_COMPARE[TIMER_CHANNEL_CONNECTION_INTERVAL]),
                (uint32_t) (&NRF_RADIO->TASKS_RXEN));

        /* Configure PPI channel that will capture the current timer value
           for each packet when it receives a valid address */
        ppi_channel_enable(
                PPI_CHANNEL_ADDRESS_RECEIVED,
                (uint32_t) (&NRF_RADIO->EVENTS_ADDRESS),
                (uint32_t) (&NRF_TIMER0->TASKS_CAPTURE[TIMER_CHANNEL_CAPTURE_ADDRESS]));

        /* Configure radio for PPI triggering */
        radio_interleaved_rx_tx_ppi();

        /* Setup data channel */
        ll_set_next_data_channel();
        radio_setup_data(ll_get_data_channel_index(), _access_address, _crc_init);

        /* Setup radio PDU */
        radio_set_pdu(&_rx_pdu);

        /* Setup radio event handler */
        radio_set_event_handler(&_ll_handle_slave_packet_received);
    }

    /* Configure supervision timeout:
     *      Per specification, when a connection is not established, the
     *      supervision timeout shall be:
     *          t_timeout = 6 * connectionInterval
     *
     *      Note that the connection interval is a multiple of 1.25 ms.
     */
    timer_configure(
            TIMER_CHANNEL_SUPERVISION_TIMEOUT,
            &_ll_handle_supervision_timeout,
            t_reference + (12 * _interval * 1250));
}


/* -------------------------------------------------------------------------- */
/*          Public API                                                        */
/* -------------------------------------------------------------------------- */

void ll_connect(struct ll_pdu_adv pdu, uint32_t t_reference) {
    struct ll_pdu_payload_connect_req *payload;
    enum ll_state prev_state;

    /* Initialize event queue */
    _event_queue.head = _event_queue.tail = 0;

    /* Enable software interrupt */
    NVIC_ClearPendingIRQ(SWI1_IRQn);
    NVIC_SetPriority(SWI1_IRQn, DANDELION_IRQ_PRIORITY_LOW);
    NVIC_EnableIRQ(SWI1_IRQn);

    /* Initialize connection buffers */
    ll_buffer_init(&_tx_buffer);
    ll_buffer_init(&_rx_buffer);

    /* Inform host that a connection has been created */
    _ll_dispatch_connection_event(LL_CONNECTION_EVENT_CONNECTION_CREATED);

    /* Go into connection state */
    prev_state = ll_get_state();

    if (!ll_set_state(LL_STATE_CONNECTION)) {
        /* We were unable to enter the connection state */
        _ll_dispatch_connection_event(LL_CONNECTION_EVENT_CONNECTION_LOST);
        return;
    }

    /* Retrieve packet payload */
    payload = (struct ll_pdu_payload_connect_req *) pdu.payload;

    /* Compute the connection role */
    if (prev_state == LL_STATE_INITIATING) {
        /* We entered the connection state from the initiating state */
        _role = LL_CONNECTION_ROLE_MASTER;

        /* Set peer address */
        _peer_addr.type = BDADDR_TYPE_RANDOM;
        memcpy(_peer_addr.data, payload->adv_a, BDADDR_LEN);

    } else {
        /* We entered the connection state from the advertising state */
        _role = LL_CONNECTION_ROLE_SLAVE;

        /* Set peer address */
        _peer_addr.type = BDADDR_TYPE_RANDOM;
        memcpy(_peer_addr.data, payload->init_a, BDADDR_LEN);
    }

    if (pdu.pdu_type != LL_PDU_ADV_TYPE_CONNECT_REQ) {
        /* The PDU we were given is not a connect request */
        _ll_dispatch_connection_event(LL_CONNECTION_EVENT_CONNECTION_LOST);
        return;
    }

    /* Configure channel hopping sequence */
    if (!ll_configure_data_channel_hopping(payload->ch_m, payload->hop)) {
        /* The connect request has an invalid channel map or hop value */
        _ll_dispatch_connection_event(LL_CONNECTION_EVENT_CONNECTION_LOST);
        return;
    }

    /* Initialize remaining data */
    _access_address         = payload->aa;
    _crc_init               = payload->crc_init;
    _window_offset          = payload->win_offset;
    _window_size            = payload->win_size;
    _interval               = payload->interval;
    _supervision_timeout    = payload->timeout;
    _conn_established       = false;
    _transmit_seq_num       = 0;
    _next_expected_seq_num  = 0;
    t_last_anchor           = t_reference;

    /* Initialize transmit PDU to the empty PDU */
    _ll_init_empty_pdu(&_tx_pdu);

    /* Initialize header data for flow control with initial values */
    _tx_pdu.sn      = _transmit_seq_num;
    _tx_pdu.nesn    = _next_expected_seq_num;
    _tx_pdu.md      = 0;

    /* Always use worst-case value for sleep clock accuracy */
    switch ((enum ll_pdu_sca) payload->sca) {
    case LL_PDU_SCA_251_TO_500_PPM: accum_ppm = 500; break;
    case LL_PDU_SCA_151_TO_250_PPM: accum_ppm = 250; break;
    case LL_PDU_SCA_101_TO_150_PPM: accum_ppm = 150; break;
    case LL_PDU_SCA_076_TO_100_PPM: accum_ppm = 100; break;
    case LL_PDU_SCA_051_TO_075_PPM: accum_ppm =  75; break;
    case LL_PDU_SCA_031_TO_050_PPM: accum_ppm =  50; break;
    case LL_PDU_SCA_021_TO_030_PPM: accum_ppm =  30; break;
    case LL_PDU_SCA_000_TO_020_PPM: accum_ppm =  20; break;
    }

    /* Combine with this device's clock accuracy */
    accum_ppm += LL_CLOCK_SCA_PPM;

    /* Finally, establish the connection */
    _ll_establish_connection(t_reference);
}

enum ll_connection_role ll_connection_get_role(void) {
    return _role;
}

struct bdaddr ll_connection_peer_addr(void) {
    return _peer_addr;
}

void ll_set_connection_event_handler(
        void (*handler)(enum ll_connection_event)) {
    _handler = handler;
}

struct ll_buffer *ll_get_connection_tx_buffer(void) {
    return &_tx_buffer;
}

struct ll_buffer *ll_get_connection_rx_buffer(void) {
    return &_rx_buffer;
}

void ll_terminate(int8_t exit_code) {
    /* Create link layer control PDU */
    struct ll_pdu_data pdu;
    struct ll_pdu_payload_control *payload;

    /* Fill header */
    pdu.llid = LL_PDU_DATA_LLID_CONTROL;
    pdu.length = 2;

    /* Fill payload */
    payload = (struct ll_pdu_payload_control *) pdu.payload;

    payload->opcode = LL_PDU_CONTROL_OPCODE_TERMINATE_IND;
    payload->ctr_data[0] = (uint8_t) exit_code;

    /* Put payload in TX buffer */
    while (!ll_buffer_space_available(&_tx_buffer)) {
        /* Wait for space to be available */
        ;
    }

    /* Put */
    ll_buffer_put(&_tx_buffer, &pdu);
}
