/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     ble
 * @{
 *
 * @file        ll.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

/* Bluetooth stack */
#include "ble/ll.h"

/**
 * @brief       Holds this link layer's device address
 */
static const struct bdaddr *_addr;

void ll_init(const struct bdaddr *addr) {
    if (ll_get_state() == LL_STATE_UNKNOWN && addr != NULL) {
        /* Initialize hardware */
        radio_init();
        random_init();
        timer_init();

        /* Store device address */
        _addr = addr;

        /* Initialize the link layer state */
        ll_state_init();
    }
}

void ll_set_bdaddr(const struct bdaddr *addr) {
    if (addr != NULL) {
        _addr = addr;
    }
}

const struct bdaddr *ll_get_bdaddr(void) {
    return _addr;
}
