/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     ble
 * @{
 *
 * @file        timer.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#include "ble/timer.h"

/**
 * @brief       Holds the assigned timer actions
 */
static void (*volatile _actions[TIMER_MAX_CHANNELS])(void);


/* -------------------------------------------------------------------------- */
/*          Public API                                                        */
/* -------------------------------------------------------------------------- */


void timer_init(void) {
    /* Set timer mode */
    NRF_TIMER0->MODE = (TIMER_MODE_MODE_Timer << TIMER_MODE_MODE_Pos);

    /* Set 32-bit mode for compare registers */
    NRF_TIMER0->BITMODE =
            (TIMER_BITMODE_BITMODE_32Bit << TIMER_BITMODE_BITMODE_Pos);

    /* Set timer pre-scaler (1 tick = 1 us) */
    NRF_TIMER0->PRESCALER = 4;

    /* Enable timer interrupts on NVIC */
    NVIC_ClearPendingIRQ(TIMER0_IRQn);
    NVIC_SetPriority(TIMER0_IRQn, DANDELION_IRQ_PRIORITY_HIGH);
    NVIC_EnableIRQ(TIMER0_IRQn);
}

uint32_t timer_read(void) {
    /* Capture current timer value into channel (3) */
    NRF_TIMER0->TASKS_CAPTURE[3] = 1;

    /* Read channel (3) */
    return NRF_TIMER0->CC[3];
}

void timer_configure(uint32_t channel, void (*action)(void), uint32_t time) {
    assert(channel < TIMER_MAX_CHANNELS && action != NULL);

    /* Store action */
    _actions[channel] = action;

    /* Configure time */
    NRF_TIMER0->CC[channel] = time;

    /* Enable interrupt */
    NRF_TIMER0->EVENTS_COMPARE[channel] = 0;
    NRF_TIMER0->INTENSET = (TIMER_INTENSET_COMPARE0_Msk << channel);
}

void timer_cancel(uint32_t channel) {
    /* Disable interrupt */
    NRF_TIMER0->INTENCLR = (TIMER_INTENCLR_COMPARE0_Msk << channel);

    /* Clear callback */
    _actions[channel] = NULL;
}

void timer_update(uint32_t channel, uint32_t time) {
    assert(channel < TIMER_MAX_CHANNELS);

    /* Update time */
    NRF_TIMER0->CC[channel] = time;
}

uint32_t timer_remaining_time(uint32_t channel) {
    assert(channel < TIMER_MAX_CHANNELS);

    return NRF_TIMER0->CC[channel] - timer_read();
}

void timer_start(void) {
    NRF_TIMER0->TASKS_START = 1;
}

void timer_stop(void) {
    NRF_TIMER0->TASKS_STOP = 1;
}

void timer_clear(void) {
    NRF_TIMER0->TASKS_CLEAR = 1;
}

void isr_timer0(void) {
    uint32_t i;
    void (*action)(void) = NULL;

    for (i = 0; i < TIMER_MAX_CHANNELS; ++i) {
        if (NRF_TIMER0->EVENTS_COMPARE[i]) {
            action = _actions[i];

            if (action != NULL) {
                action();
            }

            /* Clear event */
            NRF_TIMER0->EVENTS_COMPARE[i] = 0;
        }
    }
}
