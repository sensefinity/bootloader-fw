/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     ble
 * @{
 *
 * @file        ll_control.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

/* Bluetooth stack */
#include "ble/ll_control.h"

/* -------------------------------------------------------------------------- */
/*          Public API                                                        */
/* -------------------------------------------------------------------------- */

void ll_control_termination_procedure(uint8_t error_code) {
    struct ll_pdu_data pdu;
    struct ll_pdu_payload_control *payload;

    /* Initialize header */
    pdu.llid    = LL_PDU_DATA_LLID_CONTROL;
    pdu.length  = 2;

    /* Initialize payload */
    payload = (struct ll_pdu_payload_control *) pdu.payload;

    payload->opcode         = LL_PDU_CONTROL_OPCODE_TERMINATE_IND;
    payload->ctr_data[0]    = error_code;

    /* Insert into transmit buffer */
    while (!ll_buffer_space_available(ll_get_connection_tx_buffer())) {
        /* Wait for available space */
        ;
    }

    /* Put PDU into the buffer */
    ll_buffer_put(ll_get_connection_tx_buffer(), &pdu);
}
