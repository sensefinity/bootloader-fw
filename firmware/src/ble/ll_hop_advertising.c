/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     ble
 * @{
 *
 * @file        ll_hop_advertising.c
 * @brief       Implementation of the hop sequence for advertising channels
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

/* Bluetooth stack */
#include "ble/ll.h"

/**
 * @brief       Holds the current advertising channel map
 */
static uint32_t _advertising_channel_map = LL_ADVERTISING_CHANNEL_MAP_ALL;

/**
 * @brief       Holds the number of enabled advertising channels
 */
static uint32_t _enabled_advertising_channel_count = 3;

/**
 * @brief       Holds the current advertising index
 */
static uint32_t _advertising_index = 37;


/* -------------------------------------------------------------------------- */
/*          Public API                                                        */
/* -------------------------------------------------------------------------- */


bool ll_set_advertising_channel_map(uint32_t map) {
    /* Filter out unwanted bits */
    map &= LL_ADVERTISING_CHANNEL_MAP_ALL;

    if (map == 0) {
        /* There are no enabled advertising channels */
        return 0;
    }

    _advertising_channel_map = map;

    /* Compute number of enabled advertising channels */
    _enabled_advertising_channel_count = 0;
    _enabled_advertising_channel_count += ((map >> 0) & 0x1);
    _enabled_advertising_channel_count += ((map >> 1) & 0x1);
    _enabled_advertising_channel_count += ((map >> 2) & 0x1);

    return true;
}

void ll_set_initial_advertising_channel(void) {
    if (_advertising_channel_map & LL_ADVERTISING_CHANNEL_MAP_37) {
        /* Start with advertising channel (37) */
        _advertising_index = 37;

    } else if (_advertising_channel_map & LL_ADVERTISING_CHANNEL_MAP_38) {
        /* Start with advertising channel (38) */
        _advertising_index = 38;

    } else if (_advertising_channel_map & LL_ADVERTISING_CHANNEL_MAP_39) {
        /* Start with advertising channel (39) */
        _advertising_index = 39;
    }
}

bool ll_set_next_advertising_channel(void) {
    if (_advertising_index == 37) {
        if (_advertising_channel_map & LL_ADVERTISING_CHANNEL_MAP_38) {
            /* Channel 38 is the next in the sequence */
            _advertising_index = 38;
            return true;

        } else if (_advertising_channel_map & LL_ADVERTISING_CHANNEL_MAP_39) {
            /* Channel 39 is the next in the sequence */
            _advertising_index = 39;
            return true;
        }

    } else if (_advertising_index == 38) {
        if (_advertising_channel_map & LL_ADVERTISING_CHANNEL_MAP_39) {
            /* Channel 39 is the next in the sequence */
            _advertising_index = 39;
            return true;
        }
    }

    return false;
}

uint32_t ll_get_advertising_channel_index(void) {
    return _advertising_index;
}

uint32_t ll_get_enabled_advertising_channel_count(void) {
    return _enabled_advertising_channel_count;
}
