/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     ble
 * @{
 *
 * @file        radio.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#include "ble/radio.h"

/**
 * @brief       Map of Bluetooth channel indexes to their respective frequencies
 *              offset from 2400 MHz
 */
static const uint32_t _CHANNEL_FREQUENCIES[] = {
     4,  6,  8, 10, 12, 14, 16, 18, 20, 22,
    24, 28, 30, 32, 34, 36, 38, 40, 42, 44,
    46, 48, 50, 52, 54, 56, 58, 60, 62, 64,
    66, 68, 70, 72, 74, 76, 78,  2, 26, 80
};

/**
 * @brief       Holds the assigned radio event handler
 */
static void (*volatile _handler)(enum radio_event) = NULL;

/**
 * @brief       Holds the base radio shortcuts
 */
static uint32_t _base_shorts = 0;

/**
 * @brief       Holds the deferred interrupt service routine to be called for
 *              each implemented radio mode
 */
static void (*_deferred_isr)(void) = NULL;


/* -------------------------------------------------------------------------- */
/*          Private API                                                       */
/* -------------------------------------------------------------------------- */

/**
 * @brief       Tests whether the radio is currently busy
 * @returns     True if the radio is busy or false, otherwise.
 */
static inline bool _radio_busy(void) {
    return NRF_RADIO->STATE != RADIO_STATE_STATE_Disabled;
}

/**
 * @brief       Tests whether the radio is currently ready
 * @returns     True if the radio is ready or false, otherwise.
 */
static inline bool _radio_ready(void) {
    return NRF_RADIO->STATE == RADIO_STATE_STATE_RxIdle
            || NRF_RADIO->STATE == RADIO_STATE_STATE_TxIdle;
}

/**
 * @brief       Triggers an event to the registered event handler
 * @param[in]   event
 *              The event to be triggered.
 */
static inline void _trigger_event(enum radio_event event) {
    void (*handler)(enum radio_event) = _handler;

    if (handler != NULL) {
        handler(event);
    }
}

/**
 * @brief       Deferred interrupt service routine that handles the interrupts
 *              for the single-shot transmit mode
 */
static void _deferred_isr_tx(void) {
    if (NRF_RADIO->EVENTS_DISABLED) {
        /* Clear interrupt */
        NRF_RADIO->INTENCLR = RADIO_INTENCLR_DISABLED_Msk;

        /* Trigger event */
        _trigger_event(RADIO_EVENT_TX_DONE);

        /* Clear event */
        NRF_RADIO->EVENTS_DISABLED = 0;
    }
}

/**
 * @brief       Deferred interrupt service routine that handles the interrupts
 *              for the single-shot receive mode
 */
static void _deferred_isr_rx(void) {
    if (NRF_RADIO->EVENTS_ADDRESS) {
        /* Trigger event */
        _trigger_event(RADIO_EVENT_RX_STARTED);

        /* Clear event */
        NRF_RADIO->EVENTS_ADDRESS = 0;
    }

    if (NRF_RADIO->EVENTS_DISABLED) {
        /* Clear interrupts */
        NRF_RADIO->INTENCLR =
                (RADIO_INTENCLR_ADDRESS_Msk | RADIO_INTENCLR_DISABLED_Msk);

        /* Trigger event */
        _trigger_event(RADIO_EVENT_RX_DONE);

        /* Clear event */
        NRF_RADIO->EVENTS_DISABLED = 0;
    }
}

/**
 * @brief       Deferred interrupt service routine that handles the interrupts
 *              for the continuous receive mode
 */
void _deferred_isr_continuous_rx(void) {
    if (NRF_RADIO->EVENTS_READY) {
        /* Trigger event */
        _trigger_event(RADIO_EVENT_RX_READY);

        /* Clear event */
        NRF_RADIO->EVENTS_READY = 0;
    }

    if (NRF_RADIO->EVENTS_ADDRESS) {
        /* Trigger event */
        _trigger_event(RADIO_EVENT_RX_STARTED);

        /* Clear event */
        NRF_RADIO->EVENTS_ADDRESS = 0;
    }

    if (NRF_RADIO->EVENTS_END) {
        if (_base_shorts != 0) {
            /* The radio is configured to read RSSI, but since the radio is
               never disabled, the RSSISTOP task is never triggered; We need
               to trigger it manually */
            NRF_RADIO->TASKS_RSSISTOP = 1;
        }

        /* Trigger event */
        _trigger_event(RADIO_EVENT_RX_DONE);

        /* Clear event */
        NRF_RADIO->EVENTS_END = 0;
    }
}

/**
 * @brief       Deferred interrupt service routine that handles the interrupts
 *              for the interleaved receive and transmit mode
 */
void _deferred_isr_interleaved_rx_tx(void) {
    if (NRF_RADIO->EVENTS_ADDRESS) {
        /* Clear interrupt */
        NRF_RADIO->INTENCLR = RADIO_INTENCLR_ADDRESS_Msk;

        /* Trigger event */
        _trigger_event(RADIO_EVENT_RX_STARTED);

        /* Clear event */
        NRF_RADIO->EVENTS_ADDRESS = 0;
    }

    if (NRF_RADIO->EVENTS_DISABLED) {
        if (NRF_RADIO->SHORTS & RADIO_SHORTS_DISABLED_TXEN_Msk) {
            /* Receiving has finished */
            NRF_RADIO->SHORTS &= ~RADIO_SHORTS_DISABLED_TXEN_Msk;

            /* Trigger event */
            _trigger_event(RADIO_EVENT_RX_DONE);

        } else {
            /* Clear interrupt */
            NRF_RADIO->INTENCLR = RADIO_INTENCLR_DISABLED_Msk;

            /* Transmitting has finished */
            _trigger_event(RADIO_EVENT_TX_DONE);
        }

        /* Clear event */
        NRF_RADIO->EVENTS_DISABLED = 0;
    }
}

/**
 * @brief       Deferred interrupt service routine that handles the interrupts
 *              for the interleaved transmit and receive mode
 */
void _deferred_isr_interleaved_tx_rx(void) {
    if (NRF_RADIO->EVENTS_ADDRESS) {
        /* Clear interrupt */
        NRF_RADIO->INTENCLR = RADIO_INTENCLR_ADDRESS_Msk;

        /* Trigger event */
        _trigger_event(RADIO_EVENT_RX_STARTED);

        /* Clear event */
        NRF_RADIO->EVENTS_ADDRESS = 0;
    }

    if (NRF_RADIO->EVENTS_DISABLED) {
        if (NRF_RADIO->SHORTS & RADIO_SHORTS_DISABLED_RXEN_Msk) {
            /* Transmitting has finished */
            NRF_RADIO->SHORTS &= ~RADIO_SHORTS_DISABLED_RXEN_Msk;

            /* Set TIFS */
            NRF_RADIO->TIFS = 0;

            /* Trigger interrupt on ADDRESS event */
            NRF_RADIO->EVENTS_ADDRESS = 0;
            NRF_RADIO->INTENSET = RADIO_INTENSET_ADDRESS_Msk;

            /* Trigger event */
            _trigger_event(RADIO_EVENT_TX_DONE);

        } else {
            /* Clear interrupt */
            NRF_RADIO->INTENCLR = RADIO_INTENCLR_DISABLED_Msk;

            /* Set TIFS */
            NRF_RADIO->TIFS = RADIO_TIFS;

            /* Receiving has finished */
            _trigger_event(RADIO_EVENT_RX_DONE);
        }

        /* Clear event */
        NRF_RADIO->EVENTS_DISABLED = 0;
    }
}


/* -------------------------------------------------------------------------- */
/*          Public API                                                        */
/* -------------------------------------------------------------------------- */


void radio_init(void) {
    if (_radio_busy()) {
        /* Radio is currently busy */
        return;
    }

    // Set radio for BLE 1Mbit mode
    NRF_RADIO->MODE = RADIO_MODE_MODE_Ble_1Mbit << RADIO_MODE_MODE_Pos;

    // Check if we need to override the radio trim values
    if (((NRF_FICR->OVERRIDEEN & FICR_OVERRIDEEN_BLE_1MBIT_Msk)
            >> FICR_OVERRIDEEN_BLE_1MBIT_Pos)
                    == FICR_OVERRIDEEN_BLE_1MBIT_Override) {
        // Set trim values and enable them
        NRF_RADIO->OVERRIDE0 = NRF_FICR->BLE_1MBIT[0];
        NRF_RADIO->OVERRIDE1 = NRF_FICR->BLE_1MBIT[1];
        NRF_RADIO->OVERRIDE2 = NRF_FICR->BLE_1MBIT[2];
        NRF_RADIO->OVERRIDE3 = NRF_FICR->BLE_1MBIT[3];
        NRF_RADIO->OVERRIDE4 = (NRF_FICR->BLE_1MBIT[4] |
                (RADIO_OVERRIDE4_ENABLE_Enabled << RADIO_OVERRIDE4_ENABLE_Pos));
    }

    /* As a simplification, we shall always receive or transmit on the
       radio's ADDR0 (even though it supports 8 addresses). This means that
       we need to change the access address, when the radio is not enabled */
    NRF_RADIO->TXADDRESS    = 0;
    NRF_RADIO->RXADDRESSES  = 1;

    /* Configure CRC (3 bytes, skip address) */
    NRF_RADIO->CRCCNF =
            (RADIO_CRCCNF_SKIPADDR_Skip << RADIO_CRCCNF_SKIPADDR_Pos)   |
            (RADIO_CRCCNF_LEN_Three     << RADIO_CRCCNF_LEN_Pos);
    NRF_RADIO->CRCPOLY = 0x0000065B;

    /* Packet structure:
     *      | 1 byte (S0) | 8 bits (Length) | 0 bits (S1)
     */
    NRF_RADIO->PCNF0 =
            (0 << RADIO_PCNF0_S1LEN_Pos)    |
            (8 << RADIO_PCNF0_LFLEN_Pos)    |
            (1 << RADIO_PCNF0_S0LEN_Pos);

    /* Other packet configurations:
     *      - Use little endian for S0, Length, S1 fields;
     *      - Use data whitening;
     *      - Do not include static data;
     *      - Use 4-byte access address;
     *      - Use the maximum payload size;
     */
    NRF_RADIO->PCNF1 =
            (RADIO_PCNF1_ENDIAN_Little      << RADIO_PCNF1_ENDIAN_Pos)      |
            (RADIO_PCNF1_WHITEEN_Enabled    << RADIO_PCNF1_WHITEEN_Pos)     |
            (0                              << RADIO_PCNF1_STATLEN_Pos)     |
            (3                              << RADIO_PCNF1_BALEN_Pos)       |
            (RADIO_PDU_PAYLOAD_SIZE         << RADIO_PCNF1_MAXLEN_Pos);

    /* Set TIFS */
    NRF_RADIO->TIFS = RADIO_TIFS;

    /* Set default transmit power */
    NRF_RADIO->TXPOWER =
            RADIO_TXPOWER_TXPOWER_Pos4dBm << RADIO_TXPOWER_TXPOWER_Pos;

    /* Enable peripheral interrupts */
    NVIC_ClearPendingIRQ(RADIO_IRQn);
    NVIC_SetPriority(RADIO_IRQn, DANDELION_IRQ_PRIORITY_HIGH);
    NVIC_EnableIRQ(RADIO_IRQn);
}

void radio_setup_advertising(uint32_t channel) {
    assert(channel < 40);

    if (_radio_busy()) {
        /* Radio is currently busy */
        return;
    }

    /* Setup maximum packet length */
    NRF_RADIO->PCNF1 &= ~RADIO_PCNF1_MAXLEN_Msk;
    NRF_RADIO->PCNF1 |=
            (LL_PDU_ADV_PAYLOAD_SIZE << RADIO_PCNF1_MAXLEN_Pos);

    /* Setup frequency */
    NRF_RADIO->FREQUENCY = _CHANNEL_FREQUENCIES[channel];

    /* Setup data whitening */
    NRF_RADIO->DATAWHITEIV = channel;

    /* Configure base address */
    NRF_RADIO->BASE0 = (RADIO_ADV_CHANNEL_AA << 8);

    /* Configure prefix */
    NRF_RADIO->PREFIX0 = (RADIO_ADV_CHANNEL_AA >> 24) & RADIO_PREFIX0_AP0_Msk;

    /* Configure CRC initialization value */
    NRF_RADIO->CRCINIT = RADIO_ADV_CHANNEL_CRC_INIT;
}

void radio_setup_data(uint32_t channel, uint32_t address, uint32_t crc_init) {
    assert(channel < 40);

    if (_radio_busy()) {
        /* Radio is currently busy */
        return;
    }

    /* Setup maximum packet length */
    NRF_RADIO->PCNF1 &= ~RADIO_PCNF1_MAXLEN_Msk;
    NRF_RADIO->PCNF1 |=
            ((LL_PDU_DATA_PAYLOAD_SIZE + LL_PDU_DATA_MIC_SIZE)
                    << RADIO_PCNF1_MAXLEN_Pos);

    /* Setup frequency */
    NRF_RADIO->FREQUENCY = _CHANNEL_FREQUENCIES[channel];

    /* Setup data whitening */
    NRF_RADIO->DATAWHITEIV = channel;

    /* Configure base address */
    NRF_RADIO->BASE0 = (address << 8);

    /* Configure prefix */
    NRF_RADIO->PREFIX0 = (address >> 24) & RADIO_PREFIX0_AP0_Msk;

    /* Configure CRC initialization value */
    NRF_RADIO->CRCINIT = crc_init;
}

void radio_set_pdu(void *pdu) {
    NRF_RADIO->PACKETPTR = (uint32_t) pdu;
}

void radio_tx(void) {
    while (_radio_busy()) {
        /* Actively wait while the radio is busy */
        ;
    }

    /* Setup shortcuts:
     *      READY_START
     *          When the radio is ready to transmit, automatically start.
     *      END_DISABLE
     *          When the radio finishes transmitting, automatically disable.
     */
    NRF_RADIO->SHORTS =
            (RADIO_SHORTS_READY_START_Enabled
                    << RADIO_SHORTS_READY_START_Pos)    |
            (RADIO_SHORTS_END_DISABLE_Enabled
                    << RADIO_SHORTS_END_DISABLE_Pos)    |
            _base_shorts;

    /* Trigger interrupt for the DISABLED event */
    NRF_RADIO->EVENTS_DISABLED = 0;
    NRF_RADIO->INTENSET = RADIO_INTENSET_DISABLED_Msk;

    /* Setup deferred interrupt service routine */
    _deferred_isr = &_deferred_isr_tx;

    /* Enable radio */
    NRF_RADIO->TASKS_TXEN = 1;
}

void radio_rx(void) {
    while (_radio_busy()) {
        /* Actively wait while the radio is busy */
        ;
    }

    /* Setup shortcuts:
     *      READY_START
     *          When the radio is ready to transmit, automatically start.
     *      END_DISABLE
     *          When the radio finishes transmitting, automatically disable.
     */
    NRF_RADIO->SHORTS =
            (RADIO_SHORTS_READY_START_Enabled
                    << RADIO_SHORTS_READY_START_Pos)    |
            (RADIO_SHORTS_END_DISABLE_Enabled
                    << RADIO_SHORTS_END_DISABLE_Pos)    |
            _base_shorts;

    /* Trigger interrupts for the ADDRESS and DISABLED events */
    NRF_RADIO->EVENTS_ADDRESS = 0;
    NRF_RADIO->EVENTS_DISABLED = 0;
    NRF_RADIO->INTENSET =
            (RADIO_INTENSET_ADDRESS_Msk | RADIO_INTENSET_DISABLED_Msk);

    /* Setup deferred interrupt service routine */
    _deferred_isr = &_deferred_isr_rx;

    /* Enable radio */
    NRF_RADIO->TASKS_RXEN = 1;
}

void radio_continuous_rx(void) {
    if (_radio_busy()) {
        /* Disable radio */
        radio_disable();
    }

    /* Set only the base shortcuts */
    NRF_RADIO->SHORTS = _base_shorts;

    /* Trigger interrupts for the READY, ADDRESS and END events */
    NRF_RADIO->EVENTS_READY = 0;
    NRF_RADIO->EVENTS_ADDRESS = 0;
    NRF_RADIO->EVENTS_END = 0;
    NRF_RADIO->INTENSET = (
            RADIO_INTENSET_READY_Msk    |
            RADIO_INTENSET_ADDRESS_Msk  |
            RADIO_INTENSET_END_Msk);

    /* Setup deferred interrupt service routine */
    _deferred_isr = &_deferred_isr_continuous_rx;

    /* Enable radio */
    NRF_RADIO->TASKS_RXEN = 1;
}

void radio_interleaved_rx_tx(void) {
    /* Perform setup of the interleaved Rx-TX mode */
    radio_interleaved_rx_tx_ppi();

    /* Trigger now */
    NRF_RADIO->TASKS_RXEN = 1;
}

void radio_interleaved_rx_tx_ppi(void) {
    if (_radio_busy()) {
        /* Disable radio */
        radio_disable();
    }

    /* Setup shortcuts:
     *      READY_START
     *          When the radio is ready to receive, automatically start.
     *      END_DISABLE
     *          When the radio finishes receiving, automatically disable.
     *      DISABLED_TXEN
     *          After the radio is disabled, enable it in transmit mode.
     */
    NRF_RADIO->SHORTS =
            (RADIO_SHORTS_READY_START_Enabled
                    << RADIO_SHORTS_READY_START_Pos)    |
            (RADIO_SHORTS_END_DISABLE_Enabled
                    << RADIO_SHORTS_END_DISABLE_Pos)    |
            (RADIO_SHORTS_DISABLED_TXEN_Enabled
                    << RADIO_SHORTS_DISABLED_TXEN_Pos)  |
            _base_shorts;

    /* Trigger interrupt on the ADDRESS and DISABLED events */
    NRF_RADIO->EVENTS_ADDRESS = 0;
    NRF_RADIO->EVENTS_DISABLED = 0;
    NRF_RADIO->INTENSET =
            (RADIO_INTENSET_ADDRESS_Msk | RADIO_INTENSET_DISABLED_Msk);

    /* Setup deferred interrupt service routine */
    _deferred_isr = &_deferred_isr_interleaved_rx_tx;
}

void radio_interleaved_tx_rx_ppi(void) {
    if (_radio_busy()) {
        /* Disable radio */
        radio_disable();
    }

    /* Setup shortcuts:
     *      READY_START
     *          When the radio is ready to transmit, automatically start.
     *      END_DISABLE
     *          When the radio finishes transmitting, automatically disable.
     *      DISABLED_RXEN
     *          After the radio is disabled, enable it in receive mode.
     */
    NRF_RADIO->SHORTS =
            (RADIO_SHORTS_READY_START_Enabled
                    << RADIO_SHORTS_READY_START_Pos)    |
            (RADIO_SHORTS_END_DISABLE_Enabled
                    << RADIO_SHORTS_END_DISABLE_Pos)    |
            (RADIO_SHORTS_DISABLED_TXEN_Enabled
                    << RADIO_SHORTS_DISABLED_RXEN_Pos)  |
            _base_shorts;

    /* Trigger interrupt on the DISABLED event */
    NRF_RADIO->EVENTS_DISABLED = 0;
    NRF_RADIO->INTENSET = RADIO_INTENSET_DISABLED_Msk;

    /* Setup deferred interrupt service routine */
    _deferred_isr = &_deferred_isr_interleaved_tx_rx;
}

void radio_start(void) {
    if (_radio_ready()) {
        /* Trigger manual start task */
        NRF_RADIO->TASKS_START = 1;
    }
}

void radio_disable(void) {
    /* Disable all radio interrupts */
    NVIC_DisableIRQ(RADIO_IRQn);

    /* Clear all radio interrupts */
    NRF_RADIO->INTENCLR = 0xFFFFFFFF;

    /* Clear all radio shortcuts */
    NRF_RADIO->SHORTS = 0;

    /* Disable radio */
    NRF_RADIO->TASKS_DISABLE = 1;

    /* Set TIFS */
    NRF_RADIO->TIFS = RADIO_TIFS;

    /* Make sure the radio is disabled */
    while (_radio_busy()) {
        ;
    }

    /* Clear radio events */
    NRF_RADIO->EVENTS_READY = 0;
    NRF_RADIO->EVENTS_ADDRESS = 0;
    NRF_RADIO->EVENTS_END = 0;
    NRF_RADIO->EVENTS_DISABLED = 0;

    /* Re-enable radio interrupts */
    NVIC_ClearPendingIRQ(RADIO_IRQn);
    NVIC_EnableIRQ(RADIO_IRQn);
}

void radio_set_rssi_enable(bool enable) {
    while (_radio_busy()) {
        /* We can't set the flag while the radio is busy */
        ;
    }

    if (enable) {
        /* Enable */
        _base_shorts |= (
                RADIO_SHORTS_ADDRESS_RSSISTART_Msk  |
                RADIO_SHORTS_DISABLED_RSSISTOP_Msk);

    } else {
        /* Disable */
        _base_shorts &= ~(
                RADIO_SHORTS_ADDRESS_RSSISTART_Msk  |
                RADIO_SHORTS_DISABLED_RSSISTOP_Msk);
    }
}

int32_t radio_read_rssi(void) {
    return -NRF_RADIO->RSSISAMPLE;
}

bool radio_crc_match(void) {
    return NRF_RADIO->CRCSTATUS;
}

void radio_set_event_handler(void (*handler)(enum radio_event)) {
    _handler = handler;
}

void isr_radio(void) {
    void (*deferred_isr)(void) = _deferred_isr;

    /* Invoke the deferred interrupt service routine */
    if (deferred_isr != NULL) {
        deferred_isr();
    }
}
