/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     ble
 * @{
 *
 * @file        ll_advertising.c
 * @brief       Implementation of the advertising state part of the link layer
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

/* Bluetooth stack */
#include "ble/ll.h"


/**
 * @brief       Helper macro that computes the minimum value
 */
#ifndef MIN
#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#endif

/**
 * @brief       Holds the timer channels to be used for scheduling
 */
#define TIMER_CHANNEL_ADVERTISING_PACKET    (0)
#define TIMER_CHANNEL_RECEIVE_TIMEOUT       (1)

/**
 * @brief       Holds the PPI channel that we'll use to measure the end time
 *              of a received connect request
 */
#define PPI_CHANNEL_CONNECT_REQ_END         (0)


/**
 * @brief       Holds the current scheduling indentifier.
 */
static sched_id_t _sched_id;

/**
 * @brief       Holds the link layer advertising parameters
 * @details     This structure is statically initialized with the default
 *              parameters provided in the Bluetooth Core Specification Version
 *              4.1 [Vol 2] 7.8.5 - LE Set Advertising Parameters Command.
 */
static struct ll_advertising_params _params = {
    .interval           = 0x0800,
    .type               = LL_ADVERTISING_TYPE_CONNECTABLE_UNDIRECTED,
    .direct_addr        = {
        .type           = BDADDR_TYPE_RANDOM,
        .data           = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }
    },
    .channel_map        = LL_ADVERTISING_CHANNEL_MAP_ALL
};

/**
 * @brief       Holds the advertising data
 */
static const uint8_t *_advertsing_data = NULL;

/**
 * @brief       Holds the size of the advertising data
 */
static size_t _advertising_data_size = 0;

/**
 * @brief       Holds the scan response data
 */
static const uint8_t *_scan_response_data = NULL;

/**
 * @brief       Holds the scan response data size
 */
static size_t _scan_response_data_size = 0;

/**
 * @brief       Holds the PDUs to be used for advertising
 */
static struct ll_pdu_adv _send_pdu;
static struct ll_pdu_adv _recv_req_pdu;
static struct ll_pdu_adv _send_rsp_pdu;

/**
 * @brief       Inform if we are sending the last packet of an advertising event
 */
bool last_packet_in_adv_event = false;


/* -------------------------------------------------------------------------- */
/*          Private API                                                       */
/* -------------------------------------------------------------------------- */

/**
 * @brief       Initializes the send PDU as a function of the desired type
 * @param[in]   type
 *              The type of PDU; Only LL_PDU_ADV_TYPE_ADV_* PDUs may be
 *              initialized.
 */
static void _ll_init_send_pdu(enum ll_pdu_adv_type type) {
    struct ll_pdu_payload_adv_ind *p0;
    struct ll_pdu_payload_adv_direct_ind *p1;

    switch (type) {
    case LL_PDU_ADV_TYPE_ADV_IND:
    case LL_PDU_ADV_TYPE_ADV_NONCONN_IND:
    case LL_PDU_ADV_TYPE_ADV_SCAN_IND:
        /* Payload is shared among all three types */
        _send_pdu.pdu_type  = type;
        _send_pdu.tx_add    = ll_get_bdaddr()->type;
        _send_pdu.rx_add    = 0;
        _send_pdu.length    = BDADDR_LEN + _advertising_data_size;

        /* Initialize payload */
        p0 = (struct ll_pdu_payload_adv_ind *) _send_pdu.payload;

        memcpy(p0->adv_a, ll_get_bdaddr()->data, BDADDR_LEN);
        memcpy(p0->adv_data, _advertsing_data, _advertising_data_size);

        break;

    case LL_PDU_ADV_TYPE_ADV_DIRECT_IND:
        /* Initialize header */
        _send_pdu.pdu_type  = type;
        _send_pdu.tx_add    = ll_get_bdaddr()->type;
        _send_pdu.rx_add    = _params.direct_addr.type;
        _send_pdu.length    = 2 * BDADDR_LEN;

        /* Initialize payload */
        p1 = (struct ll_pdu_payload_adv_direct_ind *) _send_pdu.payload;

        memcpy(p1->adv_a, ll_get_bdaddr()->data, BDADDR_LEN);
        memcpy(p1->init_a, _params.direct_addr.data, BDADDR_LEN);

        break;

    default:
        /* Cannot initialize other types */
        return;
    }
}

/**
 * @brief       Performs the initialization of the send response PDU
 * @details     The send response PDU is always a SCAN_RSP PDU.
 */
static void _ll_init_send_rsp_pdu(void) {
    /* Initialize header */
    _send_rsp_pdu.pdu_type  = LL_PDU_ADV_TYPE_SCAN_RSP;
    _send_rsp_pdu.tx_add    = ll_get_bdaddr()->type;
    _send_rsp_pdu.rx_add    = 0;
    _send_rsp_pdu.length    = BDADDR_LEN + _scan_response_data_size;

    /* Initialize payload */
    struct ll_pdu_payload_scan_rsp *payload =
            (struct ll_pdu_payload_scan_rsp *) _send_rsp_pdu.payload;

    memcpy(payload->adv_a, ll_get_bdaddr()->data, BDADDR_LEN);
    memcpy(payload->scan_rsp_data, _scan_response_data,
            _scan_response_data_size);
}

/**
 * @brief       Performs the initialization of the PDU bank
 * @details     The PDU bank is the bank of supporting PDUs for the advertising
 *              state. The PDUs are initialized as a function of the
 *              advertising type.
 */
static void _ll_init_pdu_bank(void) {
    switch (_params.type) {
    case LL_ADVERTISING_TYPE_CONNECTABLE_UNDIRECTED:
        _ll_init_send_pdu(LL_PDU_ADV_TYPE_ADV_IND);
        _ll_init_send_rsp_pdu();
        break;

    case LL_ADVERTISING_TYPE_CONNECTABLE_DIRECTED_LOW_DUTY:
    case LL_ADVERTISING_TYPE_CONNECTABLE_DIRECTED_HIGH_DUTY:
        _ll_init_send_pdu(LL_PDU_ADV_TYPE_ADV_DIRECT_IND);
        break;

    case LL_ADVERTISING_TYPE_NONCONNECTABLE_UNDIRECTED:
        _ll_init_send_pdu(LL_PDU_ADV_TYPE_ADV_NONCONN_IND);
        break;

    case LL_ADVERTISING_TYPE_SCANNABLE_UNDIRECTED:
        _ll_init_send_pdu(LL_PDU_ADV_TYPE_ADV_SCAN_IND);
        _ll_init_send_rsp_pdu();
        break;
    }
}

/**
 * @brief       Computes and returns a random advertising delay in microseconds
 * @details     The advertising delay is the ammount of time to be added to
 *              the advertising interval in order to perturb adverting events
 *              in time, reducing the probability of collisions.
 *              Per specification, this is a random time between 0 and 10 ms.
 */
static uint32_t _ll_random_advertising_delay(void) {
    return (10000 * random_next()) / 256;
}

/**
 * @brief       Computes and returns the optimal hop delay in microseconds
 * @details     The hop delay is ammount of time between the start of two
 *              consecutive advertising channel hops. Per specification, this
 *              time shall be a value less than or equal to 10 ms when using
 *              connectable undirected, connectable directed low duty cycle,
 *              non-connectable undirected or scannable undirected advertising.
 *              When using connectable directed high duty cycle advertising,
 *              this value shall be less than or equal to 3.75 ms.
 */
static uint32_t _ll_hop_delay(void) {
    uint32_t time_us = 0;

    switch (_params.type) {
    case LL_ADVERTISING_TYPE_CONNECTABLE_UNDIRECTED:
    case LL_ADVERTISING_TYPE_CONNECTABLE_DIRECTED_LOW_DUTY:
        /* These are referred to as connectable advertising modes, which have
           a lower limit of 20 ms for the advertising interval. Since we want
           to be able to accomodate all advertising intervals easily, we shall
           fix the time between packets to be 5 ms */
        time_us = 5000;
        break;
    case LL_ADVERTISING_TYPE_NONCONNECTABLE_UNDIRECTED:
    case LL_ADVERTISING_TYPE_SCANNABLE_UNDIRECTED:
        /* These are referred to as non-connectable advertising modes, which
           have a lower limit of 100 ms for the advertising interval. In order
           to maximize power savings, we'll use the minimum interval between
           consecutive packets( 1,3ms ), being the maximum value 10ms */
        time_us = 1300;
        break;

    case LL_ADVERTISING_TYPE_CONNECTABLE_DIRECTED_HIGH_DUTY:
        /* In high duty cycle mode, the time between two consecutive packets
           in the same advertising channel shall be <= 3.75 ms. As such, the
           time between two consecutive advertising packets shall be a function
           of the number of enabled channels */
        time_us = 3750 / ll_get_enabled_advertising_channel_count();
        break;
    }

    return time_us;
}

/**
 * @brief       Prototype for the function that internally enables the
 *              advertising state
 */
static void _ll_advertise_enable(void);

/**
 * @brief       Prototype for the function that internally disables the
 *              advertising state
 */
static void _ll_advertise_disable(void);

/**
 * @brief       Prototype for the function that sends an advertising packet and
 *              schedules the next one in the configured hop sequence
 */
static void _ll_send_advertising_packet(void);

/**
 * @brief       Prototype for the function that receive Radio event when an
 *              scan response packet has been sent
 */
static void _ll_handle_send_scan_response(enum radio_event event);

/**
 * @brief       Start a new advertising event and schedule a new one using
 *              RTC schedule to save power
 */
static void _ll_handle_advertising_event_interval(void) {

    last_packet_in_adv_event = false;
    /* Start HFClock  */
    clock_hfclk_start();
    /* Schedule first advertising packet of the advertising interval */
    timer_stop();
    timer_clear();
    timer_configure(
            TIMER_CHANNEL_ADVERTISING_PACKET,
            &_ll_send_advertising_packet,
            1);

    /* Start timer */
    timer_start();
}

/**
 * @brief
 */
static void _ll_advertising_event_stop(void) {

    /* Cancel configured timers */
    timer_cancel(TIMER_CHANNEL_RECEIVE_TIMEOUT);

    /* Disable radio */
    radio_disable();

    /* Stop timer */
    timer_stop();
    timer_clear();

    /* Stop HFClock */
    clock_hfclk_stop();

}

/**
 * @brief       Handles the receive timeout
 */
static void _ll_handle_receive_timeout(void) {

    /* Last packet of the advertising event? */
    if(last_packet_in_adv_event) {
        /* Disable radio */
        radio_disable();

        /* Delay stop event, TODO just works */
        timer_configure(
                TIMER_CHANNEL_RECEIVE_TIMEOUT,
                &_ll_advertising_event_stop,
                timer_read() + 15);
    } else {
        /* Disable radio */
        radio_disable();

        /* Cancel receive timeout channel */
        timer_cancel(TIMER_CHANNEL_RECEIVE_TIMEOUT);
    }
}

/**
 * @brief       Handles a received scan request
 */
static void _ll_handle_scan_request(void) {
    struct ll_pdu_payload_scan_req *payload;

    if (_params.type == LL_ADVERTISING_TYPE_CONNECTABLE_UNDIRECTED
            || _params.type == LL_ADVERTISING_TYPE_SCANNABLE_UNDIRECTED) {
        /* We can handle the scan request, but we first need to check if
           the scan request is directed at us */
        payload = (struct ll_pdu_payload_scan_req *) _recv_req_pdu.payload;

        if (memcmp(payload->adv_a, ll_get_bdaddr()->data, BDADDR_LEN) == 0
                && _recv_req_pdu.rx_add == ll_get_bdaddr()->type) {
            /* The scan request is directed at us */
            radio_set_pdu(&_send_rsp_pdu);

            /* Are we sending the last packet of an advertising event? */
            if(last_packet_in_adv_event) {
                radio_set_event_handler(&_ll_handle_send_scan_response);
            } else {
                /* Disable radio event handler, since we don't need to know when
                   the packet has been sent */
                radio_set_event_handler(NULL);
            }
        } else {
            /* The scan request is not directed at us */
            radio_disable();
            if(last_packet_in_adv_event) {
                _ll_advertising_event_stop();
            }
        }

    } else {
        /* Can't handle scan request */
        radio_disable();
        if(last_packet_in_adv_event) {
            _ll_advertising_event_stop();
        }
    }
}

/**
 * @brief       Handles a received connect request
 */
static void _ll_handle_connect_request(void) {
    struct ll_pdu_payload_connect_req *payload;

    uint32_t t_reference = NRF_TIMER0->CC[2];

    /* Disable radio */
    radio_disable();

    if (_params.type == LL_ADVERTISING_TYPE_CONNECTABLE_UNDIRECTED
            || _params.type == LL_ADVERTISING_TYPE_CONNECTABLE_DIRECTED_LOW_DUTY
            || _params.type == LL_ADVERTISING_TYPE_CONNECTABLE_DIRECTED_HIGH_DUTY) {
        /* We're using connectable advertising; As such, we need to check if
           the received connect request is directed at us */
        payload = (struct ll_pdu_payload_connect_req *) _recv_req_pdu.payload;

        if (memcmp(payload->adv_a, ll_get_bdaddr()->data, BDADDR_LEN) == 0
                && _recv_req_pdu.rx_add == ll_get_bdaddr()->type) {
            /* The connect request is directed at us; We need to accept the
               the connection */
            timer_cancel(TIMER_CHANNEL_ADVERTISING_PACKET);
            timer_cancel(TIMER_CHANNEL_RECEIVE_TIMEOUT);

            /* Disable PPI channel */
            ppi_channel_disable(PPI_CHANNEL_CONNECT_REQ_END);

            /* Connect to peer */
            ll_connect(_recv_req_pdu, t_reference);
        }
    }
}

/**
 * @brief       Radio event handler function that handles the case when a
 *              request has been received as a consequence of the advertising
 *              state.
 * @param[in]   event
 *              The event to be handled.
 */
static void _ll_handle_request(enum radio_event event) {
    switch (event) {
    case RADIO_EVENT_RX_STARTED:
        /* Reception has started, so we can disable the receive timeout */
        timer_cancel(TIMER_CHANNEL_RECEIVE_TIMEOUT);
        break;

    case RADIO_EVENT_RX_DONE:
        /* Reception is done */
        if (!radio_crc_match()) {
            /* The CRC does not match, cancel */
            radio_disable();
            if(last_packet_in_adv_event) {
                _ll_advertising_event_stop();
            }
            break;
        }

        switch (_recv_req_pdu.pdu_type) {
        case LL_PDU_ADV_TYPE_SCAN_REQ:
            _ll_handle_scan_request();
            break;

        case LL_PDU_ADV_TYPE_CONNECT_REQ:
            _ll_handle_connect_request();
            break;

        default:
            /* We can't handle the received PDU */
            radio_disable();
            if(last_packet_in_adv_event) {
                _ll_advertising_event_stop();
            }
            break;
        }

        break;

    default:
        /* We don't handle other events */
        return;
    }
}

/**
 * @brief       Radio event handler function that handles the case when an
 *              advertising packet has been sent
 * @param[in]   event
 *              The generated radio event.
 */
static void _ll_advertising_packet_sent(enum radio_event event) {
    if (event != RADIO_EVENT_TX_DONE) {
        /* Sanity check */
        return;
    }

    if (_params.type == LL_ADVERTISING_TYPE_NONCONNECTABLE_UNDIRECTED) {
        /* We're using non-connectable advertising, so we can exit */
        return;
    }

    /* We not using non-connectable advertising; That means we need to open
       up the radio for receiving requests */
    radio_interleaved_rx_tx();

    /* Setup radio receive timeout */
    timer_configure(
            TIMER_CHANNEL_RECEIVE_TIMEOUT,
            &_ll_handle_receive_timeout,
            timer_read() + RADIO_TIFS + 50);

    /* Configure radio PDU */
    radio_set_pdu(&_recv_req_pdu);

    /* Configure radio handler */
    radio_set_event_handler(&_ll_handle_request);
}


/**
 * @brief       Radio event handler function that handles the case when an
 *              scan response packet has been sent
 * @param[in]   event
 *              The generated radio event.
 */
static void _ll_handle_send_scan_response(enum radio_event event) {
    if (event != RADIO_EVENT_TX_DONE) {
        /* Sanity check */
        return;
    }

    if(last_packet_in_adv_event) {
        /* Close advertising event, back to power save mode */
        _ll_advertising_event_stop();
    }
}


/**
 * @brief       Sends an advertising packet and schedules the next one in
 *              the configured hop sequence
 */
static void _ll_send_advertising_packet(void) {
    uint32_t delay;

    /* Setup advertising channel */
    radio_setup_advertising(ll_get_advertising_channel_index());

    if (ll_set_next_advertising_channel()) {
        /* Hop sequence is still valid, schedule next packet */
        delay = _ll_hop_delay();

    } else {
        /* Reset advertising channel index */
        ll_set_initial_advertising_channel();

        if (_params.type == LL_ADVERTISING_TYPE_CONNECTABLE_DIRECTED_HIGH_DUTY) {
            /* The next interval is equal to the hop delay, although we need
               to check whether we have to exit advertising mode */
            if (timer_read() > (_params.interval * 625 - _ll_hop_delay())) {
                /* We need to exit advertising state */
                ll_set_advertise_enable(false);
                return;
            }

            /* We didn't exit advertising state */
            delay = _ll_hop_delay();

        } else {
            /* Send last advertising packet */
            last_packet_in_adv_event = true;

            /* Transmit */
            radio_set_pdu(&_send_pdu);
            radio_set_event_handler(&_ll_advertising_packet_sent);
            radio_tx();

            /*_ll_advertising_event_stop();*/

            /* Schedule next advertising event */
            /* Initial value is divided by 1000, because the scheduler
                work on milliseconds, not microseconds like the timer */
            _sched_id = sched_timeout(&_ll_handle_advertising_event_interval,
                ((_params.interval * 625 + _ll_random_advertising_delay())
                    - ((ll_get_enabled_advertising_channel_count() - 1)
                        * _ll_hop_delay() ) ) /1000 );

            /* Go sleep till next event */
            return;
        }
    }

    /* Schedule next packet */
    timer_update(
            TIMER_CHANNEL_ADVERTISING_PACKET,
            timer_read() + delay);

    /* Transmit */
    radio_set_pdu(&_send_pdu);
    radio_set_event_handler(&_ll_advertising_packet_sent);
    radio_tx();
}

/**
 * @brief       Enables the advertising state
 */
static void _ll_advertise_enable(void) {
    /* Initialize PDU bank */
    _ll_init_pdu_bank();

    /* Set advertising channel map */
    ll_set_advertising_channel_map(_params.channel_map);

    /* Initialize the advertising channel index */
    ll_set_initial_advertising_channel();

    /* Setup PPI to save connection request packets reference timing */
    ppi_channel_enable(
            PPI_CHANNEL_CONNECT_REQ_END,
            (uint32_t) (&NRF_RADIO->EVENTS_END),
            (uint32_t) (&NRF_TIMER0->TASKS_CAPTURE[2]));

    /* Start first advertising interval */
    _ll_handle_advertising_event_interval();
}

/**
 * @brief       Disasbles the advertising state
 */
static void _ll_advertise_disable(void) {
    /* Stop timer */
    timer_stop();

    /* Cancel RTC scheduler */
    sched_cancel(_sched_id);

    /* Disable radio (in case it is currently enabled) */
    radio_disable();

    /* Cancel configured timers */
    timer_cancel(TIMER_CHANNEL_ADVERTISING_PACKET);
    timer_cancel(TIMER_CHANNEL_RECEIVE_TIMEOUT);

    /* Disable PPI channel */
    ppi_channel_disable(PPI_CHANNEL_CONNECT_REQ_END);
}

/* -------------------------------------------------------------------------- */
/*          Public API                                                        */
/* -------------------------------------------------------------------------- */

void ll_set_advertising_data(const uint8_t *data, size_t size) {
    _advertsing_data = data;

    if (data == NULL) {
        _advertising_data_size = 0;
    } else {
        _advertising_data_size = MIN(size, LL_ADVERTISING_DATA_SIZE);
    }
}

void ll_set_scan_response_data(const uint8_t *data, size_t size) {
    _scan_response_data = data;

    if (data == NULL) {
        _scan_response_data_size = 0;
    } else {
        _scan_response_data_size = MIN(size, LL_SCAN_RESPONSE_DATA_SIZE);
    }
}

bool ll_set_advertising_params(struct ll_advertising_params params) {
    switch (params.type) {
    case LL_ADVERTISING_TYPE_CONNECTABLE_UNDIRECTED:
    case LL_ADVERTISING_TYPE_CONNECTABLE_DIRECTED_LOW_DUTY:
        if (params.interval < LL_ADVERTISING_INTERVAL_MIN
                || params.interval > LL_ADVERTISING_INTERVAL_MAX) {
            /* Advertising interval is invalid */
            return false;
        }

        break;

    case LL_ADVERTISING_TYPE_CONNECTABLE_DIRECTED_HIGH_DUTY:
        /* Advertising interval has a fixed value (1.28s) */
        params.interval = 0x0800;

        break;

    case LL_ADVERTISING_TYPE_NONCONNECTABLE_UNDIRECTED:
    case LL_ADVERTISING_TYPE_SCANNABLE_UNDIRECTED:
        if (params.interval < LL_ADVERTISING_INTERVAL_NONCONN_MIN
                || params.interval > LL_ADVERTISING_INTERVAL_MAX) {
            /* Advertising interval is invalid */
            return false;
        }

        break;

    default:
        /* Do nothing */
        break;
    }

    /* Set advertising channel map */
    if (!ll_set_advertising_channel_map(_params.channel_map)) {
        /* Setting the channel map failed */
        return false;
    }

    /* Set new advertising parameters */
    _params = params;

    return true;
}

struct ll_advertising_params ll_get_advertising_params(void) {
    return _params;
}

bool ll_set_advertise_enable(bool enable) {
    if (enable) {
        if (!ll_set_state(LL_STATE_ADVERTISING)) {
            /* Can't perform state transition */
            return false;
        }

        /* Enable advertising */
        _ll_advertise_enable();

    } else {
        if (!ll_set_state(LL_STATE_STANDBY)) {
            /* Can't perform state transition */
            return false;
        }

        /* Disable advertising */
        _ll_advertise_disable();
    }

    return true;
}
