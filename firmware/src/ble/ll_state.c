/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     ble
 * @{
 *
 * @file        ll_state.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

/* Bluetooth stack */
#include "ble/ll_state.h"

/* Utilities */
#include "util/debug.h"

/**
 * @brief       Holds the current link layer state
 */
static enum ll_state _current_state = LL_STATE_UNKNOWN;

void ll_state_init(void) {
    if (_current_state == LL_STATE_UNKNOWN) {
        /* Initialize the standby state */
        _current_state = LL_STATE_STANDBY;
    }
}

bool ll_set_state(enum ll_state state) {
    /* Holds whether the proposed transition is valid */
    bool valid;

    switch (state) {
    case LL_STATE_STANDBY:
        /* Transition from any state to standby state */
        valid = _current_state != LL_STATE_UNKNOWN;
        break;

    case LL_STATE_ADVERTISING:
    case LL_STATE_SCANNING:
    case LL_STATE_INITIATING:
        /* Transition from standby to advertising, scanning or initiating */
        valid = _current_state == LL_STATE_STANDBY;
        break;

    case LL_STATE_CONNECTION:
        /* Transition from advertising or initiating to connection */
        valid =
                _current_state == LL_STATE_ADVERTISING  ||
                _current_state == LL_STATE_INITIATING;
        break;

    default:
        /* Unknown state */
        valid = false;
    };

    if (valid) {
        /* Perform state transition */
        _current_state = state;

        if (state == LL_STATE_STANDBY || state == LL_STATE_ADVERTISING) {
            /* We went to standby or advertising states; As such we don't need
                that much clock precision anymore */
            clock_hfclk_stop();
        } else {
            /* We went from standby to another "active state", set high
               as such, we need high precision */
            clock_hfclk_start();
        }

        return true;
    }

    return false;
}

enum ll_state ll_get_state(void) {
    return _current_state;
}
