/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     ble
 * @{
 *
 * @file        ll_initiating.c
 * @brief       Implementation of the initiating state part of the link layer
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

/* Bluetooth stack */
#include "ble/ll.h"

/**
 * @brief       Holds the timer channels to be used for scheduling
 */
#define TIMER_CHANNEL_SCANNING_START        (0)
#define TIMER_CHANNEL_SCANNING_STOP         (1)

/**
 * @brief       Holds the current host channel classification
 */
static uint64_t _host_channel_classification = LL_DATA_CHANNEL_MAP_ALL;

/**
 * @brief       Holds the link layer connection parameters for the initiating
 *              state
 */
static struct ll_connection_params _params;

/**
 * @brief       Holds the PDU to be used to receive advertising packets
 */
static struct ll_pdu_adv _recv_pdu;

/**
 * @brief       Holds the connect request PDU to be sent to a peer device
 */
static struct ll_pdu_adv _connect_req;


/* -------------------------------------------------------------------------- */
/*          Private API                                                       */
/* -------------------------------------------------------------------------- */

/**
 * @brief       Checks whether a value is a power of two
 * @param[in]   value
 *              The value to be checked.
 * @returns     True if the value is a power of two or false, otherwise.
 */
static bool _ll_is_pow2(uint32_t value) {
    while ((value & 1) == 0 && value > 1) {
        /* While the value is even and greater than (1) */
        value >>= 1;
    }

    return value == 1;
}

/**
 * @brief       Generates a random access address
 * @details     The random access address follows the rules described in the
 *              Bluetooth Core Specification Version 4.1 [Vol 6] 2.1.2 Access
 *              Address and these rules are:
 *                  * No more than six consecutive zeros or ones;
 *                  * Cannot be equal to the advertising channel access address
 *                    and must differ by more than one bit;
 *                  * All four octets cannot be equal;
 *                  * No more than 24 transitions;
 *                  * Minimum of two transitions in the most significant six
 *                    bits;
 */
static uint32_t _ll_generate_random_access_address(void) {
    /* Holds the generated octets */
    uint32_t octets[4];

    /* Holds the generated access address */
    uint32_t address;

    do {
        /* Generate four new octets */
        octets[0] = (uint32_t) random_generate();
        octets[1] = (uint32_t) random_generate();
        octets[2] = (uint32_t) random_generate();
        octets[3] = (uint32_t) random_generate();

        if (octets[0] == octets[1] && octets[0] == octets[2]
                && octets[0] == octets[3]) {
            /* All octets are equal */
            continue;
        }

        /* Pack octets into an access address */
        address = 0;
        address |= (octets[0] << 0);
        address |= (octets[1] << 8);
        address |= (octets[2] << 16);
        address |= (octets[3] << 24);

        /* The random generator is configured with digital error correction
           which removes any bias towards '0' or '1'. This means that we can
           trust the random number generator to be able to generate access
           addresses compatible with with the rules for consecutive zeros or
           ones and minimum/maximum number of transitions */

        if (address == RADIO_ADV_CHANNEL_AA) {
            /* The address was equal to advertising channel address */
            continue;
        }

        if (_ll_is_pow2(address ^ RADIO_ADV_CHANNEL_AA)) {
            /* The address differs by a single bit from the advertising channel
               address */
            continue;
        }

        /* All rules check out */
        break;

    } while (true);

    return address;
}

/**
 * @brief       Generates a random 24-bit CRC initialization value
 * @returns     A new random 24-bit CRC initialization value.
 */
static uint32_t _ll_generate_random_crc_init(void) {
    uint32_t crc_init = 0;

    crc_init |= (((uint32_t) (random_generate())) << 0);
    crc_init |= (((uint32_t) (random_generate())) << 8);
    crc_init |= (((uint32_t) (random_generate())) << 16);

    return crc_init;
}

/**
 * @brief       Generates a random hop value
 * @details     Per specification, the hop value is a random value between
 *              5 and 16.
 * @returns     A new random hop value.
 */
static uint32_t _ll_generate_random_hop(void) {
    return 5 + (random_generate() % (16 - 5 + 1));
}

/**
 * @brief       Initializes the connect request
 * @details     The connect request is the packet that will be sent to a peer
 *              device in order to create a connection.
 */
static void _ll_init_connect_req(void) {
    struct ll_pdu_payload_connect_req *payload;

    /* Initialize header */
    _connect_req.pdu_type   = LL_PDU_ADV_TYPE_CONNECT_REQ;
    _connect_req.tx_add     = ll_get_bdaddr()->type;
    _connect_req.rx_add     = _params.peer_addr.type;
    _connect_req.length     = sizeof(struct ll_pdu_payload_connect_req);

    /* Initialize payload */
    payload = (struct ll_pdu_payload_connect_req *) _connect_req.payload;

    memcpy(payload->init_a, ll_get_bdaddr()->data, BDADDR_LEN);
    memcpy(payload->adv_a, _params.peer_addr.data, BDADDR_LEN);

    /* Fill payload data */
    payload->aa         = _ll_generate_random_access_address();
    payload->crc_init   = _ll_generate_random_crc_init();
    payload->win_size   = 1;    /* 1.25 ms */
    payload->win_offset = 0;    /*    0 ms */
    payload->interval   = _params.conn_interval;
    payload->latency    = _params.conn_latency;
    payload->timeout    = _params.supervision_timeout;
    payload->ch_m       = _host_channel_classification;
    payload->hop        = _ll_generate_random_hop();
    payload->sca        = LL_CLOCK_SCA;
}

/**
 * @brief       Handles the transmission of the connect request
 * @details     Also handles the transition into connection state.
 * @param[in]   event
 *              The event to be handled.
 */
static void _ll_connect_request_sent(enum radio_event event) {
    /* Holds the timestamp of the radio event */
    uint32_t timestamp = timer_read();

    if (event == RADIO_EVENT_TX_DONE) {
        /* Cancel the timers that may be in use for the initiating state */
        timer_cancel(TIMER_CHANNEL_SCANNING_START);
        timer_cancel(TIMER_CHANNEL_SCANNING_STOP);

        /* Connect to peer */
        ll_connect(_connect_req, timestamp);
    }
}

/**
 * @brief       Tests whether the received packet contains connectable
 *              advertising with the peer we're searching for
 */
static bool _ll_is_peer_device_found(void) {
    /* Payload pointers to be used for handling of connectable advertising
       packets */
    struct ll_pdu_payload_adv_ind *p0;
    struct ll_pdu_payload_adv_direct_ind *p1;

    if (_recv_pdu.pdu_type == LL_PDU_ADV_TYPE_ADV_IND) {
        /* The peer is using connectable undirected advertising; Verify
           if the peer address matches the peer we want to connect to */
        p0 = (struct ll_pdu_payload_adv_ind *) _recv_pdu.payload;

        if (memcmp(p0->adv_a, _params.peer_addr.data, BDADDR_LEN) == 0
                && _recv_pdu.tx_add == _params.peer_addr.type) {
            /* This is the device we're looking for */
            return true;
        }

    } else if (_recv_pdu.pdu_type == LL_PDU_ADV_TYPE_ADV_DIRECT_IND) {
        /* The peer is using connectable directed advertising; Verify if the
           peer address matches the peer we want to connect to and if the
           peer wants to connect to us */
        p1 = (struct ll_pdu_payload_adv_direct_ind *) _recv_pdu.payload;

        if (memcmp(p1->adv_a, _params.peer_addr.data, BDADDR_LEN) == 0
                && _recv_pdu.tx_add == _params.peer_addr.type
                && memcmp(p1->init_a, ll_get_bdaddr()->data, BDADDR_LEN) == 0
                && _recv_pdu.rx_add == ll_get_bdaddr()->type) {
            /* This is the device we're looking for */
            return true;
        }
    }

    return false;
}

/**
 * @brief       Handles the reception of an advertising packet
 * @param[in]   event
 *              The radio event to be handled.
 */
static void _ll_advertising_packet_received(enum radio_event event) {
    if (event == RADIO_EVENT_RX_DONE) {
        if (radio_crc_match()) {
            /* The CRC matches */
            if (_ll_is_peer_device_found()) {
                /* We found our peer, send connect request */
                radio_set_pdu(&_connect_req);
                radio_set_event_handler(&_ll_connect_request_sent);

            } else {
                /* We didn't find our peer yet, restart radio */
                radio_disable();
                radio_interleaved_rx_tx();
            }

        } else {
            /* The CRC does not match, restart radio */
            radio_disable();
            radio_interleaved_rx_tx();
        }
    }
}

/**
 * @brief       Handles the end of a scanning window
 */
static void _ll_handle_scanning_window(void) {
    /* Disable radio */
    radio_disable();

    /* Cancel scanning stop channel */
    timer_cancel(TIMER_CHANNEL_SCANNING_STOP);
}

/**
 * @brief       Starts a new scanning window
 * @details     Also schedules the next scanning window.
 */
static void _ll_scan_start(void) {
    /* Make sure radio is disabled */
    radio_disable();

    /* Setup advertising channel */
    radio_setup_advertising(ll_get_advertising_channel_index());

    if (!ll_set_next_advertising_channel()) {
        /* Setup next advertising channel */
        ll_set_initial_advertising_channel();
    }

    /* Set radio PDU */
    radio_set_pdu(&_recv_pdu);

    /* Set radio event handler */
    radio_set_event_handler(&_ll_advertising_packet_received);

    if (_params.scan_interval != _params.scan_window) {
        /* Since the interval is not equal to the window, the host is not
           using continuous scanning mode; As such, we need to schedule the
           end of the scanning window */
        timer_configure(
            TIMER_CHANNEL_SCANNING_STOP,
            &_ll_handle_scanning_window,
            timer_read() + _params.scan_window * 625);
    }

    /* Schedule the start of the next scanning interval */
    timer_update(
        TIMER_CHANNEL_SCANNING_START,
        timer_read() + _params.scan_interval * 625);

    /* Enable radio */
    radio_interleaved_rx_tx();
}

/**
 * @brief       Initiates the search for the peer device
 * @details     The search of the peer device performs passive scanning search
 *              for a connectable advertising packet coming from the peer
 *              device.
 */
static void _ll_search_peer(void) {
    /* Initialize the connect request to be sent to the peer */
    _ll_init_connect_req();

    /* Initialze the advertising channel hop sequence */
    ll_set_advertising_channel_map(_params.channel_map);
    ll_set_initial_advertising_channel();

    /* Schedule the start of the scanning window */
    timer_stop();
    timer_clear();
    timer_configure(
            TIMER_CHANNEL_SCANNING_START,
            &_ll_scan_start,
            1);

    timer_start();
}


/* -------------------------------------------------------------------------- */
/*          Public API                                                        */
/* -------------------------------------------------------------------------- */

void ll_set_host_channel_classification(uint64_t channel_map) {
    /* Remove unwanted bits (if present) */
    channel_map &= LL_DATA_CHANNEL_MAP_ALL;

    if (channel_map != 0) {
        /* Set new channel map */
        _host_channel_classification = channel_map;
    }
}

bool ll_create_connection(struct ll_connection_params params) {
    /* Validate scan parameters */
    if (params.scan_window > params.scan_interval) {
        /* The window is larger than the interval */
        return false;
    }

    if (params.scan_interval < LL_SCAN_INTERVAL_MIN
            || params.scan_interval > LL_SCAN_INTERVAL_MAX) {
        /* The scan interval is not within valid range */
        return false;
    }

    if (params.scan_window < LL_SCAN_WINDOW_MIN
            || params.scan_window > LL_SCAN_WINDOW_MAX) {
        /* The scan window is not within valid range */
        return false;
    }

    if (params.conn_interval < LL_CONN_INTERVAL_MIN
            || params.conn_interval > LL_CONN_INTERVAL_MAX) {
        /* The connection interval is not within valid range */
        return false;
    }

    if (params.conn_latency > LL_CONN_LATENCY_MAX) {
        /* The connection slave latency is not within valid range */
        return false;
    }

    if (params.supervision_timeout < LL_SUPERVISION_TIMEOUT_MIN
            || params.supervision_timeout > LL_SUPERVISION_TIMEOUT_MAX) {
        /* The supervision timeout is not within valid range */
        return false;
    }

    /* Filter out unwanted bits in the advertising channel map */
    params.channel_map &= LL_ADVERTISING_CHANNEL_MAP_ALL;

    if (params.channel_map == 0) {
        /* There are no enabled advertising channels */
        return false;
    }

    /* All parameters are valid */
    _params = params;

    if (!ll_set_state(LL_STATE_INITIATING)) {
        /* Can't perform state transition */
        return false;
    }

    /* Enable search for the peer device */
    _ll_search_peer();

    return true;
}

bool ll_create_connection_cancel(void) {
    /* Temporarily disable RADIO and TIMER0 interrupts */
    NVIC_DisableIRQ(RADIO_IRQn);
    NVIC_DisableIRQ(TIMER0_IRQn);

    if (ll_get_state() != LL_STATE_INITIATING) {
        /* We're not in the initiating state, re-enable disabled interrupts and
           return */
        NVIC_EnableIRQ(RADIO_IRQn);
        NVIC_EnableIRQ(TIMER0_IRQn);
        return false;
    }

    /* Cancel timers */
    timer_cancel(TIMER_CHANNEL_SCANNING_START);
    timer_cancel(TIMER_CHANNEL_SCANNING_STOP);

    /* Disable radio in case it was enabled */
    radio_disable();

    /* Set standby state */
    ll_set_state(LL_STATE_STANDBY);

    /* Re-enable disabled interrupts */
    NVIC_ClearPendingIRQ(RADIO_IRQn);
    NVIC_ClearPendingIRQ(TIMER0_IRQn);
    NVIC_EnableIRQ(RADIO_IRQn);
    NVIC_EnableIRQ(TIMER0_IRQn);

    return true;
}
