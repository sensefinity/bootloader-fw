/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     util
 * @{
 *
 * @file        columbus.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Rui Pires <rui.pires@sensefinity.com>
 *
 * @}
 */

#include "sensoroid/columbus.h"


/********************************** Public ***********************************/

void columbus_set_message_version(uint8_t *message, uint8_t version) {
    message[COLUMBUS_MESSAGE_VERSION] = version;
}

uint8_t columbus_get_message_version(uint8_t *message) {
    return message[COLUMBUS_MESSAGE_VERSION];
}

void columbus_set_serial(uint8_t *message, uint64_t serial) {
    int i;
    uint8_t *ptr = (uint8_t *) &serial;

    /* The serial number always starts @ offset = 1 */
    for (i = (sizeof(uint64_t) - 1); i >= 0; --i) {
        message[COLUMBUS_MESSAGE_DEVICE_SERIAL + i] = *ptr++;
    }
}

uint64_t columbus_get_serial(const uint8_t *message) {
    int i;
    uint64_t serial;
    uint8_t *ptr = (uint8_t *) &serial;

    /* The timestamp always starts @ offset = 1 */
    for (i = (sizeof(uint64_t) - 1); i >= 0; --i) {
        *ptr++ = message[COLUMBUS_MESSAGE_DEVICE_SERIAL + i];
    }

    return serial;
}

void columbus_set_timestamp(uint8_t *message, uint64_t timestamp) {
    int i;
    uint8_t *ptr = (uint8_t *) &timestamp;

    /* The timestamp always starts @ offset = 9 */
    for (i = (sizeof(uint64_t) - 1); i >= 0; --i) {
        message[COLUMBUS_MESSAGE_TIMESTAMP + i] = *ptr++;
    }
}

uint64_t columbus_get_timestamp(const uint8_t *message) {
    int i;
    uint64_t timestamp;
    uint8_t *ptr = (uint8_t *) &timestamp;

    /* The timestamp always starts @ offset = 9 */
    for (i = (sizeof(uint64_t) - 1); i >= 0; --i) {
        *ptr++ = message[COLUMBUS_MESSAGE_TIMESTAMP + i];
    }
    return timestamp;
}

void columbus_set_message_type(uint8_t *message, uint8_t type) {
    message[COLUMBUS_MESSAGE_TYPE] = type;
}

uint8_t columbus_get_message_type(uint8_t *message) {
    return message[COLUMBUS_MESSAGE_TYPE];
}

void columbus_set_data_size(uint8_t *message, uint32_t size) {
    int i;
    uint8_t *ptr = (uint8_t *) &size;

    /* The data size always starts @ offset = 18 */
    for (i = (sizeof(uint32_t) - 1); i >= 0; --i) {
        message[COLUMBUS_MESSAGE_DATA_SIZE + i] = *ptr++;
    }
}


/* Message direction */
void columbus_set_message_direction (uint8_t *message, uint8_t dir) {
	message[COLUMBUS_MESSAGE_DIRECTION] = dir;
}

uint8_t columbus_get_message_direction (uint8_t *message) {
	return message[COLUMBUS_MESSAGE_DIRECTION];
}


/* Measurement messages specific functions */

void columbus_set_measurement_sensor(uint8_t *message, uint8_t value) {
    message[COLUMBUS_MESSAGE_MEASUREMENT_SENSOR] = value;
}

void columbus_set_measurement_type(uint8_t *message, uint8_t value) {
    message[COLUMBUS_MESSAGE_MEASUREMENT_TYPE] = value;
}

void columbus_set_measurement_value(uint8_t *message, uint32_t value) {

    /* Save on message specific field */
    message[COLUMBUS_MESSAGE_MEASUREMENT_VALUE + 0] = (uint8_t) (value >> 24);
    message[COLUMBUS_MESSAGE_MEASUREMENT_VALUE + 1] = (uint8_t) (value >> 16);
    message[COLUMBUS_MESSAGE_MEASUREMENT_VALUE + 2] = (uint8_t) (value >>  8);
    message[COLUMBUS_MESSAGE_MEASUREMENT_VALUE + 3] = (uint8_t) (value >>  0);
}


void columbus_set_measurement_value_width(uint8_t *message, void * value, uint8_t width) {

    /* for little endian systems */
    switch(width) {
    case MEASURE_VALUE_2_BYTE:
        message[COLUMBUS_MESSAGE_MEASUREMENT_VALUE + 0] = (uint8_t) ((*(uint16_t *)value) >>  8);
        message[COLUMBUS_MESSAGE_MEASUREMENT_VALUE + 1] = (uint8_t) ((*(uint16_t *)value) >>  0);
        break;
    case MEASURE_VALUE_4_BYTE:
        message[COLUMBUS_MESSAGE_MEASUREMENT_VALUE + 0] = (uint8_t) ((*(uint32_t *)value) >> 24);
        message[COLUMBUS_MESSAGE_MEASUREMENT_VALUE + 1] = (uint8_t) ((*(uint32_t *)value) >> 16);
        message[COLUMBUS_MESSAGE_MEASUREMENT_VALUE + 2] = (uint8_t) ((*(uint32_t *)value) >>  8);
        message[COLUMBUS_MESSAGE_MEASUREMENT_VALUE + 3] = (uint8_t) ((*(uint32_t *)value) >>  0);
        break;
    case MEASURE_VALUE_8_BYTE:
        message[COLUMBUS_MESSAGE_MEASUREMENT_VALUE + 0] = (uint8_t) ((*(uint64_t *)value) >> 56);
        message[COLUMBUS_MESSAGE_MEASUREMENT_VALUE + 1] = (uint8_t) ((*(uint64_t *)value) >> 48);
        message[COLUMBUS_MESSAGE_MEASUREMENT_VALUE + 2] = (uint8_t) ((*(uint64_t *)value) >> 40);
        message[COLUMBUS_MESSAGE_MEASUREMENT_VALUE + 3] = (uint8_t) ((*(uint64_t *)value) >> 32);
        message[COLUMBUS_MESSAGE_MEASUREMENT_VALUE + 4] = (uint8_t) ((*(uint64_t *)value) >> 24);
        message[COLUMBUS_MESSAGE_MEASUREMENT_VALUE + 5] = (uint8_t) ((*(uint64_t *)value) >> 16);
        message[COLUMBUS_MESSAGE_MEASUREMENT_VALUE + 6] = (uint8_t) ((*(uint64_t *)value) >>  8);
        message[COLUMBUS_MESSAGE_MEASUREMENT_VALUE + 7] = (uint8_t) ((*(uint64_t *)value) >>  0);
        break;
    case MEASURE_VALUE_16_BYTE:
        /* do nothing */
        break;
    default:
        /* do nothing */
        break;
    }
}

void columbus_format_measurement_value(void * res, void * integer_part, void * fractional_part, uint8_t width, uint8_t format) {
    uint16_t i16 = 0;
    uint32_t i32 = 0;
    uint64_t i64 = 0uLL;
    void * x;
    /* for little endian systems */
    switch(width) {
    case MEASURE_VALUE_2_BYTE:
        x = &i16;
        switch(format) {
        case MEASURE_FORMAT_2_BYTE_12_0:
            i16 |= (*((uint16_t*)integer_part)<<0);
            break;
        case MEASURE_FORMAT_2_BYTE_4_8:
            i16 |= (*((uint16_t*)integer_part)<<8);
            i16 |= (*((uint16_t*)fractional_part)>>8);/* we want only the 8 high bits of the value */
            break;
        case MEASURE_FORMAT_2_BYTE_0_12:
            i16 |= (*((uint16_t*)fractional_part)>>4);/* we want only the 12 high bits of the value */
            break;
        default:
            break;
        }
        i16 &= 0x0FFF; /* clear out of place btis from the value */
        i16 |= ((uint16_t)width<<8) | ((uint16_t)format<<8);
        *((uint16_t *)res) = *((uint16_t *)x);
        break;
    case MEASURE_VALUE_4_BYTE:
        x = &i32;
        switch(format) {
        case MEASURE_FORMAT_4_BYTE_27_0:
            i32 |= (*((uint32_t*)integer_part)<<0);
            break;
        case MEASURE_FORMAT_4_BYTE_24_3:
            i32 |= (*((uint32_t*)integer_part)<<3);
            i32 |= (*((uint32_t*)fractional_part)>>29);
            break;
        case MEASURE_FORMAT_4_BYTE_11_16:
            i32 |= (*((uint32_t*)integer_part)<<16);
            i32 |= (*((uint32_t*)fractional_part)>>16);
            break;
        case MEASURE_FORMAT_4_BYTE_3_24:
            i32 |= (*((uint32_t*)integer_part)<<24);
            i32 |= (*((uint32_t*)fractional_part)>>8);
            break;
        case MEASURE_FORMAT_4_BYTE_0_27:
            i32 |= (*((uint32_t*)fractional_part)>>5);
            break;
        default:
            break;
        }
        i32 &= 0x07FFFFFF; /* clear out of place btis from the value */
        i32 |= ((uint32_t)width<<24) | ((uint32_t)format<<24);
        *((uint32_t *)res) = *((uint32_t *)x);
        break;
    case MEASURE_VALUE_8_BYTE:
        x = &i64;
        switch(format) {
        case MEASURE_FORMAT_8_BYTE_59_0:
            i64 |= (*((uint64_t*)integer_part)<<0uLL);
            break;
        case MEASURE_FORMAT_8_BYTE_51_8:
            i64 |= (*((uint64_t*)integer_part)<<8uLL);
            i64 |= (*((uint64_t*)fractional_part)>>56uLL);
            break;
        case MEASURE_FORMAT_8_BYTE_43_16:
            i64 |= (*((uint64_t*)integer_part)<<16uLL);
            i64 |= (*((uint64_t*)fractional_part)>>48uLL);
            break;
        case MEASURE_FORMAT_8_BYTE_35_24:
            i64 |= (*((uint64_t*)integer_part)<<24uLL);
            i64 |= (*((uint64_t*)fractional_part)>>40uLL);
            break;
        case MEASURE_FORMAT_8_BYTE_27_32:
            i64 |= (*((uint64_t*)integer_part)<<32uLL);
            i64 |= (*((uint64_t*)fractional_part)>>32uLL);
            break;
        case MEASURE_FORMAT_8_BYTE_19_40:
            i64 |= (*((uint64_t*)integer_part)<<40uLL);
            i64 |= (*((uint64_t*)fractional_part)>>24uLL);
            break;
        case MEASURE_FORMAT_8_BYTE_11_48:
            i64 |= (*((uint64_t*)integer_part)<<48uLL);
            i64 |= (*((uint64_t*)fractional_part)>>16uLL);
            break;
        case MEASURE_FORMAT_8_BYTE_2_57:
            i64 |= (*((uint64_t*)integer_part)<<57uLL);
            i64 |= (*((uint64_t*)fractional_part)>>7uLL);
            break;
        default:
            break;
        }
        i64 &= 0x07FFFFFFFFFFFFFFuLL; /* clear out of place btis from the value */
        i64 |= ((uint64_t)width<<56uLL) | ((uint64_t)format<<56uLL);
        *((uint64_t *)res) = *((uint64_t *)x);
        break;
    case MEASURE_VALUE_16_BYTE:
        /* do nothing */
        break;
    default:
        /* do nothing */
        break;
    }
}


/* Ping messages specific functions */

void columbus_ping_set_signal(uint8_t *message,int8_t signal) {
	message[COLUMBUS_MESSAGE_PING_SIGNAL] = signal;
}
