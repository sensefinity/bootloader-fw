/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     sensoroid
 * @{
 *
 * @file        queue.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */
#include "sensoroid/queue.h"

/*********************************** Private **********************************/

/**
 * @brief       Holds the element queue.
 */
static struct queue_element _elements[QUEUE_MAX_ELEMENTS];

/**
 * @brief       Holds the total number of reads from the queue.
 */
static size_t _reads = 0;

/**
 * @brief       Holds the total number of writes to the queue.
 */
static size_t _writes = 0;

/**
 * @brief       Holds whether there's currently a reservation in the queue.
 */
static bool _reserved = false;

/************************************ Public **********************************/

struct queue_element *queue_reserve(void) {
    struct queue_element *element = NULL;

    atomic(
        if (!_reserved && !queue_full()) {
            _reserved = true;
            element = &_elements[_writes % QUEUE_MAX_ELEMENTS];
        }
    );

    return element;
}

bool queue_reserved(void) {
    return _reserved;
}

void queue_commit(void) {
    atomic(
        if (_reserved) {
            _reserved = false;
            ++_writes;
        }
    );
}

void queue_release(void) {
    atomic(
        if (_reserved) {
            _reserved = false;
        }
    );
}

struct queue_element *queue_peek(void) {
    struct queue_element *element = NULL;

    atomic(
        if (!queue_empty()) {
            element = &_elements[_reads % QUEUE_MAX_ELEMENTS];
        }
    );

    return element;
}

struct queue_element *queue_peek_ahead(size_t offset) {
    struct queue_element *element = NULL;

    atomic(
        if (!queue_empty() && _writes - _reads > offset) {
          element = &_elements[(_reads + offset) % QUEUE_MAX_ELEMENTS];
        }
    );

    return element;
}


void queue_delete(void) {
    atomic(
        if (!queue_empty()) {
            ++_reads;
        }
    );
}

bool queue_empty(void) {
    bool result;
    atomic(result = (_reads == _writes));
    return result;
}

bool queue_full(void) {
    bool result;
    atomic(result = ((_writes - _reads) == QUEUE_MAX_ELEMENTS));
    return result;
}

size_t queue_length(void){
  return _writes - _reads;
}
