/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     sensoroid
 * @{
 *
 * @file        mote.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */
#include "sensoroid/mote.h"

/* C standard library */
#include <stdint.h>
#include <stdbool.h>

/* API */
#include "api/sched.h"
#include "api/sysclk.h"

/* Dandelion library */
#include "dandelion.h"

/* BLE stack */
#include "ble/l2cap.h"
#include "ble/ll.h"
#include "ble/ll_buffer.h"
#include "ble/ll_control.h"
#include "ble/ll_pdu.h"
#include "ble/random.h"

/* Utilities */
#include "util/debug.h"
#include "util/delay.h"
#include "util/reset_reason.h"

/* Peripherals */
#include "periph/adc.h"
#include "periph/gpio.h"
#include "periph/pin_name.h"

/* Reverse routing */
#include "sensoroid/reverse_msg_table.h"
#include "sensoroid/routing_reverse.h"

/* Expansion */
#include "expansion/expansion_services.h"

/*********************************** Private **********************************/

/* Timing and neighboring definitions */
#define NEIGHBOR_TABLE_SIZE     (10)
#define MAX_POSITION_NEIGHBORS  (10)
#define MAX_PROXIMITY_NEIGHBORS (NEIGHBOR_TABLE_SIZE)
#define SCANNING_DURATION       (2400)
#define TIMEOUT_DURATION        (5000)
#define ADVERTISING_DURATION    (3000)

#define MAX_NETWORK_STATUS_NODES    (3)
#define ONE_MINUTE_INTERVAL         (1000 * 60  * 1)
#define SCAN_FOR_CONNECTIVITY_INIT  (1000 * 10) /* 10 seconds */

/* Sequencial scannigns aux */
#define WAIT_FOR_SCAN_MAX           (1000 * 60 * 15)    /* 20 seconds */
#define WAIT_FOR_SCAN_INIT          (14062)             /* 14 seconds */

/* Columbus fixed-point definitions */
#define FBITS   (16)
#define IBITS   (11)
#define HEADER  (0b01010 << 27)
#define FMASK   ((1 << FBITS) - 1)
#define IMASK   (((1 << (FBITS + IBITS)) - 1) & ~FMASK)

/**
 * @brief       Computes the static array length.
 */
#define ARRAY_LEN(x) ((sizeof(x)) / sizeof((x)[0]))

/**
 * @brief       Conditional re-definition of the MIN macro.
 */
#ifndef MIN
#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#endif

/**
 * @brief       Self-enumeration of the current mote state.
 */
enum mote_state {
    MOTE_STATE_STANDBY              = 0,
    MOTE_STATE_SCANNING,
    MOTE_STATE_ADVERTISING,
    MOTE_STATE_CONNECTION

} _state = MOTE_STATE_STANDBY;

/**
 * @brief       Holds the service flags for this device.
 */
static uint32_t _flags;

/**
 * @brief       Holds the services configuration intervals.
 */
 static uint64_t battery_interval;
 static uint64_t ping_interval;
 static uint64_t network_status_interval;
 static uint64_t position_interval;


 /**
  * @brief       Holds the time that we must wait between sequencial scans
  */
static uint64_t wait_for_next_scan = WAIT_FOR_SCAN_INIT;

/**
 * @brief       Holds the scaling factor to apply to ADC samples in order to
 *              convert them into the appropriate voltage.
 */
static const float ADC_SCALE = ADC_REFERENCE / (float) ((1 << ADC_RESOLUTION) - 1);

/**
 * @brief       Implements a general Columbus "Hello" message.
 */
/* static uint8_t _columbus_hello[] = { */
/*     0x01,                                               /\* Message Version *\/ */
/*     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,     /\* Serial *\/ */
/*     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,     /\* Timestamp *\/ */
/*     0x01,                                               /\* Message Type *\/ */
/*     0x00, 0x00, 0x00, 0x0B,                             /\* Total Data Size *\/ */
/*     0x09,                                               /\* Firmware Text Size *\/ */
/*     'D', 'a', 'n', 'd', 'e', 'l', 'i', 'o', 'n',        /\* Firmware Text *\/ */
/*     0x00                                                /\* ICCID Size *\/ */
/* }; */


/**
 * @brief       Implements a general Columbus "Position" message for recording
 *              neighbor devices.
 */
static uint8_t _columbus_position[24 + MAX_POSITION_NEIGHBORS * 10] = {
    0x02,                                               /* Message Version */
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,     /* Serial */
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,     /* Timestamp */
    0x05,                                               /* Sub-Message Type - Position */
    0x00, 0x00, 0x00, 0x00,                             /* Total Data Size */
    0x04,                                               /* Position - RSSI */
    0x00                                                /* RSSI Count */

    /* Collection of pairs of Serial (8 bytes) + RSSI (2 bytes) */
};

/**
 * @brief       Implements a general Columbus "Proximity" message.
 */
static uint8_t _columbus_proximity[] = {
    0x01,                                               /* Message Version */
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,     /* Serial */
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,     /* Timestamp */
    0x08,                                               /* Sub-Message Type - Proximity */
    0x00, 0x00, 0x00, 0x0A,                             /* Total Data Size */
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,     /* Target Serial */
    0x00, 0x00                                          /* Target Value */
};

/**
 * @brief       Implements a Columbus measurement for battery voltage.
 */
static uint8_t _columbus_measurement_voltage[] = {
    0x01,                                               /* Message Version */
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,     /* Serial */
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,     /* Timestamp */
    0x06,                                               /* Message Type (Measurement) */
    0x00, 0x00, 0x00, 0x06,                             /* Data Size */
    0x00,                                               /* Measurement Sensor */
    0x07,                                               /* Measurement Type (Battery Voltage) */
    0x00, 0x00, 0x00, 0x00                              /* Measurement Value (FP @ 32-bit) */
};

/**
 * @brief       Implements a general Columbus "Network status" message. MAX=3;
 */
static uint8_t _columbus_network_status[22 + MAX_NETWORK_STATUS_NODES * 14] = {
    0x02,                                               /* Message Version */
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,     /* Serial */
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,     /* Timestamp */
    0x09,                                               /* Sub-Message Type ( 9 -network status) */
    0x00, 0x00, 0x00, 0x00,                             /* Total Data Size */
    /* Collection of pairs of Serial (8 bytes) + RSSI (2 bytes) + Metric (2 bytes) + sequence_number(2 bytes) */
};

/**
 * @brief       Sets the serial number on a Columbus message.
 * @param       message
 *              Pointer to the Columbus message.
 * @param       serial
 *              The serial number to set.
 */
static void _columbus_set_serial(uint8_t *message, uint64_t serial) {
    int i;
    uint8_t *ptr = (uint8_t *) &serial;

    /* The serial number always starts @ offset = 1 */
    for (i = (sizeof(uint64_t) - 1); i >= 0; --i) {
        message[1 + i] = *ptr++;
    }
}

/**
 * @brief       Retrieves the serial number of a Columbus message.
 * @param       message
 *              Pointer to the message.
 * @return      The serial number.
 */
static uint64_t _columbus_get_serial(const uint8_t *message) {
    int i;
    uint64_t serial;
    uint8_t *ptr = (uint8_t *) &serial;

    /* The timestamp always starts @ offset = 1 */
    for (i = (sizeof(uint64_t) - 1); i >= 0; --i) {
        *ptr++ = message[1 + i];
    }

    return serial;
}

/**
 * @brief       Sets the timestamp on a Columbus message.
 * @param       message
 *              Pointer to the Columbus message.
 * @param       timestamp
 *              The timestamp to set.
 */
static void _columbus_set_timestamp(uint8_t *message, uint64_t timestamp) {
    int i;
    uint8_t *ptr = (uint8_t *) &timestamp;

    /* The timestamp always starts @ offset = 9 */
    for (i = (sizeof(uint64_t) - 1); i >= 0; --i) {
        message[9 + i] = *ptr++;
    }
}

/**
 * @brief       Obtains the timestamp of a Columbus message.
 * @param       message
 *              Pointer to the Columbus message.
 * @return      The timestamp of the Columbus message.
 */
static uint64_t _columbus_get_timestamp(const uint8_t *message) {
    int i;
    uint64_t timestamp;
    uint8_t *ptr = (uint8_t *) &timestamp;

    /* The timestamp always starts @ offset = 9 */
    for (i = (sizeof(uint64_t) - 1); i >= 0; --i) {
        *ptr++ = message[9 + i];
    }

    return timestamp;
}

/**
 * @brief       Sets the size of the data portion on a Columbus message.
 * @param       message
 *              Pointer to the Columbus message.
 * @param       size
 *              The size to be set.
 */
static void _columbus_set_data_size(uint8_t *message, uint32_t size) {
    int i;
    uint8_t *ptr = (uint8_t *) &size;

    /* The data size always starts @ offset = 18 */
    for (i = (sizeof(uint32_t) - 1); i >= 0; --i) {
        message[18 + i] = *ptr++;
    }
}

/**
 * @brief       Sets a battery voltage level on the appropriate Columbus
 *              measurement message.
 * @param       voltage
 *              The voltage to be set.
 */
static void _columbus_set_battery_voltage(float voltage) {
    uint32_t encoded;

    /* Scale value value and apply correction factor */
    voltage *= ((float) (1L << FBITS));
    /*value += ((value >= 0.0F) ? 0.5F : -0.5F);*/

    /* Encode value */
    encoded = (uint32_t) voltage;
    encoded &= (IMASK | FMASK);
    encoded |= HEADER;

    _columbus_measurement_voltage[24 + 0] = (uint8_t) (encoded >> 24);
    _columbus_measurement_voltage[24 + 1] = (uint8_t) (encoded >> 16);
    _columbus_measurement_voltage[24 + 2] = (uint8_t) (encoded >>  8);
    _columbus_measurement_voltage[24 + 3] = (uint8_t) (encoded >>  0);
}

/**
 * @brief       Holds the BLE advertising data.
 */
static uint8_t _adv_data[7 + sizeof(struct sensoroid_data)];

/**
 * @brief       Holds the BLE scan response data.
 */
static const uint8_t _SCAN_RSP_DATA[] = {
    0x0a,           /* Length */
    0x09,           /* Type: Complete Local Name */
    'S', 'e', 'n', 's', 'o', 'r', 'o', 'i', 'd',

    0x05,           /* Length */
    0x12,           /* Type: Slave Connection Interval Range */
    0x50, 0x00,     /* Minimum: 0x0050 * 1.25 ms => 100 ms */
    0x20, 0x03,     /* Maximum: 0x0320 * 1.25 ms => 1000 ms */

    0x02,           /* Length */
    0x0a,           /* Type: Tx Power Level */
    0x00            /* Value: 0 dBm */
};

/**
 * @brief       Holds a pointer to the Sensoroid data.
 */
static struct sensoroid_data *_sensoroid_data =
        (struct sensoroid_data *) (_adv_data + 7);

/**
 * @brief       Holds the current scheduling indentifier.
 */
static sched_id_t _sched_id;

/**
 * @brief       Holds a scheduler identifier for timeout purposes.
 */
static sched_id_t _timeout_id;

/**
 * @brief       Defines the structure of one neighbor.
 */
struct neighbor {
    struct sensoroid_data   data;           /* The raw Sensoroid data */
    struct bdaddr           addr;           /* Address of peer device */
    uint64_t                t_found;        /* The time at which it was added */
    uint64_t                t_updated;      /* The time at which it was updated */
    int32_t                 rssi_accum;     /* The accumulation of the various RSSIs */
    int32_t                 rssi_count;     /* The number of received RSSIs */
};

/**
 * @brief       Holds the table of neighbors devices.
 */
static struct neighbor _neighbor_table[NEIGHBOR_TABLE_SIZE];

/**
 * @brief       Holds the number of neighbor devices in the table.
 */
static size_t _neighbor_count = 0;

/**
 * @brief       Holds the table of tracking devices (for proximity).
 */
static struct neighbor _tracking_table[MAX_PROXIMITY_NEIGHBORS];

/**
 * @brief       Holds the the number of neighbor devices in the tracking table.
 */
static size_t _tracking_count = 0;

/**
 * @brief       Holds the BLE advertising parameters.
 */
static const struct ll_advertising_params _ADV_PARAMS = {
    .type           = LL_ADVERTISING_TYPE_CONNECTABLE_UNDIRECTED,
    .interval       = 0x0500,       /* 80 ms */
    .channel_map    = LL_ADVERTISING_CHANNEL_MAP_ALL
};

/**
 * @brief       Holds the BLE scanning parameters.
 */
static const struct ll_scan_params _SCAN_PARAMS = {
    .type           = LL_SCAN_TYPE_PASSIVE,
    .interval       = 0x0500,       /* 800 ms */
    .window         = 0x0500,       /* 800 ms */
    .channel_map    = LL_ADVERTISING_CHANNEL_MAP_ALL
};

/**
 * @brief       Holds the BLE connection parameters.
 */
static struct ll_connection_params _conn_params = {
    .scan_interval          = 0x0500,       /* 800 ms */
    .scan_window            = 0x0500,       /* 800 ms */
    .conn_interval          = 0x0010,       /* 20 ms */
    .conn_latency           = 0,
    .supervision_timeout    = 0x01F4,       /* 5000 ms */
    .channel_map            = LL_ADVERTISING_CHANNEL_MAP_ALL,

    /* .peer_addr is left blank */
};

/**
 * @brief       Initializes the advertising data.
 */
static void _init_adv_data(void) {
    /* Initialize base data */
    _adv_data[0] = 0x02;        /* Length */
    _adv_data[1] = 0x01;        /* Type: Flags */
    _adv_data[2] = 0x06;

    /* Value of Flags: LE General Discoverable Mode | BR/EDR Not Supported */

    _adv_data[3] = 3 + sizeof(struct sensoroid_data);
    _adv_data[4] = 0xFF;        /* Type: Manufacturer-specific Data */
    _adv_data[5] = 0xFF;        /* Company Identifier (16-bit) */
    _adv_data[6] = 0xFF;
}

/* Prototype for the function that handles the mote state machine */
static void _handle_interval(void);

/**
 * @brief       Handles connection events from BLE in the master role.
 * @param       event
 *              The event to be handled.
 */
static void _master_connection_handler(enum ll_connection_event event) {
    static struct queue_element *element = NULL;
    static size_t bytes_sent = 0;
    static size_t last_size;
    static uint64_t timestamp;
    static enum ll_connection_event last_event;
    struct l2cap_header *header;
    struct ll_pdu_data pdu;
    struct ll_pdu_data rev_pdu;
    uint32_t length;
    static struct msg_element reverse_message;
    static size_t reverse_bytes;
    size_t len;

    switch (event) {
    case LL_CONNECTION_EVENT_CONNECTION_CREATED:
        dputs("[mote] master connection created");

        /* Initialize reverse message for receiving */
        reverse_bytes = 0;
        reverse_message.size = 0;

        last_event = LL_CONNECTION_EVENT_CONNECTION_CREATED;

        /* A new connection was created; Obtain the next queue element to be
           sent */
        sched_cancel(_timeout_id);
        element = queue_peek();
        last_size = element->size;
        bytes_sent = 0;

        /* Timestamp message */
        timestamp = _columbus_get_timestamp(element->data);
        _columbus_set_timestamp(element->data, sysclk_get() - timestamp);

        /* Segment initial packet */
        length = MIN(
                LL_PDU_DATA_PAYLOAD_SIZE,
                element->size + sizeof(struct l2cap_header));

        /* Initialize packet header fields */
        pdu.llid    = LL_PDU_DATA_LLID_START;
        pdu.length  = length;

        /* Retrieve pointer to the L2CAP header */
        header = (struct l2cap_header *) pdu.payload;

        /* Initialize header */
        header->length      = element->size;
        header->channel_id  = SENSOROID_L2CAP_ASSIGNED_NUMBER;

        /* Copy data into the PDU */
        memcpy(pdu.payload + sizeof(struct l2cap_header), element->data,
                length - sizeof(struct l2cap_header));

        /* Accumulate the number of sent bytes */
        bytes_sent += (length - sizeof(struct l2cap_header));

        /* Put into link layer buffer */
        ll_buffer_put(ll_get_connection_tx_buffer(), &pdu);
        break;

    case LL_CONNECTION_EVENT_CONNECTION_ESTABLISHED:
        dputs("[mote] master connection established");
        last_event = LL_CONNECTION_EVENT_CONNECTION_ESTABLISHED;
        break;

    case LL_CONNECTION_EVENT_CONNECTION_LOST:
    case LL_CONNECTION_EVENT_CONNECTION_TERMINATED_OK:
    case LL_CONNECTION_EVENT_CONNECTION_TERMINATED_ERROR:
        /* The connection was terminated */
        if (last_size != bytes_sent) {
            /* Restore old timestamp, since we failed to send a full packet */
            _columbus_set_timestamp(element->data, timestamp);
        }

        if (last_event == LL_CONNECTION_EVENT_CONNECTION_CREATED) {
            /* Connection not established, node failure */
            dputs("[mote] master connection not established, node failure");
            mesh_node_failure();
        }

        //TODO
        if (reverse_message.size > 0 &&
            _columbus_get_serial(reverse_message.msg)
                == sensoroid_get_serial()) {
            /* The configuration message is targeted at us; Update & Reset */
            reverse_message.msg[reverse_message.size] = '\0';
            dprintf("New configuration message: %s\r\n", ((char *) reverse_message.msg) + 22);
            sensoroid_patch_configuration(((char *) reverse_message.msg) + 22);
            NVIC_SystemReset();
        }

        _sched_id = sched_timeout(&_handle_interval, SCHED_TASK_MS_MIN);
        break;

    case LL_CONNECTION_EVENT_DATA_SENT:
        dputs("[mote] master data sent");

        /* One packet has been sent; Verify if we need to further segment
           the packet */
        if (bytes_sent == element->size) {
            /* We're all done */
            queue_delete();

            if (queue_empty()) {
                /* We sent everything */
                ll_control_termination_procedure(0x00);
                return;
            }

            /* Prepare next packet */
            element = queue_peek();
            last_size = element->size;
            bytes_sent = 0;

            /* Timestamp message */
            timestamp = _columbus_get_timestamp(element->data);
            _columbus_set_timestamp(element->data, sysclk_get() - timestamp);

            /* Segment initial packet */
            length = MIN(
                    LL_PDU_DATA_PAYLOAD_SIZE,
                    element->size + sizeof(struct l2cap_header));

            /* Initialize packet header fields */
            pdu.llid    = LL_PDU_DATA_LLID_START;
            pdu.length  = length;

            /* Retrieve pointer to the L2CAP header */
            header = (struct l2cap_header *) pdu.payload;

            /* Initialize header */
            header->length      = element->size;
            header->channel_id  = SENSOROID_L2CAP_ASSIGNED_NUMBER;

            /* Copy data into the PDU */
            memcpy(pdu.payload + sizeof(struct l2cap_header), element->data,
                    length - sizeof(struct l2cap_header));

            /* Accumulate the number of sent bytes */
            bytes_sent += (length - sizeof(struct l2cap_header));

            /* Put into link layer buffer */
            ll_buffer_put(ll_get_connection_tx_buffer(), &pdu);
            return;
        }

        /* We're sending another part of the packet; This means that we don't
           need to include the L2CAP header */
        length = MIN(LL_PDU_DATA_PAYLOAD_SIZE, element->size - bytes_sent);

        /* Initialize packet header fields */
        pdu.llid    = LL_PDU_DATA_LLID_CONTINUATION;
        pdu.length  = length;

        /* Copy data into the PDU */
        memcpy(pdu.payload, element->data + bytes_sent, length);

        /* Accumulate the number of bytes sent */
        bytes_sent += length;

        /* Put into link layer buffer */
        ll_buffer_put(ll_get_connection_tx_buffer(), &pdu);
        break;

    case LL_CONNECTION_EVENT_DATA_RECEIVED:
        /* Get packet */
        ll_buffer_get(ll_get_connection_rx_buffer(), &rev_pdu);

        if (rev_pdu.llid == LL_PDU_DATA_LLID_START) {
            /* This is the start of a new packet */
            header = (struct l2cap_header *) rev_pdu.payload;
            reverse_message.size = header->length;

            len = rev_pdu.length - sizeof(struct l2cap_header);
            memcpy(reverse_message.msg,
                rev_pdu.payload + sizeof(struct l2cap_header), len);
            reverse_bytes = len;

        } else {
            /* This is the continuation of a packet */
            len = rev_pdu.length;
            memcpy(reverse_message.msg + reverse_bytes, rev_pdu.payload, len);
            reverse_bytes += len;
        }

        if (reverse_bytes == reverse_message.size) {
        	//TODO node_receive correctly!
            rmt_insert(reverse_message.msg, reverse_message.size);
        }

        break;

    default:
        /* Ignore remaining events */
        return;
    }
}

/**
 * @brief       Segments and sends a configuration message.
 * @param       reverse_message
 *              Pointer to the reverse message.
 * @return      A number of packets resulting from segmentation.
 */
static size_t _segment_and_send(struct msg_element *reverse_message) {
    struct l2cap_header *header;
    struct ll_pdu_data pdu;
    size_t len;
    size_t size = reverse_message->size;
    bool start = true;
    size_t packet_count = 0;
    uint8_t *ptr = reverse_message->msg;

    while (size != 0) {
        if (start) {
            /* Start of packet */
            start = false;

            /* Compute length of data */
            len = MIN(size, LL_PDU_DATA_PAYLOAD_SIZE - sizeof(struct l2cap_header));

            /* Initialize packet header */
            pdu.llid = LL_PDU_DATA_LLID_START;
            pdu.length = len + sizeof(struct l2cap_header);

            /* Initialize L2CAP header */
            header = (struct l2cap_header *) pdu.payload;
            header->length = size;
            header->channel_id = SENSOROID_L2CAP_ASSIGNED_NUMBER;

            /* Copy data into the PDU */
            memcpy(pdu.payload + sizeof(struct l2cap_header), ptr, len);

        } else {
            /* Continuation of packet */
            len = MIN(size, LL_PDU_DATA_PAYLOAD_SIZE);

            /* Initialize packet header */
            pdu.llid = LL_PDU_DATA_LLID_CONTINUATION;
            pdu.length = len;

            /* Copy data into the PDU */
            memcpy(pdu.payload, ptr, len);
        }

        /* Queue packet for transmission */
        ll_buffer_put(ll_get_connection_tx_buffer(), &pdu);

        /* Prepare next iteration */
        size -= len;
        ptr += len;
        ++packet_count;
    }

    return packet_count;
}

/**
 * @brief       Handles connection events from BLE in the slave role.
 * @param       event
 *              The event to be handled.
 */
static void _slave_connection_handler(enum ll_connection_event event) {
    static struct queue_element *element = NULL;
    static size_t bytes_received = 0;
    static uint64_t timestamp;
    static struct bdaddr peer_addr;
    struct l2cap_header *header;
    struct ll_pdu_data pdu;
    struct routing_reverse_entry reverse_entry;
    static int reverse_index;
    struct msg_element *reverse_message;
    static size_t reverse_packet_count = 0;

    switch (event) {
    case LL_CONNECTION_EVENT_CONNECTION_CREATED:
        /* A new connection was created, prevent scheduler from changing our
           data from our known state */
        sched_cancel(_sched_id);

        /* Retrieve address of peer */
        peer_addr = ll_connection_peer_addr();

        reverse_index = routing_reverse_search_msg(&peer_addr);
        if (reverse_index != ROUTING_REVERSE_TABLE_CAPACITY) {
            /* We have a configuration message to be sent... */
            reverse_message = rmt_get_message(reverse_index);

            /* Update message TTL*/
            routing_reverse_update_out_timestamp(reverse_message->msg);

            /* Segment and send */
            reverse_packet_count = _segment_and_send(reverse_message);

        } else {
            reverse_packet_count = 0;
        }

        /* Initialize reassembly state variables */
        element = NULL;
        bytes_received = 0;
        break;

    case LL_CONNECTION_EVENT_CONNECTION_LOST:
    case LL_CONNECTION_EVENT_CONNECTION_TERMINATED_OK:
    case LL_CONNECTION_EVENT_CONNECTION_TERMINATED_ERROR:
        /* The connection ended, make sure there is no pending data to be
           commited in case the connection was terminated with an error */
        if (queue_reserved()) {
            queue_release();
        }

        /* Go back to advertising, increasing our sequence number */
        _sched_id = sched_timeout(&_handle_interval, SCHED_TASK_MS_MIN);

        break;

    case LL_CONNECTION_EVENT_DATA_RECEIVED:
        /* We received new data, as such we need to handle it, making sure that
           we're communicating with a valid Sensoroid device */
        ll_buffer_get(ll_get_connection_rx_buffer(), &pdu);

        if (pdu.llid == LL_PDU_DATA_LLID_START) {
            /* This is the start of a new packet */
            timestamp = sysclk_get();
            queue_release();
            element = queue_reserve();

            if (element == NULL) {
                /* We have no more available space */
                ll_control_termination_procedure(0xFF);
                return;
            }

            /* Reset count for the number of received bytes */
            bytes_received = 0;

            /* Retrieve L2CAP header */
            header = (struct l2cap_header *) pdu.payload;

            if (header->channel_id != SENSOROID_L2CAP_ASSIGNED_NUMBER) {
                /* The L2CAP channel ID does not match our assigned number, so
                   we can conclude that we're not communicating with a valid
                   Sensoroid device */
                ll_control_termination_procedure(0xFF);
                return;
            }

            /* Initialize element data */
            element->size = header->length;

            /* Accumulate number of received bytes */
            bytes_received = pdu.length - sizeof(struct l2cap_header);

            /* Copy partial data */
            memcpy(element->data, pdu.payload + sizeof(struct l2cap_header),
                    bytes_received);

        } else {
            if (element == NULL || bytes_received == 0) {
                /* Invalid state */
                ll_control_termination_procedure(0xFF);
                return;
            }

            /* This is the continuation of a packet, copy data */
            memcpy(element->data + bytes_received, pdu.payload, pdu.length);

            /* Accumulate number of received bytes */
            bytes_received += pdu.length;
        }

        if (bytes_received >= element->size) {
            /* We have a full packet, perform timestamping */
            _columbus_set_timestamp(element->data,
                    timestamp - _columbus_get_timestamp(element->data));

            /* Update reverse routing table... */
            reverse_entry.origin_node_serial = _columbus_get_serial(element->data);
            reverse_entry.intermediate_node_address = peer_addr;
            reverse_entry.life_time = 0;
            routing_reverse_new_message(&reverse_entry);

            /* Finally, commit it to the queue */
            queue_commit();
        }

        break;

    case LL_CONNECTION_EVENT_DATA_SENT:
        if (--reverse_packet_count == 0) {
            /* Configuration message sent; Delete */
            rmt_clear_index(reverse_index);
        }

        break;

    default:
        /* Ignore remaining events */
        return;
    }
}

/**
 * @brief       Configures and starts the scanning state.
 */
static void _scanning_start(void) {
    _state = MOTE_STATE_SCANNING;
    _neighbor_count = 0;
    ll_set_scan_enable(true);
}

/**
 * @brief       Stops the scanning state.
 */
static void _scanning_stop(void) {
    size_t i;
    struct mesh_entry entry;

    ll_set_scan_enable(false);

    /* Process neighbor table in order to update mesh protocol */
    for (i = 0; i < _neighbor_count; ++i) {
        if (_neighbor_table[i].data.serial_gateway != 0) {
            /* The neighbor has valid routing information, update mesh */
            entry.gateway_serial    = _neighbor_table[i].data.serial_gateway;
            entry.node_serial       = _neighbor_table[i].data.serial_node;
            entry.sequence_number   = _neighbor_table[i].data.seqno;
            entry.rssi              = (_neighbor_table[i].rssi_accum / _neighbor_table[i].rssi_count);
            entry.metric            = _neighbor_table[i].data.metric;
            entry.addr              = _neighbor_table[i].addr;
            entry.life_time         = 500;

            /* Announce new routing message */
            mesh_new_routing_message(&entry);
        }
    }
}

/**
 * @brief       Configures and starts the advertising state.
 */
static void _advertising_start(void) {
    const struct mesh_entry *entry;
    entry = mesh_next_hop_select();

    if (entry != NULL) {
        /* We still have connectivity */
        _sensoroid_data->serial_gateway = entry->gateway_serial;
        _sensoroid_data->seqno = entry->sequence_number;
        _sensoroid_data->metric = entry->metric;

    } else {
        /* We have no connectivity anymore */
        _sensoroid_data->serial_gateway = 0;
        _sensoroid_data->seqno = 0;
        _sensoroid_data->metric = 0;
    }

    /* Set connection handler */
    ll_set_connection_event_handler(&_slave_connection_handler);

    /* Configure advertising data */
    ll_set_advertising_data(_adv_data, ARRAY_LEN(_adv_data));

    /* Finally, enter advertising state */
    _state = MOTE_STATE_ADVERTISING;
    ll_set_advertise_enable(true);
}

/**
 * @brief       Stops the advertising state.
 */
static void _advertising_stop(void) {
    ll_set_advertise_enable(false);
}

/**
 * @brief       Creates a connection to a peer device.
 * @param       entry
 *              The selected mesh entry for the peer device.
 */
static void _connection_create(const struct mesh_entry *entry) {
    if (entry == NULL) {
        /* NOTE: This should never happen, it's just a precaution in case there
           is a bug in the mesh protocol */
        dputs("[mote] invalid mesh entry");
        return;
    }

    dprintf("[mote] connecting to serial=0x%08x%08x, addr=",
            (unsigned) (entry->node_serial >> 32), (unsigned) (entry->node_serial));
    for (int i = BDADDR_LEN - 1; i >= 0; --i) {
        dprintf("%02x", entry->addr.data[i]);
        if (i != 0) {
            dputchar(':');
        }
    }
    dputchar('\r');
    dputchar('\n');

    _state = MOTE_STATE_CONNECTION;
    _conn_params.peer_addr = entry->addr;
    ll_set_connection_event_handler(&_master_connection_handler);
    ll_create_connection(_conn_params);
}

/**
 * @brief       Cancels a previously created connection.
 */
static void _connection_cancel(void) {
    if (ll_create_connection_cancel()) {
        /* Go back to the state machine */
        _sched_id = sched_timeout(&_handle_interval, SCHED_TASK_MS_MIN);

        /* Signal that we were unable to communicate with the peer */
        mesh_node_failure();

        dputs("[mote] could not create connection, announcing failure");
    }
}

/**
 * @brief       Comparator function for quick sort.
 * @param       e1
 *              Pointer to the first element.
 * @param       e2
 *              Pointer to the second element.
 * @return      The result of comparing the elements by RSSI.
 */
static int _sort_by_rssi(const void *e1, const void *e2) {
    const struct neighbor *n1 = (const struct neighbor *) e1;
    const struct neighbor *n2 = (const struct neighbor *) e2;
    int32_t rssi1 = n1->rssi_accum / n1->rssi_count;
    int32_t rssi2 = n2->rssi_accum / n2->rssi_count;
    return (int) (rssi2 - rssi1);
}

/**
 * @brief       Parses data from the table of neighbors and sends a position
 *              message.
 */
static void _send_position(void) {
    size_t i;
    int j;
    struct queue_element *element;
    size_t elements;
    int offset;
    uint8_t *ptr;

    /* Try to reserve a new queue element */
    element = queue_reserve();
    if (element == NULL) {
        /* We were unable to reserve space in the queue, as such we won't
           be sending a new message */
        return;
    }

    /* Compute number of elements to use for positioning */
    elements = MIN(_neighbor_count, MAX_POSITION_NEIGHBORS);

    /* Initialize base data */
    _columbus_set_serial(_columbus_position, sensoroid_get_serial());
    _columbus_set_timestamp(_columbus_position, sysclk_get());
    _columbus_set_data_size(_columbus_position, 2 + elements * 10);

    /* Initialize RSSI Count field (number of devices) */
    _columbus_position[23] = elements;

    if (_neighbor_count > elements) {
        /* We have more neighbors than we want, as such, we'll sort the
           neighbors by RSSI, so that we have only the "best" devices */
        qsort(_neighbor_table, _neighbor_count,
                sizeof(struct neighbor), &_sort_by_rssi);
    }

    for (i = 0; i < elements; ++i) {
        /* Compute offset of the <Serial, RSSI> pair in the message */
        offset = 24 + 10 * i;

        /* Set serial number for the current element */
        ptr = (uint8_t *) &_neighbor_table[i].data.serial_node;

        for (j = sizeof(uint64_t) - 1; j >= 0; --j) {
            _columbus_position[offset++] = ptr[j];
        }

        /* Set RSSI for the current element */
        _columbus_position[offset++] = 0xFF;
        _columbus_position[offset] =
                (_neighbor_table[i].rssi_accum / _neighbor_table[i].rssi_count);
    }

    /* Initialize queue element */
    element->size = 24 + elements * 10;
    memcpy(element->data, _columbus_position, element->size);

    /* Commit message to the queue */
    queue_commit();
}

/**
 * @brief       Adds one proximity message to the queue of messages.
 * @param       serial
 *              The serial number of the device.
 * @param       value
 *              The value for proximity.
 *              For now, a value of zero means that we approached another
 *              device while a different value means that we moved away from
 *              the device.
 */
static void _send_proximity_message(uint64_t serial, uint64_t timestamp, uint8_t value) {
    struct queue_element *element;

    element = queue_reserve();
    if (element != NULL) {
        /* Set base values */
        _columbus_set_serial(_columbus_proximity, sensoroid_get_serial());
        _columbus_set_timestamp(_columbus_proximity, timestamp);

        /* Set target serial number on the proximity message */
        _columbus_proximity[22] = (serial >> 56) & 0xFF;
        _columbus_proximity[23] = (serial >> 48) & 0xFF;
        _columbus_proximity[24] = (serial >> 40) & 0xFF;
        _columbus_proximity[25] = (serial >> 32) & 0xFF;
        _columbus_proximity[26] = (serial >> 24) & 0xFF;
        _columbus_proximity[27] = (serial >> 16) & 0xFF;
        _columbus_proximity[28] = (serial >>  8) & 0xFF;
        _columbus_proximity[29] = (serial >>  0) & 0xFF;

        /* Set value on the message */
        /* NOTE: Using 2-byte fixed point format without decimal part */
        _columbus_proximity[30] = 0;
        _columbus_proximity[31] = value;

        /* Copy message to the queue element */
        element->size = ARRAY_LEN(_columbus_proximity);
        memcpy(element->data, _columbus_proximity, element->size);

        /* Commit message to the queue */
        queue_commit();
    }
}

/**
 * @brief       Parses data from the table of neighbors and sends a proximity
 *              message.
 */
static void _send_proximity(void) {
    int i;
    int j;
    struct neighbor *neighbor;
    uint64_t t_now = sysclk_get();
    bool tracking;

    /* Perform cleanup on the tracking table */
    for (i = 0; i < (int) _tracking_count; ++i) {
        neighbor = &_tracking_table[i];

        if ((t_now - neighbor->t_updated) >= 6000) {
            /* The last update was longer than 6 seconds ago; Consider that
               we lost the device */
            _send_proximity_message(neighbor->data.serial_node,
                    neighbor->t_updated, 1);

            /* Remove it from the table */
            for (j = i; j < (((int) _tracking_count) - 1); ++j) {
                _tracking_table[j] = _tracking_table[j + 1];
            }

            --i;
            --_tracking_count;
        }
    }

    /* Parse table of neighbors */
    for (i = 0; i < (int) _neighbor_count; ++i) {
        neighbor = &_neighbor_table[i];

        /* Verify if we're already tracking the current neighbor */
        tracking = false;
        for (j = 0; j < (int) _tracking_count; ++j) {
            if (neighbor->data.serial_node
                    == _tracking_table[j].data.serial_node) {
                /* Update device */
                _tracking_table[j].t_updated = neighbor->t_updated;
                tracking = true;
                break;
            }
        }

        if (!tracking) {
            /* We have found a new device, add it to the table if we have
               enough space for it */
            if ((_tracking_count + 1) < MAX_PROXIMITY_NEIGHBORS) {
                /* Save neighbor */
                _tracking_table[_tracking_count++] = *neighbor;
                _send_proximity_message(neighbor->data.serial_node,
                        neighbor->t_found, 0);
            }
        }
    }
}

/**
 * @brief       Handles all of the enabled services, taking appropriate action.
 */
static void _handle_services(void) {
    if (_flags & MOTE_SERVICE_POSITION) {
        /* The positioning service is enabled */
        if (_neighbor_count != 0) {
            _send_position();
        }
    }

    if (_flags & MOTE_SERVICE_PROXIMITY) {
        /* The proximity service is enabled; Send proximity information */
        _send_proximity();
    }
}

/**
 * @brief       Parses a sensoroid_data struct from the advertising data in
 *              an advertising report.
 * @param       report
 *              The report to be parsed.
 * @param       data
 *              Pointer to the output structure.
 * @returns     A boolean value indicating if the parsing procedure was
 *              successful.
 */
static bool _parse_sensoroid_data(const struct ll_advertising_report *report,
        struct sensoroid_data *data) {
    size_t i = 0;

    while (i < report->size) {
        if ((i + report->data[i]) < report->size) {
            /* We have valid GAP data */
            if (report->data[i + 1] == 0xFF) {
                /* We have found manufacturer-specific data */
                if ((report->data[i] - 3) == sizeof(struct sensoroid_data)
                        && report->data[i + 2] == 0xFF
                        && report->data[i + 3] == 0xFF) {
                    /* The expected company identifier matches (0xFFFF) as
                       such we'll assume we've found a valid Sensoroid */
                    memcpy(data, &report->data[i + 4], sizeof(struct sensoroid_data));

                    return true;
                }
            }

            /* Advance to next GAP data */
            i += (report->data[i] + 1);
        }
    }

    /* We have malformed GAP data and/or we didn't find a Sensoroid device */
    return false;
}

/**
 * @brief       Handles advertising reports from the BLE scanning state.
 * @param       report
 *              The report to be handled.
 */
static void _advertising_report_handler(struct ll_advertising_report report) {
    struct sensoroid_data data;
    size_t i;
    bool exists = false;
    uint64_t t_now;

    if (report.type == LL_ADVERTISING_REPORT_TYPE_CONNECTABLE_UNDIRECTED
            || report.type == LL_ADVERTISING_REPORT_TYPE_SCANNABLE_UNDIRECTED) {
        if (_parse_sensoroid_data(&report, &data)) {
            /* Add data to table of neighbors */
            for (i = 0; i < _neighbor_count; ++i) {
                if (_neighbor_table[i].data.serial_node == data.serial_node) {
                    /* The neighbor already exists, update */
                    _neighbor_table[i].data = data;
                    _neighbor_table[i].t_updated = sysclk_get();
                    _neighbor_table[i].rssi_accum += report.rssi;
                    _neighbor_table[i].rssi_count += 1;
                    exists = true;
                }
            }

            if (!exists) {
                /* Device does not yet exist, add to table if we have enough
                   space */
                if ((_neighbor_count + 1) < NEIGHBOR_TABLE_SIZE) {
                    t_now = sysclk_get();
                    _neighbor_table[_neighbor_count].data = data;
                    _neighbor_table[_neighbor_count].addr = report.addr;
                    _neighbor_table[_neighbor_count].t_found = t_now;
                    _neighbor_table[_neighbor_count].t_updated = t_now;
                    _neighbor_table[_neighbor_count].rssi_accum = report.rssi;
                    _neighbor_table[_neighbor_count].rssi_count = 1;
                    ++_neighbor_count;
                }
            }
        }
    }
}

/**
 * @brief       Computes a random number of sleep milliseconds.
 * @returns     A random number of sleep milliseconds.
 */
static uint64_t _random_sleep_ms(void) {
  /* Generate new random number */
      uint64_t value = random_generate();

      value *= 2ULL;
      if (value < 100ULL) {
          value = 100ULL;
      }

      /* add 1 second to random value and return */
      return value + (1<<10);
}
/**
 * @brief       Handles all the requirements for the battery service.
 */
static void _handle_battery_service(void) {
    /*dputs("[_handle_battery_service] invoked");*/
    /* Holds the time at which we performed the last measurement */
    static uint64_t t_measurement = 0;
    uint64_t t_now;
    uint16_t sample;
    float voltage;
    struct queue_element *element;

    /* Read current time */
    t_now = sysclk_get();

    if (t_measurement == 0 || (t_now - t_measurement) >= battery_interval) {
        dputs("[_handle_battery_service] new measurement needed");

        element = queue_reserve();
        if (element == NULL) {
            dputs("[_handle_battery_service] could not reserve element");
            return;
        }

        /* Take new measurement */
        t_measurement = t_now;
        dputs("[_handle_battery_service] taking measurement");

        /* READ STUFF... */
        gpio_output_init(VBAT_POWER);
        gpio_output_set(VBAT_POWER);
        delay_us(VBAT_SETTLING_US);
        sample = adc_sample(adc_pin_as_channel(VBAT_IN));
        gpio_output_clear(VBAT_POWER);
        voltage = ADC_SCALE * (float) sample;
        voltage *= VBAT_SCALE;
        dprintf("[_handle_battery_service] voltage: %f\r\n", voltage);

        /* SET STUFF ON MESSAGE... */
        _columbus_set_serial(_columbus_measurement_voltage, sensoroid_get_serial());
        _columbus_set_timestamp(_columbus_measurement_voltage, t_now);
        _columbus_set_battery_voltage(voltage);

        /* COMMIT STUFF TO QUEUE... */
        element->size = ARRAY_LEN(_columbus_measurement_voltage);
        memcpy(element->data, _columbus_measurement_voltage, element->size);
        queue_commit();
    }
}

/**
 * @brief       Handles the ping service, sends periodic ping messages
 */
static void _handle_ping_service(void) {
    /*dputs("[_handle_ping_service] invoked");*/

    /* Holds the time at which we add the last message */
    static uint64_t t_last_ping = 0;
    uint64_t t_now;
    struct queue_element *element;

    /* Read current time */
    t_now = sysclk_get();

    if (t_last_ping == 0 || (t_now - t_last_ping) >= ping_interval) {
        dputs("[_handle_ping_service] new ping needed!");

        element = queue_reserve();
        if (element == NULL) {
            dputs("[_handle_ping_service] could not reserve element");
            return;
        } else {
			/* Update t_last_ping */
			t_last_ping = t_now;


			uint8_t columbus_signal[COLUMBUS_MEASUREMENT_MESSAGE_SIZE-2];
			memset(columbus_signal,0x00,COLUMBUS_MEASUREMENT_MESSAGE_SIZE-2);

			/* Insert initial "Hello" message into the queue */
			/* Initialize columbus message */
			columbus_set_message_version(columbus_signal, 1);
			columbus_set_serial(columbus_signal, sensoroid_get_serial());
			columbus_set_timestamp(columbus_signal, sysclk_get());
			columbus_set_message_type(columbus_signal, COLUMBUS_MEASUREMENT_TYPE);
			columbus_set_data_size(columbus_signal,4);

			/* Add signal */
			/* TODO  MEASUREMENT SENSOR */
			columbus_set_measurement_sensor(columbus_signal, 0x01); /* sensor id seq - 0x01 */
			columbus_set_measurement_type(columbus_signal, COLUMBUS_MEASURE_RSSI); /* type = 1 - temperature */
			//columbus_set_measurement_value(columbus_signal, ((int16_t) mesh_next_hop_get_signal()));

			uint16_t rh;
			uint16_t rh_int;
			uint16_t rh_frac;

			rh = 0;
		    rh_int =  (int16_t) mesh_next_hop_get_signal();
		    rh_frac = 0;
		    columbus_format_measurement_value(
		            &rh,
		            &rh_int,
		            &rh_frac,
		            MEASURE_VALUE_2_BYTE,
					MEASURE_FORMAT_2_BYTE_12_0);

		    columbus_set_measurement_value_width(columbus_signal, &rh ,MEASURE_VALUE_2_BYTE);


			/* COMMIT MSG TO QUEUE... */
			element->size =COLUMBUS_MEASUREMENT_MESSAGE_SIZE-2;
			memcpy(element->data, columbus_signal, element->size);
			dprintf("Element [%u]\t", element->size);
			 size_t a;
			for (a = 0; a < element->size; ++a) {
				dprintf("%02x", element->data[a]);
				}
			dprintf("\r\n");

			queue_commit();
			dputs("[_handle_ping_service] ping added to the queue");
        }
    }
}

/**
 * @brief       Handles the network status service, sends periodic network status messages
 */
static void _handle_network_status_service(){
  dputs("[_handle_network_status_service] invoked");
  /* Holds the time at which we add the last message */
  static uint64_t t_last_ns = 0;
  uint64_t t_now;
  struct queue_element *element;
  int j;
  uint8_t *ptr;
  int offset;
  /* Read current time */
  t_now = sysclk_get();
  if (t_last_ns == 0 || (t_now - t_last_ns) >= network_status_interval) {
      dputs("[_handle_network_status_service] new network status needed!");
      /* Update t_last_ping */
      t_last_ns = t_now;
      int table_count = mesh_table_count();
      /* No Connectivity -> no message */
      if (table_count == 0) return;
      element = queue_reserve();
      if (element == NULL) {
          dputs("[_handle_network_status_service] could not reserve element");
          return;
      }
      /* SET STUFF ON MESSAGE... */
      _columbus_set_serial(_columbus_network_status, ficr_device_id());
      _columbus_set_timestamp(_columbus_network_status, t_now);
      int i;
      struct mesh_entry  *meshy;
      _columbus_set_data_size(_columbus_network_status, table_count * 14);
      if (table_count > MAX_NETWORK_STATUS_NODES)
          table_count = MAX_NETWORK_STATUS_NODES;
      for (i = 0; i < table_count; ++i) {
        /* Compute offset of the <Serial, RSSI> pair in the message */
        offset = 22 + 14 * i;
        /* Get entry */
        meshy = mesh_get_entry(i);
        dprintf("I-%d, GADDR-%ld ",i,(long) meshy->gateway_serial);
        dprintf("NADDR-%ld ",(long) meshy->node_serial);
        dprintf("Sequence_number %d, metric %d, Rssi %d \n", meshy->sequence_number, meshy->metric, meshy->rssi);
        /* Set serial number for the current element */
        ptr = (uint8_t *) &meshy->node_serial;
        for (j = sizeof(uint64_t) - 1; j >= 0; --j) {
              _columbus_network_status[offset++] = ptr[j];
        }
        /* Set RSSI for the current element */
        _columbus_network_status[offset++] = 0xff;
        _columbus_network_status[offset++] = meshy->rssi;
        /* Set metric */
        _columbus_network_status[offset++] = meshy->metric >>8;
        _columbus_network_status[offset++] = meshy->metric ;
        /* Set sequence_number */
        _columbus_network_status[offset++] = meshy->sequence_number >> 8;
        _columbus_network_status[offset++] = meshy->sequence_number;
      }
      /* Initialize queue element */
      element->size = 22 + table_count * 14;
      memcpy(element->data, _columbus_network_status, element->size);
      /* Commit message to the queue */
      queue_commit();
      /* debug print message*/
      size_t a;
      dprintf("Element [%u]\t", element->size);
      for (a = 0; a < element->size; ++a) {
          dprintf("%02x", element->data[a]);
      }
      dprintf("\r\n");
      dputs("[_handle_network_status_service] network status added to the queue");
    }
  }

/**
 * @brief       Handles services that do not depend on BLE.
 */
static void _handle_additional_services(void) {
    /*dputs("[_handle_additional_services] invoked");*/
    /* Holds the time at which we performed the last one minute tasks */
    static uint64_t t_last_minute = 0;

    uint64_t t_now;

    /* Read current time */
    t_now = sysclk_get();

    if (_flags & MOTE_SERVICE_BATTERY) {
        /* Handle battery service, since it is enabled */
        _handle_battery_service();
    }
    if (_flags & MOTE_SERVICE_PING){
      /* Handle ping service, since it is enabled */
      _handle_ping_service();
    }
    if (_flags & MOTE_SERVICE_NETWORK_STATUS){
      /* Handle networks service, since it is enabled */
      _handle_network_status_service();
    }

    if (t_last_minute == 0 || (t_now - t_last_minute) >= ONE_MINUTE_INTERVAL) {
      /* tasks executed one time per minute */
      dputs(" One minute tasks ");

      t_last_minute=t_now;
      rmt_check_ttl();

      if (_flags & MOTE_SERVICE_MESH) {
          /* Decrease routing entries life time */
          mesh_decrease_life_time();
      }
    }
}

/**
 * @brief       Handles the mote state machine.
 */
static void _handle_interval(void) {
    /*dputs("[_handle_interval]");*/

    /* Holds the time at which we scan for the last position message */
    static uint64_t t_last_position = 0;
    static uint64_t t_last_scan_for_connectivity = 0;

    uint64_t t_now;

    /* Handle services that do not depend on BLE */
    _handle_additional_services();

    switch (_state) {
    case MOTE_STATE_STANDBY:

        if (_flags & MOTE_SERVICE_POSITION) {
          /* Position service is active */

            /* Read current time */
            t_now = sysclk_get();

            if (t_last_position == 0 || (t_now - t_last_position) >= position_interval) {
                /* It's time to create a new position message */
                dputs("[_handle_interval] new position needed!");
                t_last_position= t_now;
                _scanning_start();
                _sched_id = sched_timeout(&_handle_interval, SCANNING_DURATION);
                return;
              }
        }

        if (_flags & MOTE_SERVICE_PROXIMITY) {
            /* The enabled services require constant scanning, as such, we
               need to go into the scanning state */
            dputs("[_handle_interval] new proximity needed!");
            _scanning_start();
            _sched_id = sched_timeout(&_handle_interval, SCANNING_DURATION);
            return;
        }

        if (queue_peek() != NULL) {
            /* We have new data to be sent over to the next hop */
            /*dputs("[_handle_interval] WE have data to send!");*/
            if (mesh_next_hop_select() != NULL) {
                /* But we still have connectivity, as such, we can go
                   directly to the connection state */
                _connection_create(mesh_next_hop_select());
                _timeout_id = sched_timeout(&_connection_cancel, TIMEOUT_DURATION);
                return;

            } else {
                /* But we have no connectivity, as such, we need to go into
                   scanning to find nearby devices */
                  /*dputs("[_handle_interval] But we have no connectivity!");*/
                   /* Read current time */
                   t_now = sysclk_get();

                   /* To prevent permanent scannigns for connectivity, check last scanning time */
                   if (t_last_scan_for_connectivity == 0 || (t_now - t_last_scan_for_connectivity) >= wait_for_next_scan) {
                       /* Yes, we can scan again */
                       dputs("[_handle_interval] scan for connectivity!");
                       if(wait_for_next_scan < WAIT_FOR_SCAN_MAX ){
                            /* We still don't have connectivity, increase waiting time */
                            dprintf("update wait_for_next_scan %ld\r\n",wait_for_next_scan);
                             wait_for_next_scan *= 2;
                        }

                       t_last_scan_for_connectivity = t_now;
                       _scanning_start();
                       _sched_id = sched_timeout(&_handle_interval, SCANNING_DURATION);
                       return;
                     }
            }
        } else {
            /*dputs("[_handle_interval] We have no data to send!");*/
        }

        /* No enabled service requires constant scanning and we have no data
           to be sent over to the next hop */
        if (_flags & MOTE_SERVICE_MESH) {
            /* The mesh service is enabled */
            if (mesh_next_hop_select() != NULL) {
                /* And we have connectivity, go to advertising */
                _advertising_start();
                _sched_id = sched_timeout(&_handle_interval, ADVERTISING_DURATION);

            } else {
                /* But we have no connectivity, go to scanning */
                _scanning_start();
                _sched_id = sched_timeout(&_handle_interval, SCANNING_DURATION);
            }

        } else {
            /* The mesh service is not enabled, remain in standby */
            _sched_id = sched_timeout(&_handle_interval, _random_sleep_ms());
        }

        break;

    case MOTE_STATE_SCANNING:
        /* We just finished scanning, as such we can assume that we have new
           data about the surrounding devices, but first stop scanning and
           handle the enabled services */
        _scanning_stop();
        _handle_services();

        dputs("[mote] We just finished scanning");

        if(mesh_next_hop_select() != NULL){
            /* We have connectivity, reset waiting time for sequencial scans */
            wait_for_next_scan= WAIT_FOR_SCAN_INIT;
        }

        if (queue_peek() != NULL && mesh_next_hop_select() != NULL) {
            /* We have a valid next hop and new data to be sent, as such we
               will create a connection, so that we can send the data to the
               peer device */
            _connection_create(mesh_next_hop_select());
            _timeout_id = sched_timeout(&_connection_cancel, TIMEOUT_DURATION);

        } else {
            dputs("[mote] no connectivity for connection or no data to send");

            /* We either don't have a valid next hop or no new data to be sent;
               That means that we cannot create a connection */
            if (_flags & MOTE_SERVICE_MESH) {
                /* The mesh service is enabled, so we'll take our time to
                   advertise ourselves, making sure we update nearby devices
                   using our own information */
                _advertising_start();
                _sched_id = sched_timeout(&_handle_interval, ADVERTISING_DURATION);

            } else {
                /* The mesh service is not enabled; As such we have no need to
                   advertise ourselves, instead we'll just go to the standby
                   state to save power */
                _state = MOTE_STATE_STANDBY;
                _sched_id = sched_timeout(&_handle_interval, _random_sleep_ms());
            }
        }

        break;

    case MOTE_STATE_CONNECTION:
        /* We just finished a connection */
        if ((_flags & MOTE_SERVICE_MESH) && mesh_next_hop_select() != NULL) {
            /* We have the mesh service enabled and we have a valid next hop,
               as such, we can take the time to advertise ourselves */
            _advertising_start();
            _sched_id = sched_timeout(&_handle_interval, ADVERTISING_DURATION);

        } else {
            /* We don't have the mesh service enabled or a valid next hop;
               As such we have no need to advertise ourselves, we can go
               directly to the standby state to save power */
            _state = MOTE_STATE_STANDBY;
            _sched_id = sched_timeout(&_handle_interval, _random_sleep_ms());
        }

        break;

    case MOTE_STATE_ADVERTISING:
        /* We just finished advertising and during that time we were able to
           update nearby devices or receive new information from them; From here
           let's just save some power and go the standby state */
        _advertising_stop();
        _state = MOTE_STATE_STANDBY;
        _sched_id = sched_timeout(&_handle_interval, _random_sleep_ms());
        break;
    }
}

/************************************ Public **********************************/

void sensoroid_mote_init(uint32_t flags,struct properties *_properties) {
    struct queue_element *element;

    /* _properties to intervals */
    battery_interval = _properties->battery.interval;
    ping_interval = _properties->ping.interval;
    network_status_interval = _properties->network.interval;
    position_interval = _properties->position.interval;

    /* Save service flags */
    _flags = flags;

    mesh_init(flags,_properties->mesh.interval);

    /* Initialize advertising data */
    _init_adv_data();

    /* Initialize base Sensoroid data */
    _sensoroid_data->type           = SENSOROID_TYPE_MOTE;
    _sensoroid_data->serial_node    = sensoroid_get_serial();
    _sensoroid_data->serial_gateway = 0;
    _sensoroid_data->seqno          = 0;
    _sensoroid_data->metric         = 0;

    /* Set constant data for advertising state */
    ll_set_advertising_params(_ADV_PARAMS);
    ll_set_scan_response_data(_SCAN_RSP_DATA, ARRAY_LEN(_SCAN_RSP_DATA));

    /* Set constant data for the scanning state */
    ll_set_advertising_report_handler(&_advertising_report_handler);
    ll_set_scan_params(_SCAN_PARAMS);

    /* ADD HELLO Message */
    element = queue_reserve();
    if (element != NULL) {
      /* Reset reason */
      size_t text_len;
      char * reset_text = reset_reason_get_text(&text_len);

      /* Hello payload */
      uint8_t c_hello_p[1 + 0x09 + text_len + 1];
      c_hello_p[0] = 0x09 + text_len;   /* Firmware version + reset_reasons text Size */
      memcpy(&(c_hello_p[1]), "Dandelion", 0x09);/* Firmware version text */
      memcpy(&(c_hello_p[10]), reset_text, text_len);/* Reset reasons text*/
      c_hello_p[1 + 0x09 + text_len] = 0x00; /* ICCID size */


      uint8_t c_hello[COLUMBUS_HEADER_SIZE + COLUMBUS_HELLO_DATA_SIZE + text_len];

      /* Insert initial "Hello" message into the queue */
      /* Initialize columbus message */
      columbus_set_message_version(c_hello, 1);
      columbus_set_serial(c_hello, sensoroid_get_serial());
      columbus_set_timestamp(c_hello, sysclk_get());
      columbus_set_message_type(c_hello, COLUMBUS_HELLO_TYPE);
      columbus_set_data_size(c_hello, COLUMBUS_HELLO_DATA_SIZE + text_len);

      /* Copy payload */
      memcpy(&c_hello[COLUMBUS_HEADER_SIZE],c_hello_p,COLUMBUS_HELLO_DATA_SIZE + text_len);

      /* Initialize element and commit it to the queue */
      element->size = COLUMBUS_HEADER_SIZE + COLUMBUS_HELLO_DATA_SIZE + text_len;
      memcpy(element->data, c_hello, element->size);
      queue_commit();
    }

    /* Schedule first state transition */
    _sched_id = sched_timeout(&_handle_interval, SCHED_TASK_MS_MIN);
}
