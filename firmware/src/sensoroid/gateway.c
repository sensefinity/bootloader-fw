/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     sensoroid
 * @{
 *
 * @file        gateway.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Tiago Tom�s <tiago.tomas@sensefinity.com>
 *
 * @}
 */
#include "sensoroid/gateway.h"

/* C standard library */
#include <stdint.h>
#include <inttypes.h>
#include <string.h>

/* Sensoroid library */
#include "sensoroid/sensoroid.h"

/* API */
#include "api/sched.h"
#include "api/sysclk.h"

/* BLE stack */
#include "ble/l2cap.h"
#include "ble/ll.h"
#include "ble/ll_buffer.h"
#include "ble/ll_control.h"
#include "ble/ll_pdu.h"

/* Reverse routing */
#include "sensoroid/reverse_msg_table.h"
#include "sensoroid/routing_reverse.h"

/* Mesh */
#include "sensoroid/mesh.h"

/* Utilities */
#include "util/debug.h"
#include "util/reset_reason.h"

/* */
#include "sensoroid/queue.h"
#include "sensoroid/columbus.h"

/* Expansion protocol */
#include "expansion/expansion.h"
#include "expansion/expansion_definitions.h"
#include "expansion/exp_packet.h"
#include "expansion/expansion_services.h"
#include "expansion/uplink.h"

/*********************************** Private **********************************/

/**
 * @brief       Conditional re-definition of the MIN macro.
 */
#ifndef MIN
#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#endif

/**
 * @brief       Holds the routing sequence number increment periodicity
 */
#define ROUTING_SN_PERIOD    (1000 * 60) /* 10 sec */

/**
 * @brief       Computes the static array length.
 */
#define ARRAY_LEN(x) ((sizeof(x)) / sizeof((x)[0]))

/**
 * @brief       Holds the BLE advertising data.
 */
static uint8_t _adv_data[7 + sizeof(struct sensoroid_data)];

/**
 * @brief       Holds the BLE scan response data.
 */
static const uint8_t _SCAN_RSP_DATA[] = {
    0x0a,           /* Length */
    0x09,           /* Type: Complete Local Name */
    'S', 'e', 'n', 's', 'o', 'r', 'o', 'i', 'd',

    0x05,           /* Length */
    0x12,           /* Type: Slave Connection Interval Range */
    0x50, 0x00,     /* Minimum: 0x0050 * 1.25 ms => 100 ms */
    0x20, 0x03,     /* Maximum: 0x0320 * 1.25 ms => 1000 ms */

    0x02,           /* Length */
    0x0a,           /* Type: Tx Power Level */
    0x00            /* Value: 0 dBm */
};

/**
 * @brief       Holds a pointer to the Sensoroid data.
 */
static struct sensoroid_data *_sensoroid_data =
        (struct sensoroid_data *) (_adv_data + 7);

/**
 * @brief       Holds the current scheduling indentifier.
 */
static sched_id_t _sched_id;


/**
 *  @brief      Holds the state for the connectivity of the gateway interface
 */
static bool interface_connectivity = true;


/**
 * @brief       Self-enumeration of the current gateway state.
 * @details     GATEWAY_CONTINUE_STATE, continue on-going operation,
 *              GATEWAY_CHANGING_STATE, change operation based on
 *              connectivity state of the interface.
 */
enum gateway_state {
    GATEWAY_CONTINUE_STATE              = 0,
    GATEWAY_CHANGING_STATE,
} gw_state = GATEWAY_CONTINUE_STATE;


/**
 * @brief       Holds the BLE advertising parameters.
 */
static struct ll_advertising_params _adv_params = {
    .type           = LL_ADVERTISING_TYPE_CONNECTABLE_UNDIRECTED,
    .interval       = 0x00A0,       /* 100 ms  (min ) */
    .channel_map    = LL_ADVERTISING_CHANNEL_MAP_ALL
};

/**
 * @brief       Sets the timestamp on a Columbus message.
 * @param       message
 *              Pointer to the Columbus message.
 * @param       timestamp
 *              The timestamp to set.
 */
static void _columbus_set_timestamp(uint8_t *message, uint64_t timestamp) {
    int i;
    uint8_t *ptr = (uint8_t *) &timestamp;

    /* The timestamp always starts @ offset = 9 */
    for (i = (sizeof(uint64_t) - 1); i >= 0; --i) {
        message[9 + i] = *ptr++;
    }
}

/**
 * @brief       Obtains the timestamp of a Columbus message.
 * @param       message
 *              Pointer to the Columbus message.
 * @return      The timestamp of the Columbus message.
 */
static uint64_t _columbus_get_timestamp(const uint8_t *message) {
    int i;
    uint64_t timestamp;
    uint8_t *ptr = (uint8_t *) &timestamp;

    /* The timestamp always starts @ offset = 9 */
    for (i = (sizeof(uint64_t) - 1); i >= 0; --i) {
        *ptr++ = message[9 + i];
    }

    return timestamp;
}

/**
 * @brief       Retrieves the serial number of a Columbus message.
 * @param       message
 *              Pointer to the message.
 * @return      The serial number.
 */
static uint64_t _columbus_get_serial(const uint8_t *message) {
    int i;
    uint64_t serial;
    uint8_t *ptr = (uint8_t *) &serial;

    /* The timestamp always starts @ offset = 1 */
    for (i = (sizeof(uint64_t) - 1); i >= 0; --i) {
        *ptr++ = message[1 + i];
    }

    return serial;
}

/**/

static uint8_t uplink_seq_number = 0;

static uint64_t t_last_uplink_retry = 0;


static void _init_adv_data(void) {
    /**
    * @brief       Initializes the advertising data.
    */
    /* Initialize base data */
    _adv_data[0] = 0x02;        /* Length */
    _adv_data[1] = 0x01;        /* Type: Flags */
    _adv_data[2] = 0x06;

    /* Value of Flags: LE General Discoverable Mode | BR/EDR Not Supported */

    _adv_data[3] = 3 + sizeof(struct sensoroid_data);
    _adv_data[4] = 0xFF;        /* Type: Manufacturer-specific Data */
    _adv_data[5] = 0xFF;        /* Company Identifier (16-bit) */
    _adv_data[6] = 0xFF;
}


/**
 * @brief       Update advertising data, increment sequence number and
 *              verifies if the gateway interface has connectivity to
 *              dispatch the messages to machinates.
 */
static void _update_sensoriod_data(void) {

    /* Increment sequence number */
    ++_sensoroid_data->seqno;

    if (gw_state == GATEWAY_CHANGING_STATE) {
        /* We are in changing of state, we lost connectivity or have it again */

        if (interface_connectivity) {
            /* We have connectivity again */

            /* Restart metric */
            _sensoroid_data->metric = 0;

            /* TODO */
            /* Back to connectable advertising */
            /*_adv_params.type           = LL_ADVERTISING_TYPE_CONNECTABLE_UNDIRECTED;
            ll_set_advertising_params(_adv_params);*/

        } else {
            /* We lost connectivity */

            /* Set metric to MAX_METRIC */
            _sensoroid_data->metric         = MAX_METRIC;

            /* TODO */
            /* Set advertising non-connectable */
            /*_adv_params.type           = LL_ADVERTISING_TYPE_SCANNABLE_UNDIRECTED;
            ll_set_advertising_params(_adv_params);*/
        }

        /* Back to gateway continue state */
        gw_state = GATEWAY_CONTINUE_STATE;
    }

}

/**
 * @brief       Enters the advertising state.
 */
static void _advertising_start(void) {
    /* Set advertising data (or update it) and enable advertising */
    ll_set_advertising_data(_adv_data, ARRAY_LEN(_adv_data));
    ll_set_advertise_enable(true);
}

/**
 * @brief       Exits the advertising state.
 */
static void _advertising_stop(void) {
    /* Stop advertising */
    ll_set_advertise_enable(false);
}

/**
 * @brief       Handles the scheduling interval.
 */
static void _sched_interval(void) {
    _advertising_stop();
    _update_sensoriod_data();
    _advertising_start();
}

/**
 * @brief       Segments and sends a configuration message.
 * @param       reverse_message
 *              Pointer to the reverse message.
 * @return      A number of packets resulting from segmentation.
 */
static size_t _segment_and_send(struct msg_element *reverse_message) {
    struct l2cap_header *header;
    struct ll_pdu_data pdu;
    size_t len;
    size_t size = reverse_message->size;
    bool start = true;
    size_t packet_count = 0;
    uint8_t *ptr = reverse_message->msg;

    while (size != 0) {
        if (start) {
            /* Start of packet */
            start = false;

            /* Compute length of data */
            len = MIN(size, LL_PDU_DATA_PAYLOAD_SIZE - sizeof(struct l2cap_header));

            /* Initialize packet header */
            pdu.llid = LL_PDU_DATA_LLID_START;
            pdu.length = len + sizeof(struct l2cap_header);

            /* Initialize L2CAP header */
            header = (struct l2cap_header *) pdu.payload;
            header->length = size;
            header->channel_id = SENSOROID_L2CAP_ASSIGNED_NUMBER;

            /* Copy data into the PDU */
            memcpy(pdu.payload + sizeof(struct l2cap_header), ptr, len);

        } else {
            /* Continuation of packet */
            len = MIN(size, LL_PDU_DATA_PAYLOAD_SIZE);

            /* Initialize packet header */
            pdu.llid = LL_PDU_DATA_LLID_CONTINUATION;
            pdu.length = len;

            /* Copy data into the PDU */
            memcpy(pdu.payload, ptr, len);
        }

        /* Queue packet for transmission */
        ll_buffer_put(ll_get_connection_tx_buffer(), &pdu);

        /* Prepare next iteration */
        size -= len;
        ptr += len;
        ++packet_count;
    }

    return packet_count;
}

/**
 * @brief       Handles connection events from BLE.
 * @param       event
 *              The event to be handled.
 */
static void _connection_handler(enum ll_connection_event event) {
    static struct queue_element *element = NULL;
    static size_t bytes_received = 0;
    static uint64_t timestamp;
    static struct bdaddr peer_addr;
    struct l2cap_header *header;
    struct ll_pdu_data pdu;
    struct routing_reverse_entry reverse_entry;
    static int reverse_index;
    struct msg_element *reverse_message;
    static size_t reverse_packet_count = 0;

    switch (event) {
    case LL_CONNECTION_EVENT_CONNECTION_CREATED:
        /* A new connection was created, prevent scheduler from changing our
           data from our known state */
        sched_cancel(_sched_id);

        /* Retrieve address of peer */
        peer_addr = ll_connection_peer_addr();

        reverse_index = routing_reverse_search_msg(&peer_addr);
        routing_reverse_table_print();
        rmt_print();
        if (reverse_index != ROUTING_REVERSE_TABLE_CAPACITY) {
        	dputs("We have a reverse message to be sent...");
            /* We have a configuration message to be sent... */
            reverse_message = rmt_get_message(reverse_index);

            /* Update message TTL*/
            //routing_reverse_update_out_timestamp(reverse_message->msg);

            /* Segment and send */
            reverse_packet_count = _segment_and_send(reverse_message);

        } else {
            reverse_packet_count = 0;
        }

        /* Initialize reassembly state variables */
        element = NULL;
        bytes_received = 0;
        break;

    case LL_CONNECTION_EVENT_CONNECTION_LOST:
    case LL_CONNECTION_EVENT_CONNECTION_TERMINATED_OK:
    case LL_CONNECTION_EVENT_CONNECTION_TERMINATED_ERROR:
        /* The connection ended, make sure there is no pending data to be
           commited in case the connection was terminated with an error */
        if (queue_reserved()) {
            queue_release();
        }

        /* Go back to advertising, increasing our sequence number */
        ++_sensoroid_data->seqno;
        _advertising_start();
        _sched_id = sched_interval(&_sched_interval, ROUTING_SN_PERIOD);
        break;

    case LL_CONNECTION_EVENT_DATA_RECEIVED:
        /* We received new data, as such we need to handle it, making sure that
           we're communicating with a valid Sensoroid device */
        ll_buffer_get(ll_get_connection_rx_buffer(), &pdu);

        if (pdu.llid == LL_PDU_DATA_LLID_START) {
            /* This is the start of a new packet */
            timestamp = sysclk_get();
            queue_release();
            element = queue_reserve();

            if (element == NULL) {
                /* We have no more available space */
                ll_control_termination_procedure(0xFF);
                return;
            }

            /* Reset count for the number of received bytes */
            bytes_received = 0;

            /* Retrieve L2CAP header */
            header = (struct l2cap_header *) pdu.payload;

            if (header->channel_id != SENSOROID_L2CAP_ASSIGNED_NUMBER) {
                /* The L2CAP channel ID does not match our assigned number, so
                   we can conclude that we're not communicating with a valid
                   Sensoroid device */
                ll_control_termination_procedure(0xFF);
                return;
            }

            /* Initialize element data */
            element->size = header->length;

            /* Accumulate number of received bytes */
            bytes_received = pdu.length - sizeof(struct l2cap_header);

            /* Copy partial data */
            memcpy(element->data, pdu.payload + sizeof(struct l2cap_header),
                    bytes_received);

        } else {
            if (element == NULL || bytes_received == 0) {
                /* Invalid state */
                ll_control_termination_procedure(0xFF);
                return;
            }

            /* This is the continuation of a packet, copy data */
            memcpy(element->data + bytes_received, pdu.payload, pdu.length);

            /* Accumulate number of received bytes */
            bytes_received += pdu.length;
        }

        if (bytes_received >= element->size) {
            /* We have a full packet, perform timestamping */
            _columbus_set_timestamp(element->data,
                    timestamp - _columbus_get_timestamp(element->data));

            /* Update reverse routing table... */
            reverse_entry.origin_node_serial = _columbus_get_serial(element->data);
            reverse_entry.intermediate_node_address = peer_addr;
            reverse_entry.life_time = 0;
            routing_reverse_new_message(&reverse_entry);

            queue_commit();

            /* Gateway debug */
            dputs("[gateway] Message added to the queue!");

            dprintf("[gateway] Received from serial=0x%08x%08x  \r\n",
                    (unsigned) (_columbus_get_serial(element->data) >> 32),
                     (unsigned) (_columbus_get_serial(element->data)));

            if(element->data[17] == 6) {
                dprintf("[gateway] Measurement message \r\n");
                dprintf("[gateway] Measurement type = %02x \r\n", element->data[23]);

            }else if (element->data[17]==1) {
                dprintf("[gateway] Hello message \r\n");
            }

            dputchar('\r');
            dputchar('\n');

            /* Fini */
        }

        break;

    case LL_CONNECTION_EVENT_DATA_SENT:
        if (--reverse_packet_count == 0) {
            /* Configuration message sent; Delete */
            rmt_clear_index(reverse_index);
        }

        break;

    default:
        /* Ignore remaining events */
        return;
    }
}

/************************************ Public **********************************/

void sensoroid_gateway_init() {
    /* Initialize advertising data */
    _init_adv_data();

    /* Initialize base Sensoroid data */
    _sensoroid_data->type           = SENSOROID_TYPE_GATEWAY;
    _sensoroid_data->serial_node    = sensoroid_get_serial();
    _sensoroid_data->serial_gateway = sensoroid_get_serial();
    _sensoroid_data->seqno          = 0;

    /* Base initial metric on the gateway interface MAX no Up-link yet */
    _sensoroid_data->metric         = 0;

    /* Set constant data for advertising state */
    ll_set_advertising_params(_adv_params);
    ll_set_scan_response_data(_SCAN_RSP_DATA, ARRAY_LEN(_SCAN_RSP_DATA));

    /* Set (slave) connection event handler */
    ll_set_connection_event_handler(&_connection_handler);

    /* Start advertising */
    _advertising_start();

    /* Schedule an interval so that the sequence number increases */
    _sched_id = sched_interval(&_sched_interval, ROUTING_SN_PERIOD);


    /* ADD HELLO Message */
    struct queue_element * element;

    /* Reset reason */
    size_t text_len;
    char * reset_text = reset_reason_get_text(&text_len);

    /* Hello payload */
    uint8_t c_hello_p[1 + 0x09 + text_len + 1];
    c_hello_p[0] = 0x09 + text_len;   /* Firmware version + reset_reasons text Size */
    memcpy(&(c_hello_p[1]), "Dandelion", 0x09);/* Firmware version text */
    memcpy(&(c_hello_p[10]), reset_text, text_len);/* Reset reasons text*/
    c_hello_p[1 + 0x09 + text_len] = 0x00; /* ICCID size */


    uint8_t c_hello[COLUMBUS_HEADER_SIZE + COLUMBUS_HELLO_DATA_SIZE + text_len];

    element = queue_reserve();
    if (element != NULL) {
        /* Insert initial "Hello" message into the queue */
        /* Initialize columbus message */
        columbus_set_message_version(c_hello, 1);
        columbus_set_serial(c_hello, sensoroid_get_serial());
        columbus_set_timestamp(c_hello, sysclk_get());
        columbus_set_message_type(c_hello, COLUMBUS_HELLO_TYPE);
        columbus_set_data_size(c_hello, COLUMBUS_HELLO_DATA_SIZE + text_len);

        /* Copy payload */
        memcpy(&c_hello[COLUMBUS_HEADER_SIZE],c_hello_p,COLUMBUS_HELLO_DATA_SIZE + text_len);

        /* Initialize element and commit it to the queue */
        element->size = COLUMBUS_HEADER_SIZE + COLUMBUS_HELLO_DATA_SIZE + text_len;
        memcpy(element->data, c_hello, element->size);
        queue_commit();
    }

}

void sensoroid_gateway_resume(void) {
    _advertising_start();
}

void sensoroid_gateway_update_connectivity(bool state) {
    /* Update state */
    interface_connectivity = state;
    /* Set in changing state, to update advertising parameters */
    gw_state = GATEWAY_CHANGING_STATE;
}


void sensoroid_gateway_loop(void) {

	struct queue_element *element;
	uint64_t t_now;
	uint64_t t_message;

	/* TODO Just based on GSM for now */
	if(expansion_service_is_active(EX_PROTOCOL_APP_GSM) &&  expansion_tx_status_ok() ) {
		/* We have up-link, and expansion tx is OK, lets dispatch columbus messages */

		/* Get current system time */
		t_now = sysclk_get();

		if (t_last_uplink_retry == 0 || ((t_now - t_last_uplink_retry) >= UPLINK_SEND_RETRY_PERIOD)) {
			//dputs("[sensoroid_gateway_loop] New up-link retry!!");

			/* Update last retry */
		    t_last_uplink_retry = t_now;

			/* Peek the next element from the queue */
			element = queue_peek();


			/* Expansion payload size and total message size */
			size_t exp_payload_size = element->size;
		    size_t message_size = EX_PROTOCOL_HEADER_SIZE + exp_payload_size;

			if (element != NULL && c_queue_reserve(get_exp_tx_queue_pointer(),message_size)) {
				/* We have data to send and have free space in the Tx expansion queue */
				dputs("[sensoroid_gateway_loop]  We have data to send!!");
				/* Retrieve message's timestamp */
				t_message = _columbus_get_timestamp(element->data);

				/* Compute adjusted timestamp and set it on the message */
				_columbus_set_timestamp(element->data, t_now - t_message);

				/* Get up-link expansion serial */
				//TODO just works for now
				uint64_t expansion_serial = broadcast_serial;

				/* Create Expansion packet header */
				uint8_t exp_header[EX_PROTOCOL_HEADER_SIZE];
				/* Prevention */
				memset(exp_header,0x00,EX_PROTOCOL_HEADER_SIZE);

				/* Expansion header */
				exp_set_origin_serial(exp_header, sensoroid_get_serial());
				exp_set_dst_serial(exp_header, expansion_serial); /* Set expansion up-link board address */
				exp_set_message_type(exp_header, EX_PROTOCOL_COLUMBUS_MESSAGE); /* Columbus message */
				exp_set_message_from_base(exp_header);

				/* Uplink sub header */
				//uplink_set_direction(exp_header, UPLINK_FORWARD);
				//uplink_set_seq_number(exp_header, uplink_seq_number);

				exp_set_message_size(exp_header, exp_payload_size);


				uint8_t queue[message_size];

				/* Copy uplink Columbus message into the queue and commit it */
				/* Copy exp_header header*/
				memcpy(queue, exp_header, EX_PROTOCOL_HEADER_SIZE);
				/* Copy Columbus message */
				memcpy(&queue[EX_PROTOCOL_HEADER_SIZE], element->data, element->size);

				c_queue_copy_and_commit_data(get_exp_tx_queue_pointer(),queue, message_size);

				dprintf("[sensoroid_gateway_loop] message committed!");
				queue_delete();

			} else {
					/* We were not able to reserve space */
					return;
				}
		}

	} else {
		/*dprintf("[sensoroid_gateway_loop] GSM NOT active");*/
		}
}

void gateway_uplink_ack_received(uint8_t seq_number){

	dprintf("[gateway_uplink_ack_received]");
	/* Verify sequence number */
	if(uplink_seq_number==seq_number) {

		/* Matching uplink_seq_number, message is "saved" can be deleted here */
		queue_delete();

		/* "Increment" uplink_seq_number */
		if(uplink_seq_number != 0xFF){
			uplink_seq_number++;
		} else {
			uplink_seq_number = 0;
		}
	}

}
