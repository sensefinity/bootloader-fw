/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     application
 * @{
 *
 * @file        mesh.c
 * @brief       Multi-hop funtions, mesh routing table management
 *
 * @author      Rui Pires
 *
 * @}
 */

/* Interface */
#include "sensoroid/mesh.h"

/* Utilities */
#include "util/debug.h"
#include "sensoroid/mote.h"

/********************************** Private ***********************************/

/**
 * @brief       Holds the table of mesh entries
 */
static struct mesh_entry _mesh_table[MESH_TABLE_CAPACITY];

/**
 * @brief       Holds the number of entries in the _mesh_table
 */
static uint8_t _mesh_table_count = 0;

/**
 * @brief       Holds the number of connection failures
 */
static uint8_t _node_failure_counter = 0;

/**
 * @brief       Holds the service flags for this device.
 */
static uint32_t _flags;

/**
 * @brief       Holds the initial life time for each mesh routing entry.
 */
static int _mesh_init_life_time;
/*
*   Private API
*/

/**
 * @brief       Inserts a single device at the provided index in the mesh table
 * @param[in]   i, the index where to insert the new entry.
 * @param[in]   mesh_entry, pointer to the entry to be inserted at the specificed index.
 */
static void _mesh_insert(int i, const struct mesh_entry *mesh_entry) {
    int j;
    int grow = 0;

    if (i < 0 || i >= MESH_TABLE_CAPACITY) {
        /* Invalid index */
        return;
    }

    if (_mesh_table_count != MESH_TABLE_CAPACITY) {
        /* Insertion will cause the table to "grow" */
        grow = 1;
    }

    /* Shift elements to the right to make space for the new element */
    for (j = _mesh_table_count + grow - 1; j > i; --j) {
        _mesh_table[j] = _mesh_table[j - 1];
    }

    /* Insert new element */
    memcpy(&_mesh_table[i], mesh_entry, sizeof(struct mesh_entry));

    /* Modify number of elements in the table */
    _mesh_table_count += grow;
}

/**
 * @brief       Removes a single device by index from the mesh table
 * @param[in]   i, the index of the element to be removed.
 */
void _mesh_remove(int i) {

    if (i < 0 || i >= _mesh_table_count || _mesh_table_count == 0) {
        /* No device can be removed either due to an invalid index or the
           fact that no device exists in the table */
        return;
    }

    for (; i < (_mesh_table_count - 1); ++i) {
        _mesh_table[i] = _mesh_table[i + 1];
    }

    --_mesh_table_count;
}


/**
 * @brief       rssi weight to add to the metric value
 * @param[in]   rssi felt
 */
uint8_t _mesh_add_rssi_weight(int rssi){

  /* Assume values between -1 and -100, but expected are [-50,-80] */
  if(rssi > MESH_RSSI_THRESHOLD) return 0;
  else{
      if(rssi > -90) return 1;
      else{
        if(rssi > -95) return 2;
        else return 3;
      }
    }
}


/**
 * @brief      battery weight to add to the metric value
 */
uint8_t _mesh_add_battery_weight(){

  /* TODO getbattery() percentage */
/*
  uint8_t battery = xxx;
  return (100-battery)/(100/(1+ MESH_BATTERY_WEIGHT));
*/
  return 0;
}

/**
 * @brief       Increments routing metric, based on battery level, and rssi.
 * @param[in]   rssi felt.
 */
uint8_t _mesh_metric_plus(int rssi){

  return  MESH_HOP_COUNT_WEIGHT + _mesh_add_rssi_weight(rssi) + _mesh_add_battery_weight();
}

/* Entries types in the mesh table.
*  life time > 0, valid entrie.
*  life time =0; && metric = MAX_METRIC;invalid entrie wainting for a new one
*  saved to prevent loops
*/

/* Node failure -> wait for new routing message;
*  Decrease life time -> message is too old, wait for new routing message;
*/

/*
* Entries life time should be greater than routing periodicity, 3 times > for example.
*/

/*
*   Public API
*/


void mesh_init(uint32_t flags,uint64_t mesh_init_lt){

    /* Set inicial entries life_time */
    _mesh_init_life_time = mesh_init_lt;

    /* Save service flags */
    _flags = flags;
}

/**
 * @brief       New Routing message received, verify if its valid and save it
 *              in the mesh table
 * @param[in]   mesh_entry, point to the mesh entrie
 */
void mesh_new_routing_message(struct mesh_entry * mesh_entry){

  /* Update metric value */
  if (mesh_entry->metric < MAX_METRIC) {
      mesh_entry->metric += _mesh_metric_plus(mesh_entry->rssi);
  } else {
      /* Invalid Metric discard routing message */
      return;
  }

  dprintf("[mesh] mesh_new_routing_message serial=0x%08x%08x, addr=",
          (unsigned) (mesh_entry->node_serial >> 32), (unsigned) (mesh_entry->node_serial));
  for (int a = BDADDR_LEN - 1; a >= 0; --a) {
      dprintf("%02x", mesh_entry->addr.data[a]);
      if (a != 0) {
          dputchar(':');
      }
  }
  dputchar('\r');
  dputchar('\n');

  int i;
  for (i = MESH_TABLE_CAPACITY  - 1 ; i >= 0 ;i--){

    /* Reject routing message if:
    *  - Is a repeted message;
    *  - Bad metric;
    *  - Bad sequence_number;
    */

    /*   (_mesh_table[i].life_time > 0 || (_flags & MOTE_SERVICE_MESH))) */

    /* Routing message reply, reject if i am a router or i already have a valid entrie */
    if (mesh_entry->gateway_serial == _mesh_table[i].gateway_serial &&
        mesh_entry->node_serial == _mesh_table[i].node_serial &&
        mesh_entry->sequence_number == _mesh_table[i].sequence_number &&
        (_mesh_table[i].life_time > 0 || (_flags & MOTE_SERVICE_MESH)) ){
             dputs("Routing  reply");
             return;
         }


    /* Check if it is ofered a good metric */
    if ((mesh_entry->metric > (_mesh_table[i].metric + 10)) &&
          (_mesh_table[i].life_time > 0  ) ){
            dputs("VERY BAD metric!!");
            return;
      }

    /* Invalid sequence_number */
    if ( mesh_entry->gateway_serial == _mesh_table[i].gateway_serial &&
        mesh_entry->sequence_number < (_mesh_table[i].sequence_number - SEQUENCE_NUMBER_THRESHOLD_ACCEPTANCE) &&
        mesh_entry->sequence_number > (_mesh_table[i].sequence_number - GATEWAY_RESET_THRESHOLD) ){
          dputs(" Bad sequence_number !!");
          return;
        }

    /* Remove table entries if:
    *   - Its a new round sequence_number++;
    *   - Gateway restart new_sequence_number < older_sequence_number;
    */

    /* If it is a new routing round, sequence_number++ offered by the same node,
      remove older entries */
    if (mesh_entry->gateway_serial ==_mesh_table[i].gateway_serial &&
       (mesh_entry->node_serial == _mesh_table[i].node_serial &&
        mesh_entry->sequence_number >= _mesh_table[i].sequence_number)){
            _mesh_remove(i);
            continue;
            }

      /* If it is a new routing round, and we have a very old sequence_number++,
        remove entries */
      if (mesh_entry->gateway_serial ==_mesh_table[i].gateway_serial &&
          (mesh_entry->sequence_number > (_mesh_table[i].sequence_number + SEQUENCE_NUMBER_THRESHOLD_ACCEPTANCE)) ){
            _mesh_remove(i);
            continue;
          }

    /* Gateway restart, too old sequence_number */
    if (mesh_entry->gateway_serial ==_mesh_table[i].gateway_serial &&
        mesh_entry->sequence_number < (_mesh_table[i].sequence_number - GATEWAY_RESET_THRESHOLD) &&
        _mesh_table[i].life_time == 0){
            _mesh_remove(i);
            continue;
          }
  }


  /* Insertion in the mesh table */
  /* Find the best place to insert the new entry */
  for (i = 0; i < _mesh_table_count; ++i){

      if (mesh_entry->metric < _mesh_table[i].metric) {
          /* This is the insertion index! */
          break;
      }

      if (mesh_entry->metric == _mesh_table[i].metric) {
          /* We have the same number of hops; Decide by RSSI */
          if (mesh_entry->rssi > _mesh_table[i].rssi) {
              /* Consider this the insertion index */
              break;
          }
      }
  }

  if (i < MESH_TABLE_CAPACITY) {

      dprintf("[mesh]  Insert mesh_new_routing_message serial=0x%08x%08x, addr=",
              (unsigned) (mesh_entry->node_serial >> 32), (unsigned) (mesh_entry->node_serial));
      for (int a = BDADDR_LEN - 1; a >= 0; --a) {
          dprintf("%02x", mesh_entry->addr.data[a]);
          if (a != 0) {
              dputchar(':');
          }
      }
      dputchar('\r');
      dputchar('\n');

      /* We have a valid index for insertion */
      /* Set life_time */
      mesh_entry->life_time = _mesh_init_life_time;
      _mesh_insert(i, mesh_entry);

      if(i==0) _node_failure_counter = 0;
      //mesh_table_print();
  }
}


/**
 * @brief       Decrease entries life time, revome if needed
 */
void mesh_decrease_life_time(){
  dputs("[mesh] mesh_decrease_life_time invoked");
  int i, a;
  for (i = 0; i < _mesh_table_count; ++i) {

    /* "Remove" entry if life time is too low */
    if(_mesh_table[i].life_time == TIMER_DECREASE_COUNT ){
      /* Node is off
      *  Set life time to zero
      *  Set offered metric to the MAX.
      *  Reorder the table:
      *   - insert in new position
      *   - remove older
      */
      _mesh_table[i].life_time = 0;
      _mesh_table[i].metric=MAX_METRIC;

      if(_mesh_table_count > 1){

        for (a = 1; a < _mesh_table_count; ++a){

            if (_mesh_table[i].metric < _mesh_table[a].metric) {
                /* This is the insertion index! */
                break;
            }

            if (_mesh_table[i].metric == _mesh_table[a].metric) {
                /* We have the same number of hops; Decide by RSSI */
                if (_mesh_table[i].rssi > _mesh_table[a].rssi) {
                    /* Consider this the insertion index */
                    break;
                }
            }
        }

        if (a < MESH_TABLE_CAPACITY)
            _mesh_insert(a, &_mesh_table[i]);

        _mesh_remove(i);
        i--;
      }
       continue;
    }else{
        /* Reduce Life time for a valid entry */
        if(_mesh_table[i].life_time <= _mesh_init_life_time && _mesh_table[i].life_time != 0){
            _mesh_table[i].life_time -= TIMER_DECREASE_COUNT;
        }
    }
  }
  //mesh_table_print();
}

/**
 * @brief       Select next hop if it exists
 */
struct mesh_entry *mesh_next_hop_select() {
  if (_mesh_table_count == 0)
      return NULL;
  else {
      int i;
      for (i = 0; i < _mesh_table_count; ++i) {
          if (_mesh_table[i].life_time > 0 &&  _mesh_table[i].life_time <= _mesh_init_life_time){
              return &_mesh_table[i];
        }
      }
    }
    return NULL;
}

/**
 * @brief       Mesh table positon 1 failed, Increment failure_counter
 *              and update table.
 */

void mesh_node_failure(){

  /* Increment _node_failure_counter */
  _node_failure_counter++;

  if(_node_failure_counter < NODE_FAILURE_MAX){
    dputs("[mesh] Increment _node_failure_counter");
    return;
  }

  /*  _node_failure_counter reach the limit -> remove entry*/
  dputs("[mesh] _node_failure_counter high -> \"remove\" entry");
  _node_failure_counter = 0;

  /* Node used if off
  *  Set life time to zero
  *  Set offered metric to the MAX.
  *  Reorder the table:
  *   - insert in new position
  *   - remove older
  */

  uint8_t i;

  _mesh_table[0].life_time=0;
  _mesh_table[0].metric=MAX_METRIC;

  if(_mesh_table_count > 1){

    for (i = 1; i < _mesh_table_count; ++i){

        if (_mesh_table[0].metric < _mesh_table[i].metric) {
            /* This is the insertion index! */
            break;
        }

        if (_mesh_table[0].metric == _mesh_table[i].metric) {
            /* We have the same number of hops; Decide by RSSI */
            if (_mesh_table[0].rssi > _mesh_table[i].rssi) {
                /* Consider this the insertion index */
                break;
            }
        }
    }

    if (i < MESH_TABLE_CAPACITY)
        _mesh_insert(i, &_mesh_table[0]);

    _mesh_remove(0);

    }
}


/**
 * @brief       Verify if node has conectivity
 */
bool mesh_has_conectivity(){

  if (_mesh_table_count == 0)
      return false;

  uint8_t i;
  for (i = 0; i < _mesh_table_count; ++i) {
      if (_mesh_table[i].life_time > 0 &&  _mesh_table[i].life_time <= _mesh_init_life_time){
          return true;
        }
    }
    return false;
}

/**
 * @brief       Resets the routing table
 */
void mesh_reset(){
  memset(&_mesh_table,0,sizeof(struct mesh_entry)*MESH_TABLE_CAPACITY);
  _mesh_table_count = 0;
}


struct mesh_entry *mesh_get_entry(int index){
  return &_mesh_table[index];
}

int mesh_table_count(){
  return _mesh_table_count;
}


int8_t mesh_next_hop_get_signal(void){

	struct mesh_entry * mesh = mesh_next_hop_select();
	if(mesh!=NULL) {
		return mesh->rssi;
	} else {
		return 0;
	}
}



/*
* DEBUG
*/

/*
uint8_t mesh_get_table_count(){
  return _mesh_table_count;
}*/

void mesh_table_print(){

  uint8_t i;

  dprintf("###-  Table _mesh_table_count %d -###\n", _mesh_table_count);

  for (i = 0; i < MESH_TABLE_CAPACITY; ++i) {
    dprintf("I-%d, GADDR-%ld ",i,(long) _mesh_table[i].gateway_serial);
    dprintf("NADDR-%ld ",(long) _mesh_table[i].node_serial);
    dprintf("Sequence_number %d, metric %d, life_time %d, Rssi %d  \n", _mesh_table[i].sequence_number, _mesh_table[i].metric,_mesh_table[i].life_time, _mesh_table[i].rssi);
  }

  dprintf("###-  Table END -###\n");

}
