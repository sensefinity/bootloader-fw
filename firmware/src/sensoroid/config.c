/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     sensoroid
 * @{
 *
 * @file        config.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

/* Sensoroid library */
#include "sensoroid/sensoroid.h"

/* Applications */
#include "app/butterfinger.h"
#include "app/grinder.h"
#include "app/temperature.h"
#include "app/humidity.h"
#include "app/ir_distance_sensor.h"
#include "app/wine.h"
#include "app/barometer.h"
#include "app/accelerometer.h"
#include "app/orientation.h"
#include "app/motion.h"
#include "app/transient.h"
#include "app/ping.h"

#include "app/telco_power_management.h"

#include "sensoroid/gateway.h"

/* Peripherals */
#include "periph/ficr.h"
#include "periph/pin_name.h"

/* Utilities */
#include "util/debug.h"
#include "util/delay.h"

#include "sensoroid/gateway.h"
#include "app/positioning.h"

/* Expansion protocol */
#include "expansion/expansion_protocol.h"
#include "expansion/expansion_services.h"
#include "expansion/expansion_definitions.h"

#include "util/utils.h"


/*********************************** Private **********************************/

/* Helper macros */
#define streq(a, b)     (strcmp(a, b) == 0)
#define prefix(p, s)    (strncmp(p, s, strlen(p)) == 0)

/* Constants */
#define PAGE_SIZE       (1024)

/**
 * @brief       Configuration symbol provided to us by the linkerscript.
 */
extern char _configuration;

/**
 * @brief       Holds a buffer for handling the patching of new configuration.
 */
static char _cbuffer[PAGE_SIZE] __attribute__((aligned (4)));


/**
 * @brief       Holds the device configuration properties.
 */
static struct properties _properties;

/**
 * @brief       Splits an input string into lines.
 * @param       input
 *              Pointer to the input string. On the first call, this function
 *              expects a valid pointer to a string. On subsequent calls, this
 *              function expects a NULL pointer.
 * @param       buffer
 *              Output buffer for the line.
 * @return      A boolean value indicating whether a new line was read.
 */
 int readline(const char *input, char *buffer) {
    static const char *p = NULL;

    /* check whether we have a new string to be processed */
    if (input != NULL) {
        p = input;
    }

    /* no string to be processed */
    if (p == NULL) {
        return 0;
    }

    /* skip non-printable characters */
    while (*p != '\0' && !isprint((int) *p)) {
        ++p;
    }

    /* check whether we've reached the end of the string */
    if (*p == '\0') {
        p = NULL;
        return 0;
    }

    /* copy printable characters to the buffer */
    while (*p != '\0' && isprint((int) *p)) {
        *buffer++ = *p++;
    }

    /* finish string and return line */
    *buffer = '\0';
    return 1;
}


#if defined(COLD_TEST)
//#define DEVICE_SERIAL                   151608127200004001
//#define DEVICE_SERIAL                   161608127200003002
//#define DEVICE_SERIAL                   171608127200002003
#define DEVICE_SERIAL                   181608127200001004

#endif

#if defined(GATEWAY_TEST)
//#define DEVICE_SERIAL                   191608127200000005
#define DEVICE_SERIAL                   101608126200009006
//#define DEVICE_SERIAL                   111608126200008007
//#define DEVICE_SERIAL                   121608126200007008
#endif

/**
 * @brief       Sets the configuration defaults.
 */
static void set_defaults(void) {

#if defined(COLD_TEST)
    _properties.type                    = SENSOROID_TYPE_MOTE;
    _properties.interface               = INTERFACE_UNDEFINED;
    _properties.mesh.enabled            = true; /* mesh/advertising disabled */
    _properties.mesh.interval           = 50000; /* 5 sec with device route */
    _properties.battery.enabled         = false;
    _properties.battery.interval        = 900000;
    _properties.temperature.enabled     = true;
    _properties.temperature.interval    = 307200/*3686400*/; /* 1h sec*/
    _properties.humidity.enabled        = true;
    _properties.humidity.interval       = 307200/*3686400*/; /* 1h sec*/
    _properties.ping.enabled            = false;
    _properties.ping.interval           = 300000;
    _properties.position.enabled        = false;
    _properties.position.interval       = 7500;
    _properties.irds.enabled            = false;
    _properties.irds.interval           = 10000;
    _properties.network.enabled         = false;
    _properties.network.interval        = 10000;
    _properties.wine.enabled            = false;
    _properties.wine.interval           = 0;  /* Not applicable */
    _properties.grinder.enabled         = false;
    _properties.grinder.interval        = 0; /* Not applicable */
    _properties.barometer.enabled       = false;
    _properties.barometer.interval      = 10000;
    _properties.accelerometer.enabled   = false;
    _properties.accelerometer.interval  = 0; /* Not applicable */
    _properties.orientation.enabled     = false;
    _properties.orientation.interval    = 10000;
    _properties.motion.enabled          = false;
    _properties.motion.interval         = 0; /* Not applicable */
    _properties.transient.enabled       = false;
    _properties.transient.interval      = 0; /* Not applicable */
#elif defined(GATEWAY_TEST)
    _properties.type                    = SENSOROID_TYPE_GATEWAY;
    _properties.interface               = INTERFACE_BUTTERFINGER;
    _properties.mesh.enabled            = false; /* mesh/advertising disabled */
    _properties.mesh.interval           = 50000; /* 5 sec with device route */
    _properties.battery.enabled         = false;
    _properties.battery.interval        = 900000;
    _properties.temperature.enabled     = false;
    _properties.temperature.interval    = 307200/*3686400*/; /* 1h sec*/
    _properties.humidity.enabled        = false;
    _properties.humidity.interval       = 307200/*3686400*/; /* 1h sec*/
    _properties.ping.enabled            = true;
    _properties.ping.interval           = 1843200;
    _properties.position.enabled        = false;
    _properties.position.interval       = 7500;
    _properties.irds.enabled            = false;
    _properties.irds.interval           = 10000;
    _properties.network.enabled         = false;
    _properties.network.interval        = 10000;
    _properties.wine.enabled            = false;
    _properties.wine.interval           = 0;  /* Not applicable */
    _properties.grinder.enabled         = false;
    _properties.grinder.interval        = 0; /* Not applicable */
    _properties.barometer.enabled       = false;
    _properties.barometer.interval      = 10000;
    _properties.accelerometer.enabled   = false;
    _properties.accelerometer.interval  = 0; /* Not applicable */
    _properties.orientation.enabled     = false;
    _properties.orientation.interval    = 10000;
    _properties.motion.enabled          = false;
    _properties.motion.interval         = 0; /* Not applicable */
    _properties.transient.enabled       = false;
    _properties.transient.interval      = 0; /* Not applicable */
#else
    _properties.type = SENSOROID_TYPE_UNDEFINED;
    _properties.interface = INTERFACE_UNDEFINED;
    _properties.mesh.enabled = false;
    _properties.mesh.interval = 60000;
    _properties.battery.enabled = false;
    _properties.battery.interval = 900000;
    _properties.temperature.enabled = false;
    _properties.temperature.interval = 30000;
    _properties.humidity.enabled = false;
    _properties.humidity.interval = 0;
    _properties.ping.enabled = false;
    _properties.ping.interval = 300000;
    _properties.position.enabled = false;
    _properties.position.interval = 7500;
    _properties.irds.enabled = false;
    _properties.irds.interval = 10000;
    _properties.network.enabled = false;
    _properties.network.interval = 10000;
    _properties.wine.enabled = false;
    _properties.wine.interval = 0;  /* Not applicable */
    _properties.grinder.enabled = false;
    _properties.grinder.interval = 0; /* Not applicable */
    _properties.barometer.enabled = false;
    _properties.barometer.interval = 10000;
    _properties.accelerometer.enabled = false;
    _properties.accelerometer.interval = 0; /* Not applicable */
    _properties.orientation.enabled = false;
    _properties.orientation.interval = 10000;
    _properties.motion.enabled = false;
    _properties.motion.interval = 0; /* Not applicable */
    _properties.transient.enabled = false;
    _properties.transient.interval = 0; /* Not applicable */
#endif
}

/**
 * @brief       Retrieves the Sensoroid type by name.
 * @param       name
 *              The name of the type.
 * @return      The target type or undefined.
 */
static enum sensoroid_type get_type(const char *name) {
    if (streq(name, "GATEWAY")) return SENSOROID_TYPE_GATEWAY;
    if (streq(name, "BEACON")) return SENSOROID_TYPE_BEACON;
    if (streq(name, "MOTE")) return SENSOROID_TYPE_MOTE;
    return SENSOROID_TYPE_UNDEFINED;
}

/**
 * @brief       Converts the Sensoroid type to the corresponding string.
 * @param       type
 *              The type to be converted.
 * @return      The corresponding string.
 */
const char *get_type_name(enum sensoroid_type type) {
    if (type == SENSOROID_TYPE_GATEWAY) return "GATEWAY";
    if (type == SENSOROID_TYPE_BEACON) return "BEACON";
    if (type == SENSOROID_TYPE_MOTE) return "MOTE";
    return "UNDEFINED";
}

/**
 * @brief       Retrieves a service by name.
 * @param       name
 *              The name of the service.
 * @return      The service for the provided name or NULL if no service was
 *              found.
 */
static struct service *get_service(const char *name) {
    if (streq(name, "mesh")) return &_properties.mesh;
    if (streq(name, "battery")) return &_properties.battery;
    if (streq(name, "temperature")) return &_properties.temperature;
    if (streq(name, "humidity")) return &_properties.humidity;
    if (streq(name, "ping")) return &_properties.ping;
    if (streq(name, "position")) return &_properties.position;
    if (streq(name, "irds")) return &_properties.irds;
    if (streq(name, "network")) return &_properties.network;
    if (streq(name, "wine")) return &_properties.wine;
    if (streq(name, "grinder")) return &_properties.grinder;
    if (streq(name, "barometer")) return &_properties.barometer;
    if (streq(name, "accelerometer")) return &_properties.accelerometer;
    if (streq(name, "orientation")) return &_properties.orientation;
    if (streq(name, "motion")) return &_properties.motion;
    if (streq(name, "transient")) return &_properties.transient;
    return NULL;
}


/**
 * @brief       Retrieves a interface by name.
 * @param       name
 *              The name of the interface.
 * @return      The resulting interface or undefined.
 */
static enum interface get_interface(const char *name) {
    if (streq(name, "BUTTERFINGER")) return INTERFACE_BUTTERFINGER;
    if (streq(name, "ETHERNET")) return INTERFACE_ETHERNET;
    return INTERFACE_UNDEFINED;
}

/**
 * @brief       Converts a interface type to the corresponding string.
 * @param       value
 *              The interface to be converted.
 * @return      The corresponding string.
 */
const char *get_interface_name(enum interface value) {
    if (value == INTERFACE_BUTTERFINGER) return "BUTTERFINGER";
    if (value == INTERFACE_ETHERNET) return "ETHERNET";
    return "UNDEFINED";
}

/**
 * @brief       Parses the data for a service from the provided key and value.
 * @param       name
 *              The name of the service.
 * @param       value
 *              The value for the key.
 */
static void parse_service(const char *name, const char *value) {
    /* Retrieve service from the key */
    struct service *service = get_service(name);
    if (service == NULL) {
        dprintf("No service with name=%s was found\r\n", name);
        return;
    }

    dprintf("Service %s, enabled=", name);

    if (*value == '0') {
        /* Service is disabled, no need to parse anymore */
        dprintf("false\r\n");
        service->enabled = false;
        return;
    }

    /* Service is enabled */
    dprintf("true");
    service->enabled = true;
    dprintf(", interval=");

    ++value;
    if (*value == '\0') {
        /* No more configuration options */
        dprintf("%u [default]\r\n", (unsigned) service->interval);
        return;
    }

    if (*value == ',' && *(value + 1) != '\0') {
        ++value;

        /* Parse interval */
        service->interval = get_uint64_from_string(value);
        dprintf("%u\r\n", (unsigned) service->interval);
        return;
    }

    dprintf("%u [default, malformed]\r\n", (unsigned) service->interval);
}

/**
 * @brief       Parses a property, given the corresponding key and value.
 * @param       key
 *              The property key.
 * @param       value
 *              The property value.
 */
static void parse_property(const char *key, const char *value) {
    const char *name;

    if (streq(key, "type")) {
        /* Parse sensoroid type */
        _properties.type = get_type(value);
        dprintf("Set type=%s\r\n", get_type_name(_properties.type));

    } else if (streq(key, "interface")) {
        /* Parse gateway interface */
        _properties.interface = get_interface(value);
        dprintf("Set interface=%s\r\n", get_interface_name(_properties.interface));

    } else if (prefix("service", key)) {
        /* Parse service name from the key */
        name = key;
        while (*name != '\0' && *name != '.') {
            ++name;
        }

        if (*name == '\0' || *(++name) == '\0') {
            dprintf("Malformed key-value pair: %s=%s\r\n", key, value);
            return;
        }

        /* Parse service */
        parse_service(name, value);

    } else {
        /* Unknown key-value pair */
        dprintf("Unknown key-value pair: %s=%s\r\n", key, value);
    }
}

/**
 * @brief       Applies the previously parsed configuration.
 */
static void apply_configuration(void) {
    uint32_t flags = MOTE_SERVICE_NONE;

    switch (_properties.type) {

    case SENSOROID_TYPE_GATEWAY:

		/* Initialize Sensoroid gateway */
		sensoroid_gateway_init(_properties.interface);

		/* Initialize positioning, if enabled */
		if (_properties.positioning.enabled) {
			positioning_init(_properties.positioning.interval);
		}

		/* ADD up-link services*/
		activate_service(EX_PROTOCOL_APP_GSM);

		/* Initialize positioning, if enabled */
		if (_properties.ping.enabled) {
			dputs("PING!!");
		}

		break;

    case SENSOROID_TYPE_MOTE:
        /* Build mote service flags */
        if (_properties.mesh.enabled)       flags |= MOTE_SERVICE_MESH;
        if (_properties.position.enabled)   flags |= MOTE_SERVICE_POSITION;
        if (_properties.battery.enabled)    flags |= MOTE_SERVICE_BATTERY;
        if (_properties.ping.enabled)       flags |= MOTE_SERVICE_PING;
        if (_properties.network.enabled)    flags |= MOTE_SERVICE_NETWORK_STATUS;

        /* Initialize mote */
        sensoroid_mote_init(flags, &_properties);

        /* Initialize remaining applications */
        if (_properties.temperature.enabled) {
            dputs("[apply_configuration] activate temperature service ");
            activate_service(EX_PROTOCOL_APP_TEMPERATURE);
            //temperature_app_setup(_properties.temperature.interval);
        }

        if (_properties.humidity.enabled) {
            dputs("[apply_configuration] activate humidity service ");
            activate_service(EX_PROTOCOL_APP_HUMIDITY);
            //humidity_app_setup(_properties.temperature.interval);
        }

        if (_properties.barometer.enabled) {
            barometer_setup(_properties.barometer.interval);
        }

        if (_properties.accelerometer.enabled) {
            accelerometer_setup();

            if (_properties.orientation.enabled) {
                orientation_setup(_properties.orientation.interval);
            }

            if (_properties.motion.enabled) {
                //TODO: Parse it on .cfg!
                motion_setup(0,1,1,1,18,2);
            }

            if (_properties.transient.enabled) {
                //TODO: Parse it on .cfg!
                transient_setup(2,1,1,1,0,16,2);
            }
        }

        break;

    default:
        /* Invalid configuration */
        return;
    }
}

/**
 * @brief       Saves the configuration in the buffer to the flash.
 */
static void save_configuration(void) {
    /* Word-aligned pointers */
    uint32_t *dst = (uint32_t *) &_configuration;
    uint32_t *src = (uint32_t *) _cbuffer;

    /* Step 1: Erase configuration flash page */

    /* Enable erase mode */
    NRF_NVMC->CONFIG = (NVMC_CONFIG_WEN_Een << NVMC_CONFIG_WEN_Pos);

    while (NRF_NVMC->READY == NVMC_READY_READY_Busy) {
        /* Wait for erase mode ready */
    }

    /* Erase page */
    NRF_NVMC->ERASEPAGE = (uint32_t) dst;

    while (NRF_NVMC->READY == NVMC_READY_READY_Busy) {
        /* Wait for erase */
    }

    /* Turn off erase mode */
    NRF_NVMC->CONFIG = (NVMC_CONFIG_WEN_Ren << NVMC_CONFIG_WEN_Pos);

    while (NRF_NVMC->READY == NVMC_READY_READY_Busy) {
        /* Wait until ready */
    }

    /* Step 2: Write new configuration using word-aligned access */

    /* Compute number of words we're writing */
    size_t words = PAGE_SIZE / 4;

    while (words--) {
        /* Turn on write mode */
        NRF_NVMC->CONFIG = (NVMC_CONFIG_WEN_Wen << NVMC_CONFIG_WEN_Pos);

        while (NRF_NVMC->READY == NVMC_READY_READY_Busy) {
            /* Wait until ready */
        }

        /* Perform write */
        *dst++ = *src++;

        while (NRF_NVMC->READY == NVMC_READY_READY_Busy) {
            /* Wait for write */
        }

        /* Turn off write mode */
        NRF_NVMC->CONFIG = (NVMC_CONFIG_WEN_Ren << NVMC_CONFIG_WEN_Pos);

        while (NRF_NVMC->READY == NVMC_READY_READY_Busy) {
            /* Wait until ready */
        }
    }
}

/************************************ Public **********************************/

void sensoroid_configure(void) {
    /* line buffer */
    char line[128];
    char *key;
    char *value;
    int reading;
    uint8_t valid_config_data;

    /* set configuration defaults */
    set_defaults();

    /* retrieve device configuration */
    valid_config_data = true;
    const char *config = sensoroid_get_configuration();
    if (config == NULL) {
        dputs("No configuration present");
        valid_config_data = false;
    }

    if (valid_config_data == true) {
        /* read the first line */
        reading = readline(config, line);

        while (reading) {
            /* parse key and value */
            value = line;
            while (*value != '\0' && *value != '=') {
                ++value;
            }

            *value++ = '\0';
            key = line;

            /* Parse property */
            parse_property(key, value);

            /* read next line */
            reading = readline(NULL, line);
        }
    }

    /* Apply configuration - if no valid file, apply defaults */
    apply_configuration();
}

void sensoroid_configure_loop(uint64_t t_now) {
    switch (_properties.type) {

    case SENSOROID_TYPE_GATEWAY:

    	/* Dispatch columbus messages*/
    	sensoroid_gateway_loop();

         /* If positioning is enabled, loop it */
         if (_properties.positioning.enabled) {
             positioning_loop();
         }
         /* If Ping is enabled run!*/
         if (_properties.ping.enabled) {
        	 ping_loop(t_now, _properties.ping.interval);
         }

         break;

    case SENSOROID_TYPE_MOTE:
        if (_properties.temperature.enabled) {
        	temperature_loop(t_now,_properties.temperature.interval);

        }

        if (_properties.humidity.enabled) {
        	humidity_loop(t_now,_properties.humidity.interval);
        }

        if (_properties.barometer.enabled) {
            barometer_loop();
        }

        if (_properties.accelerometer.enabled) {
            accelerometer_loop();

            if (_properties.orientation.enabled) {
                orientation_loop();
            }

            if (_properties.motion.enabled) {
                motion_loop();
            }

            if (_properties.transient.enabled) {
                transient_loop();
            }
        }

        break;

    default:
        /* The remaining types don't need looping functions */
        return;
    }
}


const char *sensoroid_get_configuration(void) {
    const char *config = &_configuration;
    dprintf("Getting pointer to configuration @ 0x%p\r\n", config);
    if (*config == 0xFF) {
        /* Configuration block is erased */
        return NULL;
    }

    return config;
}

bool sensoroid_patch_configuration(const char *data) {
    /* Read configuration */
    const char *config = sensoroid_get_configuration();
    if (config == NULL) {
        /* No configuration present, use an empty string */
        _cbuffer[0] = '\0';
    } else {
        /* Copy available configuration */
        strcpy(_cbuffer, config);
    }

    dputs("Configuration patching requested");
    dputs("Current configuration:");
    dprintf("%s\r\n", _cbuffer);
    dputs("Patch:");
    dprintf("%s\r\n", data);

    /* Parse input data */
    char line[128];
    char key[64];
    int reading = readline(data, line);
    while (reading) {
        dprintf("Processing patch line: %s\r\n", line);

        /* Parse key */
        char *src = line;
        char *dst = key;
        while (*src != '\0' && *src != '=') {
            *dst++ = *src++;
        }

        *dst = '\0';

        /* Well-formed configuration; Verify if the key already exists */
        char *search = strstr(_cbuffer, key);
        if (search != NULL) {
            dputs("The key already exists, removing");
            /* The key already exists, as such, we'll delete the line by
               finding where it ends first */
            char *end = search;
            while (*end != '\0' && *end != '\n') {
                ++end;
            }

            if (*end == '\0') {
                /* The line we're trying to delete is the last line in
                   the configuration, as such we can delete the line by
                   simply placing the null character */
                *search = '\0';

            } else {
                /* Remove line by shifting all the character left (mind the
                   '\n' character) */
                ++end;
                while (*end != '\0') {
                    *search++ = *end++;
                }

                *search = '\0';
            }

            dputs("Configuration after removal:");
            dprintf("%s\r\n", _cbuffer);
        }

        dputs("Appending new line");

        /* Append new configuration line */
        dst = _cbuffer + strlen(_cbuffer);
        src = line;
        while (*src != '\0') {
            *dst++ = *src++;
        }

        /* Finish line and configuration string */
        *dst++ = '\r';
        *dst++ = '\n';
        *dst++ = '\0';

        dputs("Configuration after appending:");
        dprintf("%s\r\n", _cbuffer);

        /* Read next line */
        reading = readline(NULL, line);
    }

    dputs("Patching done, padding buffer to 1K (page size)");

    /* Pad buffer with zeros */
    size_t length = strlen(_cbuffer);
    memset(_cbuffer + length, 0, PAGE_SIZE - length);

    /* Write buffer back to memory */
    dputs("Saving configuration to FLASH...");
    save_configuration();
    dputs("Done!");

    /* Delay before exiting (to prevent others from resetting too soon) */
    delay_ms(2000);

    return true;
}
