/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     sensoroid
 * @{
 *
 * @file        c_queue.c
 *
 * @brief		Use of the circular queue to save messages.
 * 				First byte of the data saved indicates the next message size of the data
 * 				this way we can save any type of messages(columbus or expansion protocol)
 *
 * @author      Rui Pires
 *
 * @}
 */


/* Messages are saved in a circular queue, were the first byte indicates the next message size:
    [2|X|X|3|E|E|E|1|A|4|W|W|W|W| | | | | | |]
*/

#include "sensoroid/c_queue.h"
#include "sensoroid/circular_queue.h"


/*
	INIT
*/

void c_queue_init(circular_queue_t * me, size_t size, uint8_t * circular_queue_pointer) {
	/* Initializes the circular queue */
    circular_queue_init(me, size, circular_queue_pointer);
}

/*
	OPS
*/

bool c_queue_reserve(circular_queue_t * me, size_t size) {
	/* Try reserve space for the data plus one byte indicating the size */
    return circular_queue_reserve(me, size + 1);
}

bool c_queue_copy_and_commit_data(circular_queue_t * me , uint8_t * data, size_t size){

	/* Copy the data and one byte indicating their size */
    uint8_t aux_data[size + 1];
    aux_data[0] = (uint8_t) size;
    memcpy(&aux_data[1],data,size);
    return circular_queue_copy_and_commit_data(me, aux_data, size+1);
}

uint8_t c_queue_peek(circular_queue_t * me){

	/* Try Read the next data size */
    if(circular_queue_peek(me)!= NULL) {
        uint8_t *p;
        p = (uint8_t *) circular_queue_peek(me);
        return *p;
    } else {
        return 0;
    }

}

uint8_t* c_queue_message_peek(circular_queue_t * me, size_t msg_size) {

		if(msg_size!=c_queue_peek(me)) return NULL;
		else{
			return  (uint8_t *) (circular_queue_peek(me) + 1);
		}
}

bool c_queue_read(circular_queue_t * me , uint8_t * copy_buffer, size_t size) {

	/* Valid Read ?*/
	if(size!=c_queue_peek(me)) return false;

    /* Read the data and the first byte indicating the size */
	uint8_t aux_data[size + 1];
    if(circular_queue_read(me, aux_data, size + 1)){
    	/* Just "return" the data */
        memcpy(copy_buffer,&aux_data[1],size);
        return true;
    } else {
        return false;
    }
}

bool c_queue_delete(circular_queue_t * me , size_t size) {
	/* Valid delete? */
	if(size!=c_queue_peek(me)) return false;
    return circular_queue_delete(me, size + 1);
}

void c_queue_discard(circular_queue_t * me) {
	/* Discard reservation */
    circular_queue_discard(me);
}


/*
    STATS
*/

size_t c_queue_capacity(circular_queue_t * me) {
    return circular_queue_capacity(me);
}

size_t c_queue_free_space(circular_queue_t * me) {
    return circular_queue_free_space(me);
}

size_t c_queue_space_used(circular_queue_t * me){
    return circular_queue_space_used(me);
}

bool c_queue_is_full(circular_queue_t * me) {
    return circular_queue_is_full(me);
}

bool c_queue_is_empty(circular_queue_t * me) {
    return circular_queue_is_empty(me);
}


/*
	DEBUG
*/

/*
void c_queue_print_stats(circular_queue_t * me){
    circular_queue_print_stats(me);
}

void c_queue_print(circular_queue_t * me){
    circular_queue_print(me);
}
*/
