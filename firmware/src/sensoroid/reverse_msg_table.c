/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     sensoroid
 * @{
 *
 * @file        reverse_msg_table.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Rui Pires
 *
 * @}
 */
#include "sensoroid/reverse_msg_table.h"

#include <stdio.h>
#include <string.h>

#include "util/debug.h"

#include "api/sysclk.h"

#include "gama_node_header_message.h"

/*********************************** Private **********************************/

/**
 * @brief       Holds the element table.
 */
static struct msg_element _rmt_table[RMT_ELEMENTS];




bool _cmp_serial(uint8_t *msg, uint64_t serial2) {
  uint64_t msg_serial = gama_get_node_destination(msg);
  return (msg_serial == serial2);
}

/*********************************** Public **********************************/

bool rmt_insert(uint8_t *msg, uint16_t size){

  int i;

  for(i = 0; i<RMT_ELEMENTS; i++){

    if (_rmt_table[i].timestamp == 0) {

      /* Save message in the first free space */
      memcpy(&_rmt_table[i].msg, msg, size);
      _rmt_table[i].size = size;

      /* Update incoming timestamp, sum remaining */
      uint64_t timestamp = gama_get_node_timestamp(msg);
      _rmt_table[i].timestamp = sysclk_get() + timestamp;
      return true;
    }
  }
  return false;
}

struct msg_element *rmt_get_message(int index) {
    return &_rmt_table[index];
}

/*
uint8_t *rmt_search_msg(uint64_t origin_node_serial){

  int i;
  int life_time_min = RMT_LIFE_TIME_INIT + 1;
  int return_index = RMT_ELEMENTS;
  for(i = 0; i < RMT_ELEMENTS; i++){
     Search for the older message in the table
    if (cmp_serial(_rmt_table[i].msg, origin_node_serial) &&
            _rmt_table[i].life_time < life_time_min) {
         Update search
        return_index = i;
        life_time_min = _rmt_table[i].life_time;
      }
  }

  if (return_index != RMT_ELEMENTS) return &_rmt_table[return_index].msg[0];
  else return NULL;
}
*/

uint8_t *rmt_search_msg(uint64_t origin_node_serial){

  int i;
  int return_index = RMT_ELEMENTS;
  for(i = 0; i < RMT_ELEMENTS; i++){
    /* TODO Search for the older message in the table */
    if (_cmp_serial(_rmt_table[i].msg, origin_node_serial) ) {
       return _rmt_table[return_index].msg;
      }
  }
  return NULL;
}


void rmt_clear_index(int index){

  memset(&_rmt_table[index].msg, 0x00, RMT_MAX_ELEMENT_SIZE);
  _rmt_table[index].timestamp = 0;
  _rmt_table[index].size = 0;

}

void rmt_check_ttl(){
  int i;

  /* Read clock */
  uint64_t mytimestamp = sysclk_get() ;

  for(i = 0; i < RMT_ELEMENTS; i++){
    /* Check messages TTL */
    if (_rmt_table[i].timestamp < mytimestamp ) {
      /* TTL expired */
      rmt_clear_index(i);
      dputs("[rmt_check_ttl] delete message");
    }
  }
}


/*
* Debug
*/

void rmt_print() {

  int i;
  dprintf("###-  _reverse_routing_Table %d -###\n", RMT_ELEMENTS);
  for (i = 0; i < RMT_ELEMENTS; i++) {
      if (_rmt_table[i].timestamp != 0) {
        dprintf("I-%d, ORIGIN -%ld ",i,(long) gama_get_node_destination(_rmt_table[i].msg));
        dprintf("I-%d, TIMESTAMP -%ld ",i,(long) _rmt_table[i].timestamp);
      }
    }
    dprintf("###-  Table END -###\n");
}
