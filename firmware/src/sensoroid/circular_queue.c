/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     sensoroid
 * @{
 *
 * @file        circular_queue.c
 *
 * @brief		Circular buffer code, to save any type of data.
 * 				One byte of the total size is sacrificed to distinguish between an
 * 				empty and  an full queue.
 *
 *  			Start pointer, pointer to Write;
 *  			End pointer, pointer to Read;
 *  						[ | | | | | | | | ]
 *  			- S = E queue is Empty
 *  			- S+1 = E queue is full
 *
 * @author      Rui Pires
 *
 * @}
 */


#include "sensoroid/circular_queue.h"
#include "util/atomic.h"


/* -------------------------------------------------------------------------- */
/*          Private API                                                       */
/* -------------------------------------------------------------------------- */
bool _circular_queue_can_copy_sequencialy(circular_queue_t * me, size_t data_size) {

    if (me->p_start >=  me->p_end){
        return  (circular_queue_capacity(me) -  me->p_start) >= data_size ;
    }else{
        return (me->p_end - me->p_start - 1) >= data_size;
    }

}

bool _circular_queue_can_read_sequencialy(circular_queue_t * me, size_t data_size) {

    if (circular_queue_is_empty(me)) return false;
    if (me->p_start >  me->p_end){
        return  (me->p_start - me->p_end) >= data_size;
    }else{
        return  (circular_queue_capacity(me) -  me->p_end) >= data_size ;
    }
}

/* -------------------------------------------------------------------------- */
/*          Public API                                                        */
/* -------------------------------------------------------------------------- */

/*
	INIT
*/

void circular_queue_init(circular_queue_t * me, size_t size, uint8_t * circular_queue_pointer) {
    /* Initialize circular_queue structure */
    me->size = size;
    me->reserved = false;
    me->p_start = me->p_end = 0;

    me->circular_queue_pointer = circular_queue_pointer;
    /* Clear data, just because */
    memset(me->circular_queue_pointer, 0x00, me->size);
}


/*
	AUX
*/
void circular_queue_reset_pointers(circular_queue_t * me) {
    me->p_start = me->p_end = 0;
}

/*
	OPS
*/

bool circular_queue_reserve(circular_queue_t * me, size_t size) {

    bool b_return= false;
    atomic(
    	if (!me->reserved && (circular_queue_free_space(me) >= size)) {
            /* We can reserve the space needed */
            /* Set reservation flag active */
            me->reserved = true;
            /* Return to the free space beginning */
            b_return = true;
        }
    );

    /* Return */
	return b_return;
}

bool circular_queue_copy_and_commit_data(circular_queue_t * me , uint8_t * data, size_t size) {

    bool b_return= false;

    atomic(
        if (me->reserved && (circular_queue_free_space(me) >= size)) {
            /* We can copy the data */
            void *p = (void *) (me->circular_queue_pointer + me->p_start);
            /* Copy data */
            if(_circular_queue_can_copy_sequencialy(me,size)){
                /* Copy data sequentially */
                memcpy(p,data,size);
                me->p_start+=size;
            } else {
                /* Copy data partially */
                memcpy(p,data, me->size -  me->p_start);
                memcpy(me->circular_queue_pointer,&data[me->size -  me->p_start],size-(me->size -  me->p_start));
                me->p_start = size - (me->size -  me->p_start);
            }
             b_return= true;
        }

        /* Free reservation */
        me->reserved = false;
    );
     return b_return;
}

bool circular_queue_read_and_delete(circular_queue_t * me , uint8_t * copy_buffer, size_t size) {

    bool b_return= false;

    atomic(
        if(circular_queue_space_used(me)>=size){
            /* We have data to read */
            /* Copy data */
            if(_circular_queue_can_read_sequencialy(me,size)){
                /* Copy/read data sequentially*/
                memcpy(copy_buffer,(void *) (me->circular_queue_pointer + me->p_end), size);
                me->p_end +=size;
            } else {
                /* Copy/read data partially */
                memcpy(copy_buffer, (void *)me->circular_queue_pointer + me->p_end, me->size -  me->p_end);
                memcpy(&copy_buffer[me->size -  me->p_end], (void *) me->circular_queue_pointer, size-(me->size -  me->p_end));

                me->p_end = size-(me->size -  me->p_end);
            }
        }

        if(circular_queue_is_empty(me)) circular_queue_reset_pointers(me);

        b_return = true;
    );

    return b_return;
}


bool circular_queue_read(circular_queue_t * me , uint8_t * copy_buffer, size_t size) {

    bool b_return= false;

    atomic(
        if(circular_queue_space_used(me)>=size){
            /* We have data to read */
            /* Copy data */
            if(_circular_queue_can_read_sequencialy(me,size)){
                /* Copy/read data sequentially*/
                memcpy(copy_buffer,(void *) (me->circular_queue_pointer + me->p_end), size);
            } else {
                /* Copy/read data partially */
                memcpy(copy_buffer, (void *)me->circular_queue_pointer + me->p_end, me->size -  me->p_end);
                memcpy(&copy_buffer[me->size -  me->p_end], (void *) me->circular_queue_pointer, size-(me->size -  me->p_end));
            }
        }
        b_return = true;
    );
    return b_return;
}


bool circular_queue_delete(circular_queue_t * me , size_t size) {

    bool b_return= false;

    atomic(
        if(circular_queue_space_used(me)>=size){
            /* We have data to read */
            /* Copy data */
            if(_circular_queue_can_read_sequencialy(me,size)){
                me->p_end +=size;
            } else {
                me->p_end = size-(me->size -  me->p_end);
            }
        }

        if(circular_queue_is_empty(me)) circular_queue_reset_pointers(me);

        b_return = true;
    );

    return b_return;
}


void circular_queue_discard(circular_queue_t * me){
    atomic(
        if (me->reserved) me->reserved = false;
   );
}

void* circular_queue_peek(circular_queue_t * me) {
    void* p = NULL;
    atomic(
        if(!circular_queue_is_empty(me)){
            p = &me->circular_queue_pointer[me->p_end];
        }
    );
    return p;
}


/*
    STATS
*/

size_t circular_queue_capacity(circular_queue_t * me) {
    return me->size -1 ;
}

size_t circular_queue_free_space(circular_queue_t * me) {

    if (me->p_start >=  me->p_end){
        return circular_queue_capacity(me) - ( me->p_start - me->p_end);
    }else{
        return me->p_end - me->p_start - 1;
    }
}

size_t circular_queue_space_used(circular_queue_t * me) {
    return circular_queue_capacity(me) - circular_queue_free_space(me);
}

bool circular_queue_is_full(circular_queue_t * me) {
    return circular_queue_free_space(me) == 0;
}

bool circular_queue_is_empty(circular_queue_t * me) {
    return circular_queue_free_space(me) == circular_queue_capacity(me);
}




/*
	DEBUG
*/

/*
void circular_queue_print_stats(circular_queue_t * me) {

	dprintf(" p_start %d, p_end %d \n",me->p_start, me->p_end);
    dprintf(" Free space %d \n", circular_queue_free_space(me));
    dprintf(" Space used %d \n", circular_queue_space_used(me));
    dprintf(" Empty %d\n",circular_queue_is_empty(me));
    dprintf(" Full %d\n",circular_queue_is_full(me));

}

void circular_queue_print(circular_queue_t * me) {
	uint8_t e;
	printf("BB[");
	for(e = 0; e < me->size ; e++)
		printf("%02x,",me->circular_queue_pointer[e] & 0xff);
	printf("]\n");
}
*/
