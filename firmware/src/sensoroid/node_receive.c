#include "sensoroid/node_receive.h"

/**/

#include "sensoroid/columbus.h"
#include "gama_node_header_message.h"

#include "sensoroid/config.h"
#include "sensoroid/fw_reassemble_app.h"
#include "sensoroid/reverse_msg_table.h"

#include "util/debug.h"

#define GAMA_NODE_HEADER_SIZE_TEST		(26)

#define GAMA_START_VERSION 				(2)

#define GAMA_CONFIGURATION_MESSAGE_TYPE	(11)
#define GAMA_FIRMWARE_MESSAGE_TYPE		(7)

bool node_receive_is_msg_to_me(uint8_t * gama_msg) {

	/* Verify Gama version */
	if(columbus_get_message_version(gama_msg) < GAMA_START_VERSION) {
		 dputs("[node_receive_is_msg_to_me] COLUMBUS");
		if (columbus_get_serial(gama_msg) == sensoroid_get_serial()) return true;
		else return false;
	} else {
		/* Gama */
		dputs("[node_receive_is_msg_to_me] GAMA");
		/* Compare destination serial */
		dputs("[node_receive_is_msg_to_me] after");
		if (gama_get_node_destination(gama_msg) == sensoroid_get_serial()){
			return true;
		} else {
			return false;
		}
	}
	return false;
}


void node_receive_new_message(uint8_t * msg_pointer) {

	/* Separate messages based on their type */

	/* TODO not reassemble based on msg type */

	if(node_receive_is_msg_to_me(msg_pointer)) {

		switch (gama_get_node_message_type(msg_pointer)){

		case GAMA_CONFIGURATION_MESSAGE_TYPE:
			dputs("[node_receive_new_message] !");
			/* The configuration message is targeted at us; Update & Reset */
			dprintf("[node_receive_new_message] New configuration message: %s\r\n", ((char *) msg_pointer) + GAMA_NODE_HEADER_SIZE_TEST);
			if(sensoroid_patch_configuration(((char *)  msg_pointer) + GAMA_NODE_HEADER_SIZE_TEST)) {
				NVIC_SystemReset();
			}
			break;

		case GAMA_FIRMWARE_MESSAGE_TYPE:
			dprintf("[node_receive_new_message] New firmware message\r\n");
			/* Call firmware reassemble application */
			fw_reassemble_app_new_message(msg_pointer);
			break;

		default:
			dprintf("[node_receive_new_message] default \r\n");
			break;
		}

	} else {
		/* Downlink message not to me, add to the reverse message table */
		dputs("[node_receive_new_message] add to the reverse message table!");
		rmt_insert(msg_pointer, gama_get_node_data_size(msg_pointer) + GAMA_NODE_HEADER_SIZE_TEST);
	}


}
