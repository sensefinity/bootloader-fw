

#include "sensoroid/fw_reassemble_app.h"
#include "util/bitmap.h"

#include "util/debug.h"
#include "gama_node_header_message.h"


/* FOTA status fields */
static uint16_t current_sequence_number = 0;
static uint16_t segment_counter = 0;
static bool update_in_progress = false;
static uint32_t offset_sum=0;


#define FW_MESSAGE_PAYLOAD_SIZE   78


/* BIT MAP to save messages received */
#define BITMAP_SIZE			  (160)
static uint8_t bitmap[BITMAP_SIZE];


void reset_fw_reasemble_status(void);

void fw_reassemble_init(void) {
	bitmap_clean(bitmap,BITMAP_SIZE);
}

/* Assuming order */
void fw_reassemble_app_new_message(uint8_t * message) {

	dputs("[fw_reassemble_app_new_message]");

	uint16_t msg_sequence_number = gama_get_node_seq_number(message);
	uint16_t msg_segment = gama_get_node_seg_number(message);
	uint16_t msg_segment_count = gama_get_node_seg_count(message);

	dprintf("msg_sequence_number %04x \n", msg_sequence_number);
	dprintf("msg_segment %04x \n", msg_segment);
	dprintf("msg_segment_count %04x \n", msg_segment_count);


	/* Valid sequence number? */
	if(!update_in_progress || msg_sequence_number==current_sequence_number) {
		/* Current Segment, or new fw update */

		if(!update_in_progress) {
			dputs("[fw_reassemble_app_new_message] New firmware update is in progress ");
			reset_fw_reasemble_status();
			/* New firmware update is in progress */
			update_in_progress = true;
			current_sequence_number = msg_sequence_number;
		}

		dputs("[fw_reassemble_app_new_message] Valid message ");

		/* Valid segment? */
		if(!bitmap_is_value_present(bitmap, msg_segment)) {
			/* New segment received */
			dputs("[fw_reassemble_app_new_message]  New segment received! ");
			/* Write in flash*/
			uint8_t data_size;

			/* TODO check sum */
			if(msg_segment == msg_segment_count) {
				/* Last MSG */
				size_t cksum_size;
				uint8_t sec = gama_get_node_security_type(message);
				if(sec == 0x00)	cksum_size = 2;
				else	cksum_size = 4;
				data_size = gama_get_node_data_size(message) - cksum_size;
			} else {
				/* Intermediate */
				data_size = gama_get_node_data_size(message);
			}

			dprintf("offset %d \n", offset_sum);
			dprintf("data_size %d \n", data_size);
			/* TODO if data_size !=0 -> write(offset_sum, message + header_size , data_size)*/
			if(1) {
				/* If write OK Update bitmap */
				segment_counter++;
				offset_sum += data_size;
				bitmap_update(bitmap,msg_segment);
			}

			if(segment_counter == (msg_segment_count+1) && bitmap_is_complete(bitmap,msg_segment_count)){
				/* Try Build new firmware */
				dputs("[fw_reassemble_app_new_message]  Try build new FW! ");

			}
		} else {
			dputs("[fw_reassemble_app_new_message] Repeated segment received! ");
		}

	} else {
		/* Different sequence number and we have one update in progress, discard message */
		dputs("[fw_reassemble_app_new_message]  Different sequence number!! ");
	}
}


void reset_fw_reasemble_status(void) {
	bitmap_clean(bitmap,BITMAP_SIZE);
	update_in_progress = false;
	current_sequence_number = 0;
	segment_counter = 0;
	offset_sum = 0;
}
