/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     application
 * @{
 *
 * @file        routing.c
 * @brief       Reverse messages routing implementation
 *
 * @author      Rui Pires
 *
 * @}
 */

/* Interface */
#include "sensoroid/routing_reverse.h"

/* Utilities */
#include "util/debug.h"

#include <stdio.h>
#include <string.h>

/*  API */
#include "api/sysclk.h"


/********************************** Private ***********************************/

/**
 * @brief       Holds the table of reverse routing entries
 */
static struct routing_reverse_entry _reverse_routing_table[ROUTING_REVERSE_TABLE_CAPACITY];

/**
 * @brief       Clear index table entry
 */
void routing_clear_entry(int index){
  memset(&_reverse_routing_table[index], 0 ,sizeof(struct routing_reverse_entry));
}

/**
 * @brief       Compare two ble addresses
 * @param[in]   struct bdaddr *address1, struct bdaddr *address2
 * @param[out]  boolean false if addresses are equal.
 */
bool _memcmp_ble_addr(struct bdaddr *address1, struct bdaddr *address2){
  return memcmp(address1->data, address2->data, BDADDR_LEN);
}


/**
 * @brief       Sets the timestamp on a Columbus message.
 * @param       message
 *              Pointer to the Columbus message.
 * @param       timestamp
 *              The timestamp to set.
 */
static void _columbus_set_timestamp(uint8_t *message, uint64_t timestamp) {
    int i;
    uint8_t *ptr = (uint8_t *) &timestamp;

    /* The timestamp always starts @ offset = 9 */
    for (i = (sizeof(uint64_t) - 1); i >= 0; --i) {
        message[9 + i] = *ptr++;
    }
}

/**
 * @brief       Obtains the timestamp of a Columbus message.
 * @param       message
 *              Pointer to the Columbus message.
 * @return      The timestamp of the Columbus message.
 */
static uint64_t _columbus_get_timestamp(const uint8_t *message) {
    int i;
    uint64_t timestamp;
    uint8_t *ptr = (uint8_t *) &timestamp;

    /* The timestamp always starts @ offset = 9 */
    for (i = (sizeof(uint64_t) - 1); i >= 0; --i) {
        *ptr++ = message[9 + i];
    }

    return timestamp;
}


/********************************** Public ***********************************/

bool routing_reverse_new_message(struct routing_reverse_entry * routing_reverse_entry){

  /* Set new entry life time */
  routing_reverse_entry->life_time = ROUTING_REVERSE_LIFE_TIME_INIT;

  /* Free space index */
  int free_index = ROUTING_REVERSE_TABLE_CAPACITY;

  int i;
  /* Verify if entry already exits*/
  for(i = 0; i< ROUTING_REVERSE_TABLE_CAPACITY; i++) {

    if(routing_reverse_entry->origin_node_serial == _reverse_routing_table[i].origin_node_serial){
        /* Entry already exits, update */
        memcpy(&_reverse_routing_table[i], routing_reverse_entry, sizeof(struct routing_reverse_entry));
        return true;
      }

    /* Look for the first free space */
    if ( _reverse_routing_table[i].origin_node_serial == 0 &&
       free_index == ROUTING_REVERSE_TABLE_CAPACITY)
          free_index = i;
  }

  if(free_index != ROUTING_REVERSE_TABLE_CAPACITY) {
    /* New entry and we have free space, save the new element */
    memcpy(&_reverse_routing_table[free_index], routing_reverse_entry, sizeof(struct routing_reverse_entry));
    return true;
  } else {
    /* We cannot save the new intry, return false */
    return false;
   }

}

void routing_reverse_decrease_life_time(){

  int i;
  /* Loop for each entry and decrease their life_time */
  for(i = 0; i< ROUTING_REVERSE_TABLE_CAPACITY; i++){

    /* Clear entry if their life_time is too low */
    if(_reverse_routing_table[i].life_time <= TIMER_DECREASE_COUNT)
      routing_clear_entry(i);
    else _reverse_routing_table[i].life_time -= TIMER_DECREASE_COUNT;
  }

}

struct bdaddr *routing_reverse_next_hop_select(uint64_t origin_node_serial){

  int i;
  /* Loop for each entry */
  for (i = 0; i< ROUTING_REVERSE_TABLE_CAPACITY; i++) {

    /* Verify if we have routing for the specific origin node */
    if (_reverse_routing_table[i].origin_node_serial == origin_node_serial)
        return &_reverse_routing_table[i].intermediate_node_address;
  }
  return NULL;
}


void routing_reverse_node_failure(struct bdaddr node_address){

  int i;
  /* Loop for each entry */
  for (i = 0; i< ROUTING_REVERSE_TABLE_CAPACITY; i++) {
    /* Search for the specific node */
    if(!_memcmp_ble_addr(&_reverse_routing_table[i].intermediate_node_address,&node_address)){
        /* Clear all entries used by intermediate node */
        routing_clear_entry(i);
      }
  }
}


int routing_reverse_search_msg(struct bdaddr *node_address){

  int i;
  for (i = 0; i< ROUTING_REVERSE_TABLE_CAPACITY; i++) {
    /* Search for the specific node */
    if(!_memcmp_ble_addr(&_reverse_routing_table[i].intermediate_node_address, node_address)){
          dprintf("Routing_search I-%d, ORIGIN -%ld  \r\n",i,(long) _reverse_routing_table[i].origin_node_serial);
          /* Search for a message for this origin node and return their index */
          uint8_t *msg = rmt_search_msg(_reverse_routing_table[i].origin_node_serial);
          if(msg!=NULL) return i;
      }
  }
  return i;
}


void routing_reverse_reset(){
    memset(&_reverse_routing_table, 0 ,sizeof(struct routing_reverse_entry) * ROUTING_REVERSE_TABLE_CAPACITY );
}


void routing_reverse_update_out_timestamp(uint8_t *msg){

  /* Read clock  */
  uint64_t my_timestamp = sysclk_get() ;
  /* Read message timestamp */
  uint64_t msg_timestamp = _columbus_get_timestamp(msg);

  /* Compare both */
  if(msg_timestamp > my_timestamp){
    /* Message TTL is positive, update remaining */
    msg_timestamp -= my_timestamp;
    _columbus_set_timestamp(msg, msg_timestamp);

  }else{
    /* Message TTL is negative, set only 10 seconds (10 * 1000) */
    msg_timestamp = 10000;
    _columbus_set_timestamp(msg, msg_timestamp);
  }

}


/*
  DEBUG
*/

void routing_reverse_table_print(){

  int i;
  dprintf("###-  _reverse_routing_Table %d -###\n", ROUTING_REVERSE_TABLE_CAPACITY);
  for (i = 0; i < ROUTING_REVERSE_TABLE_CAPACITY; ++i) {
    dprintf("I-%d, ORIGIN -%ld ",i,(long) _reverse_routing_table[i].origin_node_serial);
    dprintf("Node Address = ");
    int a;
    for (a = BDADDR_LEN - 1; a >= 0; --a) {
        dprintf("%02x", _reverse_routing_table[i].intermediate_node_address.data[a]);
        if (a != 0) {
            putchar(':');
        }
    }
    dputchar('\r');
    dputchar('\n');
    dprintf("Life_time %d \n", _reverse_routing_table[i].life_time);
  }
  dprintf("###-  Table END -###\n");

}
