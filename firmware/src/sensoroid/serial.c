/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     sensoroid
 * @{
 *
 * @file        serial.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Rui Pires
 *
 * @}
 */



#include "sensoroid/serial.h"


/* Utilities */
#include "util/debug.h"
#include "sensoroid/config.h"


#include "util/utils.h"


/* Helper macros */
#define streq(a, b)     (strcmp(a, b) == 0)
#define prefix(p, s)    (strncmp(p, s, strlen(p)) == 0)

/* Constants */
#define PAGE_SIZE       (1024)
/**
 * @brief       Configuration symbol provided to us by the linkerscript.
 */
extern char _serial;


/* cold test */
//#define DEVICE_SERIAL                   151608127200004001
//#define DEVICE_SERIAL                   161608127200003002
//#define DEVICE_SERIAL                   171608127200002003
//#define DEVICE_SERIAL                   181608127200001004



/* Gateway */
//#define DEVICE_SERIAL                   191608127200000005
//#define DEVICE_SERIAL                   101608126200009006
//#define DEVICE_SERIAL                   111608126200008007
//#define DEVICE_SERIAL                   121608126200007008


#define DEVICE_SERIAL                   -5978986677809826781

/*************************PRIVATE*******************************/

static uint64_t device_serial = DEVICE_SERIAL;
//static uint64_t device_serial = 0xAABBCCDDAABBCCDD;


static const char *sensoroid_get_serial_page_pointer(void) {
    const char *serial = &_serial;
    dprintf("Getting pointer to serial @ 0x%p\r\n", serial);
    if (*serial == 0xFF) {
        /* Configuration block is erased */
        return NULL;
    }
    return serial;
}


/**
 * @brief       Parses and returns a serial number.
 * @param       value
 *              The value to be parsed.
 * @return      The parsed serial if not zero or the hardware device ID if
 *              zero.
 */
uint64_t get_serial_from_string(const char *value) {
    uint64_t serial = get_uint64_from_string(value);
    return serial;
}

/**
 * @brief       Parses a property, given the corresponding key and value.
 * @param       key
 *              The property key.
 * @param       value
 *              The property value.
 */
static void parse_property(const char *key, const char *value) {

     if (streq(key, "serial")) {
        /* Parse sensoroid serial number */
        uint64_t serial = get_serial_from_string(value);
        if(serial!=0ULL) {
        	device_serial = serial;
        	dprintf("Set serial to 0x%08x%08x\r\n",
        			(unsigned) (device_serial >> 32),
					(unsigned) (device_serial));
        } else{
        	dputs("Don't update serial! using default!");
        }
     } else {
        /* Unknown key-value pair */
        dprintf("Unknown key-value pair: %s=%s\r\n", key, value);
    }
}


/*************************PUBLIC*******************************/
uint64_t sensoroid_get_serial(void) {
	return device_serial;
}

void sensoroid_read_serial(void) {
	/* line buffer */
	    char line[128];
	    char *key;
	    char *value;
	    int reading;
	    uint8_t valid_config_data;


	    /* retrieve device configuration */
	    valid_config_data = true;
	    const char *serial = sensoroid_get_serial_page_pointer();
	    if (serial == NULL) {
	        dputs("No Serial present");
	        valid_config_data = false;
	    }

	    if (valid_config_data == true) {
	        /* read the first line */
	        reading = readline(serial, line);

	        while (reading) {
	            /* parse key and value */
	            value = line;
	            while (*value != '\0' && *value != '=') {
	                ++value;
	            }

	            *value++ = '\0';
	            key = line;

	            /* Parse property */
	            parse_property(key, value);

	            /* read next line */
	            reading = readline(NULL, line);
	        }
	    }




}
