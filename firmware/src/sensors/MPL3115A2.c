/**:
 * Copyright (c) 2016 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     sensors
 * @{
 *
 * @file        MPL3115A2.c
 * @brief       MPL3115A2 drivers
 *
 * @author      Tiago Nascimento <tiago.nascimento@sensefinity.com>
 *
 * @}
 */

/* Interface */
#include "sensors/MPL3115A2.h"
#include "sensors/MMA8652.h"

/********************************** Private ***********************************/

/**
 * @brief       It will contain the I2C configuration values
 */
static struct I2C_params _press_i2c_conf;

/**
 * @brief       It will store data from the MPL3115A2 sensor
 */
static struct MPL3115A2_data _mpl3115a2_data;

/**
 * @brief       The current output rate of the sensor
 */
uint8_t _output_rate;

/**
 * @brief       Clear data structure fields
 */
void mpl3115a2_clear_data(void) {
  _mpl3115a2_data.press_alt   = 0;
  _mpl3115a2_data.temperature = 0;
}

/**
 * @brief       Put the sensor on or off
 * @details     Set the power bit of the sensor.
 * @param       power_on
 *              True for power on the sensor, false for power off.
 */
bool mpl3115a2_set_power_bit(bool power_on) {
  uint8_t ctrl_1_reg[1];
  uint8_t ctrl_1_data[1];

  /* Read content of CTRL_REG_1 */
  ctrl_1_reg[0] = MPL3115A2_CTRL1_REG;
  i2c_write(ctrl_1_reg, 1, false);
  ctrl_1_data[0] = 0;
  i2c_read(ctrl_1_data, 1);

  if(power_on) {
    /* Set SBYB mask while holding all other values of CTRL_REG_1 */
    ctrl_1_data[0] |= MPL3115A2_CTRL1_SBYB_MASK;
  }
  else {
    /* Clear SBYB mask while holding all other values of CTRL_REG_1 */
    ctrl_1_data[0] &= ~MPL3115A2_CTRL1_SBYB_MASK;
  }

  i2c_write(ctrl_1_reg, 1, false);
  i2c_write(ctrl_1_data, 1, true);

  return true;
}

/**
 * @brief       Check the current output sample rate of the MPL3115a2 sensor
 * @details     Read the bit 3, 4 and 5 of the MPL3115A2_CTRL1 register.
 *							Next update the global variable output_rate.
 */
void mpl3115a2_get_output_rate(void) {
  uint8_t ctrl_1_reg[1];
  uint8_t ctrl_1_data[1];

  /* Read contents of CTRL_REG_1 */
  ctrl_1_reg[0] = MPL3115A2_CTRL1_REG;
  i2c_write(ctrl_1_reg, 1, false);
  ctrl_1_data[0] = 0;
  i2c_read(ctrl_1_data, 1);

  ctrl_1_data[0] &= MPL3115A2_CTRL1_OS_MASK;

  _output_rate = ctrl_1_data[0] >> MPL3115A2_CTRL1_OS_SHIFT;
}

/* -------------------------------------------------------------------------- */
/*          Public API                                                        */
/* -------------------------------------------------------------------------- */

bool mpl3115a2_reserve_i2c_bus(void) {
	  _press_i2c_conf.frequency  = I2C_FREQUENCY_K100;
	  _press_i2c_conf.scl        = MPL3115A2_SCL;
	  _press_i2c_conf.sda        = MPL3115A2_SDA;

	  /* Try to access the I2C bus */
	  if(!i2c_init(_press_i2c_conf)) {
	    return false;
	  }

		uint8_t who_am_i[1];
		i2c_set_address(MPL3115A2_ADDR);          /* set MPL3115A2 I2C address */
		who_am_i[0] = MPL3115A2_WHO_AM_I_REG;	    /* write who am i register address */

		i2c_write(who_am_i, 1, false); 			    /* do not send stop signal */
	  who_am_i[0] = 0;
		i2c_read(who_am_i, 1);  		            /* read 1 byte and send stop now */

	  if (who_am_i[0] != MPL3115A2_WHO_AM_I) {
	    /* There is some problem in communicating with the sensor. Let's free the I2C bus! */
	    i2c_shutdown();
	    dputs("[mma8652_reserve_i2c_bus] Seems that MPL3115A2 sensor isn't connected to the board");
	    return false;                       /* It seems that the sensor isn't connected to the board! */
	  }
	  else {
	    return true;
	  }
}

bool mpl3115a2_power_on(void) {
  dputs("[mpl3115a2_power_on] Powering on MPL3115A2 sensor...");

  /* Power on the sensor! */
  mpl3115a2_set_power_bit(true);

  mpl3115a2_get_output_rate();

  return true;
}

bool mpl3115a2_power_off(void) {
  dputs("[mpl3115a2_power_off] Shutting down MPL3115A2 sensor...");

  /* Power off the sensor! */
  mpl3115a2_set_power_bit(false);

  return true;
}

bool mpl3115a2_barometer_enable() {
	dputs("[mpl3115a2_barometer_enable] Setting barometer mode");

	uint8_t ctrl_1_reg[1];
	uint8_t ctrl_1_data[1];

	/* Read content of CTRL_REG_1 */
	ctrl_1_reg[0] = MPL3115A2_CTRL1_REG;
	i2c_write(ctrl_1_reg, 1, false);
	ctrl_1_data[0] = 0;
	i2c_read(ctrl_1_data, 1);

	//Barometer mode!
	ctrl_1_data[0] &= ~MPL3115A2_CTRL1_ALT_MASK;

	i2c_write(ctrl_1_reg, 1, false);
	i2c_write(ctrl_1_data, 1, true);

	return true;
}

bool mpl3115a2_altimeter_enable() {
	dputs("[mpl3115a2_altimeter_enable] Setting altimeter mode");

	uint8_t ctrl_1_reg[1];
	uint8_t ctrl_1_data[1];

	/* Read content of CTRL_REG_1 */
	ctrl_1_reg[0] = MPL3115A2_CTRL1_REG;
	i2c_write(ctrl_1_reg, 1, false);
	ctrl_1_data[0] = 0;
	i2c_read(ctrl_1_data, 1);

	//Altimeter mode!
	ctrl_1_data[0] |= MPL3115A2_CTRL1_ALT_MASK;

	i2c_write(ctrl_1_reg, 1, false);
	i2c_write(ctrl_1_data, 1, true);

	return true;
}

bool mpl3115a2_set_raw(bool raw_enable) {
	dputs("[mpl3115a2_set_raw] Setting raw mode");

	uint8_t ctrl_1_reg[1];
	uint8_t ctrl_1_data[1];

	/* Read content of CTRL_REG_1 */
	ctrl_1_reg[0] = MPL3115A2_CTRL1_REG;
	i2c_write(ctrl_1_reg, 1, false);
	ctrl_1_data[0] = 0;
	i2c_read(ctrl_1_data, 1);

	if(raw_enable) {
	  ctrl_1_data[0] |= MPL3115A2_CTRL1_RAW_MASK;
	}
	else {
	  ctrl_1_data[0] &= ~MPL3115A2_CTRL1_RAW_MASK;
	}

	i2c_write(ctrl_1_reg, 1, false);
	i2c_write(ctrl_1_data, 1, true);

	return true;
}

bool mpl3115a2_set_output_rate(uint8_t output_rate) {
  dputs("[mpl3115a2_set_output_rate] Setting output sample rate");

  /* Sanity check */
  assert((output_rate == MPL3115A2_CTRL1_OS_1) ||
    (output_rate == MPL3115A2_CTRL1_OS_2)   ||
    (output_rate == MPL3115A2_CTRL1_OS_4)   ||
    (output_rate == MPL3115A2_CTRL1_OS_8)   ||
    (output_rate == MPL3115A2_CTRL1_OS_16)  ||
    (output_rate == MPL3115A2_CTRL1_OS_32)  ||
    (output_rate == MPL3115A2_CTRL1_OS_64)  ||
    (output_rate == MPL3115A2_CTRL1_OS_128));

  /* Power off the sensor before set registers */
  mpl3115a2_set_power_bit(false);
  uint8_t ctrl_1_reg[1];
  uint8_t ctrl_1_data[1];

  /* Read contents of CTRL_REG_1 */
  ctrl_1_reg[0] = MPL3115A2_CTRL1_REG;
  i2c_write(ctrl_1_reg, 1, false);
  ctrl_1_data[0] = 0;
  i2c_read(ctrl_1_data, 1);

  /* Clear the bits of the output sample rate with the MPL3115A2_CTRL1_OS mask */
  ctrl_1_data[0] &= ~MPL3115A2_CTRL1_OS_MASK;
  /* Set the output sample rate and turn on again the sensor */
  ctrl_1_data[0] |= output_rate;
  i2c_write(ctrl_1_reg, 1, false);
  i2c_write(ctrl_1_data, 1, true);

  /* Power on the sensor again! */
  mpl3115a2_set_power_bit(true);

  _output_rate = output_rate;

  return true;
}

bool mpl3115a2_new_data_available(void) {
  uint8_t new_data[1];
  new_data[0] = MPL3115A2_DR_STATUS_REG;
  i2c_write(new_data, 1, false); 			   /* do not send stop signal */
  new_data[0] = 0;
	i2c_read(new_data, 1);  		           /* read 1 byte and send stop now */

  if (new_data[0]) {
    _mpl3115a2_data.status = 1;
    return true;
  }
  else {
    _mpl3115a2_data.status = 0;
    return false;
  }
}

struct MPL3115A2_data mpl3115a2_read_preassure(void) {
  dputs("[mpl3115a2_read_preassure] Reading new preassure value");

  /*
  ** Read 20 bit pressure data results and store in 8 bit registers
  */
  uint8_t preassure_info[3];

  preassure_info[0] = MPL3115A2_OUT_P_MSB_REG;
  i2c_write(preassure_info, 1, false); 			     /* do not send stop signal */
  memset(preassure_info, 0, sizeof(preassure_info));
  /* Read 3 byte of the pressure value in one read and send stop now */
	i2c_read(preassure_info, 3);

  /* Save new preassure info on the MPL3115A2 data structure */
  _mpl3115a2_data.press_alt |= (preassure_info[0] << 12) | (preassure_info[1] << 4) | (preassure_info[2] & MPL3115A2_OUT_P_LSB_MASK);

  return _mpl3115a2_data;
}

struct MPL3115A2_data mpl3115a2_read_temperature(void) {
  dputs("[mpl3115a2_read_temperature] Reading new temperature value");

  /*
  ** Read 12 bit temperature data results and store in 8 bit registers
  */
  uint8_t temperature_info[2];

  temperature_info[0] = MPL3115A2_OUT_T_MSB_REG;
  i2c_write(temperature_info, 1, false); 			     /* do not send stop signal */
  memset(temperature_info, 0, sizeof(temperature_info));
  /* Read 2 byte of the temperature value in one read and send stop now */
	i2c_read(temperature_info, 2);

  /* Save new temperature info on the MPL3115A2 data structure */
  _mpl3115a2_data.temperature |= (temperature_info[0] << 4) | (temperature_info[1] & MPL3115A2_OUT_T_LSB_MASK);

  return _mpl3115a2_data;
}

struct MPL3115A2_data mpl3115a2_read_data(void) {
  mpl3115a2_clear_data();

  mpl3115a2_read_preassure();
  mpl3115a2_read_temperature();

  return _mpl3115a2_data;
}

struct MPL3115A2_data mpl3115a2_get_data(void) {
  return _mpl3115a2_data;
}
