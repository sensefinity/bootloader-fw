/**:
 * Copyright (c) 2016 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     sensors
 * @{
 *
 * @file        MMA8652.c
 * @brief       MMA8652 drivers
 *
 * @author      Tiago Nascimento <tiago.nascimento@sensefinity.com>
 *
 * @}
 */

/* Interface */
#include "sensors/MMA8652.h"

/********************************** Private ***********************************/

/**
 * @brief       It contains the I2C configuration values
 */
static struct I2C_params _press_i2c_conf;

/**
 * @brief       It stores xyz axis data from the MMA8652 sensor
 */
static struct MMA8652_xyz_data _mma8652_xyz_data;

/**
 * @brief       It stores motion data from the MMA8652 sensor
 */
static struct MMA8652_motion_data _mma8652_motion_data;

/**
 * @brief       It stores transient data from the MMA8652 sensor
 */
static struct MMA8652_transient_data _mma8652_transient_data;

/**
 * @brief       It stores orientation data from the MMA8652 sensor
 */
static struct MMA8652_orientation_data _mma8652_orientation_data;

/**
 * @brief       Indicates the dynamic range of the sensor
 */
uint8_t _dynamic_range;

/**
 * @brief       Indicates the current active data rate of the sensor
 */
uint8_t _active_data_rate;

/**
 * @brief       Indicates the current oversampling mode in active mode
 */
uint8_t _oversampling_active_mode;

/**
 * @brief       Indicates the current oversampling mode in sleep mode
 */
uint8_t _oversampling_sleep_mode;

/**
 * @brief       Indicates the current data filter status
 */
bool _hp_filter_status;

/**
 * @brief       Indicates the current high-pass filter cutoff frequency
 */
uint8_t _hp_cutoff_frequency;

/**
 * @brief       Indicates the current sleep data rate of the sensor
 */
uint8_t _sleep_data_rate;

/**
 * @brief       Indicates the current read mode of the sensor (false = normal read; true y= fast read)
 */
bool _fast_read;

/**
 * @brief       Clear xyz data structure fields
 */
void mma8652_clear_xyz_data(void) {
  _mma8652_xyz_data.x_raw  = 0;
  _mma8652_xyz_data.y_raw  = 0;
  _mma8652_xyz_data.z_raw  = 0;
  _mma8652_xyz_data.x_mg   = 0;
  _mma8652_xyz_data.y_mg   = 0;
  _mma8652_xyz_data.z_mg   = 0;
  _mma8652_xyz_data.status = false;
}

/**
 * @brief       Clear motion data structure fields
 */
void mma8652_clear_motion_data(void) {
  _mma8652_motion_data.x_motion   = false;
  _mma8652_motion_data.x_polarity = false;
  _mma8652_motion_data.y_motion   = false;
  _mma8652_motion_data.y_polarity = false;
  _mma8652_motion_data.z_motion   = false;
  _mma8652_motion_data.z_polarity = false;
  _mma8652_motion_data.status     = false;
}

/**
 * @brief       Clear transient data structure fields
 */
void mma8652_clear_transient_data(void) {
  _mma8652_transient_data.x_transient = false;
  _mma8652_transient_data.x_polarity  = false;
  _mma8652_transient_data.y_transient = false;
  _mma8652_transient_data.y_polarity  = false;
  _mma8652_transient_data.z_transient = false;
  _mma8652_transient_data.z_polarity  = false;
  _mma8652_transient_data.status      = false;
}

/**
 * @brief       Clear orientation data structure fields
 */
void mma8652_clear_orientation_data(void) {
  _mma8652_orientation_data.front           = false;
  _mma8652_orientation_data.landscape_right = false;
  _mma8652_orientation_data.portrait_up     = false;
  _mma8652_orientation_data.status          = false;
}

/**
 * @brief       Put the sensor on or off
 * @details     Set the power bit of the sensor.
 * @param       power_on
 *              True for power on the sensor, false for power off.
 */
bool mma8652_set_power_bit(bool power_on) {
  uint8_t ctrl_1_reg[1];
  uint8_t ctrl_1_data[1];

  /* Read content of CTRL_REG_1 */
  ctrl_1_reg[0] = MMA8652_CTRL_1_REG;
  i2c_write(ctrl_1_reg, 1, false);
  ctrl_1_data[0] = 0;
  i2c_read(ctrl_1_data, 1);

  if(power_on) {
    /* Set SBYB mask while holding all other values of CTRL_REG_1 */
    ctrl_1_data[0] |= MMA8652_CTRL1_ACTIVE_MASK;
  }
  else {
    /* Clear SBYB mask while holding all other values of CTRL_REG_1 */
    ctrl_1_data[0] &= ~MMA8652_CTRL1_ACTIVE_MASK;
  }

  i2c_write(ctrl_1_reg, 1, false);
  i2c_write(ctrl_1_data, 1, true);

  return true;
}

/**
 * @brief       Check the current dynamic range of the MMA8652 sensor
 * @details     Read the bit 1 and 2 of the MMA8652_XYZ_DATA_CFG register in order to check the
 *							current dynamic range. Update the global variable dynamic_range.
 */
void mma8652_get_dynamic_range(void) {
  uint8_t xyz_cfg_reg[1];
  uint8_t xyz_cfg_data[1];

  /* Read content of XYZ_DATA_CFG_REG */
  xyz_cfg_reg[0] = MMA8652_XYZ_DATA_CFG_REG;
  i2c_write(xyz_cfg_reg, 1, false);
  xyz_cfg_data[0] = 0;
  i2c_read(xyz_cfg_data, 1);

  xyz_cfg_data[0] &= MMA8652_XYZ_CFG_FS_MASK;

  _dynamic_range = xyz_cfg_data[0];
}

/**
 * @brief       Check the current active data rate of the MMA8652 sensor
 * @details     Read the bit 3, 4 and 5 of the CTRL_REG_1 register in order to check the
 *							current active data rate. Update the global variable active_data_rate.
 */
void mma8652_get_active_data_rate(void) {
    uint8_t ctrl_1_reg[1];
    uint8_t ctrl_1_data[1];

    /* Read content of CTRL_REG_1 */
    ctrl_1_reg[0] = MMA8652_CTRL_1_REG;
    i2c_write(ctrl_1_reg, 1, false);
    ctrl_1_data[0] = 0;
    i2c_read(ctrl_1_data, 1);

    ctrl_1_data[0] &= MMA8652_CTRL1_DR_MASK;

    _active_data_rate = ctrl_1_data[0] >> MMA8652_CTRL1_DR_SHIFT;
}

/**
 * @brief       Check the current oversampling mode in active mode
 * @details     Read the bit 0 and 1 of the CTRL_REG_2 register in order to check the
 *							current oversampling in active mode. Update the global variable oversampling_active_mode.
 */
void mma8652_get_oversampling_active_mode(void) {
  uint8_t ctrl_2_reg[1];
  uint8_t ctrl_2_data[1];

  /* Read content of MMA8652_CTRL_2_REG */
  ctrl_2_reg[0] = MMA8652_CTRL_2_REG;
  i2c_write(ctrl_2_reg, 1, false);
  ctrl_2_data[0] = 0;
  i2c_read(ctrl_2_data, 1);

  ctrl_2_data[0] &= MMA8652_CTRL2_MODS_MASK;

  _oversampling_active_mode = ctrl_2_data[0] >> MMA8652_CTRL2_MODS_SHIFT;
}

/**
 * @brief       Check if the high-pass filter is on or not
 */
void mma8652_get_hp_filter_status(void) {
  uint8_t xyz_cfg_reg[1];
  uint8_t xyz_cfg_data[1];

  xyz_cfg_reg[0] = MMA8652_XYZ_DATA_CFG_REG;
  i2c_write(xyz_cfg_reg, 1, false);
  xyz_cfg_data[0] = 0;
  i2c_read(xyz_cfg_data, 1);

  xyz_cfg_data[0] &= MMA8652_XYZ_CFG_HPF_OUT_MASK;

 /* My high-pass filter is on */
  if (xyz_cfg_data[0] == MMA8652_XYZ_CFG_HPF_OUT_MASK) {
   _hp_filter_status = true;
  }
  /* My high-pass filter is off */
 else {
   _hp_filter_status = false;
  }
}

/**
 * @brief       Check the current cutoff frequency
 * @details     Read the bit 0 and 1 of the MMA8652_HP_FILTER_CUTOFF register in order to check the
 *							current cutoff frequency. Update the global variable _cutoff_frequency.
 */
void mma8652_get_cutoff_frequency(void) {
  uint8_t filter_cutoff_reg[1];
  uint8_t filter_cutoff_data[1];

  filter_cutoff_reg[0] = MMA8652_HP_FILTER_CUTOFF_REG;
  i2c_write(filter_cutoff_reg, 1, false);
  filter_cutoff_data[0] = 0;
  i2c_read(filter_cutoff_data, 1);

  filter_cutoff_data[0] &= MMA8652_HP_FILTER_CUTOFF_SEL_MASK;

  _hp_cutoff_frequency = filter_cutoff_data[0] >> MMA8652_HP_FILTER_CUTOFF_SEL_SHIFT;
}

/**
 * @brief       Check the current sleep data rate of the MMA8652 sensor
 * @details     Read the bit 6 and 7 of the CTRL_REG_1 register in order to check the
 *							current sleep data rate. Update the global variable _sleep_data_rate.
 */
void mma8652_get_sleep_data_rate(void) {
  uint8_t ctrl_1_reg[1];
  uint8_t ctrl_1_data[1];

  ctrl_1_reg[0] = MMA8652_CTRL_1_REG;
  i2c_write(ctrl_1_reg, 1, false);
  ctrl_1_data[0] = 0;
  i2c_read(ctrl_1_data, 1);

  ctrl_1_data[0] &= MMA8652_CTRL1_ASLP_RATE_MASK;

  _sleep_data_rate = ctrl_1_data[0] >> MMA8652_CTRL1_ASLP_RATE_SHIFT;
}

/**
 * @brief       Check the current oversampling mode in sleep mode
 * @details     Read the bit 3 and 4 of the CTRL_REG_2 register in order to check the
 *							current oversampling in sleep mode. Update the global variable oversampling_sleep_mode.
 */
void mma8652_get_oversampling_sleep_mode(void) {
  uint8_t ctrl_2_reg[1];
  uint8_t ctrl_2_data[1];

  /* Read content of MMA8652_CTRL_2_REG */
  ctrl_2_reg[0] = MMA8652_CTRL_2_REG;
  i2c_write(ctrl_2_reg, 1, false);
  ctrl_2_data[0] = 0;
  i2c_read(ctrl_2_data, 1);

  ctrl_2_data[0] &= MMA8652_CTRL2_SMODS_MASK;

  _oversampling_sleep_mode = ctrl_2_data[0] >> MMA8652_CTRL2_SMODS_SHIFT;
}

/**
 * @brief       Check if the sensor is in the fast read or normal read state
 * @details     Reads the bit 1 of the CTRL_REG_1 in order to check the current read state.
 *							If in 1 is in the fast read state, otherwise is in the normal read state.
 *							Update the global variable fast_mode.
 */
void mma8652_get_fast_mode(void) {
  uint8_t ctrl_1_reg[1];
  uint8_t ctrl_1_data[1];

  /* Read content of CTRL_REG_1 */
  ctrl_1_reg[0] = MMA8652_CTRL_1_REG;
  i2c_write(ctrl_1_reg, 1, false);
  ctrl_1_data[0] = 0;
  i2c_read(ctrl_1_data, 1);

  ctrl_1_data[0] &= MMA8652_CTRL1_F_READ_MASK;

  /* My current state is fast read */
  if (ctrl_1_data[0] == MMA8652_CTRL1_F_READ_MASK) {
    _fast_read = true;
  }
  /* My current state is normal read */
  else {
    _fast_read = false;
  }
}

/**
 * @brief       Calculate the mg acceleration for the x, y and z axis.
 * @details     From the three axis raw counts calculate the the respectivily mg acceleration.
 *              For this the formula x_mg = 1000 * x_raw + 512 >> 1000 will be used.
 */
void mma8852_calculate_mg(void) {
  /* Now calculate the accelerations in mg (1000g) depending on the current read mode and the dynamic range! *
   * For example, in the case of an active 2G MODE with NORMAL READ, the formula to calculate the mg         *
   * acceleration from the raw counts collected from the sensor will be:                                     *
   * 1g = 1000mg = 1024 counts, so x_mg = 1000 * x_raw / 1024. The final formula will be:                    *
   *                                                                                                         *
   *                               x_mg = 1000 * x_raw + 512 >> 1000                                         *
   *                                                                                                         *
   * The fractions greater than or equal to 0.5 (if the division operation is chosen) would have been        *
   * truncated by the shifting operation. To compensate for this drawback that the shifting operation        *
   * brings (and to get a more accurate rounded number), 512 is added before shifting. This addition         *
   * allows the fractions greater or equal to 0.5 to be rounded up to an integer after the shift.            *
   * The shift of 10 corresponds to the division of 1024 expressed in the formula above.                     */
  if(_fast_read) {
    switch(_dynamic_range) {
      case MMA8652_XYZ_CFG_FS_2G:
        _mma8652_xyz_data.x_mg = (1000 * _mma8652_xyz_data.x_raw + 32) >> 6;
        _mma8652_xyz_data.y_mg = (1000 * _mma8652_xyz_data.y_raw + 32) >> 6;
        _mma8652_xyz_data.z_mg = (1000 * _mma8652_xyz_data.z_raw + 32) >> 6;

        break;
      case MMA8652_XYZ_CFG_FS_4G:
        _mma8652_xyz_data.x_mg = (1000 * _mma8652_xyz_data.x_raw + 16) >> 5;
        _mma8652_xyz_data.y_mg = (1000 * _mma8652_xyz_data.y_raw + 16) >> 5;
        _mma8652_xyz_data.z_mg = (1000 * _mma8652_xyz_data.z_raw + 16) >> 5;

        break;
      case MMA8652_XYZ_CFG_FS_8G:
        _mma8652_xyz_data.x_mg = (1000 * _mma8652_xyz_data.x_raw + 8) >> 4;
        _mma8652_xyz_data.y_mg = (1000 * _mma8652_xyz_data.y_raw + 8) >> 4;
        _mma8652_xyz_data.z_mg = (1000 * _mma8652_xyz_data.z_raw + 8) >> 4;

        break;
      default:
        break;
    }
  }
  else {
    switch(_dynamic_range) {
      case MMA8652_XYZ_CFG_FS_2G:
        _mma8652_xyz_data.x_mg = (1000 * _mma8652_xyz_data.x_raw + 512) >> 10;
        _mma8652_xyz_data.y_mg = (1000 * _mma8652_xyz_data.y_raw + 512) >> 10;
        _mma8652_xyz_data.z_mg = (1000 * _mma8652_xyz_data.z_raw + 512) >> 10;

        break;
      case MMA8652_XYZ_CFG_FS_4G:
        _mma8652_xyz_data.x_mg = (1000 * _mma8652_xyz_data.x_raw + 256) >> 9;
        _mma8652_xyz_data.y_mg = (1000 * _mma8652_xyz_data.y_raw + 256) >> 9;
        _mma8652_xyz_data.z_mg = (1000 * _mma8652_xyz_data.z_raw + 256) >> 9;

        break;
      case MMA8652_XYZ_CFG_FS_8G:
        _mma8652_xyz_data.x_mg = (1000 * _mma8652_xyz_data.x_raw + 128) >> 8;
        _mma8652_xyz_data.y_mg = (1000 * _mma8652_xyz_data.y_raw + 128) >> 8;
        _mma8652_xyz_data.z_mg = (1000 * _mma8652_xyz_data.z_raw + 128) >> 8;

        break;
      default:
        break;
    }
  }
}

/* -------------------------------------------------------------------------- */
/*          Public API                                                        */
/* -------------------------------------------------------------------------- */

/* -------------------------------------------------------------------------- */
/*          MMA8652 Configurations                                            */
/* -------------------------------------------------------------------------- */

bool mma8652_reserve_i2c_bus(void) {
  _press_i2c_conf.frequency  = I2C_FREQUENCY_K100;
  _press_i2c_conf.scl        = MMA8652_SCL;
  _press_i2c_conf.sda        = MMA8652_SDA;

  /* Try to access the I2C bus */
  if(!i2c_init(_press_i2c_conf)) {
	  return false;
  }

	uint8_t who_am_i[1];
	i2c_set_address(MMA8652_ADDR);          /* set MPL3115A2 I2C address */
	who_am_i[0] = MMA8652_WHO_AM_I_REG;	    /* write who am i register address */
	i2c_write(who_am_i, 1, false); 			    /* do not send stop signal */
  who_am_i[0] = 0;
	i2c_read(who_am_i, 1);  		            /* read 1 byte and send stop now */

  if (who_am_i[0] != MMA8652_WHO_AM_I) {
    /* There is some problem in communicating with the sensor. Let's free the I2C bus! */
    i2c_shutdown();
    dputs("[mma8652_reserve_i2c_bus] Seems that MMA8652 sensor isn't connected to the board");
    return false;                       /* It seems that the sensor isn't connected to the board! */
  }
  else {
    return true;
  }
}

bool mma8652_power_on(void) {
  dputs("[mma8652_power_on] Powering on MMA8652 sensor...");

  mma8652_set_power_bit(true);

  mma8652_get_dynamic_range();
  mma8652_get_active_data_rate();
  mma8652_get_oversampling_active_mode();
  mma8652_get_hp_filter_status();
  mma8652_get_cutoff_frequency();
  mma8652_get_sleep_data_rate();
  mma8652_get_oversampling_sleep_mode();
  mma8652_get_fast_mode();

  return true;
}

bool mma8652_power_off(void) {
  dputs("[mma8652_power_off] Shutting down MMA8652 sensor...");

  mma8652_set_power_bit(false);

  return true;
}

bool mma8652_set_dynamic_range(uint8_t dynamic_range) {
  dputs("[mma8652_set_dynamic_range] Setting MMA8652 dynamic range");

  /* Sanity check */
  assert((dynamic_range == MMA8652_XYZ_CFG_FS_2G) ||
    (dynamic_range == MMA8652_XYZ_CFG_FS_4G) ||
    (dynamic_range == MMA8652_XYZ_CFG_FS_8G));

  /* Power off the sensor before set registers */
  mma8652_set_power_bit(false);

  uint8_t xyz_cfg_reg[1];
  uint8_t xyz_cfg_data[1];

  /* Read content of XYZ_DATA_CFG_REG */
  xyz_cfg_reg[0] = MMA8652_XYZ_DATA_CFG_REG;
  i2c_write(xyz_cfg_reg, 1, false);
  xyz_cfg_data[0] = 0;
  i2c_read(xyz_cfg_data, 1);

  /* Clear XYZ_CFG_FS mask while holding all other values of XYZ_DATA_CFG_REG */
  xyz_cfg_data[0] &= ~MMA8652_XYZ_CFG_FS_MASK;
  /* Change the current dynamic value while holding all other values of XYZ_DATA_CFG_REG */
  xyz_cfg_data[0] |= dynamic_range;
  i2c_write(xyz_cfg_reg, 1, false);
  i2c_write(xyz_cfg_data, 1, true);

  /* Power on the sensor again! */
  mma8652_set_power_bit(true);

  _dynamic_range = dynamic_range;

  return true;
}

bool mma8652_set_active_data_rate(uint8_t active_data_rate) {
  dputs("[mma8652_set_data_rate] Setting MMA8652 active data rate");

  /* Sanity check */
  assert((active_data_rate == MMA8652_CTRL1_DR_800_Hz) ||
    (active_data_rate == MMA8652_CTRL1_DR_400_Hz) ||
    (active_data_rate == MMA8652_CTRL1_DR_200_Hz) ||
    (active_data_rate == MMA8652_CTRL1_DR_100_Hz) ||
    (active_data_rate == MMA8652_CTRL1_DR_50_Hz) ||
    (active_data_rate == MMA8652_CTRL1_DR_12_5_Hz) ||
    (active_data_rate == MMA8652_CTRL1_DR_6_25_Hz) ||
    (active_data_rate == MMA8652_CTRL1_DR_1_56_Hz));

  /* Power off the sensor before set registers */
  mma8652_set_power_bit(false);

  uint8_t ctrl_1_reg[1];
  uint8_t ctrl_1_data[1];

  /* Read content of CTRL_REG_1 */
  ctrl_1_reg[0] = MMA8652_CTRL_1_REG;
  i2c_write(ctrl_1_reg, 1, false);
  ctrl_1_data[0] = 0;
  i2c_read(ctrl_1_data, 1);

  /* Clear MMA8652_CTRL1_DR mask while holding all other values of CTRL_REG_1 */
  ctrl_1_data[0] &= ~MMA8652_CTRL1_DR_MASK;
  /* Change the current active data rate and turn on again the sensor */
  ctrl_1_data[0] |= active_data_rate;
  i2c_write(ctrl_1_reg, 1, false);
  i2c_write(ctrl_1_data, 1, true);

  /* Power on the sensor again! */
  mma8652_set_power_bit(true);

  _active_data_rate = active_data_rate;

  return true;
}

bool mma8652_set_oversampling_active_mode(uint8_t oversampling_active_mode) {
  dputs("[mma8652_set_oversampling_active_mode] Setting MMA8652 oversampling of active mode");

  /* Sanity check */
  assert((oversampling_active_mode == MMA8652_CTRL2_MODS_NORMAL) ||
    (oversampling_active_mode == MMA8652_CTRL2_MODS_L_N_L_PWR) ||
    (oversampling_active_mode == MMA8652_CTRL2_MODS_H_RES) ||
    (oversampling_active_mode == MMA8652_CTRL2_MODS_L_PWR));

  /* Power off the sensor before set registers */
  mma8652_set_power_bit(false);

  uint8_t ctrl_2_reg[1];
  uint8_t ctrl_2_data[1];

  /* Read content of MMA8652_CTRL_2_REG */
  ctrl_2_reg[0] = MMA8652_CTRL_2_REG;
  i2c_write(ctrl_2_reg, 1, false);
  ctrl_2_data[0] = 0;
  i2c_read(ctrl_2_data, 1);

  /* Clear MMA8652_CTRL2_MODS mask while holding all other values of CTRL_REG_2 */
  ctrl_2_data[0] &= ~MMA8652_CTRL2_MODS_MASK;
  /* Change the current oversampling mode while holding all other values of CTRL_REG_2 */
  ctrl_2_data[0] |= oversampling_active_mode;
  i2c_write(ctrl_2_reg, 1, false);
  i2c_write(ctrl_2_data, 1, true);

  /* Power on the sensor again! */
  mma8652_set_power_bit(true);

  _oversampling_active_mode = oversampling_active_mode;

  return true;
}

bool mma8652_set_hp_filter(bool enable_hp_filter) {
  /* Power off the sensor before set registers */
  mma8652_set_power_bit(false);

  uint8_t xyz_cfg_reg[1];
  uint8_t xyz_cfg_data[1];

  /* Read content of MMA8652_XYZ_DATA_CFG */
  xyz_cfg_reg[0] = MMA8652_XYZ_DATA_CFG_REG;
  i2c_write(xyz_cfg_reg, 1, false);
  xyz_cfg_data[0] = 0;
  i2c_read(xyz_cfg_data, 1);

  if(enable_hp_filter) {
      dputs("[mma8652_set_hp_filter] High-pass filter turned on");
      xyz_cfg_data[0] |= MMA8652_XYZ_CFG_HPF_OUT_MASK;
  }
  else {
      dputs("[mma8652_set_hp_filter] High-pass filter turned off");
      xyz_cfg_data[0] &= ~MMA8652_XYZ_CFG_HPF_OUT_MASK;
  }

  i2c_write(xyz_cfg_reg, 1, false);
  i2c_write(xyz_cfg_data, 1, true);

  /* Enable bypass high-pass filter (HPF) for pulse processing function and
   * low-pass filter (LPF) for pulse processing function */
  uint8_t filter_cutoff_reg[1];
  uint8_t filter_cutoff_data[1];

  /* Read content of MMA8652_HP_FILTER_CUTOFF_REG */
  filter_cutoff_reg[0] = MMA8652_HP_FILTER_CUTOFF_REG;
  i2c_write(filter_cutoff_reg, 1, false);
  filter_cutoff_data[0] = 0;
  i2c_read(filter_cutoff_data, 1);

  if(enable_hp_filter) {
      filter_cutoff_data[0] |= MMA8652_HP_FILTER_CUTOFF_PULSE_HPF_BY_MASK;
  }
  else {
      filter_cutoff_data[0] &= ~MMA8652_HP_FILTER_CUTOFF_PULSE_HPF_BY_MASK;
  }

  if(enable_hp_filter) {
      filter_cutoff_data[0] |= MMA8652_HP_FILTER_CUTOFF_PULSE_HPF_EN_MASK;
  }
  else {
      filter_cutoff_data[0] &= ~MMA8652_HP_FILTER_CUTOFF_PULSE_HPF_EN_MASK;
  }

  i2c_write(filter_cutoff_reg, 1, false);
  i2c_write(filter_cutoff_data, 1, true);

  /* Power on the sensor again! */
  mma8652_set_power_bit(true);

  _hp_filter_status = enable_hp_filter;

  return true;
}

bool mma8652_set_hp_cutoff_frequency(uint8_t hp_cutoff_frequency) {
  dputs("[mma8652_set_cutoff_frequency] Setting cutoff frequency of the MMA8652 filter");

  /* Sanity check */
  assert((hp_cutoff_frequency == MMA8652_HP_FILTER_CUTOFF_SEL_00) ||
    (hp_cutoff_frequency == MMA8652_HP_FILTER_CUTOFF_SEL_01) ||
    (hp_cutoff_frequency == MMA8652_HP_FILTER_CUTOFF_SEL_10) ||
    (hp_cutoff_frequency == MMA8652_HP_FILTER_CUTOFF_SEL_11));

  /* Power off the sensor before set registers */
  mma8652_set_power_bit(false);

  /* Now configure the cutoff frequency */
  uint8_t filter_cutoff_reg[1];
  uint8_t filter_cutoff_data[1];

  /* Read content of MMA8652_HP_FILTER_CUTOFF */
  filter_cutoff_reg[0] = MMA8652_HP_FILTER_CUTOFF_REG;
  i2c_write(filter_cutoff_reg, 1, false);
  filter_cutoff_data[0] = 0;
  i2c_read(filter_cutoff_data, 1);

  filter_cutoff_data[0] &= ~MMA8652_HP_FILTER_CUTOFF_SEL_MASK;
  filter_cutoff_data[0] |= hp_cutoff_frequency;
  i2c_write(filter_cutoff_reg, 1, false);
  i2c_write(filter_cutoff_data, 1, true);

  /* Power on the sensor again! */
  mma8652_set_power_bit(true);

  _hp_cutoff_frequency = hp_cutoff_frequency;

  return true;
}

void mma8652_set_auto_sleep_mode(bool auto_sleep_on) {
  dputs("[mma8652_set_sleep_mode] Setting auto-sleep configuration");

  /* Power off the sensor before set registers */
  mma8652_set_power_bit(false);

  uint8_t ctrl_2_reg[1];
  uint8_t ctrl_2_data[1];

  /* Read content of MMA8652_CTRL_2_REG */
  ctrl_2_reg[0] = MMA8652_CTRL_2_REG;
  i2c_write(ctrl_2_reg, 1, false);
  ctrl_2_data[0] = 0;
  i2c_read(ctrl_2_data, 1);

  /* Change the current auto-sleep mode while holding all other values of CTRL_REG_2 */
  if(auto_sleep_on) {
    ctrl_2_data[0] |= MMA8652_CTRL2_SLPE_MASK;
  }
  else {
    ctrl_2_data[0] &= ~MMA8652_CTRL2_SLPE_MASK;
  }

  i2c_write(ctrl_2_reg, 1, false);
  i2c_write(ctrl_2_data, 1, true);

  /* Power on the sensor again! */
  mma8652_set_power_bit(true);
}

bool mma8652_set_sleep_data_rate(uint8_t sleep_data_rate) {
  dputs("[mma8652_set_sleep_data_rate] Setting MMA8652 sleep data rate");

  /* Sanity check */
  assert((sleep_data_rate == MMA8652_CTRL1_ASLP_50Hz) ||
    (sleep_data_rate == MMA8652_CTRL1_ASLP_12_5_Hz) ||
    (sleep_data_rate == MMA8652_CTRL1_ASLP_6_25_Hz) ||
    (sleep_data_rate == MMA8652_CTRL1_ASLP_1_56_Hz));

  /* Power off the sensor before set registers */
  mma8652_set_power_bit(false);

  uint8_t ctrl_1_reg[1];
  uint8_t ctrl_1_data[1];

  /* Read content of CTRL_REG_1 */
  ctrl_1_reg[0] = MMA8652_CTRL_1_REG;
  i2c_write(ctrl_1_reg, 1, false);
  ctrl_1_data[0] = 0;
  i2c_read(ctrl_1_data, 1);

  /* Clear MMA8652_CTRL1_ASLP_RATE mask while holding all other values of CTRL_REG_1 */
  ctrl_1_data[0] &= ~MMA8652_CTRL1_ASLP_RATE_MASK;
  /* Change the current sleep data rate and turn on again the sensor */
  ctrl_1_data[0] |= sleep_data_rate;
  i2c_write(ctrl_1_reg, 1, false);
  i2c_write(ctrl_1_data, 1, true);

  /* Power on the sensor again! */
  mma8652_set_power_bit(true);

  _sleep_data_rate = sleep_data_rate;

  return true;
}

bool mma8652_set_oversampling_sleep_mode(uint8_t oversampling_sleep_mode) {
  dputs("[mma8652_set_oversampling_sleep_mode] Setting MMA8652 oversampling of sleep mode");

  /* Sanity check */
  assert((oversampling_sleep_mode == MMA8652_CTRL2_SMODS_NORMAL) ||
    (oversampling_sleep_mode == MMA8652_CTRL2_SMODS_L_N_L_PWR) ||
    (oversampling_sleep_mode == MMA8652_CTRL2_SMODS_H_RES) ||
    (oversampling_sleep_mode == MMA8652_CTRL2_SMODS_L_PWR));

  /* Power off the sensor before set registers */
  mma8652_set_power_bit(false);

  uint8_t ctrl_2_reg[1];
  uint8_t ctrl_2_data[1];

  /* Read content of MMA8652_CTRL_2_REG */
  ctrl_2_reg[0] = MMA8652_CTRL_2_REG;
  i2c_write(ctrl_2_reg, 1, false);
  ctrl_2_data[0] = 0;
  i2c_read(ctrl_2_data, 1);

  /* Clear MMA8652_CTRL2_SMODS mask while holding all other values of CTRL_REG_2 */
  ctrl_2_data[0] &= ~MMA8652_CTRL2_SMODS_MASK;
  /* Change the current oversampling mode while holding all other values of CTRL_REG_2 */
  ctrl_2_data[0] |= oversampling_sleep_mode;
  i2c_write(ctrl_2_reg, 1, false);
  i2c_write(ctrl_2_data, 1, true);

  /* Power on the sensor again! */
  mma8652_set_power_bit(true);

  _oversampling_sleep_mode = oversampling_sleep_mode;

  return true;
}

bool mma8652_set_fast_read(bool fast_read) {
  /* Power off the sensor before set registers */
  mma8652_set_power_bit(false);

  uint8_t ctrl_1_reg[1];
  uint8_t ctrl_1_data[1];

  /* Read content of CTRL_REG_1 */
  ctrl_1_reg[0] = MMA8652_CTRL_1_REG;
  i2c_write(ctrl_1_reg, 1, false);
  ctrl_1_data[0] = 0;
  i2c_read(ctrl_1_data, 1);

  if(fast_read) {
      dputs("[mma8652_set_fast_read] Setting MMA8652 to fast read mode");
      ctrl_1_data[0] |= MMA8652_CTRL1_F_READ_MASK;
  }
  else {
      dputs("[mma8652_set_fast_read] Setting MMA8652 to normal read mode");
      ctrl_1_data[0] &= ~MMA8652_CTRL1_F_READ_MASK;
  }

  i2c_write(ctrl_1_reg, 1, false);
  i2c_write(ctrl_1_data, 1, true);

  /* Power on the sensor again! */
  mma8652_set_power_bit(true);

  _fast_read = fast_read;

  return true;
}

bool mma8652_new_xyz_data_available(void) {
  uint8_t new_data[1];
  new_data[0] = MMA8652_STATUS_REG;
  i2c_write(new_data, 1, false); 			   /* do not send stop signal */
  new_data[0] = 0;
	i2c_read(new_data, 1);  		           /* read 1 byte and send stop now */

  new_data[0] &= MMA8652_STATUS_ZYX_DR_MASK;
  if (new_data[0] == MMA8652_STATUS_ZYX_DR_MASK) {
    _mma8652_xyz_data.status = true;
    return true;
  }
  else {
    _mma8652_xyz_data.status = false;
    return false;
  }
}

struct MMA8652_xyz_data mma8652_read_xyz_data(void) {
  dputs("[mma8652_read_xyz_data] Reading new xyz axis values");

  mma8652_clear_xyz_data();

  /* My current state is fast read. The x, y and z axis have only 8 bit precision */
  if (_fast_read) {
    /*
    ** Read 8 bit xyz axis results and store in a 8 bit registers
    */
    uint8_t xyz_axis[3];
    xyz_axis[0] = MMA8652_OUT_X_MSB_REG;
    i2c_write(xyz_axis, 1, false); 			             /* do not send stop signal */
    memset(xyz_axis, 0, sizeof(xyz_axis));
    /* Read 3 positions in order to read xyz axis in only one read and send stop*/
    i2c_read(xyz_axis, 3);

    /* Save the xyz info axis on the MMA8652 data structure */
    if(xyz_axis[0] > 127) {
      _mma8652_xyz_data.x_raw |= 0xFF00;
      _mma8652_xyz_data.x_raw += xyz_axis[0];
    }
    else {
      _mma8652_xyz_data.x_raw += xyz_axis[0];
    }
    if(xyz_axis[1] > 127) {
      _mma8652_xyz_data.y_raw |= 0xFF00;
      _mma8652_xyz_data.y_raw += xyz_axis[1];
    }
    else {
      _mma8652_xyz_data.y_raw += xyz_axis[1];
    }
    if(xyz_axis[2] > 127) {
      _mma8652_xyz_data.z_raw |= 0xFF00;
      _mma8652_xyz_data.z_raw += xyz_axis[2];
    }
    else {
      _mma8652_xyz_data.z_raw += xyz_axis[2];
    }
  }
  /* My current state is normal read. The x, y and z axis have 12 bit precision */
  else {
    /*
    ** Read 12 bit xyz axis results and store in a 8 bit registers
    */
    uint8_t xyz_axis[6];
    xyz_axis[0] = MMA8652_OUT_X_MSB_REG;
    i2c_write(xyz_axis, 1, false); 			             /* do not send stop signal */
    memset(xyz_axis, 0, sizeof(xyz_axis));
    /* Read 6 positions in order to read xyz axis in only one read and send stop*/
    i2c_read(xyz_axis, 6);

    /* Save the xyz info axis on the MMA8652 data structure */
    if(xyz_axis[0] > 127) {
      _mma8652_xyz_data.x_raw |= 0xF000;
      _mma8652_xyz_data.x_raw += (xyz_axis[0] << MMA8652_OUT_X_LSB_SHIFT);
      _mma8652_xyz_data.x_raw += (xyz_axis[1] >> MMA8652_OUT_X_LSB_SHIFT);
    }
    else {
      _mma8652_xyz_data.x_raw += (xyz_axis[0] << MMA8652_OUT_X_LSB_SHIFT);
      _mma8652_xyz_data.x_raw += (xyz_axis[1] >> MMA8652_OUT_X_LSB_SHIFT);
    }
    if(xyz_axis[2] > 127) {
      _mma8652_xyz_data.y_raw |= 0xF000;
      _mma8652_xyz_data.y_raw += (xyz_axis[2] << MMA8652_OUT_Y_LSB_SHIFT);
      _mma8652_xyz_data.y_raw += (xyz_axis[3] >> MMA8652_OUT_Y_LSB_SHIFT);
    }
    else {
      _mma8652_xyz_data.y_raw += (xyz_axis[2] << MMA8652_OUT_Y_LSB_SHIFT);
      _mma8652_xyz_data.y_raw += (xyz_axis[3] >> MMA8652_OUT_Y_LSB_SHIFT);
    }
    if(xyz_axis[4] > 127) {
      _mma8652_xyz_data.z_raw |= 0xF000;
      _mma8652_xyz_data.z_raw += (xyz_axis[4] << MMA8652_OUT_Z_LSB_SHIFT);
      _mma8652_xyz_data.z_raw += (xyz_axis[5] >> MMA8652_OUT_Z_LSB_SHIFT);
    }
    else {
      _mma8652_xyz_data.z_raw += (xyz_axis[4] << MMA8652_OUT_Z_LSB_SHIFT);
      _mma8652_xyz_data.z_raw += (xyz_axis[5] >> MMA8652_OUT_Z_LSB_SHIFT);
    }
  }

  mma8852_calculate_mg();

  return _mma8652_xyz_data;
}

struct MMA8652_xyz_data mma8652_get_xyz_data(void) {
  return _mma8652_xyz_data;
}

/* -------------------------------------------------------------------------- */
/*          MMA8652 Specific Functions                                        */
/* -------------------------------------------------------------------------- */

bool mma8652_clear_interrupts() {
  dputs("[mma8652_clear_interrupts] Clear interrupt vectors");

  uint8_t ctrl_3_reg[1];
  uint8_t ctrl_3_data[1];
  ctrl_3_reg[0]  = MMA8652_CTRL_3_REG;
  ctrl_3_data[0] = 0;
  i2c_write(ctrl_3_reg, 1, false);
  i2c_write(ctrl_3_data, 1, true);

  uint8_t ctrl_4_reg[1];
  uint8_t ctrl_4_data[1];
  ctrl_4_reg[0]  = MMA8652_CTRL_4_REG;
  ctrl_4_data[0] = 0;
  i2c_write(ctrl_4_reg, 1, false);
  i2c_write(ctrl_4_data, 1, true);

  uint8_t ctrl_5_reg[1];
  uint8_t ctrl_5_data[1];
  ctrl_5_reg[0]  = MMA8652_CTRL_5_REG;
  ctrl_5_data[0] = 0;
  i2c_write(ctrl_5_reg, 1, false);
  i2c_write(ctrl_5_data, 1, true);

  return true;
}

bool mma8652_set_interrupt(uint8_t interrupt_id, enum interrupt_pin interrupt_pin, bool wake_up) {
  dputs("[mma8652_set_interrupt] Setting interrupt");

  /* Sanity check */
  assert((interrupt_id == MMA8652_CTRL4_INT_EN_DRDY_MASK) ||
    (interrupt_id == MMA8652_CTRL4_INT_EN_FF_MT_MASK)   ||
    (interrupt_id == MMA8652_CTRL4_INT_EN_PULSE_MASK)   ||
    (interrupt_id == MMA8652_CTRL4_INT_EN_LNDPRT_MASK)  ||
    (interrupt_id == MMA8652_CTRL4_INT_EN_TRANS_MASK)   ||
    (interrupt_id == MMA8652_CTRL4_INT_EN_FIFO_MASK)    ||
    (interrupt_id == MMA8652_CTRL4_INT_EN_ASLP_MASK)    ||
    (interrupt_pin == MMA8652_INT_1)                    ||
    (interrupt_pin == MMA8652_INT_2));

  /* Power off the sensor before set registers */
  mma8652_set_power_bit(false);

  uint8_t ctrl_3_reg[1];
  uint8_t ctrl_3_data[1];

  /* Read content of CTRL_REG_3 */
  ctrl_3_reg[0] = MMA8652_CTRL_3_REG;
  i2c_write(ctrl_3_reg, 1, false);
  ctrl_3_data[0] = 0;
  i2c_read(ctrl_3_data, 1);

  /* Clear MMA8652_CTRL3_IPOL and MMA8652_CTRL3_PP_OD mask while holding all other values of CTRL_REG_3. *
   * MMA8652_CTRL3_IPOL = 0: active low; MMA8652_CTRL3_PP_OD = 0: push-pull. */
  ctrl_3_data[0] &= ~MMA8652_CTRL3_IPOL_MASK & ~MMA8652_CTRL3_PP_OD_MASK;

  /* Setting wake up mode for the appropriate interrupts */
  if(wake_up) {
    if(interrupt_id == MMA8652_CTRL4_INT_EN_FF_MT_MASK) {
      ctrl_3_data[0] |= MMA8652_CTRL3_WAKE_FF_MT_MASK;
    }
    if(interrupt_id == MMA8652_CTRL4_INT_EN_PULSE_MASK) {
      ctrl_3_data[0] |= MMA8652_CTRL3_WAKE_PULSE_MASK;
    }
    if(interrupt_id == MMA8652_CTRL4_INT_EN_LNDPRT_MASK) {
      ctrl_3_data[0] |= MMA8652_CTRL3_WAKE_LNDPRT_MASK;
    }
    if(interrupt_id == MMA8652_CTRL4_INT_EN_TRANS_MASK) {
      ctrl_3_data[0] |= MMA8652_CTRL3_WAKE_TRANS_MASK;
    }
  }

  i2c_write(ctrl_3_reg, 1, false);
  i2c_write(ctrl_3_data, 1, true);

  uint8_t ctrl_4_reg[1];
  uint8_t ctrl_4_data[1];

  /* Read content of CTRL_REG_4 */
  ctrl_4_reg[0] = MMA8652_CTRL_4_REG;
  i2c_write(ctrl_4_reg, 1, false);
  ctrl_4_data[0] = 0;
  i2c_read(ctrl_4_data, 1);

  /* Set the interrupt id on CTRL_REG_4. */
  ctrl_4_data[0] |= interrupt_id;
  i2c_write(ctrl_4_reg, 1, false);
  i2c_write(ctrl_4_data, 1, true);

  uint8_t ctrl_5_reg[1];
  uint8_t ctrl_5_data[1];

  /* Read content of CTRL_REG_5 */
  ctrl_5_reg[0] = MMA8652_CTRL_5_REG;
  i2c_write(ctrl_5_reg, 1, false);
  ctrl_5_data[0] = 0;
  i2c_read(ctrl_5_data, 1);

  /* Set the interrupt pin on CTRL_REG_5. */
  if(interrupt_pin == MMA8652_INT_1) {
    ctrl_5_data[0] |= interrupt_id;
  }
  if(interrupt_pin == MMA8652_INT_2) {
    ctrl_5_data[0] &= ~interrupt_id;
  }

  i2c_write(ctrl_5_reg, 1, false);
  i2c_write(ctrl_5_data, 1, true);

  /* Power on the sensor again! */
  mma8652_set_power_bit(true);

  return true;
}

/* -------------------------------------------------------------------------- */
/*          Freefall/Motion Profile                                           */
/* -------------------------------------------------------------------------- */

bool mma8652_set_freefall_motion_profile(uint8_t profile_type, uint8_t x_enabled, uint8_t y_enabled, uint8_t z_enabled, uint8_t motion_threshold, uint8_t count_threshold) {
  /* Power off the sensor before set registers */
  mma8652_set_power_bit(false);

  uint8_t ff_mt_cfg_reg[1];
  uint8_t ff_mt_cfg_data[1];

  /* Read content of MMA8652_FF_MT_CFG */
  ff_mt_cfg_reg[0] = MMA8652_FF_MT_CFG_REG;
  i2c_write(ff_mt_cfg_reg, 1, false);
  ff_mt_cfg_data[0] = 0;
  i2c_read(ff_mt_cfg_data, 1);

  /* Set content of MMA8652_FF_MT_CFG */
  ff_mt_cfg_data[0] &= ~MMA8652_FF_MT_CFG_XEFE_MASK;
  ff_mt_cfg_data[0] &= ~MMA8652_FF_MT_CFG_YEFE_MASK;
  ff_mt_cfg_data[0] &= ~MMA8652_FF_MT_CFG_ZEFE_MASK;
  ff_mt_cfg_data[0] &= ~MMA8652_FF_MT_CFG_OAE_MASK;
  ff_mt_cfg_data[0] |= profile_type | x_enabled | y_enabled | z_enabled;
  i2c_write(ff_mt_cfg_reg, 1, false);
  i2c_write(ff_mt_cfg_data, 1, true);

  uint8_t ff_mt_ths_reg[1];
  uint8_t ff_mt_ths_data[1];

  /* Set content of MMA8652_FF_MT_THS */
  ff_mt_ths_reg[0]   = MMA8652_FF_MT_THS_REG;
  ff_mt_ths_data[0]  = 0;
  //Just for precaution because the other bit is for count mode as explained above
  ff_mt_ths_data[0] |= (motion_threshold & 0x7F);
  /* The debounce counter mode selection is set to 0. In this case increments
     or decrements debounce. The other option could be 1 and in this case the
     counter could be incremented or cleared (ff_mt_ths_data[0] |= MMA8652_FF_MT_THS_DBCNTM_MASK) */
  ff_mt_ths_data[0] &= ~MMA8652_FF_MT_THS_DBCNTM_MASK;
  i2c_write(ff_mt_ths_reg, 1, false);
  i2c_write(ff_mt_ths_data, 1, true);

  uint8_t ff_mt_count_reg[1];
  uint8_t ff_mt_count_data[1];

  /* Set content of MMA8652_FF_MT_COUNT */
  ff_mt_count_reg[0]  = MMA8652_FF_MT_COUNT_REG;
  ff_mt_count_data[0] = count_threshold;
  i2c_write(ff_mt_count_reg, 1, false);
  i2c_write(ff_mt_count_data, 1, true);

  /* Power on the sensor again! */
  mma8652_set_power_bit(true);

  return true;
}

bool mma8652_enable_freefall_motion_profile(bool enable) {
  dputs("[mma8652_enable_freefall_motion_profile] Enabling/desabling freefall/motion profile");

  /* Power off the sensor before set registers */
  mma8652_set_power_bit(false);

  uint8_t ff_mt_cfg_reg[1];
  uint8_t ff_mt_cfg_data[1];

  /* Read content of MMA8652_FF_MT_CFG */
  ff_mt_cfg_reg[0] = MMA8652_FF_MT_CFG_REG;
  i2c_write(ff_mt_cfg_reg, 1, false);
  ff_mt_cfg_data[0] = 0;
  i2c_read(ff_mt_cfg_data, 1);

  /* Set content of MMA8652_FF_MT_CFG */
  if(enable) {
    ff_mt_cfg_data[0] |= MMA8652_FF_MT_CFG_ELE_MASK;
  }
  else {
    ff_mt_cfg_data[0] &= ~MMA8652_FF_MT_CFG_ELE_MASK;
  }
  i2c_write(ff_mt_cfg_reg, 1, false);
  i2c_write(ff_mt_cfg_data, 1, true);

  /* Power on the sensor again! */
  mma8652_set_power_bit(true);

  return true;
}

struct MMA8652_motion_data mma8652_read_freefall_motion_data() {
  dputs("[mma8652_read_freefall_motion_data] Reading new freefall/motion data");

  mma8652_clear_motion_data();

  uint8_t ff_mt_src_reg[1];
  uint8_t ff_mt_src_data[1];

  /* Read content of MMA8652_FF_MT_SRC */
  ff_mt_src_reg[0] = MMA8652_FF_MT_SRC_REG;
  i2c_write(ff_mt_src_reg, 1, false);
  ff_mt_src_data[0] = 0;
  i2c_read(ff_mt_src_data, 1);

  if((ff_mt_src_data[0] & MMA8652_FF_MT_SRC_EA_MASK)  == MMA8652_FF_MT_SRC_EA_MASK)
    _mma8652_motion_data.status     = true;
  if((ff_mt_src_data[0] & MMA8652_FF_MT_SRC_ZHP_MASK) == MMA8652_FF_MT_SRC_ZHP_MASK)
    _mma8652_motion_data.z_polarity = true;
  if((ff_mt_src_data[0] & MMA8652_FF_MT_SRC_ZHE_MASK) == MMA8652_FF_MT_SRC_ZHE_MASK)
    _mma8652_motion_data.z_motion   = true;
  if((ff_mt_src_data[0] & MMA8652_FF_MT_SRC_YHP_MASK) == MMA8652_FF_MT_SRC_YHP_MASK)
    _mma8652_motion_data.y_polarity = true;
  if((ff_mt_src_data[0] & MMA8652_FF_MT_SRC_YHE_MASK) == MMA8652_FF_MT_SRC_YHE_MASK)
    _mma8652_motion_data.y_motion   = true;
  if((ff_mt_src_data[0] & MMA8652_FF_MT_SRC_XHP_MASK) == MMA8652_FF_MT_SRC_XHP_MASK)
    _mma8652_motion_data.x_polarity = true;
  if((ff_mt_src_data[0] & MMA8652_FF_MT_SRC_XHE_MASK) == MMA8652_FF_MT_SRC_XHE_MASK)
    _mma8652_motion_data.x_motion   = true;

  return _mma8652_motion_data;
}

struct MMA8652_motion_data mma8652_get_motion_data(void) {
  return _mma8652_motion_data;
}

/* -------------------------------------------------------------------------- */
/*          Transient Profile                                                 */
/* -------------------------------------------------------------------------- */

bool mma8652_set_transient_profile(uint8_t x_enabled, uint8_t y_enabled, uint8_t z_enabled, uint8_t bypass_hp_filter_enabled, uint8_t transient_threshold, uint8_t count_threshold) {
  /* Power off the sensor before set registers */
  mma8652_set_power_bit(false);

  uint8_t t_cfg_reg[1];
  uint8_t t_cfg_data[1];

  /* Read content of MMA8652_TRANSIENT_CFG */
  t_cfg_reg[0] = MMA8652_TRANSIENT_CFG_REG;
  i2c_write(t_cfg_reg, 1, false);
  t_cfg_data[0] = 0;
  i2c_read(t_cfg_data, 1);

  /* Set content of MMA8652_TRANSIENT_CFG */
  t_cfg_data[0] &= ~MMA8652_TRANSIENT_CFG_XTEFE_MASK;
  t_cfg_data[0] &= ~MMA8652_TRANSIENT_CFG_YTEFE_MASK;
  t_cfg_data[0] &= ~MMA8652_TRANSIENT_CFG_ZTEFE_MASK;
  t_cfg_data[0] &= ~MMA8652_TRANSIENT_CFG_HPF_BYP_MASK;
  t_cfg_data[0] |= x_enabled | y_enabled | z_enabled | bypass_hp_filter_enabled;
  i2c_write(t_cfg_reg, 1, false);
  i2c_write(t_cfg_data, 1, true);

  uint8_t t_ths_reg[1];
  uint8_t t_ths_data[1];

  /* Set content of MMA8652_TRANSIENT_THS */
  t_ths_reg[0]   = MMA8652_TRANSIENT_THS_REG;
  t_ths_data[0]  = 0;
  //Just for precaution because the other bit is for count mode as explained above
  t_ths_data[0] |= (transient_threshold & 0x7F);
  /* The debounce counter mode selection is set to 0. In this case increments
     or decrements debounce. The other option could be 1 and in this case the
     counter could be incremented or cleared (tt_ths_data[0] |= MMA8652_TRANSIENT_THS_DBCNTM_MASK) */
  t_ths_data[0] &= ~MMA8652_TRANSIENT_THS_DBCNTM_MASK;
  i2c_write(t_ths_reg, 1, false);
  i2c_write(t_ths_data, 1, true);

  uint8_t tt_count_reg[1];
  uint8_t tt_count_data[1];

  /* Set content of MMA8652_TRANSIENT_COUNT */
  tt_count_reg[0]  = MMA8652_TRANSIENT_COUNT_REG;
  tt_count_data[0] = count_threshold;
  i2c_write(tt_count_reg, 1, false);
  i2c_write(tt_count_data, 1, true);

  /* Power on the sensor again! */
  mma8652_set_power_bit(true);

  return true;
}

bool mma8652_enable_transient_profile(bool enable) {
  dputs("[mma8652_enable_transient_profile] Enabling/desabling Transient profile");

  /* Power off the sensor before set registers */
  mma8652_set_power_bit(false);

  uint8_t t_cfg_reg[1];
  uint8_t t_cfg_data[1];

  /* Read content of MMA8652_TRANSIENT_CFG */
  t_cfg_reg[0] = MMA8652_TRANSIENT_CFG_REG;
  i2c_write(t_cfg_reg, 1, false);
  t_cfg_data[0] = 0;
  i2c_read(t_cfg_data, 1);

  /* Set content of MMA8652_TRANSIENT_CFG */
  if(enable) {
    t_cfg_data[0] |= MMA8652_TRANSIENT_CFG_ELE_MASK;
  }
  else {
    t_cfg_data[0] &= ~MMA8652_TRANSIENT_CFG_ELE_MASK;
  }
  i2c_write(t_cfg_reg, 1, false);
  i2c_write(t_cfg_data, 1, true);

  /* Power on the sensor again! */
  mma8652_set_power_bit(true);

  return true;
}

struct MMA8652_transient_data mma8652_read_transient_data() {
  dputs("[mma8652_read_transient_data] Reading new transient data");

  mma8652_clear_transient_data();

  uint8_t t_src_reg[1];
  uint8_t t_src_data[1];

  /* Read content of MMA8652_TRANSIENT_SRC */
  t_src_reg[0] = MMA8652_TRANSIENT_SRC_REG;
  i2c_write(t_src_reg, 1, false);
  t_src_data[0] = 0;
  i2c_read(t_src_data, 1);

  if((t_src_data[0] & MMA8652_TRANSIENT_SRC_EA_MASK)  == MMA8652_TRANSIENT_SRC_EA_MASK)
    _mma8652_transient_data.status      = true;
  if((t_src_data[0] & MMA8652_TRANSIENT_SRC_Z_TRANS_POL_MASK) == MMA8652_TRANSIENT_SRC_Z_TRANS_POL_MASK)
    _mma8652_transient_data.z_polarity  = true;
  if((t_src_data[0] & MMA8652_TRANSIENT_SRC_ZTRANSE_MASK) == MMA8652_TRANSIENT_SRC_ZTRANSE_MASK)
    _mma8652_transient_data.z_transient = true;
  if((t_src_data[0] & MMA8652_TRANSIENT_SRC_Y_TRANS_POL_MASK) == MMA8652_TRANSIENT_SRC_Y_TRANS_POL_MASK)
    _mma8652_transient_data.y_polarity = true;
  if((t_src_data[0] & MMA8652_TRANSIENT_SRC_YTRANSE_MASK) == MMA8652_TRANSIENT_SRC_YTRANSE_MASK)
    _mma8652_transient_data.y_transient = true;
  if((t_src_data[0] & MMA8652_TRANSIENT_SRC_X_TRANS_POL_MASK) == MMA8652_TRANSIENT_SRC_X_TRANS_POL_MASK)
    _mma8652_transient_data.x_polarity  = true;
  if((t_src_data[0] & MMA8652_TRANSIENT_SRC_XTRANSE_MASK) == MMA8652_TRANSIENT_SRC_XTRANSE_MASK)
    _mma8652_transient_data.x_transient = true;

  return _mma8652_transient_data;
}

struct MMA8652_transient_data mma8652_get_transient_data(void) {
  return _mma8652_transient_data;
}

/* -------------------------------------------------------------------------- */
/*          Orientation Profile                                               */
/* -------------------------------------------------------------------------- */

bool mma8652_enable_orientation_detection(bool enable) {
  dputs("[mma8652_enable_orientation_detection] Enabling/desabling orientation detection");

  /* Power off the sensor before set registers */
  mma8652_set_power_bit(false);

  uint8_t pl_cfg_reg[1];
  uint8_t pl_cfg_data[1];

  /* Set content of MMA8652_PL_CFG */
  pl_cfg_reg[0]  = MMA8652_PL_CFG_REG;
  pl_cfg_data[0] = 0;
  if(enable) {
    pl_cfg_data[0] |= MMA8652_PL_CFG_PL_EN_MASK;
    /* Decrements debounce whenever the condition of interest is no longer valid */
    pl_cfg_data[0] &= ~MMA8652_PL_CFG_DBCNTM_MASK;
  }
  else {
    pl_cfg_data[0] &= ~MMA8652_PL_CFG_PL_EN_MASK;
  }
  i2c_write(pl_cfg_reg, 1, false);
  i2c_write(pl_cfg_data, 1, true);

  /* Power on the sensor again! */
  mma8652_set_power_bit(true);

  return true;
}

bool mma8652_new_orientation_data_available(void) {
  uint8_t new_orientation[1];
  new_orientation[0] = MMA8652_PL_STATUS_REG;
  i2c_write(new_orientation, 1, false);  /* do not send stop signal */
  new_orientation[0] = 0;
	i2c_read(new_orientation, 1);  		     /* read 1 byte and send stop now */

  new_orientation[0] &= MMA8652_PL_STATUS_NEWLP_MASK;
  if (new_orientation[0] == MMA8652_PL_STATUS_NEWLP_MASK) {
    _mma8652_orientation_data.status = true;
    return true;
  }
  else {
    _mma8652_orientation_data.status = false;
    return false;
  }
}

struct MMA8652_orientation_data mma8652_read_orientation_data(void) {
  dputs("[mma8652_read_orientation_data] Reading new orientation info");

  mma8652_clear_orientation_data();

  uint8_t orientation[1];
  orientation[0] = MMA8652_PL_STATUS_REG;
  i2c_write(orientation, 1, false);  /* do not send stop signal */
  orientation[0] = 0;
  i2c_read(orientation, 1);  		     /* read 1 byte and send stop now */

  if((orientation[0] & MMA8652_PL_STATUS_BAFRO_MASK) == MMA8652_PL_STATUS_BAFRO_MASK) {
    _mma8652_orientation_data.front = false;
  }
  else {
    _mma8652_orientation_data.front = true;
  }
  if((orientation[0] & MMA8652_PL_STATUS_LAPO_MASK) == MMA8652_PL_STATUS_LAPO_PORTRAIT_UP) {
    _mma8652_orientation_data.portrait_up = true;
  }
  if((orientation[0] & MMA8652_PL_STATUS_LAPO_MASK) == MMA8652_PL_STATUS_LAPO_PORTRAIT_DOWN) {
    _mma8652_orientation_data.portrait_up = false;
  }
  if((orientation[0] & MMA8652_PL_STATUS_LAPO_MASK) == MMA8652_PL_STATUS_LAPO_LANDSCAPE_RIGHT) {
    _mma8652_orientation_data.landscape_right = true;
  }
  if((orientation[0] & MMA8652_PL_STATUS_LAPO_MASK) == MMA8652_PL_STATUS_LAPO_LANDSCAPE_LEFT) {
    _mma8652_orientation_data.landscape_right = false;
  }

  return _mma8652_orientation_data;
}

struct MMA8652_orientation_data mma8652_get_orientation_data(void) {
  return _mma8652_orientation_data;
}
