/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     target
 * @{
 *
 * @file        startup.c
 * @brief       Startup code and interrupt vector definition
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

/* C standard library */
#include <stdio.h>
#include <stdint.h>

/* Dandelion library */
#include "dandelion.h"

/* Platform */
#include "nrf51.h"

/* Utilities */
#include "util/debug.h"

/**
 * @brief       Memory markers as defined in the linker script
 */
extern uint32_t _sfixed;
extern uint32_t _efixed;
extern uint32_t _etext;
extern uint32_t _srelocate;
extern uint32_t _erelocate;
extern uint32_t _szero;
extern uint32_t _ezero;
extern uint32_t _sstack;
extern uint32_t _estack;
extern uint32_t _sfixed;

/**
 * @brief       External main function
 */
extern int main(void);

/**
 * @brief       External standard C library initialization function
 */
extern void __libc_init_array(void);

/**
 * @brief       Application entry point after reset
 * @details     After a system reset, the following steps are required and
 *              carried out:
 *                  1. Load data section from flash to ram;
 *                  2. Default bss section to zero;
 *                  3. Initialize the dandelion board;
 *                  4. Calling main function;
 */
void reset_handler(void) {
    uint32_t *dst;
    uint32_t *src = &_etext;

    /* Make sure all RAM blocks are turned on */
    /* See nrf51822 Product Anomaly Notice (PAN) #16 for more details */
//    NRF_POWER->RAMON = 0xF;

    /*
     * Handle PAN 42 (See Product Anomaly Notice v2.4 - Page 29)
     * "System: Writing to RAM right after reset or turning it ON fails."
     *
     * Solution:
     * Do not access RAM for 0.5 us after a reset or enabling a RAM block.
     * Note that function calls normally make use of the RAM.
     */

    /* Delay for 8 NOP instructions (= 0.5 us) */
//    __NOP();
//    __NOP();
//    __NOP();
//    __NOP();
//    __NOP();
//    __NOP();
//    __NOP();
//    __NOP();
    __set_PSP((uint32_t)_sfixed);
    __set_CONTROL(0x2);
    __ISB();

    /* Load data section from flash to ram */
    for (dst = &_srelocate; dst < &_erelocate; ) {
        *(dst++) = *(src++);
    }

    /* Default bss section to zero */
    for (dst = &_szero; dst < &_ezero; ) {
        *(dst++) = 0;
    }

    /* Initialize Dandelion board */
    dandelion_init();

    /* Initialize standard library */
    __libc_init_array();

    /* Call main */
    if (main() < 0) {
        /* Main function exited with failure */
        dandelion_die();
    }

    /* Main function exited with success */
    while (1);
}

/**
 * @brief       Default interrupt handler
 * @details     The default interrupt handler is called when no other interrupt
 *              handler is defined.
 */
void dummy_handler(void) {
    while (1);
}

/**
 * @brief       Non-maskable interrupt handler
 * @details     This interrupt is unused, as such the Dandelion board will die
 *              if control reaches this point.
 */
void isr_nmi(void) {
    dandelion_die();
}

/**
 * @brief       Prototype for the hard fault debugging function
 */
void hard_fault_debug(unsigned long *sp) asm("_hard_fault_debug");

/**
 * @brief       Hard fault debugging function
 * @details     This function performs a short stack dump that will be useful
 *              to debug hard faults.
 * @param[in]   sp
 *              The stack pointer of the last stacked frame.
 */
void hard_fault_debug(unsigned long *sp) {
#ifndef DEBUG
    (void) sp;
#endif

#ifdef DEBUG
    volatile unsigned long stacked_r0;
    volatile unsigned long stacked_r1;
    volatile unsigned long stacked_r2;
    volatile unsigned long stacked_r3;
    volatile unsigned long stacked_r12;
    volatile unsigned long stacked_lr;
    volatile unsigned long stacked_pc;
    volatile unsigned long stacked_psr;
    volatile unsigned long _CFSR;
    volatile unsigned long _HFSR;
    volatile unsigned long _DFSR;
    volatile unsigned long _AFSR;
    volatile unsigned long _BFAR;
    volatile unsigned long _MMAR;

    /* Retrieve stacked registers */
    stacked_r0  = sp[0];
    stacked_r1  = sp[1];
    stacked_r2  = sp[2];
    stacked_r3  = sp[3];
    stacked_r12 = sp[4];
    stacked_lr  = sp[5];
    stacked_pc  = sp[6];
    stacked_psr = sp[7];

    /* Retrieve Configurable Fault Status Register */
    /* Consists of MMSR, BFSR, UFSR */
    _CFSR = *((volatile unsigned long *) (0xE000ED28));

    /* Retrieve Hard Fault Status Register */
    _HFSR = *((volatile unsigned long *) (0xE000ED2C));

    /* Retrieve Debug Fault Status Register */
    _DFSR = *((volatile unsigned long *) (0xE000ED30));

    /* Retrieve Auxiliary Fault Status Register */
    _AFSR = *((volatile unsigned long *) (0xE000ED3C));

    /* Read the Fault Address Registers. These may not contain valid values */
    /* Check BFARVALID/MMARVALID to see if they are valid values */

    /* MemManage Fault Address Register */
    _MMAR = *((volatile unsigned long *) (0xE000ED34));

    /* Bus Fault Address Register */
    _BFAR = *((volatile unsigned long *) (0xE000ED38));

    /* Print debug information */
    dprintf("Dandelion Hard Fault Handler\r\n");
    dprintf("Stacked Registers:\r\n");
    dprintf(" r0: 0x%lx\r\n", stacked_r0);
    dprintf(" r1: 0x%lx\r\n", stacked_r1);
    dprintf(" r2: 0x%lx\r\n", stacked_r2);
    dprintf(" r3: 0x%lx\r\n", stacked_r3);
    dprintf("r12: 0x%lx\r\n", stacked_r12);
    dprintf(" lr: 0x%lx\r\n", stacked_lr);
    dprintf(" pc: 0x%lx\r\n", stacked_pc);
    dprintf("psr: 0x%lx\r\n", stacked_psr);
    dprintf("Fault Registers:\r\n");
    dprintf("_CFSR: 0x%lx\r\n", _CFSR);
    dprintf("_HFSR: 0x%lx\r\n", _HFSR);
    dprintf("_DFSR: 0x%lx\r\n", _DFSR);
    dprintf("_AFSR: 0x%lx\r\n", _AFSR);
    dprintf("_MMAR: 0x%lx\r\n", _MMAR);
    dprintf("_BFAR: 0x%lx\r\n", _BFAR);
#endif

    /* Die */
    dandelion_die();
}

/**
 * @brief       Hard fault interrupt handler
 * @details     This interrupt is generated when something happened that wasn't
 *              supposed to, such as unaligned memory access. If control reaches
 *              this point, we should do a short stack dump in order to easily
 *              determine the cause.
 *              Note, the 'naked' attribute prevents GCC from generating the
 *              prologue and epilogue for C functions. As such, only assembly
 *              code shall be used.
 */
void __attribute__((naked)) isr_hard_fault(void) {
    /* Procedure:
     *      1. Determine if the processor is using the PSP or MSP stack pointers
     *         by checking bit 2 from LR register;
     *      2. Copy wither PSP or MSP conditionally to R0 register
     *         (first C function argument);
     *      3. Give up control to the debug function;
     */

    asm volatile (
        "movs   r0, #4\n\t"
        "mov    r1, lr\n\t"
        "tst    r0, r1\n\t"
        "beq    _is_msp\n\t"
        "mrs    r0, psp\n\t"
        "b      _hard_fault_debug\n\t"
    "_is_msp:\n\t"
        "mrs    r0, msp\n\t"
        "b      _hard_fault_debug\n\t"
    );
}

/* Cortex-M specific interrupt vectors */
void isr_svc(void)              __attribute__ ((weak, alias("dummy_handler")));
void isr_pendsv(void)           __attribute__ ((weak, alias("dummy_handler")));
void isr_systick(void)          __attribute__ ((weak, alias("dummy_handler")));

/* nRF51822qfaa specific interrupt vector */
void isr_power_clock(void)      __attribute__ ((weak, alias("dummy_handler")));
void isr_radio(void)            __attribute__ ((weak, alias("dummy_handler")));
void isr_uart0(void)            __attribute__ ((weak, alias("dummy_handler")));
void isr_spi0_twi0(void)        __attribute__ ((weak, alias("dummy_handler")));
void isr_spi1_twi1(void)        __attribute__ ((weak, alias("dummy_handler")));
void isr_gpiote(void)           __attribute__ ((weak, alias("dummy_handler")));
void isr_adc(void)              __attribute__ ((weak, alias("dummy_handler")));
void isr_timer0(void)           __attribute__ ((weak, alias("dummy_handler")));
void isr_timer1(void)           __attribute__ ((weak, alias("dummy_handler")));
void isr_timer2(void)           __attribute__ ((weak, alias("dummy_handler")));
void isr_rtc0(void)             __attribute__ ((weak, alias("dummy_handler")));
void isr_temp(void)             __attribute__ ((weak, alias("dummy_handler")));
void isr_rng(void)              __attribute__ ((weak, alias("dummy_handler")));
void isr_ecb(void)              __attribute__ ((weak, alias("dummy_handler")));
void isr_ccm_aar(void)          __attribute__ ((weak, alias("dummy_handler")));
void isr_wdt(void)              __attribute__ ((weak, alias("dummy_handler")));
void isr_rtc1(void)             __attribute__ ((weak, alias("dummy_handler")));
void isr_qdec(void)             __attribute__ ((weak, alias("dummy_handler")));
void isr_lpcomp(void)           __attribute__ ((weak, alias("dummy_handler")));
void isr_swi0(void)             __attribute__ ((weak, alias("dummy_handler")));
void isr_swi1(void)             __attribute__ ((weak, alias("dummy_handler")));
void isr_swi2(void)             __attribute__ ((weak, alias("dummy_handler")));
void isr_swi3(void)             __attribute__ ((weak, alias("dummy_handler")));
void isr_swi4(void)             __attribute__ ((weak, alias("dummy_handler")));
void isr_swi5(void)             __attribute__ ((weak, alias("dummy_handler")));

/**
 * @brief       Interrupt vector table
 */
__attribute__((section(".vectors")))
void (* const interrupt_vector[])(void) = {
    /* stack pointer */
    (void (*)(void)) ((unsigned long) &_estack),

    /* Cortex-M handlers */
    &reset_handler,             /* Entry point after reset */
    &isr_nmi,                   /* Non-maskable interrupt */
    &isr_hard_fault,            /* Hard fault */
    (void (*)(void)) 0UL,       /* Reserved */
    (void (*)(void)) 0UL,       /* Reserved */
    (void (*)(void)) 0UL,       /* Reserved */
    (void (*)(void)) 0UL,       /* Reserved */
    (void (*)(void)) 0UL,       /* Reserved */
    (void (*)(void)) 0UL,       /* Reserved */
    (void (*)(void)) 0UL,       /* Reserved */
    &isr_svc,                   /* Supervisor call */
    (void (*)(void)) 0UL,       /* Reserved */
    (void (*)(void)) 0UL,       /* Reserved */
    &isr_pendsv,                /* Pendsv */
    &isr_systick,               /* System tick */

    /* nRF51 specific peripheral handlers */
    &isr_power_clock,           /* Power clock */
    &isr_radio,                 /* Radio */
    &isr_uart0,                 /* UART0 */
    &isr_spi0_twi0,             /* SPI0 TWI0 */
    &isr_spi1_twi1,             /* SPI1 TWI1 */
    (void (*)(void)) 0UL,       /* Reserved */
    &isr_gpiote,                /* GPIOTE */
    &isr_adc,                   /* ADC */
    &isr_timer0,                /* Timer0 */
    &isr_timer1,                /* Timer1 */
    &isr_timer2,                /* Timer2 */
    &isr_rtc0,                  /* RTC0 */
    &isr_temp,                  /* TEMP */
    &isr_rng,                   /* RNG */
    &isr_ecb,                   /* ECB */
    &isr_ccm_aar,               /* CCM AAR */
    &isr_wdt,                   /* WDT */
    &isr_rtc1,                  /* RTC1 */
    &isr_qdec,                  /* QDEC */
    &isr_lpcomp,                /* LPCOMP */
    &isr_swi0,                  /* SWI0 */
    &isr_swi1,                  /* SWI1 */
    &isr_swi2,                  /* SWI2 */
    &isr_swi3,                  /* SWI3 */
    &isr_swi4,                  /* SWI4 */
    &isr_swi5,                  /* SWI5 */
    (void (*)(void)) 0UL,       /* Reserved */
    (void (*)(void)) 0UL,       /* Reserved */
    (void (*)(void)) 0UL,       /* Reserved */
    (void (*)(void)) 0UL,       /* Reserved */
    (void (*)(void)) 0UL,       /* Reserved */
    (void (*)(void)) 0UL        /* Reserved */
};
