/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     app
 * @{
 *
 * @file        positioning.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Tiago Tom�s <tiago.tomas@sensefinity.com>
 *
 * @}
 */

/* Interface */
#include "app/positioning.h"

/* API */
#include "api/sysclk.h"

/* BLE */
#include "ble/ll.h"

/* Sensoroid library */
#include "sensoroid/sensoroid.h"

/* Utilities */
#include "util/debug.h"

#include "sensoroid/gateway.h"

/********************************** Private ***********************************/

/* Constants */
#define NEIGHBOR_TABLE_SIZE     (10)
#define MAX_POSITION_NEIGHBORS  (10)
#define SCANNING_DURATION       (2400)

/**
 * @brief       Computes the static array length.
 */
#define ARRAY_LEN(x) ((sizeof(x)) / sizeof((x)[0]))

/**
 * @brief       Conditional re-definition of the MIN macro.
 */
#ifndef MIN
#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#endif

/**
 * @brief       Holds the positioning interval.
 */
static uint64_t _interval;

/**
 * @brief       Holds the previous timestamp.
 */
static uint64_t _prev_timestamp;

/**
 * @brief       Implements a general Columbus "Position" message for recording
 *              neighbor devices.
 */
static uint8_t _columbus_position[24 + MAX_POSITION_NEIGHBORS * 10] = {
    0x02,                                               /* Message Version */
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,     /* Serial */
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,     /* Timestamp */
    0x05,                                               /* Sub-Message Type - Position */
    0x00, 0x00, 0x00, 0x00,                             /* Total Data Size */
    0x04,                                               /* Position - RSSI */
    0x00                                                /* RSSI Count */

    /* Collection of pairs of Serial (8 bytes) + RSSI (2 bytes) */
};

/**
 * @brief       Defines the structure of one neighbor.
 */
struct neighbor {
    struct sensoroid_data   data;           /* The raw Sensoroid data */
    struct bdaddr           addr;           /* Address of peer device */
    uint64_t                t_found;        /* The time at which it was added */
    uint64_t                t_updated;      /* The time at which it was updated */
    int32_t                 rssi_accum;     /* The accumulation of the various RSSIs */
    int32_t                 rssi_count;     /* The number of received RSSIs */
};

/**
 * @brief       Holds the table of neighbors devices.
 */
static struct neighbor _neighbor_table[NEIGHBOR_TABLE_SIZE];

/**
 * @brief       Holds the number of neighbor devices in the table.
 */
static size_t _neighbor_count = 0;

/**
 * @brief       Holds the BLE scanning parameters.
 */
static const struct ll_scan_params _SCAN_PARAMS = {
    .type           = LL_SCAN_TYPE_PASSIVE,
    .interval       = 0x0500,       /* 800 ms */
    .window         = 0x0500,       /* 800 ms */
    .channel_map    = LL_ADVERTISING_CHANNEL_MAP_ALL
};

/**
 * @brief       Sets the serial number on a Columbus message.
 * @param       message
 *              Pointer to the Columbus message.
 * @param       serial
 *              The serial number to set.
 */
static void _columbus_set_serial(uint8_t *message, uint64_t serial) {
    int i;
    uint8_t *ptr = (uint8_t *) &serial;

    /* The serial number always starts @ offset = 1 */
    for (i = (sizeof(uint64_t) - 1); i >= 0; --i) {
        message[1 + i] = *ptr++;
    }
}

/**
 * @brief       Sets the timestamp on a Columbus message.
 * @param       message
 *              Pointer to the Columbus message.
 * @param       timestamp
 *              The timestamp to set.
 */
static void _columbus_set_timestamp(uint8_t *message, uint64_t timestamp) {
    int i;
    uint8_t *ptr = (uint8_t *) &timestamp;

    /* The timestamp always starts @ offset = 9 */
    for (i = (sizeof(uint64_t) - 1); i >= 0; --i) {
        message[9 + i] = *ptr++;
    }
}

/**
 * @brief       Sets the size of the data portion on a Columbus message.
 * @param       message
 *              Pointer to the Columbus message.
 * @param       size
 *              The size to be set.
 */
static void _columbus_set_data_size(uint8_t *message, uint32_t size) {
    int i;
    uint8_t *ptr = (uint8_t *) &size;

    /* The data size always starts @ offset = 18 */
    for (i = (sizeof(uint32_t) - 1); i >= 0; --i) {
        message[18 + i] = *ptr++;
    }
}

/**
 * @brief       Comparator function for quick sort.
 * @param       e1
 *              Pointer to the first element.
 * @param       e2
 *              Pointer to the second element.
 * @return      The result of comparing the elements by RSSI.
 */
static int _sort_by_rssi(const void *e1, const void *e2) {
    const struct neighbor *n1 = (const struct neighbor *) e1;
    const struct neighbor *n2 = (const struct neighbor *) e2;
    int32_t rssi1 = n1->rssi_accum / n1->rssi_count;
    int32_t rssi2 = n2->rssi_accum / n2->rssi_count;
    return (int) (rssi2 - rssi1);
}

/**
 * @brief       Parses data from the table of neighbors and sends a position
 *              message.
 */
static void _send_position(void) {
    size_t i;
    int j;
    struct queue_element *element;
    size_t elements;
    int offset;
    uint8_t *ptr;

    dputs("[Positioning] Sending position...");

    /* Try to reserve a new queue element */
    element = queue_reserve();
    if (element == NULL) {
        /* We were unable to reserve space in the queue, as such we won't
           be sending a new message */
        dputs("[Positioning] Could not reserve space in the queue");
        return;
    }

    /* Compute number of elements to use for positioning */
    elements = MIN(_neighbor_count, MAX_POSITION_NEIGHBORS);

    /* Initialize base data */
    _columbus_set_serial(_columbus_position, sensoroid_get_serial());
    _columbus_set_timestamp(_columbus_position, sysclk_get());
    _columbus_set_data_size(_columbus_position, 2 + elements * 10);

    /* Initialize RSSI Count field (number of devices) */
    _columbus_position[23] = elements;

    if (_neighbor_count > elements) {
        /* We have more neighbors than we want, as such, we'll sort the
           neighbors by RSSI, so that we have only the "best" devices */
        qsort(_neighbor_table, _neighbor_count,
                sizeof(struct neighbor), &_sort_by_rssi);
    }

    for (i = 0; i < elements; ++i) {
        /* Compute offset of the <Serial, RSSI> pair in the message */
        offset = 24 + 10 * i;

        /* Set serial number for the current element */
        ptr = (uint8_t *) &_neighbor_table[i].data.serial_node;

        for (j = sizeof(uint64_t) - 1; j >= 0; --j) {
            _columbus_position[offset++] = ptr[j];
        }

        /* Set RSSI for the current element */
        _columbus_position[offset++] = 0xFF;
        _columbus_position[offset] =
                (_neighbor_table[i].rssi_accum / _neighbor_table[i].rssi_count);
    }

    /* Initialize queue element */
    element->size = 24 + elements * 10;
    memcpy(element->data, _columbus_position, element->size);

    /* Commit message to the queue */
    queue_commit();
}

/**
 * @brief       Parses a sensoroid_data struct from the advertising data in
 *              an advertising report.
 * @param       report
 *              The report to be parsed.
 * @param       data
 *              Pointer to the output structure.
 * @returns     A boolean value indicating if the parsing procedure was
 *              successful.
 */
static bool _parse_sensoroid_data(const struct ll_advertising_report *report,
        struct sensoroid_data *data) {
    size_t i = 0;

    while (i < report->size) {
        if ((i + report->data[i]) < report->size) {
            /* We have valid GAP data */
            if (report->data[i + 1] == 0xFF) {
                /* We have found manufacturer-specific data */
                if ((report->data[i] - 3) == sizeof(struct sensoroid_data)
                        && report->data[i + 2] == 0xFF
                        && report->data[i + 3] == 0xFF) {
                    /* The expected company identifier matches (0xFFFF) as
                       such we'll assume we've found a valid Sensoroid */
                    memcpy(data, &report->data[i + 4], sizeof(struct sensoroid_data));

                    return true;
                }
            }

            /* Advance to next GAP data */
            i += (report->data[i] + 1);
        }
    }

    /* We have malformed GAP data and/or we didn't find a Sensoroid device */
    return false;
}

/**
 * @brief       Handles advertising reports from the BLE scanning state.
 * @param       report
 *              The report to be handled.
 */
static void _advertising_report_handler(struct ll_advertising_report report) {
    struct sensoroid_data data;
    size_t i;
    bool exists = false;
    uint64_t t_now;

    if (report.type == LL_ADVERTISING_REPORT_TYPE_CONNECTABLE_UNDIRECTED
            || report.type == LL_ADVERTISING_REPORT_TYPE_SCANNABLE_UNDIRECTED) {
        if (_parse_sensoroid_data(&report, &data)) {
            /* Add data to table of neighbors */
            for (i = 0; i < _neighbor_count; ++i) {
                if (_neighbor_table[i].data.serial_node == data.serial_node) {
                    /* The neighbor already exists, update */
                    _neighbor_table[i].data = data;
                    _neighbor_table[i].t_updated = sysclk_get();
                    _neighbor_table[i].rssi_accum += report.rssi;
                    _neighbor_table[i].rssi_count += 1;
                    exists = true;
                }
            }

            if (!exists) {
                /* Device does not yet exist, add to table if we have enough
                   space */
                if ((_neighbor_count + 1) < NEIGHBOR_TABLE_SIZE) {
                    t_now = sysclk_get();
                    _neighbor_table[_neighbor_count].data = data;
                    _neighbor_table[_neighbor_count].addr = report.addr;
                    _neighbor_table[_neighbor_count].t_found = t_now;
                    _neighbor_table[_neighbor_count].t_updated = t_now;
                    _neighbor_table[_neighbor_count].rssi_accum = report.rssi;
                    _neighbor_table[_neighbor_count].rssi_count = 1;
                    ++_neighbor_count;
                }
            }
        }
    }
}

/**
 * @brief       Configures and starts the scanning state.
 */
static void _scanning_start(void) {
    ll_set_advertising_report_handler(&_advertising_report_handler);
    ll_set_scan_params(_SCAN_PARAMS);
    _neighbor_count = 0;
    ll_set_scan_enable(true);
}

/**
 * @brief       Stops the scanning state.
 */
static void _scanning_stop(void) {
    ll_set_scan_enable(false);
}

/********************************** Public ************************************/

/**
 * @brief       Initializes the positioning service.
 * @param       interval
 *              The positioning interval in milliseconds.
 */
void positioning_init(uint64_t interval) {
    _interval = interval;
    _prev_timestamp = sysclk_get();
}

/**
 * @brief       Updates the positioning service.
 */
void positioning_loop(void) {
    uint64_t t_now = sysclk_get();
    uint64_t t_delta = t_now - _prev_timestamp;

    if (ll_get_state() == LL_STATE_SCANNING) {
        if (t_delta >= SCANNING_DURATION) {
            /* Stop scanning */
            _scanning_stop();

            dputs("[Positioning] Scanning stop!");

            if (_neighbor_count > 0) {
                /* Send position */
                _send_position();
            }

            /* Finally, resume advertising state for the Gateway */
            sensoroid_gateway_resume();

            /* Update previous timestamp */
            /* NOTE: A new sysclk_get is used in order to discard the processing
               time between consecutive measurements */
            _prev_timestamp = sysclk_get();
        }

    } else {
        if (t_delta >= _interval && ll_set_advertise_enable(false)) {
            dputs("[Positioning] Starting scanning!");
            /* It's time for a new position and we were able to exit the
               advertising state! Go into the scanning state for positioning */
            _scanning_start();

            /* Update previous timestamp */
            /* NOTE: A new sysclk_get is used in order to discard the processing
               time between consecutive measurements */
            _prev_timestamp = sysclk_get();
        }
    }
}
