/**
 * Copyright (c) 2016 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     app
 * @{
 *
 * @file        transient.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      João Ambrósio <joao.ambrosio@sensefinity.com>
 *
 * @}
 */

/* Interface */
#include "app/transient.h"

/********************************** Private ***********************************/

/**
 * @brief       It contains transient data
 */
static struct MMA8652_transient_data _transient_data;

/**
 * @brief       It contains transient data
 */
static bool _transient_interrupt = false;

/**
 * @brief       This function will get the new transient info
 */
void accelerometer_print_transient(void) {
  //TODO: It's only printing info. Next improvement is to do something!
  dputs("[accelerometer] Transient detected!");

  _transient_data = mma8652_get_transient_data();

  dprintf("[accelerometer] x_transient: %s\n", ((_transient_data.x_transient)?"true":"false"));
  if(_transient_data.x_transient) {
    dprintf("[accelerometer] x_polarity: %s\n", ((_transient_data.x_polarity)?"+":"-"));
  }
  dprintf("[accelerometer] y_transient: %s\n", ((_transient_data.y_transient)?"true":"false"));
  if(_transient_data.y_transient) {
    dprintf("[accelerometer] y_polarity: %s\n", ((_transient_data.y_polarity)?"+":"-"));
  }
  dprintf("[accelerometer] z_transient: %s\n", ((_transient_data.z_transient)?"true":"false"));
  if(_transient_data.z_transient) {
    dprintf("[accelerometer] z_polarity: %s\n", ((_transient_data.z_polarity)?"+":"-"));
  }
}

/**
 * @brief       This function will set the read transient flag
 */
void transient_interrupt(void) {
	_transient_interrupt = true;

	//Try now to reserve I2C bus. If not try next in the loop!
	if(mma8652_reserve_i2c_bus()) {
		mma8652_read_transient_data();
		i2c_shutdown();
		accelerometer_print_transient();
		_transient_interrupt = false;
	}
}

/********************************** Public ************************************/

/**
 * @brief       Initializes the transient measurement service
 * @param[in]   hp_cutoff_frequency
 *              The desired cutoff frequency. Possible values:
  *							- MMA8652_HP_FILTER_CUTOFF_SEL_00;
  *							- MMA8652_HP_FILTER_CUTOFF_SEL_01;
  *							- MMA8652_HP_FILTER_CUTOFF_SEL_10;
  *							- MMA8652_HP_FILTER_CUTOFF_SEL_11.
  *              Note that the filter cutoff options change based on the data rate
  *              selected. Check the datasheet for more informations.
 *              x_enabled
 *              True = enable interrupt in x, false = disable it.
 *              y_enabled
 *              True = enable interrupt in y, false = disable it.
 *              z_enabled
 *              True = enable interrupt in z, false = disable it.
 *              bypass_hp_filter_enabled
 *              True = High-pass filter bypass enabled, false = desable bypass.
 *              transient_threshold
 *              The acceleration threshold.
 *              count_threshold
 *              How many times it has to be verified to trigger the interruption.
 */
void transient_setup(uint8_t hp_cutoff_frequency, bool x_enabled, bool y_enabled, bool z_enabled, bool bypass_hp_filter_enabled, uint8_t transient_threshold, uint8_t count_threshold) {
	if(mma8652_reserve_i2c_bus()) {
		//Configure the high-pass filter
		mma8652_set_hp_filter(true);
		mma8652_set_hp_cutoff_frequency(hp_cutoff_frequency);

		mma8652_clear_interrupts();
		accelerometer_configure_interrupt(MMA8652_INT_1_PIN, &transient_interrupt);
		mma8652_set_transient_profile(((x_enabled)?MMA8652_FF_MT_CFG_XEFE_MASK:~MMA8652_FF_MT_CFG_XEFE_MASK),
				((y_enabled)?MMA8652_FF_MT_CFG_YEFE_MASK:~MMA8652_FF_MT_CFG_YEFE_MASK),
				((z_enabled)?MMA8652_FF_MT_CFG_ZEFE_MASK:~MMA8652_FF_MT_CFG_ZEFE_MASK),
				((bypass_hp_filter_enabled)?~MMA8652_TRANSIENT_CFG_HPF_BYP_MASK:MMA8652_TRANSIENT_CFG_HPF_BYP_MASK),
				transient_threshold,
				count_threshold);
		mma8652_enable_transient_profile(true);
		mma8652_set_interrupt(MMA8652_CTRL4_INT_EN_TRANS_MASK, MMA8652_INT_1, true);

		i2c_shutdown();
	}
}

/**
 * @brief       Updates the transient measurement service
 */
void transient_loop(void) {
	if(_transient_interrupt) {
		if(mma8652_reserve_i2c_bus()) {
			mma8652_read_transient_data();
			i2c_shutdown();
			accelerometer_print_transient();
			_transient_interrupt = false;
		}
	}
}
