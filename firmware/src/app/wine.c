/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     app
 * @{
 *
 * @file        wine.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

/* Interface */
#include "app/wine.h"

/* C standard library */
#include <stdint.h>
#include <stdio.h>

/* API */
#include "api/sysclk.h"

/* Peripherals */
#include "periph/pin_name.h"
#include "periph/uart0.h"

/* Sensoroid library */
#include "sensoroid/sensoroid.h"

/* Utilities */
#include "util/debug.h"

/********************************** Private ***********************************/

/* Useful definitions */
#define LINE_MAX (32)

/* Computes an array length statically */
#define ARRAY_LEN(x) ((sizeof(x)) / sizeof((x)[0]))

/**
 * @brief       Buffer for one line of input.
 */
static char _line[LINE_MAX];

/**
 * @brief       Index of the current character in the line buffer.
 */
static size_t _index = 0;

/**
 * @brief       Holds the previous wavelength.
 */
static int _prev_wavelength = 0;

/**
 * @brief       Holds a Columbus "Measurement" message.
 */
static uint8_t _columbus_measurement[] = {
    0x01,                                               /* Version */
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,     /* Device Serial */
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,     /* Timestamp */
    0x06,                                               /* Message Type (Measurement) */
    0x00, 0x00, 0x00, 0x06,                             /* Data Size */
    0x00,                                               /* Measurement Sensor */
    0x80,                                               /* Measurement Type */
    0x00, 0x00, 0x00, 0x00                              /* Measurement Value (FP @ 32-bit) */
};

/**
 * @brief       Sets the serial number on a Columbus message.
 * @param       message
 *              Pointer to the Columbus message.
 * @param       serial
 *              The serial number to set.
 */
static void _columbus_set_serial(uint8_t *message, uint64_t serial) {
    int i;
    uint8_t *ptr = (uint8_t *) &serial;

    /* The serial number always starts @ offset = 1 */
    for (i = (sizeof(uint64_t) - 1); i >= 0; --i) {
        message[1 + i] = *ptr++;
    }
}

/**
 * @brief       Sets the timestamp on a Columbus message.
 * @param       message
 *              Pointer to the Columbus message.
 * @param       timestamp
 *              The timestamp to set.
 */
static void _columbus_set_timestamp(uint8_t *message, uint64_t timestamp) {
    int i;
    uint8_t *ptr = (uint8_t *) &timestamp;

    /* The timestamp always starts @ offset = 9 */
    for (i = (sizeof(uint64_t) - 1); i >= 0; --i) {
        message[9 + i] = *ptr++;
    }
}

/**
 * @brief       Sets an unsigned value in a Columbus "Measurement" message.
 * @param       message
 *              Pointer to the Columbus message.
 * @param       value
 *              The value to be set.
 */
static void _columbus_set_uint_value(uint8_t *message, unsigned value) {
    uint32_t encoded;

    /* Encode value */
    encoded = value;

    /* Apply header (27 IBITS, 0 FBITS) */
    encoded |= (0b01000 << 27);

    /* Write value */
    message[24 + 0] = (uint8_t) (encoded >> 24);
    message[24 + 1] = (uint8_t) (encoded >> 16);
    message[24 + 2] = (uint8_t) (encoded >>  8);
    message[24 + 3] = (uint8_t) (encoded >>  0);
}

/**
 * @brief       Sets a floating point value in a Columbus "Measurement" message.
 * @param       message
 *              Pointer to the Columbus message.
 * @param       value
 *              The value to be set.
 */
static void _columbus_set_float_value(uint8_t *message, float value) {
    uint32_t encoded;

    /* NOTE: Using format 16 (FBITS), 11 (IBITS) */

    /* Scale value */
    value *= ((float) (1L << 16));

    /* Encode value */
    encoded = (uint32_t) value;
    encoded |= (0b01010 << 27);

    /* Write value */
    message[24 + 0] = (uint8_t) (encoded >> 24);
    message[24 + 1] = (uint8_t) (encoded >> 16);
    message[24 + 2] = (uint8_t) (encoded >>  8);
    message[24 + 3] = (uint8_t) (encoded >>  0);
}

/**
 * @brief       Enqueues the current Columbus message for transmission.
 */
static void _enqueue(void) {
    /* Put message in the queue */
    struct queue_element *element;
    element = queue_reserve();
    if (element == NULL) {
        return;
    }

    element->size = ARRAY_LEN(_columbus_measurement);
    memcpy(element->data, _columbus_measurement, element->size);
    queue_commit();
}

/**
 * @brief       Sends a photodetector message.
 * @param       wavelength
 *              The measurement wavelength.
 * @param       channel
 *              The measurement channel.
 * @param       value
 *              The measurement value.
 */
static void _send_photodetector_message(int wavelength, int channel, int value) {
    /* Set timestamp */
    _columbus_set_timestamp(_columbus_measurement, sysclk_get());

    /* Set measurement sensor */
    wavelength = ((wavelength - 1) & 0x3) << 2;
    channel = (channel - 1) & 0x3;
    _columbus_measurement[22] = 0x00 | wavelength | channel;

    /* Set measurement type */
    _columbus_measurement[23] = 0x80;

    /* Set measurement value */
    _columbus_set_uint_value(_columbus_measurement, (unsigned) value);

    /* Enqueue message for delivery */
    _enqueue();
}

/**
 * @brief       Sends a battery message.
 * @param       value
 *              The battery voltage.
 */
static void _send_battery_message(float value) {
    /* Set timestamp */
    _columbus_set_timestamp(_columbus_measurement, sysclk_get());

    /* Set measurement sensor */
    _columbus_measurement[22] = 0x00;

    /* Set measurement type */
    _columbus_measurement[23] = 0x07;

    /* Set measurement value */
    _columbus_set_float_value(_columbus_measurement, value);

    /* Enqueue message for delivery */
    _enqueue();
}

/**
 * @brief       Processes one line of input.
 */
static void _process_line(void) {
    int wavelength;
    int channels[3];
    int matches;
    int channel;

    dprintf("Processing line => %s\r\n", _line);
    matches = sscanf(_line, "%d %x %x %x",
        &wavelength, &channels[0], &channels[1], &channels[2]);

    if (matches == 2) {
        /* Battery */
        dprintf("Sending battery message with value=%d\r\n", channels[0]);
        _send_battery_message(((float) channels[0]) / 100.0F);

    } else if (matches == 4) {
        if (_prev_wavelength == wavelength) {
            /* Ignore repeated wavelength */
            return;
        }

        _prev_wavelength = wavelength;

        /* Photodetector */
        for (channel = 1; channel < matches; ++channel) {
            dprintf("Sending photodetector message with wavelength=%d, channel=%d, value=%d",
                wavelength, channel, channels[channel - 1]);

            _send_photodetector_message(wavelength, channel, channels[channel - 1]);
        }
    }
}

/**
 * @brief       Handles events coming from the UART.
 */
static void _uart_event(void) {
    char ch;
    while (uart0_readable()) {
        ch = (char) uart0_read();
        if (ch == '\n') {
            /* Finish line */
            _line[_index] = '\0';

            /* Process line */
            _process_line();
            _index = 0;

        } else {
            /* Keep reading line */
            _line[_index++] = ch;
        }
    }
}

/********************************** Public ************************************/

/**
 * @brief     Initializes the wine measurement service.
 */
void wine_init(void) {
    /* Initialize the UART for communication */
    uart0_init(POF_RXD, POF_TXD);
    uart0_baud(POF_BAUD);
    uart0_attach(&_uart_event);

    /* Initialize static data in the Columbus message */
    _columbus_set_serial(_columbus_measurement, sensoroid_get_serial());
}

/**
 * @brief     Updates the wine measurement service.
 */
void wine_loop(void) {

}
