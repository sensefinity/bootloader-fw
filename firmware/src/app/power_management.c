/**
 * Copyright (c) 2016 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     app
 * @{
 *
 * @file        power_management.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Jo�o Ambr�sio <joao.ambrosio@sensefinity.com>
 *
 * @}
 */

/* Interface */
#include "app/power_management.h"

/********************************** Private ***********************************/

static Power_States _current_power_state = POWERED_OFF;

/********************************** Public ************************************/

void power_management_setup(void) {
	#ifndef DEBUG
	_current_power_state = POWERED_OFF;
	#else
	_current_power_state = POWERED_ON;
	#endif
}

void power_management_loop(void) {
	switch (_current_power_state) {
		case PREPARE_POWER_OFF: {
			telco_power_management_order(TURN_OFF);
			_current_power_state = POWER_OFF;
			break;
		}

		case POWER_OFF: {
			if(c_queue_is_empty(get_exp_tx_queue_pointer())) {
				dandelion_deep_sleep();
				_current_power_state = POWERED_OFF;
			}
			break;
		}

		case POWERED_OFF: {
			break;
		}

		case POWER_ON: {
			//dandelion_init();
			telco_power_management_order(TURN_ON);
			_current_power_state = POWERED_ON;
			break;
		}

		case POWERED_ON: {
			break;
		}

		default: {
			break;
		}
	}
}

void power_management_set_state(Power_States power_state) {
	_current_power_state = power_state;
}

Power_States power_management_get_state(void) {
	return _current_power_state;
}
