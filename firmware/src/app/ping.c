/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     app
 * @{
 *
 * @file        ping.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 *
 * @}
 */

/* Interface */
#include "app/ping.h"

#include "expansion/expansion_definitions.h"
#include "expansion/exp_packet.h"
#include "expansion/expansion_services.h"
#include "sensoroid/config.h"
#include "sensoroid/c_queue.h"
#include "expansion/expansion_apps.h"
#include "expansion/expansion.h"

#include "util/debug.h"

 static uint64_t t_last_ping = 0;


void ping_loop(uint64_t t_now, uint64_t interval) {
	if (t_last_ping == 0 || (t_now - t_last_ping) >= interval) {
		dputs("[ping_loop] new ping needed!");
		t_last_ping=t_now;

		/* TODO Just based on GSM for now */
		if(expansion_service_is_active(EX_PROTOCOL_APP_GSM)) {
		 /* We have up-link lets order a new Ping!!! */

			dputs("GSM is active");
			/* GSM service is enabled */
			/* Send  command to order a ping */
			uint64_t expansion_serial = expansion_services_get_serial(EX_PROTOCOL_APP_GSM);

			/* Create Expansion packet */
			uint8_t msg[EXP_PROTOCOL_MSG_MAX_SIZE];
			/* Prevention */
			memset(msg,0x00,EXP_PROTOCOL_MSG_MAX_SIZE);

			/* Header */
			exp_set_origin_serial(msg, sensoroid_get_serial());
			exp_set_dst_serial(msg, expansion_serial); /* Set expansion board address */
			exp_set_message_type(msg, EX_PROTOCOL_APP_MESSAGE); /* Temperature App */
			exp_set_message_from_base(msg);

			/* APP message payload */
			/* GSM app */
			expansion_apps_set_app_id(msg,EX_PROTOCOL_APP_GSM);
			/* Ping order! */
			expansion_apps_set_type(msg,APP_PING_ORDER);

			/* Set payload  and message lenght */
			size_t payload_size = 2;
			size_t message_size = EX_PROTOCOL_HEADER_SIZE + payload_size;
			exp_set_message_size(msg, payload_size);

			 if(expansion_tx_status_ok()) {
				/* Add message to Tx expansion queue */
				/* Try reserve space in the queue */
				if(c_queue_reserve(get_exp_tx_queue_pointer(),message_size)) {
					c_queue_copy_and_commit_data(get_exp_tx_queue_pointer(),msg, message_size);
				} else {
					/* We were not able to reserve space, as such, don't "take" this
					measurement and wait for the next */
					return;
				}
			}
		} else {
			dputs("[ping_loop] GSM NOT active!!");
		}
	}
}
