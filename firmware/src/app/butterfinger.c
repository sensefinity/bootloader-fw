/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     app
 * @{
 *
 * @file        butterfinger.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

/* Interface */
#include "app/butterfinger.h"

/* C standard library */
#include <stddef.h>
#include <string.h>

/* API */
#include "api/sysclk.h"

/* Peripherals */
#include "periph/uart0.h"

/* Sensoroid library */
#include "sensoroid/common.h"
#include "sensoroid/queue.h"

/* Utilities */
#include "util/delay.h"

/********************************** Private ***********************************/

/* Useful definitions */
#define ASCII_SOH       (0x01)
#define ASCII_EOT       (0x04)
#define ASCII_ACK       (0x06)
#define ASCII_NAK       (0x15)
#define SYMBOL_SEND     ('S')
#define SYMBOL_ACK      ('A')
#define ACK_TIMEOUT     (2000)
#define ARRAY_LEN(x)    (sizeof(x) / sizeof((x)[0]))

/**
 * @brief       Holds the template for an acknowledgement response.
 */
static const uint8_t _ACK_TEMPLATE[] = {
    ASCII_SOH,
    SYMBOL_ACK,
    ASCII_ACK,
    ASCII_EOT
};

/**
 * @brief       Sets the timestamp on a Columbus message.
 * @param       message
 *              Pointer to the Columbus message.
 * @param       timestamp
 *              The timestamp to set.
 */
static void _columbus_set_timestamp(uint8_t *message, uint64_t timestamp) {
    int i;
    uint8_t *ptr = (uint8_t *) &timestamp;

    /* The timestamp always starts @ offset = 9 */
    for (i = (sizeof(uint64_t) - 1); i >= 0; --i) {
        message[9 + i] = *ptr++;
    }
}

/**
 * @brief       Obtains the timestamp of a Columbus message.
 * @param       message
 *              Pointer to the Columbus message.
 * @return      The timestamp of the Columbus message.
 */
static uint64_t _columbus_get_timestamp(const uint8_t *message) {
    int i;
    uint64_t timestamp;
    uint8_t *ptr = (uint8_t *) &timestamp;

    /* The timestamp always starts @ offset = 9 */
    for (i = (sizeof(uint64_t) - 1); i >= 0; --i) {
        *ptr++ = message[9 + i];
    }

    return timestamp;
}

/**
 * @brief       Clears the UART from any lingering junk data.
 */
static void _uart0_clear(void) {
    while (uart0_readable()) {
        /* There is pending junk data, read it */
        uart0_read();

        /* Wait approximately the UART's symbol time before we test it for
           readability once more */
        delay_us(105);
    }
}

/**
 * @brief       Sends one packet to the Butterfinger device.
 * @param       data
 *              Pointer to the data to be sent.
 * @param       size
 *              The size of the data to be sent.
 */
static void _butterfinger_send(const uint8_t *data, size_t size) {
    uart0_write(ASCII_SOH);
    uart0_write(SYMBOL_SEND);
    uart0_write(0x01);
    uart0_write((size >> 8) & 0xFF);
    uart0_write((size >> 0) & 0xFF);

    while (size--) {
        uart0_write(*data++);
    }

    uart0_write(ASCII_EOT);
}

/**
 * @brief       Attempts to receive an acknowledgement from the Butterfinger
 *              device.
 * @param       timeout
 *              The time to wait in milliseconds before a timeout.
 * return       True if an acknowledgement was received or false, otherwise.
 */
static bool _butterfinger_ack(uint64_t timeout) {
    uint64_t t_now;
    uint8_t buffer[ARRAY_LEN(_ACK_TEMPLATE)];
    size_t size = 0;

    /* Read the current system time */
    t_now = sysclk_get();

    do {
        if (uart0_readable()) {
            if ((size + 1) > ARRAY_LEN(_ACK_TEMPLATE)) {
                /* We don't have enough space to receive the acknowledgement,
                   as such we can conclude that we won't receive it */
                return false;
            }

            /* Read next character */
            buffer[size++] = uart0_read();

            if (size == ARRAY_LEN(_ACK_TEMPLATE)) {
                /* We received a message with the size we're looking for */
                if (memcmp(buffer, _ACK_TEMPLATE, ARRAY_LEN(_ACK_TEMPLATE)) == 0) {
                    /* We found an acknowledgement */
                    return true;
                } else {
                    /* We're surely not going to receive an acknowledgement */
                    return false;
                }
            }
        }

    } while ((sysclk_get() - t_now) <= timeout);

    /* The function timed-out */
    return false;
}

/********************************** Public ************************************/

void butterfinger_init(void) {
    /* Initialize UART */
    uart0_init(BUTTERFINGER_RXD, BUTTERFINGER_TXD);

    /* The remaining UART parameters are set to the defaults */
}

void butterfinger_loop(void) {
    struct queue_element *element;
    uint64_t t_now;
    uint64_t t_message;

    /* Get current system time */
    t_now = sysclk_get();

    /* Peek the next element from the queue */
    element = queue_peek();

    if (element != NULL) {
        /* Retrieve message's timestamp */
        t_message = _columbus_get_timestamp(element->data);

        /* Compute adjusted timestamp and set it on the message */
        _columbus_set_timestamp(element->data, t_now - t_message);

        /* Send message to the Butterfinger device */
        _butterfinger_send(element->data, element->size);

        /* Acknowledgement */
        _uart0_clear();
        if (_butterfinger_ack(ACK_TIMEOUT)) {
            /* The message was successfully sent, remove it from the queue */
            queue_delete();

        } else {
            /* We failed to send the message, restore message timestamp */
            _columbus_set_timestamp(element->data, t_message);
        }
    }
}
