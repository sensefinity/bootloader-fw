/**
 * Copyright (c) 2016 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     app
 * @{
 *
 * @file        motion.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      João Ambrósio <joao.ambrosio@sensefinity.com>
 *
 * @}
 */

/* Interface */
#include "app/motion.h"

/********************************** Private ***********************************/

/**
 * @brief       It contains freefall motion data
 */
static struct MMA8652_motion_data _motion_data;

/**
 * @brief       It contains freefall motion data
 */
static bool _motion_interrupt = false;

/**
 * @brief       This function will print the new motion info
 */
void accelerometer_print_motion(void) {
  //TODO: It's only printing info. Next improvement is to do something!
  dputs("[accelerometer] Motion detected!");

  _motion_data = mma8652_get_motion_data();

  dprintf("[accelerometer] x_motion: %s\n", ((_motion_data.x_motion)?"true":"false"));
  if(_motion_data.x_motion) {
    dprintf("[accelerometer] x_polarity: %s\n", ((_motion_data.x_polarity)?"+":"-"));
  }
  dprintf("[accelerometer] y_motion: %s\n", ((_motion_data.y_motion)?"true":"false"));
  if(_motion_data.y_motion) {
    dprintf("[accelerometer] y_polarity: %s\n", ((_motion_data.y_polarity)?"+":"-"));
  }
  dprintf("[accelerometer] z_motion: %s\n", ((_motion_data.z_motion)?"true":"false"));
  if(_motion_data.z_motion) {
    dprintf("[accelerometer] z_polarity: %s\n", ((_motion_data.z_polarity)?"+":"-"));
  }
}

/**
 * @brief       This function will set the read motion flag
 */
void motion_interrupt(void) {
	_motion_interrupt = true;

	//Try now to reserve I2C bus. If not try next in the loop!
	if(mma8652_reserve_i2c_bus()) {
		mma8652_read_freefall_motion_data();
		i2c_shutdown();
		accelerometer_print_motion();
		_motion_interrupt = false;
	}
}

/********************************** Public ************************************/

/**
 * @brief       Initializes the motion measurement service
 * @param[in]   freefall_enabled
 *              True = freefall profile, false = motion profile.
 *              x_enabled
 *              True = enable interrupt in x, false = disable it.
 *              y_enabled
 *              True = enable interrupt in y, false = disable it.
 *              z_enabled
 *              True = enable interrupt in z, false = disable it.
 *              motion_threshold
 *              The acceleration threshold.
 *              count_threshold
 *              How many times it has to be verified to trigger the interruption.
 */
void motion_setup(bool freefall_enabled, bool x_enabled, bool y_enabled, bool z_enabled, uint8_t motion_threshold, uint8_t count_threshold) {
	if(mma8652_reserve_i2c_bus()) {
		  mma8652_clear_interrupts();
		  accelerometer_configure_interrupt(MMA8652_INT_2_PIN, &motion_interrupt);
		  mma8652_set_freefall_motion_profile(((freefall_enabled)?~MMA8652_FF_MT_CFG_OAE_MASK:MMA8652_FF_MT_CFG_OAE_MASK),
		    ((x_enabled)?MMA8652_FF_MT_CFG_XEFE_MASK:~MMA8652_FF_MT_CFG_XEFE_MASK),
		    ((y_enabled)?MMA8652_FF_MT_CFG_YEFE_MASK:~MMA8652_FF_MT_CFG_YEFE_MASK),
		    ((z_enabled)?MMA8652_FF_MT_CFG_ZEFE_MASK:~MMA8652_FF_MT_CFG_ZEFE_MASK),
		    motion_threshold,
		    count_threshold);
		  mma8652_enable_freefall_motion_profile(true);
		  mma8652_set_interrupt(MMA8652_CTRL4_INT_EN_FF_MT_MASK, MMA8652_INT_2, true);

		i2c_shutdown();
	}
}

/**
 * @brief       Updates the motion measurement service
 */
void motion_loop(void) {
	if(_motion_interrupt) {
		if(mma8652_reserve_i2c_bus()) {
			mma8652_read_freefall_motion_data();
			i2c_shutdown();
			accelerometer_print_motion();
			_motion_interrupt = false;
		}
	}
}
