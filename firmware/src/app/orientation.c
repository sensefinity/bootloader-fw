/**
 * Copyright (c) 2016 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     app
 * @{
 *
 * @file        orientation.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      João Ambrósio <joao.ambrosio@sensefinity.com>
 *
 * @}
 */

/* Interface */
#include "app/orientation.h"

/********************************** Private ***********************************/

/**
 * @brief       Indicates if it's time to check if the accelerometer orientation has changed
 */
bool _check_orientation_changes = false;

/**
 * @brief       Tells if the first orientation profile was collected at the beggining.
 *              Just to force collection at the beggining.
 */
static bool _orientation_collected_at_beggining = false;

/**
 * @brief       It contains orientation data
 */
static struct MMA8652_orientation_data _orientation_data;

/**
 * @brief       Handles one interval for this application
 * @details     Effectively, this function will change the flag.
 */
static void orientation_interval(void) {
    _check_orientation_changes = true;
}

/**
 * @brief       This function will print the new accelerometer orientation info
 */
void accelerometer_print_orientation(void) {
  //TODO: It's only printing info. Next improvement is to do something!
  dputs("[accelerometer] Orientation changed!");

  _orientation_data = mma8652_get_orientation_data();

  dprintf("[accelerometer] front: %s\n", ((_orientation_data.front)?"true":"false"));
  dprintf("[accelerometer] landscape right: %s\n", ((_orientation_data.landscape_right)?"true":"false"));
  dprintf("[accelerometer] portrait up: %s\n", ((_orientation_data.portrait_up)?"true":"false"));
}

/********************************** Public ************************************/

/**
 * @brief       Initializes the orientation measurement service
 * @param[in]   interval
 *              This interval corresponds to the sample rate for checking orientation
 *              changes in the accelerometer (check the orientation changed flag).
 */
void orientation_setup(uint64_t interval) {
  _orientation_collected_at_beggining = false;

  if(mma8652_reserve_i2c_bus()) {
	  mma8652_enable_orientation_detection(true);
	  i2c_shutdown();
  }

  /* Take first measurement and schedule remaining measurements */
  orientation_interval();

  assert(sched_interval(&orientation_interval, interval) >= 0);
}

/**
 * @brief       Updates the orientation measurement service
 */
void orientation_loop(void) {
	if(_check_orientation_changes) {
		if(mma8652_reserve_i2c_bus()) {
			if(mma8652_new_orientation_data_available()) {
				_orientation_collected_at_beggining = true;
				mma8652_read_orientation_data();
				accelerometer_print_orientation();
			}

			if(_orientation_collected_at_beggining) {
				_check_orientation_changes = false;
			}

			i2c_shutdown();
		}
	}
}
