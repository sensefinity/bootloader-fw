/**
 * Copyright (c) 2016 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     app
 * @{
 *
 * @file        button.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Jo�o Ambr�sio <joao.ambrosio@sensefinity.com>
 *
 * @}
 */

/* Interface */
#include <app/button.h>

/********************************** Private ***********************************/

static bool _button_toggled = true;

static uint64_t _pressed_time = 0;

static uint64_t _unpressed_time = 0;

void button_unpressed(void);
void button_pressed(void);

void button_pressed(void) {
	_pressed_time = sysclk_get();

	button_attach_unpress_function(&button_unpressed);
}

void button_unpressed(void) {
	_unpressed_time = sysclk_get();
	_button_toggled = true;

	button_attach_press_function(&button_pressed);
}

/********************************** Public ************************************/

void button_init() {
	gpio_input_init(BUTTON_PIN, GPIO_PULL_UP);

	_button_toggled = true;
	_pressed_time = 0;
	_unpressed_time = 0;

	if(!is_button_pressed()) {
		button_attach_press_function(&button_pressed);
	}
	else {
		button_attach_unpress_function(&button_unpressed);
	}
}

void button_attach_press_function(void (*handler)(void)) {
	gpio_attach_pin_function(handler, BUTTON_PIN, PIN_POLARITY_LOW);
}

void button_attach_unpress_function(void (*handler)(void)) {
	gpio_attach_pin_function(handler, BUTTON_PIN, PIN_POLARITY_HIGH);
}

bool is_button_pressed(void) {
	return !gpio_input_read(BUTTON_PIN);
}

bool button_toggled(void) {
	return _button_toggled;
}

void set_button_toggled(bool button_toggled) {
	_button_toggled = button_toggled;
}

int64_t get_button_pressed_time(void) {
	return _unpressed_time - _pressed_time;
}

uint64_t get_button_pressed_timestamp(void) {
	return _pressed_time;
}

uint64_t get_button_unpressed_timestamp(void) {
	return _unpressed_time;
}
