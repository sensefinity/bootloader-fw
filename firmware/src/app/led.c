/**
 * Copyright (c) 2016 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     app
 * @{
 *
 * @file        led.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Jo�o Ambr�sio <joao.ambrosio@sensefinity.com>
 *
 * @}
 */

/* Interface */
#include <app/led.h>

/********************************** Private ***********************************/

/**
 * @brief     The id of the blinking schedule function
 */
static sched_id_t _blink_sched_id = -1;

/**
 * @brief     The id of the led breath schedule function
 */
static sched_id_t _fade_sched_id = -1;

static sched_id_t _blink_sched_id_timeout = -1;

static sched_id_t _fade_sched_id_timeout   = -1;

static int32_t _current_brightness = 0;

static int _number_of_blinks = -1;

static int8_t _brightness_increment = BRIGHTNESS_INCREMENT;

void led_breath_int(void) {
	_current_brightness = _current_brightness + _brightness_increment;

	if (_current_brightness == -200 || _current_brightness == 255) {
		_brightness_increment = -_brightness_increment;
	}

	if(_current_brightness >= 0) {
		pwm_set(LED_PIN, (uint8_t)_current_brightness);
	}
}

void led_fade_in_int(void) {
	if (_current_brightness == 255) {
		led_disable_fade();
		led_on();
		return;
	}

	_current_brightness = _current_brightness + _brightness_increment;

	pwm_set(LED_PIN, _current_brightness);
}

void led_fade_out_int(void) {
	if (_current_brightness == 0) {
		led_disable_fade();
		return;
	}

	_current_brightness = _current_brightness + _brightness_increment;

	pwm_set(LED_PIN, _current_brightness);
}

void led_blink_int(void) {
	led_toogle();

	if (_blink_sched_id != -1) {
		if (_number_of_blinks != -1) {
			if (_number_of_blinks == 0) {
				led_disable_blink();
			}

			if (led_status()) {
				_number_of_blinks--;
			}
		}
	}
}

void led_clear_all_int(void) {
	if (_blink_sched_id != -1) {
		led_disable_blink();
	}

	if (_fade_sched_id != -1) {
		led_disable_fade();
	}
}

/********************************** Public ************************************/

void led_init(void) {
	gpio_output_init(LED_PIN);
}

void led_on(void) {
	gpio_output_set(LED_PIN);
}

void led_off(void) {
	gpio_output_clear(LED_PIN);
}

bool led_status(void) {
	return gpio_input_read(LED_PIN);
}

void led_toogle(void) {
	gpio_output_toggle(LED_PIN);
}

void led_enable_blink(uint64_t blinking_period_ms) {
	/* Prevention! Disable all interrupts and put the led off and in the initial state */
	led_clear_all_int();

	_blink_sched_id = sched_interval(&led_blink_int, blinking_period_ms);
}

void led_enable_blink_with_duration(uint64_t blinking_period_ms,
		uint64_t blinking_duration_ms) {
	/* Prevention! Disable all interrupts and put the led off and in the initial state */
	led_clear_all_int();

	_blink_sched_id = sched_interval(&led_blink_int, blinking_period_ms);

	/* Disable it after breathing_duration_ms */
	_blink_sched_id_timeout = sched_interval(&led_disable_blink, blinking_duration_ms);
}

void led_enable_blink_with_blinking_number(uint64_t blinking_period_ms,
		uint8_t number_of_blinks) {
	/* Prevention! Disable all interrupts and put the led off and in the initial state */
	led_clear_all_int();

	_blink_sched_id = sched_interval(&led_blink_int, blinking_period_ms);

	_number_of_blinks = number_of_blinks;
}

void led_disable_blink(void) {
	if(_blink_sched_id != -1) {
		sched_cancel(_blink_sched_id);
		led_off();
		_blink_sched_id = -1;
		_number_of_blinks = -1;

		if(_blink_sched_id_timeout != -1) {
			sched_cancel(_blink_sched_id_timeout);
		}
	}
}

void led_enable_breath(void) {
	/* Prevention! Disable all interrupts and put the led off and in the initial state */
	led_clear_all_int();

	_current_brightness = -100;
	_brightness_increment = BRIGHTNESS_INCREMENT;

	pwm_set(LED_PIN, 0);
	pwm_enable();

	_fade_sched_id = sched_interval(&led_breath_int, PWM_UPDATE_FREQ);
}

void led_enable_breath_with_duration(uint64_t breathing_duration_ms) {
	/* Prevention! Disable all interrupts and put the led off and in the initial state */
	led_clear_all_int();

	_current_brightness = -100;
	_brightness_increment = BRIGHTNESS_INCREMENT;

	pwm_set(LED_PIN, 0);
	pwm_enable();

	_fade_sched_id = sched_interval(&led_breath_int, PWM_UPDATE_FREQ);

	/* Disable it after breathing_duration_ms */
	_fade_sched_id_timeout = sched_interval(&led_disable_fade, breathing_duration_ms);
}

void led_fade_in(void) {
	/* Prevention! Disable all interrupts and put the led off and in the initial state */
	led_clear_all_int();

	_current_brightness = 0;
	_brightness_increment = BRIGHTNESS_INCREMENT;

	pwm_set(LED_PIN, _current_brightness);
	pwm_enable();

	_fade_sched_id = sched_interval(&led_fade_in_int, PWM_UPDATE_FREQ);
}

void led_fade_out(void) {
	/* Prevention! Disable all interrupts and put the led off and in the initial state */
	led_clear_all_int();

	_current_brightness = 255;
	_brightness_increment = -BRIGHTNESS_INCREMENT;

	pwm_set(LED_PIN, _current_brightness);
	pwm_enable();

	_fade_sched_id = sched_interval(&led_fade_out_int, PWM_UPDATE_FREQ);
}

void led_disable_fade(void) {
	if(_fade_sched_id != -1) {
		sched_cancel(_fade_sched_id);
		pwm_disable();
		led_off();
		_fade_sched_id = -1;

		if(_fade_sched_id_timeout != -1) {
			sched_cancel(_fade_sched_id_timeout);
		}
	}
}

bool is_led_busy(void) {
	if (_blink_sched_id != -1 || _fade_sched_id != -1) {
		return true;
	} else {
		return false;
	}
}
