/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     app
 * @{
 *
 * @file        perfcool.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

/* Interface */
#include "app/perfcool.h"

/********************************** Private ***********************************/

/* Defintions for the Columbus fixed-point format @ 32-bit */
#define FBITS   (16)
#define IBITS   (11)
#define HEADER  (0b01010 << 27)
#define FMASK   ((1 << FBITS) - 1)
#define IMASK   (((1 << (FBITS + IBITS)) - 1) & ~FMASK)

/* Computes an array length statically */
#define ARRAY_LEN(x) ((sizeof(x)) / sizeof((x)[0]))

/**
 * @brief       Holds the "A" constant for the solving of a quadratic equation.
 */
static const float A = -0.00262F;

/**
 * @brief       Holds the "B" constant for the solving of a quadratic equation.
 */
static const float B = -8.194F;

/**
 * @brief       Holds the scaling factor that converts a ADC sample to a valid
 *              voltage.
 */
static const float SCALE = ADC_REFERENCE / (float) ((1 << ADC_RESOLUTION) - 1);

/**
 * @brief       Holds the ADC channel we'll use for sampling the sensor.
 */
static uint32_t _channel;

/**
 * @brief       Holds a Columbus Temperature message.
 */
static uint8_t _columbus_temperature[] = {
    0x01,                                               /* Version */
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,     /* Device Serial */
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,     /* Timestamp */
    0x06,                                               /* Message Type (Measurement) */
    0x00, 0x00, 0x00, 0x06,                             /* Data Size */
    0x01,                                               /* Measurement Sensor */
    0x01,                                               /* Measurement Type (Temperature) */
    0x00, 0x00, 0x00, 0x00                              /* Measurement Value (FP @ 32-bit) */
};

/**
 * @brief       Generic function that inverts the endianness of a sequence
 *              of bytes.
 * @param[in]   data
 *              Pointer to the data for which endianness is to be inverted.
 * @param[in]   size
 *              The size of the data for which endianness is to be inverted.
 */
static void _invert_endianness(void *data, size_t size) {
    size_t i;
    size_t j;
    uint8_t tmp;
    uint8_t *ptr = (uint8_t *) data;

    for (i = 0; i < size / 2; ++i) {
        /* Compute swap index */
        j = size - i -1;

        /* Swap */
        tmp = ptr[i];

        ptr[i] = ptr[j];
        ptr[j] = tmp;
    }
}

/**
 * @brief       Sets the serial number for the Columbus message.
 * @param       serial
 *              The serial number to be set.
 */
static void _columbus_set_serial(uint64_t serial) {
    uint8_t *serialptr = (uint8_t *) &serial;
    size_t i;

    /* Set serial number on the message */
    for (i = 0; i < sizeof(uint64_t); ++i) {
        _columbus_temperature[1 + i] = serialptr[i];
    }

    _invert_endianness(_columbus_temperature + 1, sizeof(uint64_t));
}

/**
 * @brief       Sets the timestamp for the Columbus message.
 * @param       timestamp
 *              The timestamp to be set.
 */
static void _columbus_set_timestamp(uint64_t timestamp) {
    uint8_t *timeptr = (uint8_t *) &timestamp;
    size_t i;

    /* Set serial number on the message */
    for (i = 0; i < sizeof(uint64_t); ++i) {
        _columbus_temperature[9 + i] = timeptr[i];
    }

    _invert_endianness(_columbus_temperature + 9, sizeof(uint64_t));
}

/**
 * @brief       Sets the temperature for the Columbus message.
 * @param       temperature
 *              The temperature to be set.
 */
static void _columbus_set_temperature(float value) {
    uint32_t encoded;

    /* Scale value value and apply correction factor */
    value *= ((float) (1L << FBITS));
    /*value += ((value >= 0.0F) ? 0.5F : -0.5F);*/

    /* Encode value */
    encoded = (uint32_t) value;
    encoded &= (IMASK | FMASK);
    encoded |= HEADER;

    _columbus_temperature[24 + 0] = (uint8_t) (encoded >> 24);
    _columbus_temperature[24 + 1] = (uint8_t) (encoded >> 16);
    _columbus_temperature[24 + 2] = (uint8_t) (encoded >>  8);
    _columbus_temperature[24 + 3] = (uint8_t) (encoded >>  0);
}

/**
 * @brief       Converts a pin name to the corresponding ADC channel.
 */
static uint32_t _adc_channel(enum pin_name name) {
    switch (name) {
    case AIN0: return 0;
    case AIN1: return 1;
    case AIN2: return 2;
    case AIN3: return 3;
    case AIN4: return 4;
    case AIN5: return 5;
    case AIN6: return 6;
    case AIN7: return 7;
    default:
        return ((uint32_t) -1);
    }
}

/**
 * @brief       Function to be called for each interval in the Perfect Cool
 *              "App". This function shall handle the taking of a new
 *              temperature measurement.
 */
static void _perfcool_interval(void) {
    float c;
    uint16_t sample;
    float voltage;
    float temperature;
    struct queue_element *element;

    /* Reserve space in the queue */
    element = queue_reserve();

    if (element == NULL) {
        /* We could not reserve space; As such we won't take a new measurement,
           since we don't need to */
        return;
    }

    /* Power on the sensor and wait for it to turn on */
    gpio_output_set(LMT85LP_VDD);
    delay_ms(1);

    /* Read a new sample and power off sensor */
    sample = adc_sample(_channel);
    gpio_output_clear(LMT85LP_VDD);

    /* Convert sample to a voltage that we can use */
    voltage = ((float) sample) * SCALE;

    /* The formula for the temperature is solved using a quadratic equation
       and it uses voltage in mV; Using this information, compute the (C)
       factor for the equation */
    c = 1324.0F - voltage * 1000.0F;

    /* Finally solve for temperature */
    temperature = (-B - sqrt((B * B) - 4.0F * A * c)) / (2.0F * A);

    /* Apply reference temperature (30 degrees) */
    temperature += 30.0F;

    /* Initialize Columbus message in the queue */
    _columbus_set_timestamp(sysclk_get());
    _columbus_set_temperature(temperature);
    element->size = ARRAY_LEN(_columbus_temperature);
    memcpy(element->data, _columbus_temperature, ARRAY_LEN(_columbus_temperature));
    queue_commit();
}

/********************************** Public ************************************/

void perfcool_setup(void) {
    /* Initialize the pin we'll use for powering the sensor */
    gpio_output_init(LMT85LP_VDD);
    gpio_output_clear(LMT85LP_VDD);

    /* Initialize the ADC channel */
    _channel = _adc_channel(LMT85LP_OUT);

    /* Initialize serial number for the Columbus message */
    _columbus_set_serial(sensoroid_get_serial());

    /* Create a scheduler task for the operation of the "App" */
    _perfcool_interval();
    assert(sched_interval(&_perfcool_interval, PERFCOOL_INTERVAL) >= 0);
}

void perfcool_loop(void) {}
