#include "app/telco_power_management.h"


/* Sensoroid library */
#include "sensoroid/sensoroid.h"

#include "expansion/expansion.h"
#include "expansion/expansion_services.h"
#include "expansion/exp_packet.h"
#include "expansion/expansion_protocol.h"
#include "expansion/expansion_apps.h"
#include "expansion/expansion_definitions.h"

#include "util/debug.h"

void telco_power_management_order(uint8_t order) {

	 if(expansion_service_is_active(EX_PROTOCOL_APP_GSM)) {
		/* Create/try send ORDER */
		dputs("[telco_power_management_order] in!");

		/* Send command to telco */
		uint64_t expansion_serial = expansion_services_get_serial(EX_PROTOCOL_APP_GSM);

		/* Create Expansion packet */
		uint8_t msg[EXP_PROTOCOL_MSG_MAX_SIZE];
		/* Prevention */
		memset(msg,0x00,EXP_PROTOCOL_MSG_MAX_SIZE);

		/* Header */
		exp_set_origin_serial(msg, sensoroid_get_serial());
		exp_set_dst_serial(msg, expansion_serial); /*Set expansion board address */
		exp_set_message_type(msg, EX_PROTOCOL_APP_MESSAGE); /* Temperature App */
		exp_set_message_from_base(msg);

		/* Power management message payload */
		/* Power management app */
		expansion_apps_set_app_id(msg,EX_PROTOCOL_APP_POWER_MANAGEMENT);
		/* ORDER msg */
		expansion_apps_set_type(msg, ORDER);
		expansion_pm_set_order(msg, order);

		/* Set payload  and message lenght */
		size_t payload_size = 3;
		size_t message_size = EX_PROTOCOL_HEADER_SIZE + payload_size;
		exp_set_message_size(msg, payload_size);


		/* Add message to Tx expansion queue */
		if(expansion_tx_status_ok()) {
			/* Try reserve space in the queue */
			if(c_queue_reserve(get_exp_tx_queue_pointer(),message_size)) {
				c_queue_copy_and_commit_data(get_exp_tx_queue_pointer(),msg, message_size);
			} else {
				/* We were not able to reserve space, as such, don't "take" this
				measurement and wait for the next */
				return;
			}
		}
	 }else {
		 dputs("[telco_power_management_order] GSM not active");
	 }
}
