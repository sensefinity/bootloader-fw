/**
 * Copyright (c) 2016 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     app
 * @{
 *
 * @file        barometer.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      João Ambrósio <joao.ambrosio@sensefinity.com>
 *
 * @}
 */

/* Interface */
#include "app/barometer.h"

/********************************** Private ***********************************/

/**
 * @brief       It will store data from the MPL3115A2 sensor
 */
static struct MPL3115A2_data _mpl3115a2_data;

/**
 * @brief       Indicates if it's time to collect new barometer data
 */
bool _collect_barometer_data = false;

/**
 * @brief       Indicates if I'm waiting for new data
 */
bool _waiting_for_data = false;

/**
 * @brief       Handles one interval for this application
 * @details     Effectively, this function will change the flag for taking
 *              a new measurement.
 */
static void _barometer_interval(void) {
    _collect_barometer_data = true;
}

/**
 * @brief       This function will print the raw pressure info present in the data structure
 */
void barometer_print_pressure(void) {
  //TODO: It's only printing info. Next improvement is to do something!
  dputs("[barometer] New barometer info!");

  _mpl3115a2_data = mpl3115a2_get_data();

  dprintf("[barometer] pressure: %d\n", _mpl3115a2_data.press_alt);
  dprintf("[barometer] temperature: %d\n", _mpl3115a2_data.temperature);
}

/********************************** Public ************************************/

/**
 * @brief       Initializes the barometer measurement service
 * @param[in]   interval
 *              This interval corresponds to the sample rate of collecting
 *              the preassure from the barometer sensor.
 */
void barometer_setup(uint64_t interval) {
  /* Take first measurement and schedule remaining measurements */
  _barometer_interval();
  assert(sched_interval(&_barometer_interval, interval) >= 0);
}

/**
 * @brief       Updates the barometer measurement service
 */
void barometer_loop(void) {
	if (_collect_barometer_data && !_waiting_for_data) {
		if(mpl3115a2_reserve_i2c_bus()) {
			mpl3115a2_power_on();
			mpl3115a2_set_output_rate(MPL3115A2_CTRL1_OS_128);
			mpl3115a2_barometer_enable();
			mpl3115a2_set_raw(true);

			i2c_shutdown();

			_waiting_for_data = true;
		}
	}

	if(_waiting_for_data) {
		if(mpl3115a2_reserve_i2c_bus()) {
			if(mpl3115a2_new_data_available()) {
				mpl3115a2_read_data();
				barometer_print_pressure();
		        mpl3115a2_power_off();
		        _waiting_for_data = false;
		        _collect_barometer_data = false;
			}

			i2c_shutdown();
		}
	}
}
