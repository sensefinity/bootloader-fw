/**
 * Copyright (c) 2016 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     app
 * @{
 *
 * @file        accelerometer.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      João Ambrósio <joao.ambrosio@sensefinity.com>
 *
 * @}
 */

/* Interface */
#include "app/accelerometer.h"

/********************************** Private ***********************************/

/**
 * @brief       It contains xyz axis values
 */
static struct MMA8652_xyz_data _xyz_data;

/**
 * @brief       This function will print the new xyz axis info available
 */
void accelerometer_print_xyz_axis(void) {
  //TODO: It's only printing info. Next improvement is to do something!
  dputs("[accelerometer] New xyz axis info!");

  _xyz_data = mma8652_get_xyz_data();

  dprintf("[accelerometer] x_mg: %d\n", _xyz_data.x_mg);
  dprintf("[accelerometer] y_mg: %d\n", _xyz_data.y_mg);
  dprintf("[accelerometer] z_mg: %d\n", _xyz_data.z_mg);
}

/********************************** Public ************************************/

/**
 * @brief       Initializes the accelerometer measurement service
 */
void accelerometer_setup(void) {
  if(mma8652_reserve_i2c_bus()) {
	  mma8652_power_on();

	  mma8652_set_dynamic_range(MMA8652_XYZ_CFG_FS_2G);
	  mma8652_set_active_data_rate(MMA8652_CTRL1_DR_1_56_Hz);
	  mma8652_set_oversampling_active_mode(MMA8652_CTRL2_MODS_NORMAL);

	  mma8652_set_auto_sleep_mode(true);
	  mma8652_set_sleep_data_rate(MMA8652_CTRL1_ASLP_1_56_Hz);
	  mma8652_set_oversampling_sleep_mode(MMA8652_CTRL2_SMODS_NORMAL);

	  mma8652_set_fast_read(false);

	  i2c_shutdown();
  }
}

/**
 * @brief       Updates the accelerometer measurement service
 */
void accelerometer_loop(void) {

}

void accelerometer_configure_interrupt(uint8_t pin_name, void (*handler)(void)) {
  gpio_input_init(pin_name, GPIO_PULL_UP);

  gpio_attach_pin_function(handler, pin_name, PIN_POLARITY_LOW);
}
