/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     app
 * @{
 *
 * @file        temperature.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 *
 * @}
 */

/* Interface */
#include "app/temperature.h"

/* C standard library */
#include <assert.h>

/* API */
#include "api/sched.h"

/* Sensoroid library */
#include "sensoroid/sensoroid.h"

#include "expansion/expansion.h"
#include "expansion/expansion_services.h"
#include "expansion/exp_packet.h"
#include "expansion/expansion_protocol.h"
#include "expansion/expansion_apps.h"
#include "expansion/expansion_definitions.h"


/* Utilities */
#include "util/debug.h"

#include "app/telco_power_management.h"
/********************************** Private ***********************************/

static uint64_t t_last_temperature = 0;

/**
 * @brief       Handles one interval for this application.
 * @details     Effectively, this function will "order" a new measurement of the
 *              temperature.
 */
static void _temperature_interval(void) {

    if(expansion_service_is_active(EX_PROTOCOL_APP_TEMPERATURE)) {

        dputs("Temperature is active");
        /* Temperature service is enabled */
        /* Send  command get temperature */
        uint64_t expansion_serial = expansion_services_get_serial(EX_PROTOCOL_APP_TEMPERATURE);

        /* Create Expansion packet */
        uint8_t msg[EXP_PROTOCOL_MSG_MAX_SIZE];
        /* Prevention */
        memset(msg,0x00,EXP_PROTOCOL_MSG_MAX_SIZE);

        /* Header */
        exp_set_origin_serial(msg, sensoroid_get_serial());
        exp_set_dst_serial(msg, expansion_serial); /*Set expansion board address */
        exp_set_message_type(msg, EX_PROTOCOL_APP_MESSAGE); /* Temperature App */
        exp_set_message_from_base(msg);

        /* Temprature message payload */
        /* Temperature app */
        expansion_apps_set_app_id(msg,EX_PROTOCOL_APP_TEMPERATURE);
        /* Query msg */
        expansion_apps_set_type(msg, GET_MEASURE);

        /* Set payload  and message lenght */
        size_t payload_size = 2;
        size_t message_size = EX_PROTOCOL_HEADER_SIZE + payload_size;
        exp_set_message_size(msg, payload_size);


        /* Add message to Tx expansion queue */
        if(expansion_tx_status_ok()) {
			/* Try reserve space in the queue */
			if(c_queue_reserve(get_exp_tx_queue_pointer(),message_size)) {
				c_queue_copy_and_commit_data(get_exp_tx_queue_pointer(),msg, message_size);
			} else {
				/* We were not able to reserve space, as such, don't "take" this
				measurement and wait for the next */
				return;
			}
        }

    } else {
        dputs("[_temperature_interval]  NOT active!!");
    }

}

/********************************** Public ************************************/

//void temperature_app_setup(uint64_t interval) {
//    /* Setup periodic execution */
//    //assert(sched_interval(&_temperature_interval, interval) >= 0);
//}


void temperature_loop(uint64_t t_now, uint64_t interval) {

    if (t_last_temperature == 0 || (t_now - t_last_temperature) >= interval) {
    	/* It�s time for a new temperature*/
    	telco_power_management_order(0);
    	_temperature_interval();
    	/* Update last time */
    	t_last_temperature = t_now;
    }
}


