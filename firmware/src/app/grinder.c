/**
 * Copyright (c) 2016 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     app
 * @{
 *
 * @file        grinder.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      João Ambrósio <joao.ambrosio@sensefinity.com>
 *
 * @}
 */

/* Interface */
#include "app/grinder.h"

/* C standard library */
#include <assert.h>
#include <math.h>

/* API */
#include "api/sched.h"

/* Peripherals */
#include "periph/adc.h"
#include "periph/gpio.h"

/* Sensoroid library */
#include "sensoroid/sensoroid.h"

/* Utilities */
#include "util/delay.h"
#include "util/debug.h"

/********************************** Private ***********************************/

/* Defintions for the Columbus fixed-point format @ 32-bit */
#define FBITS   (16)
#define IBITS   (11)
#define HEADER  (0b01010 << 27)
#define FMASK   ((1 << FBITS) - 1)
#define IMASK   (((1 << (FBITS + IBITS)) - 1) & ~FMASK)

/* Computes an array length statically */
#define ARRAY_LEN(x) ((sizeof(x)) / sizeof((x)[0]))

/**
 * @brief       Holds a Columbus grinder message.
 */
static uint8_t _columbus_grinder[] = {
    0x01,                                               /* Version */
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,     /* Device Serial */
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,     /* Timestamp */
    0x06,                                               /* Message Type (Measurement) */
    0x00, 0x00, 0x00, 0x06,                             /* Data Size */
    0x00,                                               /* Measurement Sensor */
    0xC,                                                /* Measurement Type (grinder power off message) - 12 */
    0x00, 0x00, 0x00, 0x00                              /* Measurement Value (float) */
};

/**
 * @brief       Indicates if a power off ocurred.
 */
uint32_t _power_off_time = 0;

/**
 * @brief       Indicates if a power off message has been sent.
 */
uint32_t _power_off_sent = 0;

/**
 * @brief       Indicates if a power on message has been sent.
 */
uint32_t _power_on_sent = 0;

/**
 * @brief       Generic function that inverts the endianness of a sequence
 *              of bytes.
 * @param[in]   data
 *              Pointer to the data for which endianness is to be inverted.
 * @param[in]   size
 *              The size of the data for which endianness is to be inverted.
 */
static void _invert_endianness(void *data, size_t size) {
    size_t i;
    size_t j;
    uint8_t tmp;
    uint8_t *ptr = (uint8_t *) data;

    for (i = 0; i < size / 2; ++i) {
        /* Compute swap index */
        j = size - i -1;

        /* Swap */
        tmp = ptr[i];

        ptr[i] = ptr[j];
        ptr[j] = tmp;
    }
}

/**
 * @brief       Sets the serial number for the Columbus message.
 * @param       serial
 *              The serial number to be set.
 */
static void _columbus_set_serial(uint64_t serial) {
    uint8_t *serialptr = (uint8_t *) &serial;
    size_t i;

    /* Set serial number on the message */
    for (i = 0; i < sizeof(uint64_t); ++i) {
        _columbus_grinder[1 + i] = serialptr[i];
    }

    _invert_endianness(_columbus_grinder + 1, sizeof(uint64_t));
}

/**
 * @brief       Sets the timestamp for the Columbus message.
 * @param       timestamp
 *              The timestamp to be set.
 */
static void _columbus_set_timestamp(uint64_t timestamp) {
    uint8_t *timeptr = (uint8_t *) &timestamp;
    size_t i;

    /* Set timestamp on the message */
    for (i = 0; i < sizeof(uint64_t); ++i) {
        _columbus_grinder[9 + i] = timeptr[i];
    }

    _invert_endianness(_columbus_grinder + 9, sizeof(uint64_t));
}

/**
 * @brief       Sets the sensor value for the Columbus message.
 * @param       uint16_t is_on
 *              The value to be set (power on message = 1; power off message = 0).
 */
static void _columbus_set_measurement_value(float is_on) {
  uint32_t encoded;

  /* Scale value value and apply correction factor */
  is_on *= ((float) (1L << FBITS));
  /*value += ((value >= 0.0F) ? 0.5F : -0.5F);*/

  /* Encode value */
  encoded = (uint32_t) is_on;
  encoded &= (IMASK | FMASK);
  encoded |= HEADER;

  _columbus_grinder[24 + 0] = (uint8_t) (encoded >> 24);
  _columbus_grinder[24 + 1] = (uint8_t) (encoded >> 16);
  _columbus_grinder[24 + 2] = (uint8_t) (encoded >>  8);
  _columbus_grinder[24 + 3] = (uint8_t) (encoded >>  0);
}

/**
 * @brief       Build a power off message to be sent to Machinates.
 */
static void _grinder_power_message(float is_on) {
    struct queue_element *element;

    /* Reserve the next element from the queue */
    element = queue_reserve();

    if (element == NULL) {
        /* We were not able to reserve space, as such, don't take this
           measurement and wait for the next */
        return;
    }

    /* Initialize Columbus message */
    _columbus_set_timestamp(sysclk_get());
    _columbus_set_measurement_value(is_on);

    /* Copy Columbus message into the queue and commit it */
    element->size = ARRAY_LEN(_columbus_grinder);
    memcpy(element->data, _columbus_grinder, element->size);
    queue_commit();
}

/********************************** Public ************************************/

/**
 * @brief     Initializes the grinder measurement service.
 */
void grinder_init(void) {
    _power_off_time = 0;
    _power_off_sent = 0;

    /* Initialize the power pin */
    gpio_input_init(GRINDER_POWER, GPIO_PULL_DISABLED);

    /* Initialize Columbus serial number */
    _columbus_set_serial(sensoroid_get_serial());
}

/**
 * @brief     Updates the grinder measurement service.
 */
void grinder_loop(void) {
    bool gpio_read = false;

    /* Read the GPIO pin in order to verify if there is a power off. */
    gpio_read = gpio_input_read(GRINDER_POWER);

    if(!_power_on_sent) {
        _grinder_power_message(1);

        dputs("[grinder] power on message inserted in the queue");

        _power_on_sent = 1;
    } else {
        /* Power off detected, let's send a message to Machinates. */
        if(gpio_read) {
            //if(!_power_off_time)
                //_power_off_time = sysclk_get();
            //else {
                //if ((sysclk_get() - _power_off_time) > POWEROFF_DETECT_DELAY) {

                    if (!_power_off_sent) {
                        _grinder_power_message(0);

                        dputs("[grinder] power off message inserted in the queue");

                        _power_off_sent = 1;
                    }

                    //_power_off_time = 0;
                //}
            //}
        } //else {
            //_power_off_time = 0;
        //}
    }
}
