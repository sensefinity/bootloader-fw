/**
 * Copyright (c) 2016 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     app
 * @{
 *
 * @file        user.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Jo�o Ambr�sio <joao.ambrosio@sensefinity.com>
 *
 * @}
 */

/* Interface */
#include <app/user.h>

/********************************** Private ***********************************/

static User_States _current_user_state = USER_POWERED_OFF;

void led_start_power_on(void) {
	led_enable_blink_with_blinking_number(250, 2);
}

void led_start_power_off(void) {
	led_enable_blink_with_blinking_number(250, 2);
}

void go_power_on(void) {
	power_management_set_state(POWER_ON);
}

void go_power_off(void) {
	power_management_set_state(PREPARE_POWER_OFF);
}

void det_init(void) {
	gpio_input_init(DET_PIN, GPIO_PULL_UP);
}

bool det_read(void) {
	return gpio_input_read(DET_PIN);
}

/********************************** Public ************************************/

void user_setup(void) {
	_current_user_state = USER_POWERED_OFF;

	/* Led init */
	led_init();

	/* Led init */
	button_init();

	/* DET pin init */
	det_init();
}

void user_loop(void) {
	switch (_current_user_state) {
		case USER_POWERED_OFF: {
			if (button_toggled() && is_button_pressed()) {
				if ((sysclk_get() - get_button_pressed_timestamp())
						>= POWER_PRESSING_TIME) {
					go_power_on();

					led_start_power_on();
					_current_user_state = USER_POWERING_ON;
					set_button_toggled(false);
				}
			}
			break;
		}

		case USER_POWERING_ON: {
			if (!is_led_busy()) {
				//led_enable_breath_with_duration(LED_SLEEP_TIMEOUT);
				led_enable_breath();
				_current_user_state = USER_POWERED_ON;
			}
			break;
		}

		case USER_POWERED_ON: {
			//if (!is_led_busy()) {
				//if (is_button_pressed()) {
					//led_enable_breath_with_duration(LED_SLEEP_TIMEOUT);
					//set_button_toggled(false);

					//break;
				//}
			//} else {
				if (button_toggled() && is_button_pressed()) {
					if ((sysclk_get() - get_button_pressed_timestamp())
							>= POWER_PRESSING_TIME) {
						led_start_power_off();
						_current_user_state = USER_POWERING_OFF;
						set_button_toggled(false);

						break;
					}
				}
			//}
			break;
		}

		case USER_POWERING_OFF: {
			if (!is_led_busy()) {
				go_power_off();
				_current_user_state = USER_POWERED_OFF;
			}
			break;
		}

		default: {
			break;
		}
	}
}

bool is_led_but_connected(void) {
	return !det_read();
}

void user_set_state(User_States state) {
	_current_user_state = state;
}
