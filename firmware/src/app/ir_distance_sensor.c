/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     app
 * @{
 *
 * @file        ir_distance_sensor.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Rui Pires
 *
 * @}
 */

/* Interface */
#include "app/ir_distance_sensor.h"

/* C standard library */
#include <assert.h>
#include <math.h>

/* API */
#include "api/sched.h"

/* Peripherals */
#include "periph/adc.h"
#include "periph/gpio.h"

/* Sensoroid library */
#include "sensoroid/sensoroid.h"

/* Utilities */
#include "util/delay.h"
#include "sensoroid/columbus.h"

/********************************** Private ***********************************/

/* Defintions for the Columbus fixed-point format @ 32-bit */
#define FBITS   (16)
#define IBITS   (11)
#define HEADER  (0b01010 << 27)
#define FMASK   ((1 << FBITS) - 1)
#define IMASK   (((1 << (FBITS + IBITS)) - 1) & ~FMASK)

/* Computes an array length statically */
#define ARRAY_LEN(x) ((sizeof(x)) / sizeof((x)[0]))

/**
 * @brief       Holds a Columbus Temperature message.
 */
static uint8_t _columbus_ir_distance_sensor[COLUMBUS_MEASUREMENT_MESSAGE_SIZE];

/**
 * @brief       Holds the scaling factor that converts a ADC sample to a valid
 *              voltage.
 */
static const float SCALE = ADC_REFERENCE / (float) ((1 << ADC_RESOLUTION) - 1);


/**
 * @brief       Holds the ADC channel we'll use to perform readings.
 */
static uint32_t _channel;

/**
 * @brief       Sets the sensor value for the Columbus message.
 * @param       uint16_t value
 *              The value to be set.
 */
static void _columbus_set_ir_distance_value(float value) {
  uint32_t encoded;

  /* Scale value and apply correction factor */
  value *= ((float) (1L << FBITS));
  /*value += ((value >= 0.0F) ? 0.5F : -0.5F);*/

  /* Encode value */
  encoded = (uint32_t) value;
  encoded &= (IMASK | FMASK);
  encoded |= HEADER;

  /* Save value on columbus message */
  columbus_set_measurement_value(_columbus_ir_distance_sensor, encoded);
}


/**
 * @brief       Handles one interval for this application.
 * @details     Effectively, this function will take a new measurement, create
 *              a new message and queue it to be sent over to the cloud.
 */
static void _irds_interval(void) {
    struct queue_element *element;
    uint16_t sample;
    float voltage;

    /* Reserve the next element from the queue */
    element = queue_reserve();

    if (element == NULL) {
        /* We were not able to reserve space, as such, don't take this
           measurement and wait for the next */
        return;
    }

    /* Power on VCC and read sample, powering off afterwards */
    gpio_output_set(IRDS_VCC);
    delay_ms(1);
    sample = adc_sample(_channel);
    gpio_output_clear(IRDS_VCC);

    /* Compute input voltage */
    voltage = ((float) sample) * SCALE;

    /* Initialize Columbus message */
    columbus_set_timestamp(_columbus_ir_distance_sensor,sysclk_get());
    _columbus_set_ir_distance_value(voltage);

    /* Copy Columbus message into the queue and commit it */
    element->size = ARRAY_LEN(_columbus_ir_distance_sensor);
    memcpy(element->data, _columbus_ir_distance_sensor, element->size);
    queue_commit();
}


/********************************** Public ************************************/

void irds_setup(uint64_t interval) {
    /* Initialize the power pin */
    gpio_output_init(IRDS_VCC);
    gpio_output_clear(IRDS_VCC);

    /* Initialize columbus message */
    columbus_set_message_version(_columbus_ir_distance_sensor,1);
    columbus_set_serial(_columbus_ir_distance_sensor,sensoroid_get_serial());
    columbus_set_message_type(_columbus_ir_distance_sensor, COLUMBUS_MEASUREMENT_TYPE);
    columbus_set_data_size(_columbus_ir_distance_sensor,COLUMBUS_IRDS_MEASUREMENT_DATA_SIZE);
    columbus_set_measurement_sensor(_columbus_ir_distance_sensor,COLUMBUS_IRDS_MEASUREMENT_SENSOR);
    columbus_set_measurement_type(_columbus_ir_distance_sensor,COLUMBUS_IRDS_MEASUREMENT_TYPE);

    /* Initialize ADC channel */
    switch (IRDS_OUT) {
    case AIN0: _channel = 0; break;
    case AIN1: _channel = 1; break;
    case AIN2: _channel = 2; break;
    case AIN3: _channel = 3; break;
    case AIN4: _channel = 4; break;
    case AIN5: _channel = 5; break;
    case AIN6: _channel = 6; break;
    case AIN7: _channel = 7; break;
    default:
        /* Something's not right... */
        return;
    }

    /* Take first measurement and schedule remaining measurements */
    _irds_interval();
    assert(sched_interval(&_irds_interval, interval) >= 0);
}

void irds_loop(void) {

}
