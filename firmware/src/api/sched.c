/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     api
 * @{
 *
 * @file        sched.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#include "api/sched.h"
#include "util/debug.h"

/**
 * @brief       Holds the maximum number of tasks that may be scheduled at
 *              any given time
 */
#define SCHED_TASK_CAPACITY         (4)

/**
 * @brief       Holds the configuration for the RTC for approximately 1 ms period
 */
#define SCHED_RTC_PRESCALER         ((1 << 5) - 1)

/**
 * @brief       Holds the array of tasks
 */
static volatile struct {
    enum {
        TASK_TYPE_TIMEOUT   = 0,
        TASK_TYPE_INTERVAL
    } type;

    bool running;
    void (*action)(void);
    uint64_t millis;

} _tasks[SCHED_TASK_CAPACITY];

/**
 * @brief       Holds the number of RTC overflows
 */
static uint64_t _overflows = 0;

/**
 * @brief       RTC1 interrupt service routine.
 */
void isr_rtc1(void) {
    uint64_t i;

    if (NRF_RTC1->EVENTS_OVRFLW) {
        /* Increment number of overflows */
        ++_overflows;

        /* There was a counter overflow */
        NRF_RTC1->EVENTS_OVRFLW = 0;
    }

    for (i = 0; i < SCHED_TASK_CAPACITY; ++i) {
        if (NRF_RTC1->EVENTS_COMPARE[i]) {
            if (_tasks[i].running) {
                if (_tasks[i].type == TASK_TYPE_TIMEOUT) {
                    /* This is a timeout task, as such we'll cancel it */
                    sched_cancel(i);

                } else if (_tasks[i].type == TASK_TYPE_INTERVAL) {
                    /* This is an interval task, as such we'll schedule the
                       next interval */
                    NRF_RTC1->CC[i] = (NRF_RTC1->COUNTER +
                            (uint32_t) (_tasks[i].millis & 0xFFFFFFULL));
                }

                /* Invoke the task action */
                _tasks[i].action();
            }

            /* Clear event */
            NRF_RTC1->EVENTS_COMPARE[i] = 0;
        }
    }
}

/* -------------------------------------------------------------------------- */
/*          Public API                                                        */
/* -------------------------------------------------------------------------- */

void sched_init(void) {
    int32_t i;

    /* Setup RTC prescaler */
    NRF_RTC1->PRESCALER = SCHED_RTC_PRESCALER;

    /* Make sure all tasks are marked as not running */
    for (i = 0; i < SCHED_TASK_CAPACITY; ++i) {
        _tasks[i].running = false;
    }

    /* Enable interrupt of counter overflow */
    NRF_RTC1->INTENSET = RTC_INTENSET_OVRFLW_Msk;

    /* Enable interrupts on NVIC */
    NVIC_ClearPendingIRQ(RTC1_IRQn);
    NVIC_SetPriority(RTC1_IRQn, DANDELION_IRQ_PRIORITY_LOW);
    NVIC_EnableIRQ(RTC1_IRQn);

    /* Clear and start RTC timer */
    NRF_RTC1->TASKS_CLEAR = 1;
    NRF_RTC1->TASKS_START = 1;
}

sched_id_t sched_timeout(void (*action)(void), uint64_t millis) {
    if (action == NULL) {
        /* We've been given an invalid task */
        return (sched_id_t) -1;
    }

    /* Truncate number of milliseconds */
    if (millis < SCHED_TASK_MS_MIN) {
        millis = SCHED_TASK_MS_MIN;
    } else if (millis > SCHED_TASK_MS_MAX) {
        millis = SCHED_TASK_MS_MAX;
    }

    sched_id_t i;

    atomic(
        for (i = 0; i < SCHED_TASK_CAPACITY; ++i) {
            if (!_tasks[i].running) {
                /* We found an empty task, store information */
                _tasks[i].type = TASK_TYPE_TIMEOUT;
                _tasks[i].action = action;
                _tasks[i].running = true;

                /* Configure RTC compare register */
                NRF_RTC1->CC[i] = NRF_RTC1->COUNTER + millis;

                /* Enable interrupt on compare register */
                NRF_RTC1->EVENTS_COMPARE[i] = 0;
                NRF_RTC1->INTENSET = (RTC_INTENSET_COMPARE0_Msk << i);
                break;
            }
        }
    );

    if (i < SCHED_TASK_CAPACITY) {
      //dprintf("[sched_timeout] id:%d\r\n", (int)i);
        return (sched_id_t) i;
    }

    /* There is no available space for a new task */
    return (sched_id_t) -1;
}

sched_id_t sched_interval(void (*action)(void), uint64_t millis) {
    if (action == NULL) {
        /* We've been given an invalid task */
        return (sched_id_t) -1;
    }

    /* Truncate number of milliseconds */
    if (millis < SCHED_TASK_MS_MIN) {
        millis = SCHED_TASK_MS_MIN;
    } else if (millis > SCHED_TASK_MS_MAX) {
        millis = SCHED_TASK_MS_MAX;
    }

    sched_id_t i;

    atomic(
        for (i = 0; i < SCHED_TASK_CAPACITY; ++i) {
            if (!_tasks[i].running) {
                /* We found an empty task, store information */
                _tasks[i].type = TASK_TYPE_INTERVAL;
                _tasks[i].action = action;
                _tasks[i].millis = millis;
                _tasks[i].running = true;

                /* Configure RTC compare register */
                NRF_RTC1->CC[i] = NRF_RTC1->COUNTER + millis;

                /* Enable interrupt on compare register */
                NRF_RTC1->EVENTS_COMPARE[i] = 0;
                NRF_RTC1->INTENSET = (RTC_INTENSET_COMPARE0_Msk << i);
                break;
            }
        }
    );

    if (i < SCHED_TASK_CAPACITY) {
      //dprintf("[sched_interval] id:%d\r\n", (int)i);
        return (sched_id_t) i;
    }

    /* There is no available space for a new task */
    return (sched_id_t) -1;
}

void sched_cancel(sched_id_t id) {
    if (id >= 0 && id < SCHED_TASK_CAPACITY) {
        atomic(
            /* Disable interrupt */
            NRF_RTC1->INTENCLR = (RTC_INTENCLR_COMPARE0_Msk << id);
            NRF_RTC1->EVENTS_COMPARE[id] = 0;

            /* Set task as not running */
            _tasks[id].running = false;
        );
    }
}

uint64_t sched_read(void) {
    atomic(
        if (NRF_RTC1->EVENTS_OVRFLW) {
            /* Increment number of overflows */
            ++_overflows;

            /* There was a counter overflow */
            NRF_RTC1->EVENTS_OVRFLW = 0;
        }
    );

    return (_overflows << 24) | NRF_RTC1->COUNTER;
}

void sched_wait(uint64_t millis) {
    uint64_t now = sched_read();

    while ((sched_read() - now) < millis) {
        /* Wait */
        ;
    }
}
