/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     api
 * @{
 *
 * @file        sysclk.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#include "api/sysclk.h"

/* Dandelion library */
#include "dandelion.h"

/* Platform */
#include "nrf51.h"
#include "nrf51_bitfields.h"

/* Utilities */
#include "util/atomic.h"

/********************************** Private ***********************************/

/* RTC macros */
#define RTC_BITS        (24)
#define RTC_FREQ        ((uint64_t) 32768)
#define RTC_OVERFLOW    ((uint64_t) ((1 << RTC_BITS) - 1))
#define RTC_PRIORITY    (DANDELION_IRQ_PRIORITY_LOW)
#define RTC_UNITS2MS(u) (((u) * (uint64_t) 1000) / RTC_FREQ)
#define RTC_MS2UNITS(m) ((((uint64_t) (m) * RTC_FREQ) + 999) / 1000)

/**
 * @brief       Holds the base RTC value.
 */
static volatile uint64_t _base = 0;

/**
 * @brief       RTC0 interrupt service routine.
 */
void isr_rtc0(void) {
    if (NRF_RTC0->EVENTS_OVRFLW) {
        /* Update base value (using overflow value) */
        _base += RTC_OVERFLOW;
        NRF_RTC0->EVENTS_OVRFLW = 0;
    }
}

/********************************** Public ************************************/

void sysclk_init(void) {
    /* Setup RTC prescaler (no prescaler) */
    NRF_RTC0->PRESCALER = 0;

    /* Enable interrupt on counter overflow */
    NRF_RTC0->INTENSET = RTC_INTENSET_OVRFLW_Msk;

    /* Enable interrupts on NVIC */
    NVIC_ClearPendingIRQ(RTC0_IRQn);
    NVIC_SetPriority(RTC0_IRQn, RTC_PRIORITY);
    NVIC_EnableIRQ(RTC0_IRQn);

    /* Clear and start RTC timer */
    NRF_RTC0->TASKS_CLEAR = 1;
    NRF_RTC0->EVENTS_OVRFLW = 0;
    NRF_RTC0->TASKS_START = 1;
}

uint64_t sysclk_get(void) {
    uint64_t units;

    atomic(
        /* Manually call ISR; This is useful because this function may be
           called from a higher priority ISR and the counter can overflow
           during that time */
        isr_rtc0();

        /* Read units */
        units = _base + NRF_RTC0->COUNTER;
    );

    return RTC_UNITS2MS(units);
}

void sysclk_set(uint64_t t_now) {
    /* Stop RTC */
    NRF_RTC0->TASKS_STOP = 1;

    /* Update base base using the provided time */
    _base = RTC_MS2UNITS(t_now);

    /* Clear and start RTC timer */
    NRF_RTC0->TASKS_CLEAR = 1;
    NRF_RTC0->TASKS_START = 1;
}
