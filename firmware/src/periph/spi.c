/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     periph
 * @{
 *
 * @file        spi.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#include "periph/spi.h"
#include "util/debug.h"
/* -------------------------------------------------------------------------- */
/*          Public API                                                        */
/* -------------------------------------------------------------------------- */

void spi_init(struct spi_params params) {
  dputs("[spi_init] Begin...");
  if (NRF_SPI0->ENABLE & SPI_ENABLE_ENABLE_Enabled) {
    /* The SPI peripheral is currently enabled, disable it */
    spi_shutdown();
  }

  /* Verify validity of the provided configuration pins */
  assert(params.sclk != NC && params.miso != NC && params.mosi != NC);

  /* Configure SPI interface pins */
  gpio_output_init(params.sclk);
  gpio_output_init(params.mosi);
  gpio_input_init (params.miso, GPIO_PULL_DISABLED);

  /* Connect input buffer on the SPI SCLK pin (required by specification) */
  NRF_GPIO->PIN_CNF[params.sclk] &= ~GPIO_PIN_CNF_INPUT_Msk;
  NRF_GPIO->PIN_CNF[params.sclk] |=
    (GPIO_PIN_CNF_INPUT_Connect << GPIO_PIN_CNF_INPUT_Pos);

  /* Set logic "0" on the MOSI output pin */
  gpio_output_clear(params.mosi);

  /* Set CPOL value on the SCLK output pin */
  switch (params.mode) {
  case SPI_MODE_0:
  case SPI_MODE_1:
    /* For SPI modes 0 and 1, CPOL = 0 */
    gpio_output_clear(params.sclk);
    break;

  case SPI_MODE_2:
  case SPI_MODE_3:
    /* For SPI modes 2 and 3, CPOL = 1 */
    gpio_output_set(params.sclk);
    break;
  }

  /* Finally, connect SPI interface pins */
  NRF_SPI0->PSELSCK = params.sclk;
  NRF_SPI0->PSELMOSI = params.mosi;
  NRF_SPI0->PSELMISO = params.miso;

  /* Configure SPI mode */
  switch (params.mode) {
  case SPI_MODE_0:
    /* CPOL = 0, CPHA = 0 */
    NRF_SPI0->CONFIG =
      (SPI_CONFIG_CPOL_ActiveHigh     << SPI_CONFIG_CPOL_Pos) |
      (SPI_CONFIG_CPHA_Leading        << SPI_CONFIG_CPHA_Pos);
    break;

  case SPI_MODE_1:
    /* CPOL = 0, CPHA = 1 */
    NRF_SPI0->CONFIG =
      (SPI_CONFIG_CPOL_ActiveHigh     << SPI_CONFIG_CPOL_Pos) |
      (SPI_CONFIG_CPHA_Trailing       << SPI_CONFIG_CPHA_Pos);
    break;

  case SPI_MODE_2:
    /* CPOL = 1, CPHA = 0 */
    NRF_SPI0->CONFIG =
      (SPI_CONFIG_CPOL_ActiveLow      << SPI_CONFIG_CPOL_Pos) |
      (SPI_CONFIG_CPHA_Leading        << SPI_CONFIG_CPHA_Pos);
    break;

  case SPI_MODE_3:
    /* CPOL = 1, CPHA = 1 */
    NRF_SPI0->CONFIG =
      (SPI_CONFIG_CPOL_ActiveLow      << SPI_CONFIG_CPOL_Pos) |
      (SPI_CONFIG_CPHA_Trailing       << SPI_CONFIG_CPHA_Pos);
    break;
    }

  /* Configure SPI bit-ordering */
  switch (params.order) {
  case SPI_ORDER_MSB_FIRST:
    /* Use MSB-first bit-ordering */
    NRF_SPI0->CONFIG |=
      (SPI_CONFIG_ORDER_MsbFirst << SPI_CONFIG_ORDER_Pos);
    break;

  case SPI_ORDER_LSB_FIRST:
    /* use LSB-first bit-ordering */
    NRF_SPI0->CONFIG |=
      (SPI_CONFIG_ORDER_LsbFirst << SPI_CONFIG_ORDER_Pos);
    break;
  }

  /* Configure SPI frequency */
  switch (params.frequency) {
  case SPI_FREQUENCY_125K:
    /* Use 125 kHz clock */
    NRF_SPI0->FREQUENCY = SPI_FREQUENCY_FREQUENCY_K125;
    break;

  case SPI_FREQUENCY_250K:
    /* Use 250 kHz clock */
    NRF_SPI0->FREQUENCY = SPI_FREQUENCY_FREQUENCY_K250;
    break;

  case SPI_FREQUENCY_500K:
    /* Use 500 kHz clock */
    NRF_SPI0->FREQUENCY = SPI_FREQUENCY_FREQUENCY_K500;
    break;

  case SPI_FREQUENCY_1M:
    /* Use 1 MHz clock */
    NRF_SPI0->FREQUENCY = SPI_FREQUENCY_FREQUENCY_M1;
    break;

  case SPI_FREQUENCY_2M:
    /* Use 2 MHz clock */
    NRF_SPI0->FREQUENCY = SPI_FREQUENCY_FREQUENCY_M2;
    break;

  case SPI_FREQUENCY_4M:
    /* Use 4 MHz clock */
    NRF_SPI0->FREQUENCY = SPI_FREQUENCY_FREQUENCY_M4;
    break;

  case SPI_FREQUENCY_8M:
    /* Use 8 MHz clock */
    NRF_SPI0->FREQUENCY = SPI_FREQUENCY_FREQUENCY_M8;
    break;
  }

  /* Finally, enable SPI peripheral */
  NRF_SPI0->ENABLE = SPI_ENABLE_ENABLE_Enabled << SPI_ENABLE_ENABLE_Pos;
  dputs("[spi_init] Done!");
}

void spi_shutdown(void) {
    /* Disable SPI peripheral */
    NRF_SPI0->ENABLE = SPI_ENABLE_ENABLE_Disabled << SPI_ENABLE_ENABLE_Pos;
}

uint8_t spi_write(uint8_t data) {
    /* Perform SPI transaction */
    NRF_SPI0->EVENTS_READY = 0;

    /* Send byte */
    NRF_SPI0->TXD = data;

    /* Wait for data to be received */
    while (NRF_SPI0->EVENTS_READY == 0) {
        /* Wait */
        ;
    }

    return NRF_SPI0->RXD;
}
