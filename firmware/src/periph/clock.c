/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     periph
 * @{
 *
 * @file        clock.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#include "periph/clock.h"

/**
 * @brief       Holds whether the high-frequency clock is started from the
 *              XTAL source
 */
static bool _hfclk_started = false;


/* -------------------------------------------------------------------------- */
/*          Public API                                                        */
/* -------------------------------------------------------------------------- */

void clock_init(void) {
    /* By default, the underlying high-frequency clock is running from the
       RC oscillator upon reset; As such, we don't need to initialize the
       high-frequency clock */

    /* Configure the XTAL frequency for high-frequency clock */
    NRF_CLOCK->XTALFREQ = CLOCK_XTALFREQ_XTALFREQ_16MHz
            << CLOCK_XTALFREQ_XTALFREQ_Pos;

    /* Configure low-frequency clock source as XTAL */
    NRF_CLOCK->LFCLKSRC = CLOCK_LFCLKSRC_SRC_Xtal << CLOCK_LFCLKSRC_SRC_Pos;

    /* Start low-frequency clock */
    NRF_CLOCK->EVENTS_LFCLKSTARTED = 0;
    NRF_CLOCK->TASKS_LFCLKSTART = 1;
    while (NRF_CLOCK->EVENTS_LFCLKSTARTED == 0);
}

void clock_hfclk_start(void) {
    if (!_hfclk_started) {
        /* Start high-frequency clock */
        NRF_CLOCK->EVENTS_HFCLKSTARTED = 0;
        NRF_CLOCK->TASKS_HFCLKSTART = 1;
        while (NRF_CLOCK->EVENTS_HFCLKSTARTED == 0);

        _hfclk_started = true;
    }
}

void clock_hfclk_stop(void) {
    if (_hfclk_started) {
        /* Stop high-frequency clock */
        NRF_CLOCK->TASKS_HFCLKSTOP = 1;

        _hfclk_started = false;
    }
}
