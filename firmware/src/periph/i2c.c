/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     periph
 * @{
 *
 * @file        i2c.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      João Ambrósio <joao.ambrosio@sensefinity.com>
 *
 * @}
 */

 /* Interface */
#include "periph/i2c.h"

/* Utilities */
#include "util/delay.h"
#include "util/debug.h"

/********************************** Private ***********************************/

/**
 * @brief Corresponds to the I2C parameters passed in the "i2c_init" function
 */
static struct I2C_params _i2c_setup;

bool i2c_is_free(void) {
  if (NRF_TWI1->ENABLE) {
    return false;
  }
  else {
    return true;
  }
}

/* -------------------------------------------------------------------------- */
/*          Public API                                                        */
/* -------------------------------------------------------------------------- */

bool i2c_init(struct I2C_params params) {
  if(!i2c_is_free()) {
    dputs("[i2c_init] Error! I2C driver is currently in use. Try it later!");
    return false;
  }

  /* Save the I2C parameters profile */
  _i2c_setup = params;

  if (NRF_TWI1->ENABLE & TWI_ENABLE_ENABLE_Enabled) {
    /* The I2C peripheral is currently enabled, disable it! */
    i2c_shutdown();
  }

  /* Verify validity of the provided I2C configuration pins */
  assert(params.scl != NC && params.sda != NC);

  /* Configure I2C interface pins */
  NRF_GPIO->PIN_CNF[params.sda] =
      (GPIO_PIN_CNF_SENSE_Disabled << GPIO_PIN_CNF_SENSE_Pos)
    | (GPIO_PIN_CNF_DRIVE_H0D1 << GPIO_PIN_CNF_DRIVE_Pos)
    | (GPIO_PIN_CNF_PULL_Pullup << GPIO_PIN_CNF_PULL_Pos)
    | (GPIO_PIN_CNF_INPUT_Connect << GPIO_PIN_CNF_INPUT_Pos)
    | (GPIO_PIN_CNF_DIR_Input << GPIO_PIN_CNF_DIR_Pos);

  NRF_GPIO->PIN_CNF[params.scl] =
     (GPIO_PIN_CNF_SENSE_Disabled << GPIO_PIN_CNF_SENSE_Pos)
    |(GPIO_PIN_CNF_DRIVE_H0D1 << GPIO_PIN_CNF_DRIVE_Pos)
    |(GPIO_PIN_CNF_PULL_Pullup << GPIO_PIN_CNF_PULL_Pos)
    |(GPIO_PIN_CNF_INPUT_Connect << GPIO_PIN_CNF_INPUT_Pos)
    |(GPIO_PIN_CNF_DIR_Input << GPIO_PIN_CNF_DIR_Pos);

  NRF_TWI1->EVENTS_RXDREADY = 0;
  NRF_TWI1->EVENTS_TXDSENT  = 0;

  /* Finally, connect I2C interface pins */
  NRF_TWI1->PSELSCL = params.scl;
  NRF_TWI1->PSELSDA = params.sda;

  /* Configure I2C frequency */
  switch (params.frequency) {
    case I2C_FREQUENCY_K100:
      /* Use 100 kHz clock */
      NRF_TWI1->FREQUENCY = TWI_FREQUENCY_FREQUENCY_K100 << TWI_FREQUENCY_FREQUENCY_Pos;
      break;

    case I2C_FREQUENCY_K250:
      /* Use 250 kHz clock */
      NRF_TWI1->FREQUENCY = TWI_FREQUENCY_FREQUENCY_K250 << TWI_FREQUENCY_FREQUENCY_Pos;
      break;

    case I2C_FREQUENCY_K400:
      /* Use 400 kHz clock */
      NRF_TWI1->FREQUENCY = TWI_FREQUENCY_FREQUENCY_K400 << TWI_FREQUENCY_FREQUENCY_Pos;
      break;
  }

  /* Finally, enable I2C peripheral */
  NRF_TWI1->ENABLE = TWI_ENABLE_ENABLE_Enabled << TWI_ENABLE_ENABLE_Pos;

  return true;
}

bool i2c_shutdown(void) {
  /* Disable I2C peripheral */
  NRF_TWI1->ENABLE = TWI_ENABLE_ENABLE_Disabled << TWI_ENABLE_ENABLE_Pos;

  return true;
}

bool i2c_set_address(uint8_t address) {
  NRF_TWI1->ADDRESS = address;

  return true;
}

bool i2c_write(uint8_t *data, uint8_t data_length, bool issue_stop_condition) {
  if(i2c_is_free()) {
    dputs("[i2c_write] Error! I2C bus is currently free. Connect first to the I2C bus!");
    return false;
  }

  uint32_t timeout = MAX_TIMEOUT_LOOPS;

  /* Data to send is empty! Are you crazy? */
  if (data_length == 0) {
    dputs("[i2c_write] Failed writing!");
    return false;
  }

  NRF_TWI1->TXD = *data++;
  NRF_TWI1->TASKS_STARTTX = 1;

  while (true) {
    while(NRF_TWI1->EVENTS_TXDSENT == 0 && (--timeout)) {}

    if (timeout == 0) {
      NRF_TWI1->EVENTS_STOPPED = 0;
      NRF_TWI1->TASKS_STOP = 1;

      /* Wait until stop sequence is sent */
      while(NRF_TWI1->EVENTS_STOPPED == 0) {}

      dputs("[i2c_write] Failed writing!");
      return false;
    }

    NRF_TWI1->EVENTS_TXDSENT = 0;

    if (--data_length == 0) {
      break;
    }

    NRF_TWI1->TXD = *data++;
  }

  if (issue_stop_condition) {
    NRF_TWI1->EVENTS_STOPPED = 0;
    NRF_TWI1->TASKS_STOP = 1;

    /* Wait until stop sequence is sent */
    while(NRF_TWI1->EVENTS_STOPPED == 0) {}
  }

  return true;
}

bool i2c_read(uint8_t *data, uint8_t data_length) {
  if(i2c_is_free()) {
    dputs("[i2c_read] Error! I2C bus is currently free. Connect first to the I2C bus!");
    return false;
  }

  uint32_t timeout = MAX_TIMEOUT_LOOPS;

  /* Data to send is empty! Are you crazy? */
  if(data_length == 0) {
    dputs("[i2c_read] Failed reading!");
    return false;
  }

  if (data_length == 1) {
    /* Only one to go. Configure PPI to send STOP bit after the last one */
    ppi_channel_enable(
      I2C_PPI_CHANNEL,
      (uint32_t)&NRF_TWI1->EVENTS_BB,
      (uint32_t)&NRF_TWI1->TASKS_STOP);
  } else {
    /* Suspend PPI for now */
    ppi_channel_enable(
      I2C_PPI_CHANNEL,
      (uint32_t)&NRF_TWI1->EVENTS_BB,
      (uint32_t)&NRF_TWI1->TASKS_SUSPEND);
  }

  NRF_TWI1->TASKS_STARTRX = 1;

  while(true) {
    while((NRF_TWI1->EVENTS_RXDREADY == 0) && (--timeout)) {}

    if(timeout == 0) {
      dputs("[i2c_read] Failed reading!");
      return false;
    }

    NRF_TWI1->EVENTS_RXDREADY = 0;
    *data++ = NRF_TWI1->RXD;

    /* Only one to go. Configure PPI to send STOP bit after the last one */
    if (--data_length == 1) {
      ppi_channel_enable(
              I2C_PPI_CHANNEL,
              (uint32_t)&NRF_TWI1->EVENTS_BB,
              (uint32_t)&NRF_TWI1->TASKS_STOP);
    }

    /* Received it all */
    if (data_length == 0)
      break;

    NRF_TWI1->TASKS_RESUME = 1;
  }

  /* wait until stop sequence is sent */
  while(NRF_TWI1->EVENTS_STOPPED == 0) {}

  NRF_TWI1->EVENTS_STOPPED = 0;

  ppi_channel_disable(I2C_PPI_CHANNEL);

  return true;
}
