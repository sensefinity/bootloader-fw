/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     periph
 * @{
 *
 * @file        power.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#include "periph/power.h"

void power_init(void) {
    /* Configure RAM blocks to be ON and disable their retention in case
       they are shut down, disable other unused blocks */
    NRF_POWER->RAMON  = POWER_RAMON_ONRAM0_Msk |
                        POWER_RAMON_ONRAM1_Msk |
                        POWER_RAMON_OFFRAM0_Msk |
                        POWER_RAMON_OFFRAM1_Msk;
    NRF_POWER->RAMONB = 0x0;

    /* Enable DC/DC converter */
    NRF_POWER->DCDCEN = 1;
}

void power_set_low_power_mode(void) {
    /* Enable low power (variable latency mode) */
    NRF_POWER->TASKS_LOWPWR = 1;
}

void power_set_constant_latency_mode(void) {
    /* Enable constant latency mode */
    NRF_POWER->TASKS_CONSTLAT = 1;
}

void power_off(void) {
    /* Enter system off */
    NRF_POWER->SYSTEMOFF = 1;
}
