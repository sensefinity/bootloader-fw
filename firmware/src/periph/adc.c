/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     periph
 * @{
 *
 * @file        adc.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#include "periph/adc.h"

/* -------------------------------------------------------------------------- */
/*          Private API                                                       */
/* -------------------------------------------------------------------------- */

static uint16_t _adc_read(void) {
    uint16_t sample;

    /* Enable ADC */
    NRF_ADC->ENABLE = (ADC_ENABLE_ENABLE_Enabled << ADC_ENABLE_ENABLE_Pos);

    NRF_ADC->EVENTS_END = 0;

    /* Start ADC conversion */
    NRF_ADC->TASKS_START = 1;

    while (NRF_ADC->EVENTS_END == 0) {
        /* Wait for the end of conversion */
        ;
    }

    /* Save ADC result */
    sample = (uint16_t) NRF_ADC->RESULT;

    /* Stop ADC conversion
       Lookup PAN_028 Rev1.1 Anomaly 1 */
    NRF_ADC->TASKS_STOP = 1;

    /* Finally, disable ADC */
    NRF_ADC->ENABLE = (ADC_ENABLE_ENABLE_Disabled << ADC_ENABLE_ENABLE_Pos);

    return sample;
}


/* -------------------------------------------------------------------------- */
/*          Public API                                                        */
/* -------------------------------------------------------------------------- */

uint16_t adc_sample(uint32_t channel) {
    /* Assert that we were given a valid ADC channel */
    assert(channel < ADC_CHANNELS);

    /* Configure ADC */
    NRF_ADC->CONFIG =
            (ADC_CONFIG_EXTREFSEL_None
                    << ADC_CONFIG_EXTREFSEL_Pos)                |
            ((ADC_CONFIG_PSEL_AnalogInput0 << channel)
                    << ADC_CONFIG_PSEL_Pos)                     |
            (ADC_CONFIG_REFSEL_VBG
                    << ADC_CONFIG_REFSEL_Pos)                   |
            (ADC_CONFIG_INPSEL_AnalogInputOneThirdPrescaling
                    << ADC_CONFIG_INPSEL_Pos)                   |
            (ADC_CONFIG_RES_10bit
                    << ADC_CONFIG_RES_Pos);

    /* Read */
    return _adc_read();
}

uint32_t adc_pin_as_channel(enum pin_name name) {
    uint32_t channel;

    switch (name) {
    case AIN0: channel = 0; break;
    case AIN1: channel = 1; break;
    case AIN2: channel = 2; break;
    case AIN3: channel = 3; break;
    case AIN4: channel = 4; break;
    case AIN5: channel = 5; break;
    case AIN6: channel = 6; break;
    case AIN7: channel = 7; break;
    default:
        channel = ((uint32_t) -1);
    }

    return channel;
}

uint16_t adc_supply(void) {
    /* Configure ADC */
    NRF_ADC->CONFIG =
            (ADC_CONFIG_EXTREFSEL_None
                    << ADC_CONFIG_EXTREFSEL_Pos)                |
            ((ADC_CONFIG_PSEL_Disabled)
                    << ADC_CONFIG_PSEL_Pos)                     |
            (ADC_CONFIG_REFSEL_VBG
                    << ADC_CONFIG_REFSEL_Pos)                   |
            (ADC_CONFIG_INPSEL_SupplyOneThirdPrescaling
                    << ADC_CONFIG_INPSEL_Pos)                   |
            (ADC_CONFIG_RES_10bit
                    << ADC_CONFIG_RES_Pos);

    /* Read */
    return _adc_read();
}
