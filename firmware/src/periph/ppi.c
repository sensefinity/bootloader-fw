/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     periph
 * @{
 *
 * @file        ppi.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#include "periph/ppi.h"

void ppi_channel_enable(ppi_channel_t channel, uint32_t eep, uint32_t tep) {
    assert(channel < PPI_AVAILABLE_CHANNELS);

    /* Configure end-points */
    NRF_PPI->CH[channel].EEP = eep;
    NRF_PPI->CH[channel].TEP = tep;

    /* Enable channel */
    NRF_PPI->CHENSET = (1 << channel);
}

void ppi_channel_disable(ppi_channel_t channel) {
    assert(channel < PPI_AVAILABLE_CHANNELS);

    /* Disable channel */
    NRF_PPI->CHENCLR = (1 << channel);
}
