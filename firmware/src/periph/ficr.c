/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     periph
 * @{
 *
 * @file        ficr.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#include "periph/ficr.h"

size_t ficr_flash_page_size(void) {
    return NRF_FICR->CODEPAGESIZE;
}

size_t ficr_flash_page_count(void) {
    return NRF_FICR->CODESIZE;
}

size_t ficr_ram_block_size(void) {
    return NRF_FICR->SIZERAMBLOCKS;
}

size_t ficr_ram_block_count(void) {
    return NRF_FICR->NUMRAMBLOCK;
}

struct bdaddr ficr_device_address(void) {
    uint64_t mac;
    struct bdaddr addr;

    /* Initialize type */
    addr.type = NRF_FICR->DEVICEADDRTYPE & 0x1;

    /* Initialize data */
    mac = (((uint64_t) NRF_FICR->DEVICEADDR[1]) << 32)
            | ((uint64_t) NRF_FICR->DEVICEADDR[0]);

    memcpy(addr.data, &mac, BDADDR_LEN);

    return addr;
}

uint64_t ficr_device_id(void) {
    return (((uint64_t) NRF_FICR->DEVICEID[1]) << 32)
            | ((uint64_t) NRF_FICR->DEVICEID[0]);
}
