/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @file        gpio.c
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 * @date        6 Oct 2015
 * @brief       Implementation of the interface in the header file of same name.
 * @see         http://www.sensefinity.com
 */

/* Interface */
#include "periph/gpio.h"

/********************************** Private ************************************/

static void (* volatile _event_handler[4])(void); /* 4 events handler */

static void (* volatile _pin_handler[P0_30 + 1])(void); /* 31 pin handler */

static uint8_t _pins_polarity[P0_30 + 1];

static uint32_t _pins_previous_states = 0;

/* Pin specific interrupt handler */
void isr_pin(void) {
	for (uint8_t _pin_name = 0; _pin_name < (P0_30 + 1); _pin_name++) {
		if (((NRF_GPIO->PIN_CNF[_pin_name] & GPIO_PIN_CNF_SENSE_Msk)
				>> GPIO_PIN_CNF_SENSE_Pos) != GPIO_SENSE_DISABLED) {

			if (gpio_input_read(_pin_name)
					!= ((_pins_previous_states & ((uint32_t) 1 << _pin_name))
							>> _pin_name)) {

				if ((_pins_polarity[_pin_name] == PIN_POLARITY_TOGGLE)
						|| ((_pins_polarity[_pin_name] == PIN_POLARITY_HIGH)
								&& (gpio_input_read(_pin_name)))
						|| ((_pins_polarity[_pin_name] == PIN_POLARITY_LOW)
								&& (!gpio_input_read(_pin_name)))) {

					_pin_handler[_pin_name]();
				}

				_pins_previous_states &= ~(((uint32_t) (1)) << _pin_name);
				_pins_previous_states |= (gpio_input_read(_pin_name)
						<< _pin_name);

				NRF_GPIO->PIN_CNF[_pin_name] &= ~GPIO_PIN_CNF_SENSE_Msk;
				if (gpio_input_read(_pin_name)) {
					NRF_GPIO->PIN_CNF[_pin_name] |= (GPIO_SENSE_LOW
							<< GPIO_PIN_CNF_SENSE_Pos);
				} else {
					NRF_GPIO->PIN_CNF[_pin_name] |= (GPIO_SENSE_HIGH
							<< GPIO_PIN_CNF_SENSE_Pos);
				}
			}
		}
	}
}

/* GPIOTE interrupt handler */
void isr_gpiote(void) {
	if (NRF_GPIOTE->EVENTS_IN[0] == 1) {
		NRF_GPIOTE->EVENTS_IN[0] = 0;
		_event_handler[0]();
	}

	if (NRF_GPIOTE->EVENTS_IN[1] == 1) {
		NRF_GPIOTE->EVENTS_IN[1] = 0;
		_event_handler[1]();
	}

	if (NRF_GPIOTE->EVENTS_IN[2] == 1) {
		NRF_GPIOTE->EVENTS_IN[2] = 0;
		_event_handler[2]();
	}

	if (NRF_GPIOTE->EVENTS_IN[3] == 1) {
		NRF_GPIOTE->EVENTS_IN[3] = 0;
		_event_handler[3]();
	}
	if (NRF_GPIOTE->EVENTS_PORT != 0) {
		isr_pin();
		NRF_GPIOTE->EVENTS_PORT = 0;
	}
}

/********************************** Public ************************************/

/******** GPIO *********/

void gpio_output_init(enum pin_name name) {
	/* Configure */
	NRF_GPIO->PIN_CNF[name] = (GPIO_PIN_CNF_DIR_Output << GPIO_PIN_CNF_DIR_Pos)
			| (GPIO_PIN_CNF_INPUT_Disconnect << GPIO_PIN_CNF_INPUT_Pos)
			| (GPIO_PIN_CNF_PULL_Disabled << GPIO_PIN_CNF_PULL_Pos)
			| (GPIO_PIN_CNF_DRIVE_H0S1 << GPIO_PIN_CNF_DRIVE_Pos)
			| (GPIO_PIN_CNF_SENSE_Disabled << GPIO_PIN_CNF_SENSE_Pos);
}

void gpio_output_set(enum pin_name name) {
	NRF_GPIO->OUTSET = PIN_NAME_MASK(name);
}

void gpio_output_clear(enum pin_name name) {
	NRF_GPIO->OUTCLR = PIN_NAME_MASK(name);
}

void gpio_output_toggle(enum pin_name name) {
	if (NRF_GPIO->OUT & PIN_NAME_MASK(name)) {
		/* Pin is currently set, so, clear it */
		NRF_GPIO->OUTCLR = PIN_NAME_MASK(name);

	} else {
		/* Pin is currently clear, so, set it */
		NRF_GPIO->OUTSET = PIN_NAME_MASK(name);
	}
}

void gpio_output_write(enum pin_name name, bool value) {
	if (value) {
		/* Set output */
		NRF_GPIO->OUTSET = PIN_NAME_MASK(name);

	} else {
		/* Clear output */
		NRF_GPIO->OUTCLR = PIN_NAME_MASK(name);
	}
}

bool gpio_output_read(enum pin_name name) {
	/* Since pin is configured as output, read from OUT register */
	return (NRF_GPIO->OUT & PIN_NAME_MASK(name)) != 0;
}

void gpio_input_init(enum pin_name name, enum gpio_pull pull) {
	/* Configure */
	NRF_GPIO->PIN_CNF[name] = (GPIO_PIN_CNF_DIR_Input << GPIO_PIN_CNF_DIR_Pos)
			| (GPIO_PIN_CNF_INPUT_Connect << GPIO_PIN_CNF_INPUT_Pos)
			| (pull << GPIO_PIN_CNF_PULL_Pos)
			| (GPIO_PIN_CNF_DRIVE_H0S1 << GPIO_PIN_CNF_DRIVE_Pos)
			| (GPIO_PIN_CNF_SENSE_Disabled << GPIO_PIN_CNF_SENSE_Pos);
}

bool gpio_input_read(enum pin_name name) {
	/* Since pin is configured as input, read from IN register */
	return (NRF_GPIO->IN & PIN_NAME_MASK(name)) != 0;
}

/******* GPIOTE ********/

void gpiote_set_event(uint8_t pin_name, uint8_t gpiote_interrupt_pin,
		uint8_t polarity) {
	assert(
			gpiote_interrupt_pin == GPIOTE_INT_0
					|| gpiote_interrupt_pin == GPIOTE_INT_1
					|| gpiote_interrupt_pin == GPIOTE_INT_2
					|| gpiote_interrupt_pin == GPIOTE_INT_3);

	gpio_disable_channel(gpiote_interrupt_pin);

	/* Configure GPIOTE interrupt pin in EVENT mode for the chosen pin */
	NRF_GPIOTE->CONFIG[gpiote_interrupt_pin] = (GPIOTE_CONFIG_MODE_Event
			<< GPIOTE_CONFIG_MODE_Pos) | (pin_name << GPIOTE_CONFIG_PSEL_Pos)
			| (polarity << GPIOTE_CONFIG_POLARITY_Pos);
}

void gpio_disable_channel(uint8_t gpiote_interrupt_pin) {
	assert(
			gpiote_interrupt_pin == GPIOTE_INT_0
					|| gpiote_interrupt_pin == GPIOTE_INT_1
					|| gpiote_interrupt_pin == GPIOTE_INT_2
					|| gpiote_interrupt_pin == GPIOTE_INT_3
					|| gpiote_interrupt_pin == GPIOTE_INT_PORT);

	/* Disable interrupt on the specific input interrupt */
	switch (gpiote_interrupt_pin) {
	case GPIOTE_INT_0: {
		NRF_GPIOTE->INTENCLR = GPIOTE_INTENSET_IN0_Set
				<< GPIOTE_INTENSET_IN0_Pos;
		NRF_GPIOTE->EVENTS_IN[GPIOTE_INT_0] = 0;
		break;
	}

	case GPIOTE_INT_1: {
		NRF_GPIOTE->INTENCLR = GPIOTE_INTENSET_IN1_Set
				<< GPIOTE_INTENSET_IN1_Pos;
		NRF_GPIOTE->EVENTS_IN[GPIOTE_INT_1] = 0;
		break;
	}

	case GPIOTE_INT_2: {
		NRF_GPIOTE->INTENCLR = GPIOTE_INTENSET_IN2_Set
				<< GPIOTE_INTENSET_IN2_Pos;
		NRF_GPIOTE->EVENTS_IN[GPIOTE_INT_2] = 0;
		break;
	}

	case GPIOTE_INT_3: {
		NRF_GPIOTE->INTENCLR = GPIOTE_INTENSET_IN3_Set
				<< GPIOTE_INTENSET_IN3_Pos;
		NRF_GPIOTE->EVENTS_IN[GPIOTE_INT_3] = 0;
		break;
	}

	case GPIOTE_INT_PORT: {
		NRF_GPIOTE->INTENCLR = GPIOTE_INTENSET_PORT_Set
				<< GPIOTE_INTENSET_PORT_Pos;
		NRF_GPIOTE->EVENTS_PORT = 0;
		break;
	}

	default: {
		break;
	}
	}
}

void gpio_enable_channel(uint8_t gpiote_interrupt_pin) {
	assert(
			gpiote_interrupt_pin == GPIOTE_INT_0
					|| gpiote_interrupt_pin == GPIOTE_INT_1
					|| gpiote_interrupt_pin == GPIOTE_INT_2
					|| gpiote_interrupt_pin == GPIOTE_INT_3
					|| gpiote_interrupt_pin == GPIOTE_INT_PORT);

	/* Enable interrupt on the specific input interrupt */
	switch (gpiote_interrupt_pin) {
	case GPIOTE_INT_0: {
		NRF_GPIOTE->INTENSET = GPIOTE_INTENSET_IN0_Set
				<< GPIOTE_INTENSET_IN0_Pos;
		break;
	}

	case GPIOTE_INT_1: {
		NRF_GPIOTE->INTENSET = GPIOTE_INTENSET_IN1_Set
				<< GPIOTE_INTENSET_IN1_Pos;
		break;
	}

	case GPIOTE_INT_2: {
		NRF_GPIOTE->INTENSET = GPIOTE_INTENSET_IN2_Set
				<< GPIOTE_INTENSET_IN2_Pos;
		break;
	}

	case GPIOTE_INT_3: {
		NRF_GPIOTE->INTENSET = GPIOTE_INTENSET_IN3_Set
				<< GPIOTE_INTENSET_IN3_Pos;
		break;
	}

	case GPIOTE_INT_PORT: {
		NRF_GPIOTE->INTENSET = GPIOTE_INTENSET_PORT_Set
				<< GPIOTE_INTENSET_PORT_Pos;
		break;
	}

	default: {
		break;
	}
	}

	/* Enable NVIC interrupt */
	NVIC_EnableIRQ(GPIOTE_IRQn);
}

void gpiote_detach_event_function(enum gpiote_interrupt gpiote_interrupt_pin) {
	assert(
			gpiote_interrupt_pin == GPIOTE_INT_0
					|| gpiote_interrupt_pin == GPIOTE_INT_1
					|| gpiote_interrupt_pin == GPIOTE_INT_2
					|| gpiote_interrupt_pin == GPIOTE_INT_3);

	/* Disable interrupt on the specific input interrupt */
	gpio_disable_channel(gpiote_interrupt_pin);

	/* Detach handler */
	_event_handler[gpiote_interrupt_pin] = NULL;
}

void gpiote_attach_event_function(void (*handler)(void),
		enum gpiote_interrupt gpiote_interrupt_pin) {
	assert(handler != NULL);

	assert(
			gpiote_interrupt_pin == GPIOTE_INT_0
					|| gpiote_interrupt_pin == GPIOTE_INT_1
					|| gpiote_interrupt_pin == GPIOTE_INT_2
					|| gpiote_interrupt_pin == GPIOTE_INT_3);

	if (_event_handler[gpiote_interrupt_pin] != NULL) {
		/* Detach the previous handler */
		gpiote_detach_event_function(gpiote_interrupt_pin);
	}

	/* Attach new handler at gpiote_interrupt_number position */
	_event_handler[gpiote_interrupt_pin] = handler;

	/* Enable interrupt on the specific input interrupt */
	gpio_enable_channel(gpiote_interrupt_pin);
}

void gpio_detach_pin_function(enum pin_name pin_number) {
	NRF_GPIO->PIN_CNF[pin_number] |= (GPIO_SENSE_DISABLED
			<< GPIO_PIN_CNF_SENSE_Pos);

	/* Detach handler */
	_pin_handler[pin_number] = NULL;

	//If there is no pin to be sensed just shut off!
	for (uint8_t _pin_name = 0; _pin_name < (P0_30 + 1); _pin_name++) {
		if (((NRF_GPIO->PIN_CNF[_pin_name] & GPIO_PIN_CNF_SENSE_Msk)
						>> GPIO_PIN_CNF_SENSE_Pos) != GPIO_SENSE_DISABLED) {
			return;
		}
	}

	/* Disable interrupt on the specific input interrupt */
	gpio_disable_channel(GPIOTE_INT_PORT);
}

void gpio_attach_pin_function(void (*handler)(void), enum pin_name pin_number,
		enum pin_polarity pin_sense) {
	assert(handler != NULL);

	if (_pin_handler[pin_number] != NULL) {
		/* Detach the previous handler */
		gpio_detach_pin_function(pin_number);
	}

	/* Attach new handler at pin_number position */
	_pin_handler[pin_number] = handler;

	/* Set pin previous state */
	_pins_previous_states &= ~(((uint32_t) (1)) << pin_number);
	_pins_previous_states |= (gpio_input_read(pin_number) << pin_number);

	/* Enable interrupt on the specific input interrupt */
	gpio_enable_channel(GPIOTE_INT_PORT);

	_pins_polarity[pin_number] = pin_sense;

	NRF_GPIO->PIN_CNF[pin_number] &= ~GPIO_PIN_CNF_SENSE_Msk;
	if (gpio_input_read(pin_number)) {
		NRF_GPIO->PIN_CNF[pin_number] |= (GPIO_SENSE_LOW
				<< GPIO_PIN_CNF_SENSE_Pos);
	} else {
		NRF_GPIO->PIN_CNF[pin_number] |= (GPIO_SENSE_HIGH
				<< GPIO_PIN_CNF_SENSE_Pos);
	}
}
