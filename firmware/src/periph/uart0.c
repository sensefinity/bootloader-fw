/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @file        uart0.c
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 * @date        8 Oct 2015
 * @brief       Implementation of the interface in the header file of same name.
 * @see         http://www.sensefinity.com
 */

/* Interface */
#include "periph/uart0.h"

/********************************** Private ***********************************/

/**
 * @brief       Pointer to a function that handles UART events.
 * @note        The function pointer is declared as volatile since it is shared
 *              with the interrupt service routine.
 */
static void (*volatile _handler)(void) = NULL;

/**
 * @brief       UART0 interrupt service routine.
 * @details     This routine is used to report UART events to the attached
 *              handler.
 */
void isr_uart0(void) {
    void (*handler)(void) = _handler;

    if (handler == NULL) {
        /* We have nothing to report */
        return;
    }

    if (NRF_UART0->EVENTS_TXDRDY || NRF_UART0->EVENTS_RXDRDY) {
        /* We're ready to transmit more data or we have new data to be
           consumed; Report to handler */
        handler();
    }
}

/********************************** Public ************************************/

void uart0_init(enum pin_name txd, enum pin_name rxd) {
    assert(txd != NC && rxd != NC && txd != rxd);

    if (_uart0_enabled()) {
        /* The UART is currently enabled, make sure it is disabled */
        uart0_disable();
    }

    /* Configure pins */
    gpio_output_init(txd);
    gpio_output_set(txd);
    gpio_input_init(rxd, GPIO_PULL_DISABLED);

    /* Set defaults */
    uart0_baud(UART0_DEFAULT_BAUDRATE);
    uart0_parity(UART0_DEFAULT_PARITY);

    /* Enable UART */
    NRF_UART0->ENABLE = (UART_ENABLE_ENABLE_Enabled << UART_ENABLE_ENABLE_Pos);

    /* Start UART transmission and reception */
    NRF_UART0->TASKS_STARTTX = 1;
    NRF_UART0->TASKS_STARTRX = 1;

    /* Clear receive event */
    NRF_UART0->EVENTS_RXDRDY = 0;

    NRF_UART0->PSELTXD = NC;
    NRF_UART0->PSELRXD = NC;
    /* Perform and dummy write so that we can lead the write instead of
       trailing it; Currently, the pins are disconnected so the data won't be
       physically transmitted on the wire */
    NRF_UART0->TXD = 0;

    /* Wait for this write to be sent, so the data won't be physically
       transmitted when pins are assigned to the TXD pin */
    while(NRF_UART0->EVENTS_TXDRDY == 0);

    /* Finally, connect UART pins, disconnecting the flow control pins */
    NRF_UART0->PSELRTS = NC;
    NRF_UART0->PSELTXD = txd;
    NRF_UART0->PSELCTS = NC;
    NRF_UART0->PSELRXD = rxd;
}

void uart0_baud(int baudrate) {
    /* Compute the UART baudrate register value */
    uint32_t value = (uint32_t)
            ((((uint64_t) baudrate) << 32) / ((uint64_t) CLOCK_HFCLK_FREQ));

    /* Round value */
    value = (value + 0x800UL) & 0xFFFFF000UL;

    /* Finally, set value */
    NRF_UART0->BAUDRATE = value;
}

void uart0_parity(bool included) {
    /* Clear the current parity bit */
    NRF_UART0->CONFIG &= ~UART_CONFIG_PARITY_Msk;

    if (included) {
        /* Include parity bit */
        NRF_UART0->CONFIG |=
                (UART_CONFIG_PARITY_Included << UART_CONFIG_PARITY_Pos);

    } else {
        /* Exclude parity bit */
        NRF_UART0->CONFIG |=
                (UART_CONFIG_PARITY_Excluded << UART_CONFIG_PARITY_Pos);
    }
}

void uart0_disable(void) {
    if (!_uart0_enabled()) {
        /* The UART is not currently enabled */
        return;
    }

    /* Detach interrupt handler, if any */
//    uart0_detach();

    /* Clear RXTO (Receiver Timeout) event */
    NRF_UART0->EVENTS_RXTO = 0;

    /* Suspend UART */
    NRF_UART0->EVENTS_RXTO = 0;
    NRF_UART0->TASKS_SUSPEND = 1;

    while (NRF_UART0->EVENTS_RXTO == 0) {
        /* Wait for the receiver timeout */
        ;
    }

    /* Clear UART events */
    NRF_UART0->EVENTS_TXDRDY = 0;
    NRF_UART0->EVENTS_RXDRDY = 0;

    /* Disconnect all UART pins */
    NRF_UART0->PSELRTS = NC;
    NRF_UART0->PSELTXD = NC;
    NRF_UART0->PSELCTS = NC;
    NRF_UART0->PSELRXD = NC;

    /* Disable UART */
    NRF_UART0->ENABLE = UART_ENABLE_ENABLE_Disabled << UART_ENABLE_ENABLE_Pos;
}

bool uart0_readable(void) {
    return NRF_UART0->EVENTS_RXDRDY != 0;
}

int uart0_read(void) {
    while (!uart0_readable()) {
        /* Wait until the UART is readable */
        ;
    }

    /* Clear event */
    NRF_UART0->EVENTS_RXDRDY = 0;

    /* Return */
    return (uint8_t) NRF_UART0->RXD;
}

bool uart0_writable(void) {
    return NRF_UART0->EVENTS_TXDRDY != 0;
}

void uart0_write(int ch) {
    while (!uart0_writable()) {
        /* Wait until the UART is writable */
        ;
    }

    /* Clear event */
    NRF_UART0->EVENTS_TXDRDY = 0;

    /* Write */
    NRF_UART0->TXD = (uint8_t) ch;
}

void uart0_attach(void (*handler)(void)) {
    assert(handler != NULL);

    if (_handler != NULL) {
        /* Detach the previous handler */
        uart0_detach();
    }

    /* Attach new handler */
    _handler = handler;

    /* Enable UART interrupts */
    /*NRF_UART0->INTENSET = (UART_INTENSET_TXDRDY_Msk | UART_INTENSET_RXDRDY_Msk);*/
    NRF_UART0->INTENSET = UART_INTENSET_RXDRDY_Msk;

    /* Enable NVIC interrupt */
    NVIC_ClearPendingIRQ(UART0_IRQn);
    NVIC_SetPriority(UART0_IRQn, UART0_PRIORITY);
    NVIC_EnableIRQ(UART0_IRQn);
}

void uart0_detach(void) {
    /* Disable NVIC interrupt */
    NVIC_DisableIRQ(UART0_IRQn);

    /* Disable UART interrupts */
    /*NRF_UART0->INTENCLR = (UART_INTENCLR_TXDRDY_Msk | UART_INTENCLR_RXDRDY_Msk);*/
    NRF_UART0->INTENCLR = UART_INTENCLR_RXDRDY_Msk;

    /* Detach handler */
    _handler = NULL;
}
