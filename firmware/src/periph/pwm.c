/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     periph
 * @{
 *
 * @file        pwm.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Jo�o Ambr�sio <joao.ambrosio@sensefinity.com>
 *
 * @}
 */

 /* Interface */
#include "periph/pwm.h"

/********************************** Private ***********************************/

/**
 * @brief       Holds number of cycles that the specified pin will be on
 */
static uint8_t _cycles_on = 0;

/**
 * @brief       Holds number of completed cycles
 */
static uint8_t _cycles_done = 0;

/**
 * @brief       Holds the pin name
 */
static enum pin_name _pin_name = 0;

void pwm_action(void) {
	_cycles_done++;

	if(_cycles_done < _cycles_on) {
		gpio_output_set(_pin_name);
	}
	else {
		gpio_output_clear(_pin_name);
	}

	if(_cycles_done == DUTY_CYCLE_SAMPLES) {
		_cycles_done = 0;
	}
}

void timer1_init(void) {
	NRF_TIMER1->SHORTS     = (TIMER_SHORTS_COMPARE0_CLEAR_Enabled << TIMER_SHORTS_COMPARE0_CLEAR_Pos);
	/* Set timer mode */
	NRF_TIMER1->MODE       = (TIMER_MODE_MODE_Timer << TIMER_MODE_MODE_Pos);
	/* Set 32-bit mode for compare registers */
	NRF_TIMER1->BITMODE    = (TIMER_BITMODE_BITMODE_24Bit << TIMER_BITMODE_BITMODE_Pos);
	/* Set timer pre-scaler (1 tick = 1 us) */
	NRF_TIMER1->PRESCALER  = 4;

    /* Enable timer interrupts on NVIC */
    NVIC_ClearPendingIRQ(TIMER1_IRQn);
    NVIC_SetPriority(TIMER1_IRQn, DANDELION_IRQ_PRIORITY_HIGH);
    NVIC_EnableIRQ(TIMER1_IRQn);
}

uint32_t timer1_read(void) {
    /* Capture current timer value into channel (3) */
    NRF_TIMER1->TASKS_CAPTURE[3] = 1;

    /* Read channel (3) */
    return NRF_TIMER1->CC[3];
}

void timer1_configure(uint32_t time) {
    /* Configure time */
    NRF_TIMER1->CC[0] = time;

    /* Enable interrupt */
    NRF_TIMER1->EVENTS_COMPARE[0] = 0;
    NRF_TIMER1->INTENSET = (TIMER_INTENSET_COMPARE0_Msk << 0);
}

void timer1_cancel() {
    /* Disable interrupt */
    NRF_TIMER1->INTENCLR = (TIMER_INTENCLR_COMPARE0_Msk << 0);
}

void timer1_update(uint32_t time) {
    /* Update time */
    NRF_TIMER1->CC[0] = time;
}

uint32_t timer1_remaining_time() {
    return NRF_TIMER1->CC[0] - timer1_read();
}

void timer1_start(void) {
    NRF_TIMER1->TASKS_START = 1;
}

void timer1_stop(void) {
    NRF_TIMER1->TASKS_STOP = 1;
}

void timer1_clear(void) {
    NRF_TIMER1->TASKS_CLEAR = 1;
}

void isr_timer1(void) {
    if (NRF_TIMER1->EVENTS_COMPARE[0]) {
    	pwm_action();

    	/* Clear event */
    	NRF_TIMER1->EVENTS_COMPARE[0] = 0;
    }
}

/* -------------------------------------------------------------------------- */
/*          Public API                                                        */
/* -------------------------------------------------------------------------- */

void pwm_set(enum pin_name pin, uint8_t cycles_on) {
	_pin_name  = pin;
	_cycles_on = cycles_on;
}

void pwm_enable(void) {
	_cycles_done = 0;

	timer1_init();
	timer1_clear();
	timer1_configure(DUTY_CYCLE_PERIOD);
	timer1_start();
}

void pwm_disable(void) {
	timer1_cancel();
	timer1_clear();
	timer1_stop();
}
