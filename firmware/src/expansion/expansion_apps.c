/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     expansion
 * @{
 *
 * @file        expansion_apps.h
 * @brief       Specific expansion applications packets definitions
 *
 * @author  Rui Pires <rui.pires@sensefinity.com>
 *
 * @}
 */

#include "expansion/expansion_apps.h"
#include "util/utils.h"



/* Application ID */
void expansion_apps_set_app_id(uint8_t * msg, uint8_t app_id){
    msg[EXPANSION_APP_ID] = app_id;
}

uint8_t expansion_apps_get_app_id(uint8_t * msg){
    return msg[EXPANSION_APP_ID];
}


 /* Specific application fields */

/* Message type */
void expansion_apps_set_type(uint8_t * msg, uint8_t message_type) {
    msg[EXPANSION_APP_MESSAGE_TYPE] = message_type;
}

uint8_t expansion_apps_get_type(uint8_t * msg) {
    return msg[EXPANSION_APP_MESSAGE_TYPE];
}

/* Sampling period */
void expansion_measurement_set_period(uint8_t * msg, uint64_t period) {
    save_uint64_t_to_array(&msg[EXPANSION_MEASUREMENT_PERIOD],period);
}

uint64_t expansion_measurement_get_period(uint8_t * msg) {
    return get_serial_from_array(&msg[EXPANSION_MEASUREMENT_PERIOD]);
}

/* POWER MANAGEMENT */
void expansion_pm_set_order(uint8_t * msg, uint8_t order) {
	msg[EXPANSION_POWER_MANAGEMENT_ORDER] = order;
}
uint8_t expansion_pm_get_order(uint8_t * msg) {
	return msg[EXPANSION_POWER_MANAGEMENT_ORDER];
}

