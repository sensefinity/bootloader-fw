/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     expansion
 * @{
 *
 * @file        expansion_services.h
 *
 * @}
 */

#include "expansion/expansion_services.h"

#include "expansion/expansion_protocol.h"
#include "expansion/exp_packet.h"

#include "sensoroid/sensoroid.h"
#include "api/sysclk.h"

/* Service look_up */
#include "expansion/look_up.h"
/* Debug */
#include "util/debug.h"


struct expansion_service  expansion_services[MAX_SERVICES];
/*struct expansion_service {
    uint8_t service_id;
    bool active;
    uint64_t serial;
    uint64_t last_look_up;
};*/

struct my_services my_services_list;
/*
struct my_services {
uint8_t services_list[MAX_SERVICES];
uint8_t number_of_services;
};*/

void expansion_services_init(void) {

    /* Init my services list */
    my_services_list.number_of_services=0;
    memset(my_services_list.list,0x00,MAX_SERVICES);

    /* Init expansion services list */
    memset(&expansion_services, 0x00, sizeof(struct expansion_service) * MAX_SERVICES);
}

bool expansion_service_is_active(uint8_t service) {
    int i;
    for(i=0; i<MAX_SERVICES; i++) {
        if((expansion_services[i].service_id == service) && expansion_services[i].active)
            return true;
    }
    return false;
}

bool activate_service(uint8_t service) {
    /* Activate service in my_services_list */
    if(my_services_list.number_of_services < MAX_SERVICES) {
        /* Add service */
        my_services_list.list[my_services_list.number_of_services] = service;
        my_services_list.number_of_services++;
        return true;
    } else {
        /* can't add service */
        return false;
    }
}

void run_services_look_up(void) {
    /* Send look up messages for all services  that we need */
    services_look_up(my_services_list.list, my_services_list.number_of_services);
}


bool expansion_services_all_active(void) {

	int i;
	for(i=0; i<my_services_list.number_of_services; i++){
		if(!expansion_service_is_active(my_services_list.list[i])) {
			/* If there is a service not active return false */
			return false;
		}
	}
	return true;
}

void expansion_service_add(uint8_t service, uint64_t serial) {

    /* Service already exists ?*/
    size_t i;
    for(i = 0 ; i < MAX_SERVICES; i++) {
        if(expansion_services[i].service_id == service && expansion_services[i].serial==serial ) {
            /* Service already exits,*/
            dputs("[expansion_service_add] Service already exists");
            expansion_services[i].active=true;
            return;
        }
    }

    /* Service create */
    for(i = 0 ; i < MAX_SERVICES; i++) {
        if(expansion_services[i].service_id == 0 ) {
            dputs("[expansion_service_add] Create service");
            /* Activate service */
            expansion_services[i].service_id=service;
            expansion_services[i].serial=serial;
            expansion_services[i].active=true;
            return;
        }
    }

}

uint64_t expansion_services_get_serial(uint8_t service) {
    size_t i;
    for(i = 0 ; i < MAX_SERVICES; i++) {
        if(expansion_services[i].service_id == service ) {
            /* Service exits */
            return expansion_services[i].serial;
        }
    }
    /* Service not found */
    return 0x00;
}
