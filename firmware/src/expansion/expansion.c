/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup
 * @{
 *
 * @file        expansion.c
 *
 * @}
 */

 #include "expansion/expansion.h"

 #include "expansion/expansion_protocol.h"
 #include "expansion/expansion_services.h"
#include "expansion/expansion_definitions.h"
 #include "expansion/exp_packet.h"
 #include "expansion/look_up.h"
 #include "expansion/uplink.h"

 /* Columbus/gama and columbus queue */
 #include "gama_node_header_message.h"
 #include "gama_generic.h"
 #include "sensoroid/columbus.h"
 #include "sensoroid/queue.h"

 #include "sensoroid/config.h"
 #include "sensoroid/gateway.h"

 #include "util/utils.h"
 #include "api/sysclk.h"
 #include "util/debug.h"

#include "sensoroid/node_receive.h"
#include "sensoroid/c_queue.h"

#define MAX_EXP_QUEUE_SIZE					(150)
#define MAX_EXPANSION_RETRIES_TO_FAIL 		(10)

static circular_queue_t exp_tx_queue;
static uint8_t exp_tx_buffer[MAX_EXP_QUEUE_SIZE];

static circular_queue_t exp_rx_queue;
static uint8_t exp_rx_buffer[MAX_EXP_QUEUE_SIZE];

/* Save when the last search for services was made */
static uint64_t last_t_services_look_up = 0;

/* Save when the last tx expansion retry was made */
static uint64_t t_last_exp_tx_retry = 0;

static size_t expansion_tx_retries_counter=0;

enum retry_status{
    retry = 0,
    nack,
	connection_fail,
};

enum retry_status expansion_tx_retry_status = retry;

/* Function prototype , compute expansion tx retry time*/
uint64_t get_retry_time(enum retry_status ret);

void dispatcher_expansion_rx(void) {

    /* Visit message queue */
	if (c_queue_peek(get_exp_rx_queue_pointer())) {

	    size_t msg_size = c_queue_peek(get_exp_rx_queue_pointer());
	    uint8_t * msg_pointer = c_queue_message_peek(get_exp_rx_queue_pointer(),msg_size);

        /* It's for me or is broadcats*/
        /* It's for me?*/
        if(cmp_64_with_array(sensoroid_get_serial(), &msg_pointer[EX_PROTOCOL_HEADER_DST_SERIAL])
            ||cmp_64_with_array(broadcast_serial, &msg_pointer[EX_PROTOCOL_HEADER_DST_SERIAL])) {

        	dputs("[dispatcher_expansion_rx] expansion msg TO ME !!");
            uint8_t message_type = msg_pointer[EX_PROTOCOL_HEADER_MESSAGE_TYPE];

            /* Deliver specific message_type */
            if(message_type == EX_PROTOCOL_LOOK_UP_MESSAGE) {
                /* Look up message_type */
                /* Deliver msg to look up message_type */
                dputs("[dispatcher_expansion_rx] Deliver message to look up service");
                look_up_message_receive(msg_pointer);
            }

            if(message_type == EX_PROTOCOL_COLUMBUS_MESSAGE) {
                /* Columbus message_type */

            	 dputs(" [dispatcher_expansion_rx] EX_PROTOCOL_COLUMBUS_MESSAGE!");

            	 if(gama_get_node_version(&msg_pointer[EX_PROTOCOL_HEADER_SIZE]) >=GAMA_START_VERSION &&
            			 gama_get_node_destination(&msg_pointer[EX_PROTOCOL_HEADER_SIZE]) !=0  ) {

            		 /* Downlink message */
            		dputs("[dispatcher_expansion_rx] Downlink msg!!");
            		/* Message its directed to us */
            		node_receive_new_message(&msg_pointer[EX_PROTOCOL_HEADER_SIZE]);

            	} else {

            		/* Add message to the columbus messages queue */
					dputs("[dispatcher_expansion_rx] EX_PROTOCOL_COLUMBUS_MESSAGE");
					struct queue_element *columbus_element;
					/* Reserve the next columbus_element from the queue */
					columbus_element = queue_reserve();
					if (columbus_element == NULL) {
						/* We were not able to reserve space, as such, don't
						 take this message from the expansion queue, just return */
						return;
					}

					/* Initialize columbus message timestamp */
					columbus_set_timestamp(&msg_pointer[EX_PROTOCOL_HEADER_SIZE], sysclk_get());

					/* Copy message into the columbus queue and commit it */
					columbus_element->size = msg_size - EX_PROTOCOL_HEADER_SIZE;
					memcpy(columbus_element->data, &msg_pointer[EX_PROTOCOL_HEADER_SIZE], columbus_element->size);
					queue_commit();
            	}
            }

            if(message_type == EX_PROTOCOL_APP_MESSAGE) {
                /* application message_type */
                dputs("[dispatcher_expansion_rx] EX_PROTOCOL_APP_MESSAGE");
                /* Do nothing */
            }

            /* Deliver specific message_type */
            if(message_type == EX_PROTOCOL_UPLINK_MESSAGE) {
            	/* Uplink message_type */
                dputs("[dispatcher_expansion_rx] Uplink  message received!");
                /* Confirm thats a acknowledgment */
                if(uplink_is_ack(msg_pointer)) {
                	/* Gateway uplink ACK received ! */
                	gateway_uplink_ack_received(uplink_get_seq_number(msg_pointer));
                }
             }
        }

        /* delete msg from expansion queue */
        dputs("[dispatcher_expansion_rx] queue_delete");
        c_queue_delete(get_exp_rx_queue_pointer(),msg_size);
    }
}


void dispatcher_expansion_tx(void) {

	if (c_queue_peek(get_exp_tx_queue_pointer())) {
		/* We have messages to send */

		/* Its time for a new retry ?? */
		/* To prevent permanent retries check t_last_exp_tx_retry */
		if (t_last_exp_tx_retry == 0 || (sysclk_get() - t_last_exp_tx_retry) >= get_retry_time(expansion_tx_retry_status)) {

			t_last_exp_tx_retry=sysclk_get();
			/* Lets send! */
			/* Update tx status */
			expansion_tx_retry_status = retry;

			bool tx_status = false;
			/* Visit queue */

			size_t msg_size = c_queue_peek(get_exp_tx_queue_pointer());
			uint8_t aux_buff[msg_size];

			/* TODO don�t do read just peek pointer */
			/* Read message */
			c_queue_read(get_exp_tx_queue_pointer(),aux_buff,msg_size);

			/* Try send */
			/*TODO prepare for nack */
			if(expansion_protocol_send_message(aux_buff, msg_size)) {
				if(expansion_receive_ack(EX_PROTOCOL_ACK_RECEIVE_TIMEOUT)) {
					expansion_tx_retries_counter=0;
					tx_status = true;
				} else {
					expansion_tx_retries_counter++;
					dputs("[dispatcher_expansion_tx] No Ack Received!");
				}
			} else {
				dputs("[dispatcher_expansion_tx] Uart communication fail!");
			}

			if(tx_status) {
				/* success on send message, change status if needed */
				if(expansion_tx_retry_status == connection_fail) expansion_tx_retry_status = retry;
				/* Delete msg from tx expansion queue */
				dputs("[dispatcher_expansion_tx] queue_delete");
				c_queue_delete(get_exp_tx_queue_pointer(),msg_size);
			} else {
				dputs("[dispatcher_expansion_tx] tx_status else!");
				if(expansion_tx_retries_counter > MAX_EXPANSION_RETRIES_TO_FAIL) {
					dputs("[dispatcher_expansion_tx] tx_status else if!");
					/* Set expansion tx state was connection fail */
					expansion_tx_retries_counter = 0;
					expansion_tx_retry_status = connection_fail;
				}
			}
		}
	}
}

void expansion_work(uint64_t t_now) {

    /* Dispatch packets from the tX expansion queue */
    dispatcher_expansion_tx();

    /* Dispatch packets from the RX expansion queue */
    dispatcher_expansion_rx();

    if(((t_now - last_t_services_look_up) > EXPANSION_SERVICES_LOOK_UP_PERIOD)
            || ((last_t_services_look_up == 0) && (t_now > 15000))) {
        /* It's time to send a new services look up message */

    	if(!expansion_services_all_active()) {
    		/* There are services not active, do look_up */
			dputs("[expansion_work] Run services look_up");
			/* run_services_look_up */
			run_services_look_up();
			/* Update last time */
			last_t_services_look_up= t_now;
    	}
    }
}


void expansion_init(void) {

	/* Initialize expansion_tx_queue */
	c_queue_init(&exp_tx_queue, MAX_EXP_QUEUE_SIZE, exp_tx_buffer);


	/* Initialize expansion_rx_queue */
	c_queue_init(&exp_rx_queue, MAX_EXP_QUEUE_SIZE, exp_rx_buffer);

	/* Initializes  the expansion protocol*/
	expansion_protocol_init();
}


circular_queue_t * get_exp_tx_queue_pointer(void) {
	return &exp_tx_queue;
}

circular_queue_t * get_exp_rx_queue_pointer(void) {
	return &exp_rx_queue;
}

bool expansion_tx_status_ok(void) {
	if(expansion_tx_retry_status != connection_fail)
		return true;
	else return false;
}

uint64_t get_retry_time(enum retry_status ret) {

	/* Return a specific time delay for each retry status */

	switch(ret) {

	   case retry :
		   return 1000;
		   break;

	   case nack :
		   return 30000;
		   break;

	   case connection_fail :
		   return 600000;
		   break;

	   default :
		   return 600000;
	}

}
