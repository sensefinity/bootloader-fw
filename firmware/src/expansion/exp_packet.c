/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     expansion
 * @{
 *
 * @file        exp_packet.c
 * @brief       Implementation of the prototypes in the header file of same name
 *
 * @author      Rui Pires <rui.pires@sensefinity.com>
 *
 * @}
 */

#include "expansion/exp_packet.h"

/**
 * @brief       General expansion protocol header

static uint8_t _expansion_protocol_header[19] = {
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,    // Serial destination
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,    // Serial origin
    0x00,                                              // Reserved + ack
    0x00,                                              // Message size
    0x00                                               // Application id
};
*/

/********************************** Public ***********************************/

/* Oringin serial */
void exp_set_origin_serial(uint8_t *message, uint64_t serial) {
    int i;
    uint8_t *ptr = (uint8_t *) &serial;


    /* The serial number always starts @ offset = 1 */
        for (i = (sizeof(uint64_t) - 1); i >= 0; --i) {
            message[EX_PROTOCOL_HEADER_ORIGIN_SERIAL + i] = *ptr++;
        }
}

uint64_t exp_get_origin_serial(uint8_t *message) {
    uint8_t i;
    uint64_t serial;
    uint8_t *ptr = (uint8_t *) &serial;

    for (i = 0 ; i < sizeof(uint64_t); i++) {
        *ptr++ = message[EX_PROTOCOL_HEADER_ORIGIN_SERIAL + i];
    }

    return serial;
}

/* Destination serial */
void exp_set_dst_serial(uint8_t *message, uint64_t serial) {
    uint8_t i;
    uint8_t *ptr = (uint8_t *) &serial;
    for (i = 0; i < (sizeof(uint64_t)); i++) {
        message[EX_PROTOCOL_HEADER_DST_SERIAL + i] = *ptr++;
    }
}

uint64_t exp_get_dst_serial(uint8_t *message) {
    uint8_t i;
    uint64_t serial;
    uint8_t *ptr = (uint8_t *) &serial;

    for (i = 0 ; i < (sizeof(uint64_t)); i++) {
        *ptr++ = message[EX_PROTOCOL_HEADER_DST_SERIAL + i];
    }
    return serial;
}

/* Reserved Byte */
void exp_set_message_ack(uint8_t *message){
    message[EX_PROTOCOL_HEADER_ACK_BYTE] |= EX_PROTOCOL_HEADER_ACK_MASK;
}

bool exp_is_ack_message(uint8_t * message) {
    return (message[EX_PROTOCOL_HEADER_ACK_BYTE] & EX_PROTOCOL_HEADER_ACK_MASK);
}

void exp_set_message_from_base(uint8_t *message){
    message[EX_PROTOCOL_HEADER_ACK_BYTE] |= EX_PROTOCOL_HEADER_FROM_BASE_MASK;
}

bool exp_is_message_from_base(uint8_t *message) {
    return (message[EX_PROTOCOL_HEADER_ACK_BYTE] & EX_PROTOCOL_HEADER_FROM_BASE_MASK);
}

/* Size */
void exp_set_message_size(uint8_t *message, uint8_t size) {
    message[EX_PROTOCOL_HEADER_MSG_LENGTH] = size;
}

uint8_t exp_get_message_size(uint8_t *message) {
        return message[EX_PROTOCOL_HEADER_MSG_LENGTH];
}


/* Message type */
void exp_set_message_type (uint8_t *message, uint8_t type) {

    message[EX_PROTOCOL_HEADER_MESSAGE_TYPE] = type;
}

uint8_t exp_get_message_type (uint8_t *message) {
        return message[EX_PROTOCOL_HEADER_MESSAGE_TYPE];
}
