
#include "expansion/uplink.h"
#include "util/debug.h"



void uplink_set_direction(uint8_t * msg,uint8_t direction) {
	msg[UPLINK_MESSAGE_DIRECTION]= direction;
}

uint8_t uplink_get_direction(uint8_t * msg) {
	return msg[UPLINK_MESSAGE_DIRECTION];
}

void uplink_set_seq_number(uint8_t * msg,uint8_t seq_numb) {
	msg[UPLINK_MESSAGE_SEQ_NUMBER]= seq_numb;
}

uint8_t uplink_get_seq_number(uint8_t * msg) {
	return msg[UPLINK_MESSAGE_SEQ_NUMBER];
}

void uplink_copy_columbus_message(uint8_t * msg, uint8_t * columbus ,size_t length){
	memcpy(&msg[UPLINK_MESSAGE_COLUMBUS_INIT], columbus, length);
}

bool uplink_is_ack(uint8_t *msg){
	dprintf("%02x", msg[UPLINK_MESSAGE_DIRECTION]);
	return msg[UPLINK_MESSAGE_DIRECTION]==UPLINK_ACK;
}
