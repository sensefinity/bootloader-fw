

#include "expansion/expansion.h"
#include "expansion/look_up.h"
#include "expansion/expansion_services.h"
#include "expansion/expansion_protocol.h"
#include "expansion/exp_packet.h"
#include "expansion/expansion_definitions.h"

#include "sensoroid/sensoroid.h"
#include "api/sysclk.h"

#include "util/utils.h"
/* Debug */
#include "util/debug.h"


void look_up_message_receive(uint8_t * msg) {

    /* Sanity Check*/
    if(msg[EX_PROTOCOL_HEADER_MESSAGE_TYPE] != EX_PROTOCOL_LOOK_UP_MESSAGE) return;

    /* Look up responce */
    /* TODO Accept query for base from expansion */
    if(msg[LOOK_UP_MESSAGE_TYPE] != look_up_response) return;

    uint64_t origin_serial =  exp_get_origin_serial(msg);
    dprintf("originS--%Lu",origin_serial);
    size_t n_services = msg[LOOK_UP_NUNBER_OF_SERVICES];
    /* Add service, or services */
    size_t i;
    dprintf("Number of services :%d \r\n",n_services);
    for( i=0; i<n_services; i++) {
        /*Save update my services */
        dprintf("expansion_service_add :%d \r\n",msg[LOOK_UP_SERVICES_LIST_START+i]);
        expansion_service_add(msg[LOOK_UP_SERVICES_LIST_START+i],origin_serial);
    }
}

bool service_look_up(uint8_t service) {

    /* Create Expansion packet */
    uint8_t msg[EXP_PROTOCOL_MSG_MAX_SIZE];
    /* Prevention */
    memset(msg,0x00,EXP_PROTOCOL_MSG_MAX_SIZE);

    /* Header */
    exp_set_origin_serial(msg, sensoroid_get_serial());
    exp_set_dst_serial(msg, broadcast_serial);  /* Set broadcast address */
    exp_set_message_type(msg, EX_PROTOCOL_LOOK_UP_MESSAGE);   /* Look up */
    exp_set_message_from_base(msg);

    /* Service look_up payload */
    /* Query msg*/
    msg[LOOK_UP_MESSAGE_TYPE] = look_up_query;
    /* Number of services */
    msg[LOOK_UP_NUNBER_OF_SERVICES] = 1;
    /* Add service to search */
    msg[LOOK_UP_SERVICES_LIST_START] = service;

    /* Set payload lenght */
    size_t payload_size = 3;
    size_t message_size = EX_PROTOCOL_HEADER_SIZE + payload_size;
    exp_set_message_size(msg, payload_size);


    if(expansion_tx_status_ok()) {
		/* Try reserve space in the queue */
		if(c_queue_reserve(get_exp_tx_queue_pointer(),message_size)) {
			c_queue_copy_and_commit_data(get_exp_tx_queue_pointer(),msg, message_size);
		} else {
			 /* We were not able to reserve space, as such, don't "take" this
			 measurement and wait for the next */
			return false;
		}
    }

    return true;
}

bool services_look_up(uint8_t * list, size_t number_of_services){

    /* Create Expansion packet */
    uint8_t msg[EXP_PROTOCOL_MSG_MAX_SIZE];
    /* Prevention */
    memset(msg,0x00,EXP_PROTOCOL_MSG_MAX_SIZE);

    /* Header */
    exp_set_origin_serial(msg, sensoroid_get_serial());
    exp_set_dst_serial(msg, broadcast_serial); /* Set broadcast address */
    exp_set_message_type(msg, EX_PROTOCOL_LOOK_UP_MESSAGE); /* Look up */
    exp_set_message_from_base(msg);

    /* Service look_up payload */
    /* Query msg*/
    msg[LOOK_UP_MESSAGE_TYPE] = look_up_query;
    /* Number of services */
    msg[LOOK_UP_NUNBER_OF_SERVICES] = number_of_services;

    /* Add query services */
    size_t i;
    for(i=0;i<number_of_services;i++) {
        /* Add service to search */
        msg[LOOK_UP_SERVICES_LIST_START + i] = list[i];
    }

    /* Set payload lenght */
    size_t payload_size = 2 + number_of_services;
    size_t message_size = EX_PROTOCOL_HEADER_SIZE + payload_size;
    exp_set_message_size(msg, payload_size);


    if(expansion_tx_status_ok()) {
		/* Try reserve space in the queue */
		if(c_queue_reserve(get_exp_tx_queue_pointer(),message_size)) {
			c_queue_copy_and_commit_data(get_exp_tx_queue_pointer(),msg, message_size);
		} else {
			 /* We were not able to reserve space, as such, don't "take" this
			 measurement and wait for the next */
			return false;
		}
    }

    return true;
}
