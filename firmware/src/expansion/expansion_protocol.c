/**
 * Copyright (c) 2016 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     expansion
 * @{
 *
 * @file        expansion_protocol.c
 * @brief
 *
 * @author      Rui Pires <rui.pires@sensefinity.com>
 *
 * @}
 */

#include "expansion/expansion_protocol.h"
#include "expansion/exp_packet.h"

/* C standard library */
#include <stddef.h>
#include <string.h>

/* Peripherals */
#include "periph/uart0.h"
#include "periph/gpio.h"


/* Utilities */
#include "util/debug.h"
#include "api/sysclk.h"

#include "expansion/expansion.h"
#include "sensoroid/c_queue.h"

#include "util/crc_ops.h"
#include "util/utils.h"



/**
 * @brief       Rx buffer to receive messages.
 */
static uint8_t _rx_buffer[EXP_PROTOCOL_MSG_MAX_SIZE];

static bool _ack_received = false;
static bool _preamble_detected = false;

/**
 * @brief       Current size of the rx buffer.
 */
size_t end_pointer = 0;

size_t start_pointer = 0;

/**
 * @brief       Indicates if a sync interrupt started a time count
 */
uint8_t time_counting;

/**
 * @brief       Indicates the time when sync pin interrupt occurred
 */
uint64_t sync_time;

/**
 * @brief       General expansion protocol header

 static uint8_t _expansion_protocol_header[19] = {
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,    // Serial destination
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,    // Serial origin
     0x00,                                              // Reserved + ack
     0x00,                                              // Message size
     0x00                                               // Application id
 };
 */

/**
 * @brief        General expansion protocol preamble
 */
static uint8_t _expansion_protocol_preamble[2] = {0xAA, 0xAA};

/**
 * @brief       General expansion protocol ack payload
 */
static uint8_t _expansion_protocol_ack[EX_PROTOCOL_ACK_SIZE] = {
		0x01,                  /* Reserved + ack */
};


void _reset_rx_buffer(void);
void _rx_handler(void);

/**
 *
 */
void expansion_protocol_sync_int(void);

void _deinit(void){
	NRF_UART0->TASKS_SUSPEND = 1;
	NRF_UART0->ENABLE = 0;
}

/**
 *
 */
void expansion_protocol_trigger_sync(void) {
	/* disable GPIOTE to recover GPIO function */
	//    gpiote_disable_channel(GPIOTE_INT_3);
	/* wait if other sync signal is being transmitted */
	while (gpio_input_read(EX_SYNC_PIN) == 0);
	/* set to high so we can initialize the pin to high */
	gpio_output_set(EX_SYNC_PIN);
	gpio_output_init(EX_SYNC_PIN); /* init output */
	gpio_output_set(EX_SYNC_PIN); /* set to high again */
	gpio_output_clear(EX_SYNC_PIN); /* clear pin */
	delay_ms(5); /* wait 5ms */
	gpio_output_set(EX_SYNC_PIN); /* set to high again */
	gpio_input_init(EX_SYNC_PIN, GPIO_PULL_UP); /* config back to input with pull up */
	/* enable sense again */
	NRF_GPIO->PIN_CNF[EX_SYNC_PIN] |= (GPIO_PIN_CNF_SENSE_Low<<GPIO_PIN_CNF_SENSE_Pos);
	/* setup GPIOTE again to give control of the pin */
	//    gpiote_set_event(EX_SYNC_PIN, GPIOTE_INT_3, GPIOTE_CONFIG_POLARITY_HiToLo);
	//    gpiote_attach_event_function(expansion_protocol_sync_int, GPIOTE_INT_PORT);
	/* wait 5ms before start sending data */
	delay_ms(5);

}

/**
 * @brief       Send expansion protocol acknowledgement packet.
 */
static void _expansion_protocol_send_ack(void) {
	size_t a;
	expansion_protocol_trigger_sync();

	if (!_uart0_enabled()) {
		/* The UART is not currently enabled */
		uart0_init(EXPANSION_UART_TXD, EXPANSION_UART_RXD);
	}
	uart0_attach(&_rx_handler);

	/* Send preamble */
	for(a = 0; a <EX_PROTOCOL_PREAMBLE_SIZE ; a++){
		uart0_write(_expansion_protocol_preamble[a]);
	}

	/* Send ack byte */
	for(a = 0; a <EX_PROTOCOL_ACK_SIZE ; a++){
		uart0_write(_expansion_protocol_ack[a]);
	}

	/* Send final byte */
	uart0_write(ASCII_EOT);

	while (!uart0_writable()) {
		/* Wait until the UART is writable */
		;
	}
	uart0_disable();
}

/**
 * @brief       Process the received message.
 */
static bool _expansion_protocol_process_msg() {

	uint8_t uax_buffer[EXP_PROTOCOL_MSG_MAX_SIZE];

	/* Put message in the beginning of the aux buffer */
	memcpy(uax_buffer,&_rx_buffer[start_pointer],EXP_PROTOCOL_MSG_MAX_SIZE- start_pointer);
	memcpy(&uax_buffer[EXP_PROTOCOL_MSG_MAX_SIZE- start_pointer],_rx_buffer,start_pointer);

	if (memcmp(uax_buffer, _expansion_protocol_preamble, EX_PROTOCOL_PREAMBLE_SIZE) == 0) {
		/* Preamble detected */
		size_t msg_size = uax_buffer[EX_PROTOCOL_PREAMBLE_SIZE + EX_PROTOCOL_HEADER_MSG_LENGTH];
		dprintf("size %d",msg_size);

		/* All message received ? */
		if(uax_buffer[EX_PROTOCOL_PREAMBLE_SIZE + EX_PROTOCOL_HEADER_SIZE
					  + msg_size + EX_PROTOCOL_CRC_SIZE]==ASCII_EOT){

			/* Get message CRC */
			uint16_t crc_cmp = get_uint16_from_array(&uax_buffer[EX_PROTOCOL_PREAMBLE_SIZE+ EX_PROTOCOL_HEADER_SIZE + msg_size]);

			/* Valid CRC? (compute and compare) */
			if(check_crc(&uax_buffer[EX_PROTOCOL_PREAMBLE_SIZE], EX_PROTOCOL_HEADER_SIZE + msg_size, crc_cmp) ) {
				dputs("[_expansion_protocol_process_msg] Valid CRC Message Received !!");


				/* Add message to Tx expansion queue */
				size_t message_size =  msg_size + EX_PROTOCOL_HEADER_SIZE;

				/* Try reserve space in the queue */
				if(c_queue_reserve(get_exp_rx_queue_pointer(),message_size)) {
					c_queue_copy_and_commit_data(get_exp_rx_queue_pointer(),&uax_buffer[EX_PROTOCOL_PREAMBLE_SIZE], message_size);
				} else {
					/* We were not able to reserve space, as such, don't "take" this
                    measurement and wait for the next */
					return false;
				}

				dputs("[_expansion_protocol_process_msg] MSG saved on exp_rx_queue -> Send ACK!!");
				/* ACK */
				_expansion_protocol_send_ack();
				return true;
			} else return true;
		} else return false;
	} else return true;
	/* Reset Pointers */
	return true;
}

/**
 * @brief       Add the received char to the rx buffer structure.
 */
void _rx_handler(void) {
	if (uart0_readable()) {
		/* Write next character */
		_rx_buffer[end_pointer++] = (uint8_t) uart0_read();
		dprintf("[_rx_handler] char rx");

		if(!_preamble_detected) {

			if (_rx_buffer[end_pointer - 1] == EX_PROTOCOL_PREAMBLE_CHARACTER &&
					_rx_buffer[end_pointer - 2] == EX_PROTOCOL_PREAMBLE_CHARACTER) {
				/* Preamble detected */
				start_pointer = end_pointer - 2;
				_preamble_detected = true;
			}

			/* Detect special case, here preamble are divided */
			if(end_pointer==1 &&
					_rx_buffer[end_pointer-1] == EX_PROTOCOL_PREAMBLE_CHARACTER &&
					_rx_buffer[EXP_PROTOCOL_MSG_MAX_SIZE-1] == EX_PROTOCOL_PREAMBLE_CHARACTER ) {
				/* Preamble detected */
				_preamble_detected = true;
				start_pointer = EXP_PROTOCOL_MSG_MAX_SIZE-1;
			}
		}

		if (_rx_buffer[end_pointer - 1] == ASCII_EOT) {
			dputs("[_rx_handler] ASCII_EOT");
			if(_expansion_protocol_process_msg()){
				/* Expansion message received, reset buffer and pointers */
				_reset_rx_buffer();
			}else{
				dputs("[_rx_handler] Not a complete message!");
			}
		}

		if (end_pointer > EXP_PROTOCOL_MSG_MAX_SIZE)
			end_pointer = 0;
		if (end_pointer > 2) {
			__NOP();
			__NOP();
			__NOP();
		}
	}
}

/**
 * @brief       Add the received char to the rx buffer structure,
 *             specific for acknowledgements
 */
void _rx_handler_ack(void) {
	if (uart0_readable()) {
		/* Read next character */
		_rx_buffer[end_pointer++] = (uint8_t) uart0_read();
		if (_rx_buffer[end_pointer - 1] == ASCII_EOT) {
			if(_rx_buffer[end_pointer - 2] == _expansion_protocol_ack[0] &&
					_rx_buffer[end_pointer - 3]== EX_PROTOCOL_PREAMBLE_CHARACTER &&
					_rx_buffer[end_pointer - 4]== EX_PROTOCOL_PREAMBLE_CHARACTER){
				/* Ack received !!*/
				_ack_received = true;
			}
		}

		if (end_pointer > EXP_PROTOCOL_MSG_MAX_SIZE)
			end_pointer = 0;
	}
}

/* Reset buffer and pointers */
void _reset_rx_buffer(void) {
	start_pointer = 0;
	end_pointer = 0;
	_preamble_detected = false;
	memset(_rx_buffer, 0x00, EXP_PROTOCOL_MSG_MAX_SIZE);
}

/********************************** Public ************************************/

bool expansion_protocol_send_message(uint8_t * msg, size_t msg_size) {

	/* Compute crc */
	uint16_t  crc_result = compute_crc(msg,msg_size);

	expansion_protocol_trigger_sync();

	/* Reset rx buffer */
	_reset_rx_buffer();
	if (!_uart0_enabled()) {
		/* The UART is not currently enabled */
		uart0_init(EXPANSION_UART_TXD, EXPANSION_UART_RXD);
	}

	/* we expect an acknowledge for every message sent - except for ack which has another function */
	uart0_attach(&_rx_handler_ack);

	size_t a;
	/* Send preamble */
	for(a = 0; a <EX_PROTOCOL_PREAMBLE_SIZE ; a++){
		uart0_write(_expansion_protocol_preamble[a]);
	}

	/* Send msg */
	/* dputs("Send-msg");*/
	for(a = 0; a  <msg_size ; a++){
		/*dprintf("%02x",msg[a]);*/
		uart0_write(msg[a]);
	}
	/* dputs("END Send-msg"); */


	/* Send CRC */
	/* Invert indianness */
	uint8_t *ptr = (uint8_t *) &crc_result;
	ptr++;
	uart0_write(*ptr);
	ptr--;
	uart0_write(*ptr);


	/* Send final byte */
	uart0_write(ASCII_EOT);
	return true;

}

bool expansion_receive_ack(uint64_t timeout) {
	bool ret = false;
	/* Wait for acknowledgement */

	/* Attach a new UART handler */
	uart0_attach(&_rx_handler_ack);

	uint64_t t_start = sysclk_get();
	_ack_received = false;
	while(sysclk_get() - t_start < timeout) {
		// delay
		delay_us(100);
		if(_ack_received) {
			_ack_received = false;
			/* Back to normal rx handler and reset buffer pointers */
			//            _reset_rx_buffer();
			//            uart0_attach(&_rx_handler);
			ret = true;
			break;
		}
	}

	/* Back to normal rx handler and reset buffer pointers */
	_reset_rx_buffer();
	uart0_attach(&_rx_handler);
	/* disable after transaction is concluded */
	//    uart0_disable();
	_deinit();
	/* Don't receive ACK */
	return ret;
}


/*  Call this function for expansion communication interrupt */
void expansion_protocol_init(void) {

	dputs("[expansion] Expansion protocol init");

	/* Initialize global variables */
	end_pointer = 0;
	start_pointer = 0;

	/* Initialize UART */
	uart0_init(EXPANSION_UART_TXD, EXPANSION_UART_RXD);

	/* Attach a new UART handler */
	uart0_attach(&_rx_handler);
	/* The remaining UART parameters are set to the defaults */

	time_counting = 1;
	sync_time = 0;

	gpio_input_init(EX_SYNC_PIN, GPIO_PULL_UP);

	//    gpiote_set_event(EX_SYNC_PIN, GPIOTE_INT_3, GPIOTE_CONFIG_POLARITY_HiToLo);
	//    gpiote_attach_event_function(expansion_protocol_sync_int, GPIOTE_INT_3);

	/* enable sense mechanism for sync pin */
	//NRF_GPIO->PIN_CNF[EX_SYNC_PIN] |= (GPIO_PIN_CNF_SENSE_Low<<GPIO_PIN_CNF_SENSE_Pos);
	/* enable gpiote port event */
	//gpiote_attach_event_function(expansion_protocol_sync_int, GPIOTE_INT_PORT);

	gpio_attach_pin_function(expansion_protocol_sync_int, EX_SYNC_PIN, PIN_POLARITY_LOW);
}

void expansion_protocol_loop(void) {
	uint64_t current_time;
	if (time_counting != 0) {
	    current_time = sysclk_get();
		/* at least 1 second has passed ? */
		if ((current_time-sync_time) > 200) {
			time_counting = 0;
			//            uart0_disable();
			_deinit();
		}
	}
}


void expansion_protocol_sync_int(void) {
	/* start a timer counting 100ms timeout */
	sync_time = sysclk_get();
	time_counting = 1;

	/* Initialize UART */
	uart0_init(EXPANSION_UART_TXD, EXPANSION_UART_RXD);
	/* The remaining UART parameters are set to the defaults */
	//uart0_attach(&_rx_handler);

	/* after timeout disable uart - executed in loop function */
	NRF_GPIOTE->EVENTS_PORT = 0;
}
