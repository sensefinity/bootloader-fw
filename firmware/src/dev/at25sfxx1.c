/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     dev
 * @{
 *
 * @file        at25sfxx1.c
 * @brief       Implementation of the prototypes in the header file of same name
 * @note        In order to keep the SPI driver in the "periph" group free for
 *              general use, this driver will use the SPI1 peripheral instead.
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

/* Interface */
#include "dev/at25sfxx1.h"

/* C standard library */
#include <assert.h>
#include <stdbool.h>
#include <stdint.h>

/* Platform */
#include "nrf51.h"
#include "nrf51_bitfields.h"

/* Peripherals */
#include "periph/gpio.h"
#include "periph/pin_name.h"

/* Utilities */
#include "util/debug.h"
#include "util/delay.h"

/********************************** Private ***********************************/

/**
 * @brief       Holds the manufacturer ID.
 */
static uint8_t manufacturer_id;

/**
 * @brief       Holds the device ID (Parts 1 and 2).
 */
static uint8_t device_id[2];

/**
 * @brief       Buffering for one block of Flash Memory.
 */
static uint8_t block[AT25SFxx1_BLOCK];

/**
 * @brief       Initializes the SPI peripheral taking into account the
 *              interface configuration for the Flash memory.
 */
static void spi1_init(void) {
    dputs("[at25sfxx1.c] Initializing SPI1 peripheral");
    (void)block;

    /* Make sure SPI is disabled */
    NRF_SPI1->ENABLE = SPI_ENABLE_ENABLE_Disabled << SPI_ENABLE_ENABLE_Pos;

    /* Configure GPIO pins */
    gpio_output_init(AT25SFxx1_SCK);
    gpio_output_init(AT25SFxx1_SI);
    gpio_input_init(AT25SFxx1_SO, GPIO_PULL_DISABLED);

    /* Connect input buffer on the SCK pin */
    NRF_GPIO->PIN_CNF[AT25SFxx1_SCK] &= ~GPIO_PIN_CNF_INPUT_Msk;
    NRF_GPIO->PIN_CNF[AT25SFxx1_SCK] |=
        (GPIO_PIN_CNF_INPUT_Connect << GPIO_PIN_CNF_INPUT_Pos);

    /* Initialize SI value */
    gpio_output_clear(AT25SFxx1_SI);

    /* Initialize SCK value; Since we're going to use SPI Mode 0, CPOL=0 */
    gpio_output_clear(AT25SFxx1_SCK);

    /* Physically connect pins */
    NRF_SPI1->PSELSCK = AT25SFxx1_SCK;
    NRF_SPI1->PSELMOSI = AT25SFxx1_SI;
    NRF_SPI1->PSELMISO = AT25SFxx1_SO;

    /* Configure SPI Mode 0 (CPOL=0, CPHA=0) and MSB-first bit ordering */
    NRF_SPI1->CONFIG =
        (SPI_CONFIG_CPOL_ActiveHigh << SPI_CONFIG_CPOL_Pos) |
        (SPI_CONFIG_CPHA_Leading << SPI_CONFIG_CPHA_Pos) |
        (SPI_CONFIG_ORDER_MsbFirst << SPI_CONFIG_ORDER_Pos);

    /* Use maximum 8 MHz clock frequency */
    NRF_SPI1->FREQUENCY = SPI_FREQUENCY_FREQUENCY_M8;

    /* Enable SPI */
    NRF_SPI1->ENABLE = SPI_ENABLE_ENABLE_Enabled << SPI_ENABLE_ENABLE_Pos;

    dputs("[at25sfxx1.c] SPI1 is initialized");
}

/**
 * @brief       Finalizes the SPI1 peripheral.
 */
static void spi1_fini(void) {
    /* Disable SPI */
    NRF_SPI1->ENABLE = SPI_ENABLE_ENABLE_Disabled << SPI_ENABLE_ENABLE_Pos;
}

/**
 * @brief       Transfers one byte between the master and the slave.
 * @param       data
 *              The byte to be sent.
 * @return      The received byte.
 */
static uint8_t spi1_transfer(uint8_t data) {
    NRF_SPI1->EVENTS_READY = 0;
    NRF_SPI1->TXD = data;
    while (NRF_SPI1->EVENTS_READY == 0) {
        /* Wait for byte to be sent */
        ;
    }

    return NRF_SPI1->RXD;
}

#ifdef DEBUG
/**
 * @brief       Checks the integrity of the expected manufacturer and family
 *              code, warning the user in case of mismatch.
 */
static void check_integrity(void) {
    bool warn = false;

    if (manufacturer_id != 0x1F) {
        dputs("[at25sfxx1.c] Unexpected Manufacturer ID");
        warn = true;
    }

    if ((device_id[0] & 0xE0) != 0x80) {
        dputs("[at25sfxx1.c] Unexpected Family Code");
        warn = true;
    }

    if (warn) {
        dputs("[at25sfxx1.c] Warning: Device may not work as expected!");
    }
}
#endif

/**
 * @brief       Enables or disables writing to the Flash Memory.
 * @param       enable
 *              A boolean value indicating whether to enable or disable writing.
 */
static void write_enable(bool enable) {
    gpio_output_clear(AT25SFxx1_NCS);
    delay_us(10);

    if (enable) {
        spi1_transfer(0x06);
    } else {
        spi1_transfer(0x04);
    }

    gpio_output_set(AT25SFxx1_NCS);

    /* Technically, not needed (20 ns), but safety first! */
    delay_us(1);
}

/**
 * @brief       Polls the Flash's status register and waits while it is busy.
 */
static void wait_while_busy(void) {
    gpio_output_clear(AT25SFxx1_NCS);
    delay_us(10);

    /* Transfer opcode */
    spi1_transfer(0x05);

    while (1) {
        uint8_t status = spi1_transfer(0x00);
        if ((status & 0x01) == 0) {
            /* Device is ready */
            break;
        }

        /* Delay until the next poll */
        delay_us(100);
    }

    gpio_output_set(AT25SFxx1_NCS);
}

/********************************** Public ************************************/

/**
 * @brief       Initializes the AT25SFxx1 device.
 */
void at25sfxx1_init(void) {
    dputs("[at25sfxx1.c] Initializing external Flash");

    /* Initialize chip-select */
    gpio_output_init(AT25SFxx1_NCS);
    gpio_output_set(AT25SFxx1_NCS);

    /* Initialize SPI interface */
    spi1_init();

    /* Make sure the device is ready for program and erase operations after
       returning from this function; See tPUW timing in datasheet */
    delay_ms(10);

    dputs("[at25sfxx1.c] External Flash is initialized");
    dputs("[at25sfxx1.c] Reading Manufacturer and Device ID");

    /* Read "Manufacturer and Device ID" */
    gpio_output_clear(AT25SFxx1_NCS);
    delay_us(10);
    spi1_transfer(0x9F);
    manufacturer_id = spi1_transfer(0x00);
    device_id[0] = spi1_transfer(0x00);
    device_id[1] = spi1_transfer(0x00);
    gpio_output_set(AT25SFxx1_NCS);

    dprintf("[at25sfxx1.c] Manufacturer ID: %02x\r\n", manufacturer_id);
    dprintf("[at25sfxx1.c] Device ID: %02x, %02x\r\n", device_id[0], device_id[1]);

#ifdef DEBUG
    /* Check the integrity of the Flash */
    check_integrity();
#endif
}

/**
 * @brief       Finalizes the AT25SFxx1 device.
 */
void at25sfxx1_fini(void) {
    /* Disable SPI1 peripheral */
    spi1_fini();

    /* TODO: Decide what to do about switching off the memory */
}

/**
 * @brief       Reads the size in bytes of the memory.
 * @return      The size in bytes of the memory.
 */
size_t at25sfxx1_sizeof(void) {
    /* Retrieve density code from Device ID (Part 1) */
    size_t density_code = (device_id[0] & 0x1F);

    /* The density code tells us the density of the memory. From my research,
       these codes are always 5-bit and use a minimum value of 00000b for
       specifying a density of 256 kbit. Each additional unit in the density
       code corresponds to a multiplication by two on the base density of
       256 kbit, as such, we can compute the final density using a left shift */
    return (1 << (18 + density_code));
}

/**
 * @brief       Reads 'nbytes' from the Flash Memory into the buffer pointed by
 *              'buf', starting at address 'addr'.
 * @param       addr
 *              A 24-bit address.
 * @param       buf
 *              The buffer where to write the read data.
 * @param       nbytes
 *              The number of bytes to read.
 */
void at25sfxx1_read(uint32_t addr, void *buf, size_t nbytes) {
    assert((addr & 0xFFFFFFUL) == addr);
    assert(buf != NULL);
    assert(nbytes <= at25sfxx1_sizeof());

    /* Start new read operation */
    gpio_output_clear(AT25SFxx1_NCS);
    delay_us(10);
    spi1_transfer(0x03);

    /* Send address */
    spi1_transfer((addr >> 16) & 0xFF);
    spi1_transfer((addr >>  8) & 0xFF);
    spi1_transfer((addr >>  0) & 0xFF);

    /* Read */
    uint8_t *ptr = (uint8_t *) buf;
    while (nbytes--) {
        *ptr++ = spi1_transfer(0x00);
    }

    /* Stop read operation */
    gpio_output_set(AT25SFxx1_NCS);
}

/**
 * @brief       Writes 'nbytes' from the buffer 'buf' to the memory starting
 *              at address 'addr'.
 * @param       addr
 *              A 24-bit address.
 * @param       buf
 *              Pointer to the data to be written.
 * @param       nbytes
 *              The number of bytes to write.
 */
void at25sfxx1_write(uint32_t addr, const void *buf, size_t nbytes) {
    assert((addr & 0xFFFFFFUL) == addr);
    assert(buf != NULL);
    assert(nbytes <= at25sfxx1_sizeof());

    /* TODO: Depends on the required interface with the file system! */
}

/**
 * @brief       Performs a full chip erase.
 */
void at25sfxx1_erase(void) {
    /* Enable write */
    write_enable(true);

    /* Issue chip erase command */
    gpio_output_clear(AT25SFxx1_NCS);
    delay_us(10);
    spi1_transfer(0x60);
    gpio_output_set(AT25SFxx1_NCS);

    /* Wait for chip erase */
    wait_while_busy();

    /* NOTE: Write enable is automatically reset to the disabled state */
}
