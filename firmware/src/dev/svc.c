#include "dev/svc.h"

#define _svc(code) __asm volatile ("svc %0" : : "I" (code) )

void __attribute__((noinline)) sv_call_new_fw(void){
  _svc(SVC_NEW_FW);
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wreturn-type"
bool __attribute__((naked, noinline)) sv_call_is_test_required(void){
  _svc(SVC_TEST_QUERY);
  __asm("bx lr");
}
#pragma GCC diagnostic pop

void __attribute__((noinline)) sv_call_test_result(test_state_t result){
  (void)result;
  _svc(SVC_TEST_RESULT);
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wreturn-type"
int __attribute__((naked, noinline)) sv_call_write_data(char *address, char *data, size_t length){
  (void)address;
  (void)data;
  (void)length;
  _svc(SVC_WRITE_DATA);
  __asm("bx lr");
}
#pragma GCC diagnostic pop

void __attribute__((noinline)) sv_call_reset(void){
  _svc(SVC_RESET);
}
