/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     application
 * @{
 *
 * @file        main.c
 * @brief       Main application
 *
 *
 * @}
 */

/* Dandelion library */
#include <app/led.h>
#include "dandelion.h"

/* Sensoroid library */
#include "sensoroid/sensoroid.h"

/* Watchdog library */
#include "periph/wdt.h"

/* Utilities */
#include "util/debug.h"
#include "util/reset_reason.h"

/* Expansion Protocol */
#include "expansion/expansion_services.h"

/* Applications */
#include "expansion/expansion.h"
#include "expansion/expansion_protocol.h"
#include "app/user.h"
#include "app/power_management.h"

void do_nothing(void) {

}

/**
 * @brief       Main application entry point
 * @details     For low power consumption, the main function shall only
 *              perform initializations and trigger asynchronous tasks.
 */

int main(void) {
    sched_id_t id;
	/* Power management init */
	power_management_setup();

	/* User interaction init */
	user_setup();

	//Led + button is attached to device
	if(is_led_but_connected()) {
		id = sched_interval(do_nothing, 512);
		while(power_management_get_state() != POWERED_ON) {
			/* User interaction loop */
			user_loop();

			/* Power management loop */
			power_management_loop();

			dandelion_sleep_force();
		}
		sched_cancel(id);
	}
	//There is no led + button attached to device
	else {
		power_management_set_state(POWER_ON);
		user_set_state(USER_POWERED_ON);
	}

	/* Setup and start watchdog timer */
	wdt_init();

	reset_reason_set(NRF_POWER->RESETREAS);

	/* Read device serial */
	sensoroid_read_serial();

	/* Perform Sensoroid auto-configuration */
	sensoroid_configure();


	/* Initializes the expansion stuff*/
	expansion_init();

	while (true) {
		/* User interaction loop */
		if(is_led_but_connected()) {
			user_loop();
		}

		/* Power management loop */
		power_management_loop();

		/* Put Dandelion to sleep */
//	    dputs("[main] cycle");
		dandelion_sleep();

		/* Reload watchdog timer for R0 */
		wdt_reload_register(0);

		/* Get current time */
		uint64_t t_now = sysclk_get();

		/* On wake-up perform configuration loop */
		sensoroid_configure_loop(t_now);

		/* Do specific work for expansion protocol */
		expansion_work(t_now);

		/* Expansion */
		expansion_protocol_loop();
	}

	/* Program execution should never reach this statement */
	return EXIT_SUCCESS;
}
