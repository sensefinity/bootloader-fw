/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     ble
 * @{
 *
 * @file        radio.h
 * @brief       Implements a simple to use radio driver
 * @details     The radio is the physical layer of the Bluetooth stack. The
 *              radio driver is compliant with the Bluetooth Core Specfication
 *              Version 4.1 [Vol 6].
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#ifndef BLE_RADIO_H
#define BLE_RADIO_H

/* Standard C library */
#include <assert.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

/* Dandelion library */
#include "dandelion.h"

/* Platform */
#include "nrf51.h"
#include "nrf51_bitfields.h"

/* Bluetooth stack */
#include "ble/ll_pdu.h"

/* Utilities */
#include "util/delay.h"

/**
 * @brief       Holds the sizes in octets for the radio PDU
 * @details     The sizes are compatible with the single packet format defined
 *              in Bluetooth Link Layer Specification.
 */
#define RADIO_PDU_SIZE_MIN          (2)
#define RADIO_PDU_SIZE_MAX          (39)
#define RADIO_PDU_PAYLOAD_SIZE      (RADIO_PDU_SIZE_MAX - RADIO_PDU_SIZE_MIN)

/**
 * @brief       Holds the inter-frame time in microseconds
 * @details     Specifically, the time between two consecutive packets on the
 *              same radio channel.
 */
#define RADIO_TIFS                  (150)
#define RADIO_TIFS_JITTER           (2)

/**
 * @brief       Holds the time it takes for the radio to prepare itself for
 *              transmission or reception
 * @details     For more details, check the Product Spefication, 8.5.6 - Radio
 *              timing parameters.
 */
#define RADIO_RAMP_TIME_TX          (140)
#define RADIO_RAMP_TIME_RX          (138)

/**
 * @brief       Holds the minimum and maximum expected times that the radio
 *              will take until it is disabled
 */
#define RADIO_DISABLE_TIME_MAX      (5)
#define RADIO_DISABLE_TIME_MIN      (1)

/**
 * @brief       Holds the channel parameters for advertising channels
 * @details     AA stands for access address and CRC_INIT stands for CRC
 *              (Cyclic Redundant Check) initialization value.
 */
#define RADIO_ADV_CHANNEL_AA        (0x8E89BED6)
#define RADIO_ADV_CHANNEL_CRC_INIT  (0x555555)

/**
 * @brief       Enumerates the various radio events
 */
enum radio_event {
    RADIO_EVENT_TX_DONE,        /* TX has finished */
    RADIO_EVENT_RX_READY,       /* Radio is ready to receive */
    RADIO_EVENT_RX_STARTED,     /* Radio has started to receive */
    RADIO_EVENT_RX_DONE         /* RX has finished */
};

/**
 * @brief       Initializes the underlying radio in Bluetooth mode
 */
void radio_init(void);

/**
 * @brief       Sets up the radio for advertising channel
 * @details     Internally, the maximum PDU size is also set accordingly.
 */
void radio_setup_advertising(uint32_t channel);

/**
 * @brief       Sets up the radio for data channel
 * @details     Internally, the maximum PDU size is also set accordingly.
 */
void radio_setup_data(uint32_t channel, uint32_t address, uint32_t crc_init);

/**
 * @brief       Sets the radio PDU
 * @param[in]   pdu
 *              Pointer to the PDU to be sent or the PDU where the received
 *              data is written.
 */
void radio_set_pdu(void *pdu);

/**
 * @brief       Enters the radio single-shot transmit mode
 * @details     In the single-shot transmit mode, the radio is prepared for
 *              transmission, taking around RADIO_RAMP_TIME_TX microseconds to
 *              become ready. After the transmission, the RADIO_EVENT_TX_DONE
 *              event is generated.
 */
void radio_tx(void);

/**
 * @brief       Enters the radio single-shot receive mode
 * @details     In the single-shot receive mode, the radio is prepared for
 *              reception, taking around RADIO_RAMP_TIME_RX microseconds to
 *              become ready. The RADIO_EVENT_RX_STARTED event is generated
 *              once a valid address is received, making it ideal for handling
 *              receive timeouts. Once the a packet is fully received, the
 *              RADIO_EVENT_RX_DONE event is generated.
 */
void radio_rx(void);

/**
 * @brief       Enters the radio continuous receive mode
 * @details     In continuous receive mode, the radio is prepared only once
 *              for reception, taking around RADIO_RAMP_TIME_RX microseconds to
 *              become ready. Once the radio is ready, the RADIO_EVENT_RX_READY
 *              event is generated. For each packet to be received, the host
 *              must manually trigger reception by invoking the radio_start()
 *              function. The RADIO_EVENT_RX_STARTED event is generated once
 *              a valid address is received. After the reception of a full
 *              packet, the RADIO_EVENT_RX_DONE event is generated. For
 *              sucessive packets, the RADIO_EVENT_RX_READY event is not
 *              generated anymore and the user may trigger the reception of
 *              another packet immediately after the RADIO_EVENT_RX_DONE
 *              event. In order to exit the continuous receive mode, the
 *              user must invoke the radio_disable() function.
 */
void radio_continuous_rx(void);

/**
 * @brief       Enters the radio's interleaved receive and transmit mode
 * @details     In the interleaved receive and transmit mode, the radio will
 *              first enter receive mode and switch to transmit mode, while
 *              also respecting the RADIO_TIFS time in microseconds between
 *              packets. In this mode, the radio is first prepared for
 *              reception, taking around RADIO_RAMP_TIME_RX microseconds until
 *              it becomes ready. A packet reception is started automatically.
 *              Once a valid address is received, the RADIO_EVENT_RX_STARTED
 *              event is generated, making it ideal to handle receive timeouts.
 *              Once a packet has been received, the RADIO_EVENT_RX_DONE event
 *              is generated. After this event, the host has around RADIO_TIFS
 *              minus the RADIO_TIFS_JITTER to prepare the transmission, by
 *              setting a different PDU pointer (for example).
 *              If the interleaved mode is not stopped by invoking the
 *              radio_disable() function, the radio will enter transmission
 *              mode and generate the RADIO_EVENT_TX_DONE event once the
 *              process is complete.
 */
void radio_interleaved_rx_tx(void);

/**
 * @brief       Same as the radio_interleaved_rx_tx() function but the triggering
 *              of the radio receive is expected by an external source using
 *              the PPI (Programmable Peripheral Interconnect)
 */
void radio_interleaved_rx_tx_ppi(void);

/**
 * @brief       Same as the radio_interleaved_rx_tx() function but performs
 *              the inverse operation and the triggering of the radio transmit
 *              is expected from an external source using the PPI (Programmable
 *              Peripheral Interconnect)
 */
void radio_interleaved_tx_rx_ppi(void);

/**
 * @brief       Starts a radio transaction
 * @details     This function only takes effect on the continuous receive
 *              mode and may only be called after the RADIO_EVENT_RX_READY
 *              event is generated.
 */
void radio_start(void);

/**
 * @brief       Disables a on-going radio transaction
 * @details     This function may be called in order to cancel any on-going
 *              transaction. Although, it is only really useful for cancelling
 *              receive operations.
 */
void radio_disable(void);

/**
 * @brief       Enables or disables the radio RSSI feature
 * @param[in]   enable
 *              If true, the radio will measure the RSSI during a receive
 *              operation, otherwise, RSSI measurement is disabled.
 */
void radio_set_rssi_enable(bool enable);

/**
 * @brief       Reads the current RSSI value
 * @details     Reading of the RSSI value should only be performed after a
 *              receive operation, i.e., after the radio generates the
 *              RADIO_EVENT_RX_DONE event.
 * @returns     The measured received signal strength indicator.
 */
int32_t radio_read_rssi(void);

/**
 * @brief       Reads whether the received CRC matches
 * @details     This function should only be called after a receive operation,
 *              i.e., after the radio generates the RADIO_EVENT_RX_DONE event.
 * @returns     True if the CRC matches or false, otherwise.
 */
bool radio_crc_match(void);

/**
 * @brief       Sets the radio's event handler
 * @details     The radio event handler is the function to be used to notify
 *              the host of the radio operations.
 */
void radio_set_event_handler(void (*handler)(enum radio_event));

/**
 * @brief       Prototype for the radio's interrupt service routine
 * @details     Serves for documentation purposes only. The radio's interrupt
 *              service routine runs on DANDELION_IRQ_PRIORITY_HIGH.
 */
void isr_radio(void);

#endif /* BLE_RADIO_H */
