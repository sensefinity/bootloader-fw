/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     ble
 * @{
 *
 * @file        bdaddr.h
 * @brief       Defines the structure of a Bluetooth device address
 * @details     This definition is compliant with the Bluetooth Core
 *              Specification, Version 4.1 [Vol 6] Part B - 1.3 Device Address.
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#ifndef BLE_BDADDR_H
#define BLE_BDADDR_H

/* Standard C library */
#include <stdint.h>

/**
 * @brief       Holds the size of a Bluetooth device address
 */
#define BDADDR_LEN (6)

/**
 * @brief       Defines the structure of the Blueooth device address
 */
struct bdaddr {
    enum {
        BDADDR_TYPE_PUBLIC  = 0,
        BDADDR_TYPE_RANDOM
    } type;

    uint8_t data[BDADDR_LEN];
};

#endif /* BLE_BDADDR_H */
