/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     ble
 * @{
 *
 * @file        ll_buffer.h
 * @brief       Implements a buffer to be used for holding the data packets to
 *              be sent or received by the link layer in the connection state in
 *              a first-in first-out manner.
 * @details     The implemented buffer is interrupt-safe for as long as there
 *              is a single producer and a single consumer.
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#ifndef BLE_LL_BUFFER_H
#define BLE_LL_BUFFER_H

/* Standard C library */
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

/* BLE stack */
#include "ble/ll_pdu.h"

/**
 * @brief       Holds the capacity for one buffer
 */
#define BUFFER_CAPACITY     (8)

/**
 * @brief       Holds the static initialization argument list
 */
#define BUFFER_STATIC_INIT  { .head = 0, .tail = 0 }

/**
 * @brief       Defines the structure of a link layer buffer
 */
struct ll_buffer {
    struct ll_pdu_data bank[BUFFER_CAPACITY];
    uint32_t head;
    uint32_t tail;
};

/**
 * @brief       Initializes and/or clears the provided buffer
 * @param[in]   buffer
 *              Pointer to the buffer.
 */
void ll_buffer_init(struct ll_buffer *buffer);

/**
 * @brief       Inserts a new PDU into the provided buffer
 * @details     The insertion operation will cause a memory copy. A PDU is
 *              only inserted if there is enough space available, as such,
 *              the host must invoke ll_buffer_space_available() beforehand
 *              in order o ascertain if the insertion will be successful.
 * @param[in]   buffer
 *              Pointer to the buffer.
 * @param[in]   pdu
 *              Pointer to the PDU.
 */
void ll_buffer_put(struct ll_buffer *buffer, const struct ll_pdu_data *pdu);

/**
 * @brief       Retrieves the next PDU from the provided buffer
 * @details     The retrieval operation will cause a memory copy. A PDU is
 *              only retrieved if there is data available, as such, the host
 *              must invoke ll_buffer_data_available() beforehand in order to
 *              ascertain if the retrieval will be successful.
 */
void ll_buffer_get(struct ll_buffer *buffer, struct ll_pdu_data *pdu);

/**
 * @brief       Tests whether there is enough space for a new PDU
 * @param[in]   buffer
 *              Pointer to the buffer.
 * @returns     True if there is enough space available or false, otherwise.
 */
bool ll_buffer_space_available(struct ll_buffer *buffer);

/**
 * @brief       Tests whether there is at least one new PDU in the buffer
 * @param[in]   buffer
 *              Pointer to the buffer.
 * @returns     True if there is at least one new PDU or false, otherwise.
 */
bool ll_buffer_data_available(struct ll_buffer *buffer);

#endif /* BLE_LL_BUFFER_H */
