/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     ble
 * @{
 *
 * @file        timer.h
 * @brief       Implements a simple to use low-level timer driver
 * @details     The timer serves as the base for the link layer scheduling
 *              algorithms.
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#ifndef BLE_TIMER_H
#define BLE_TIMER_H

/* Standard C library */
#include <assert.h>
#include <stdint.h>
#include <stdlib.h>

/* Dandelion library */
#include "dandelion.h"

/* Platform */
#include "nrf51.h"
#include "nrf51_bitfields.h"

/**
 * @brief       Helper macros for conversion into the timer's time base
 */
#define TIMER_MICROS(x)     ((x))
#define TIMER_MILLIS(x)     ((x) * 1000)

/**
 * @brief       Holds the maximum number of usable timer channels
 * @details     One channel is always reserved for reading operations.
 */
#define TIMER_MAX_CHANNELS  (4)

/**
 * @brief       Initializes the underlying timer for microsecond precision
 */
void timer_init(void);

/**
 * @brief       Reads the current timer value
 * @returns     The current timer value.
 */
uint32_t timer_read(void);

/**
 * @brief       Configures an action to be called on a specific channel
 * @param[in]   channel
 *              The channel to be configured; A number between 0 and
 *              (TIMER_MAX_CHANNELS - 1).
 * @param[in]   action
 *              The action to be associated with the provided channel.
 * @param[in]   time
 *              The time at which to trigger the action.
 */
void timer_configure(uint32_t channel, void (*action)(void), uint32_t time);

/**
 * @brief       Cancels a timer action
 * @param[in]   channel
 *              The channel associated with the timer action to be cancelled.
 *              A number between 0 and (TIMER_MAX_CHANNELS - 1).
 */
void timer_cancel(uint32_t channel);

/**
 * @brief       Updates the current value of a previously configured timer
 *              action
 * @param[in]   channel
 *              The channel to be updated. A number between 0 and
 *              (TIMER_MAX_CHANNELS - 1).
 * @param[in]   time
 *              The new time at which to trigger the action.
 */
void timer_update(uint32_t channel, uint32_t time);

/**
 * @brief       Computes the remaining time until the provided channel is
 *              triggered
 * @param[in]   channel
 *              The channel to be tested.
 * @returns     The time in microseconds until the provided channel is
 *              triggered.
 */
uint32_t timer_remaining_time(uint32_t channel);

/**
 * @brief       Starts the underlying timer
 */
void timer_start(void);

/**
 * @brief       Stops the underlying timer
 */
void timer_stop(void);

/**
 * @brief       Clears the underlying timer
 */
void timer_clear(void);

/**
 * @brief       Prototype for the timer's interrupt service routine
 * @details     Serves for documentation purposes only. The timer's interrupt
 *              service routine runs on DANDELION_IRQ_PRIORITY_HIGH.
 */
void isr_timer0(void);

#endif /* BLE_TIMER_H */
