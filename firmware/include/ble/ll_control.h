/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     ble
 * @{
 *
 * @file        ll_control.h
 * @brief       Implements the link layer control procedures following the
 *              LLCP (Link Layer Control Protocol)
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#ifndef BLE_LL_CONTROL_H
#define BLE_LL_CONTROL_H

/* Standard C library */
#include <stdint.h>

/* BLE stack */
#include "ble/ll.h"
#include "ble/ll_pdu.h"
#include "ble/ll_buffer.h"

/**
 * @brief       Performs the link layer termination procedure
 * @details     The termination procedure is used to terminate the current
 *              connection.
 * @param[in]   error_code
 *              The error code shall be set to inform the remote device why
 *              the connection is about to be terminated. See [Vol 2] Part D for
 *              details. Specifically, a value of 0x00 indicates success,
 *              otherwise, it is a failure.
 */
void ll_control_termination_procedure(uint8_t error_code);


#endif /* BLE_LL_CONTROL_H */
