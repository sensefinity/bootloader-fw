/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     ble
 * @{
 *
 * @file        ll_state.h
 * @brief       Defines link layer states with safe control functions
 * @details     This definition is compliant with the Bluetooth Core
 *              Specification, Version 4.1 [Vol 6] Part B.
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#ifndef BLE_LL_STATE_H
#define BLE_LL_STATE_H

/* Standard C library */
#include <stdbool.h>
#include <stdint.h>

/* Peripherals */
#include "periph/clock.h"

/**
 * @brief       Enumerates the various link layer states
 */
enum ll_state {
    LL_STATE_UNKNOWN        = -1,
    LL_STATE_STANDBY        =  0,
    LL_STATE_ADVERTISING,
    LL_STATE_SCANNING,
    LL_STATE_INITIATING,
    LL_STATE_CONNECTION
};

/**
 * @brief       Performs the initialization of the link layer state, effectively
 *              transitioning from LL_STATE_UNKNOWN to LL_STATE_STANDBY.
 */
void ll_state_init(void);

/**
 * @brief       Sets the new link layer state
 * @details     The new link layer state must be valid and compatible with the
 *              state diagram presented in the Bluetooth specification. This
 *              function may only be called after the link layer is initialized.
 *              The Bluetooth defines the following rules:
 *                  * The standby state can be entered from any other state;
 *                  * The advertising, scanning or initiating states may only
 *                    the entered from the standby state;
 *                  * The connection state may only be entered from the
 *                    advertising and initiating states;
 * @param[in]   state
 *              The new link layer state.
 * @returns     True if the new link layer state was entered or false, if the
 *              target state cannot be entered from the current state.
 */
bool ll_set_state(enum ll_state state);

/**
 * @brief       Retrieves the current link layer state
 * @returns     The current link layer state.
 */
enum ll_state ll_get_state(void);

#endif /* BLE_LL_STATE_H */
