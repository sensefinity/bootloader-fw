/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     ble
 * @{
 *
 * @file        gap.h
 * @brief       Implements the GAP (Generic Access Profile)
 * @details     The Generic Access Profile control how two devices discover
 *              and interact with each other, defining various device roles
 *              such as Broadcaster, Observer, Central and Peripheral.
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#ifndef BLE_LL_GAP_H
#define BLE_LL_GAP_H


#endif /* BLE_LL_GAP_H */
