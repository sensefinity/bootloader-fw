/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     ble
 * @{
 *
 * @file        ll.h
 * @brief       Implements the Bluetooth LE link layer
 * @details     This definition is compliant with the Bluetooth Core
 *              Specification, Version 4.1 [Vol 6] Part B. The following
 *              features are not present and may be implemented in the future:
 *                  * Filter Policies;
 *                  * Duplicate filtering on scanning state;
 *                  * Connection events are always marked by a single packet
 *                    sent or received from each device; This means no complex
 *                    buffer handling, radio handling and usually, makes
 *                    flow control easier while also reducing code size;
 *                  * No slave latency support; The slave will always wake
 *                    up for every connection event;
 *
 *              The link layer uses software interrupt SWI0 for sending
 *              advertising reports to the host.
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#ifndef BLE_LL_H
#define BLE_LL_H

/* Standard C library */
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

/* Dandelion library */
#include "dandelion.h"

/* Platform */
#include "nrf51.h"
#include "nrf51_bitfields.h"

/* Bluetooth stack */
#include "ble/bdaddr.h"
#include "ble/ll_buffer.h"
#include "ble/ll_pdu.h"
#include "ble/ll_state.h"
#include "ble/radio.h"
#include "ble/random.h"
#include "ble/timer.h"

/* Peripherals */
#include "periph/ppi.h"

/**
 * @brief       Initializes the link layer into a known state
 * @details     This function initializes the link layer performing the state
 *              transition from LL_STATE_UNKNOWN to LL_STATE_STANDBY. This
 *              function only takes effect when the link layer is in the
 *              LL_STATE_UNKNOWN state.
 * @param[in]   addr
 *              Pointer to this link layer's device address. If NULL, the
 *              link layer won't be initialized.
 */
void ll_init(const struct bdaddr *addr);

/**
 * @brief       Sets this link layer's device address
 * @details     This function only takes effect when the link layer is in the
 *              LL_STATE_STANDBY state.
 * @param[in]   addr
 *              Pointer to this link layer's new device address. If NULL,
 *              the link layer's device address will remain unchanged.
 */
void ll_set_bdaddr(const struct bdaddr *addr);

/**
 * @brief       Gets this link layer's current device address
 * @returns     Pointer to this link layer's current device address or NULL
 *              if the link layer is not yet initialized.
 */
const struct bdaddr *ll_get_bdaddr(void);


/* -------------------------------------------------------------------------- */
/*      Advertising Channel Hop Sequence                                      */
/* -------------------------------------------------------------------------- */

/**
 * @brief       Helper macros for the advertising channel map
 * @details     The advertising channel map controls the link layer hop sequence
 *              when using advertising channels.
 */
#define LL_ADVERTISING_CHANNEL_MAP_37       (0x1)
#define LL_ADVERTISING_CHANNEL_MAP_38       (0x2)
#define LL_ADVERTISING_CHANNEL_MAP_39       (0x4)
#define LL_ADVERTISING_CHANNEL_MAP_ALL      (\
        LL_ADVERTISING_CHANNEL_MAP_37       |\
        LL_ADVERTISING_CHANNEL_MAP_38       |\
        LL_ADVERTISING_CHANNEL_MAP_39)

/**
 * @brief       Setter for the link layer advertising channel map
 * @param[in]   map
 *              The new advertising channel map. Specifically, a bit mask
 *              where the three least significant bits represent whether
 *              each individual advertising channel is active. Refer to the
 *              LL_ADVERTISING_CHANNEL_MAP_* macros for more details. At least
 *              one channel must always be active.
 * @returns     True, if the new channel map was acknowledged or false,
 *              otherwise.
 */
bool ll_set_advertising_channel_map(uint32_t map);

/**
 * @brief       Resets the advertising index to the first enabled advertising
 *              channel
 */
void ll_set_initial_advertising_channel(void);

/**
 * @brief       Sets the next advertising index in the configured adverting
 *              map, which defines the hop sequence
 * @returns     True, if the new advertising index was set or false, otherwise.
 */
bool ll_set_next_advertising_channel(void);

/**
 * @brief       Getter for the current advertising index
 * @returns     The current advertising index.
 */
uint32_t ll_get_advertising_channel_index(void);

/**
 * @brief       Getter for the number of enabled advertising channels
 */
uint32_t ll_get_enabled_advertising_channel_count(void);

/* -------------------------------------------------------------------------- */
/*      Advertising                                                           */
/* -------------------------------------------------------------------------- */

/**
 * @brief       Holds the maximum allowed total size in bytes for the
 *              advertising data and scan response data
 */
#define LL_ADVERTISING_DATA_SIZE        (LL_PDU_ADV_PAYLOAD_SIZE - BDADDR_LEN)
#define LL_SCAN_RESPONSE_DATA_SIZE      (LL_PDU_ADV_PAYLOAD_SIZE - BDADDR_LEN)

/**
 * @brief       Holds the advertising interval limits
 * @details     The advertising interval is the time between two consecutive
 *              advertising events. Each value is interpreted as a multiple
 *              of 625 us (0.625 ms).
 */
#define LL_ADVERTISING_INTERVAL_MIN         (0x0020)
#define LL_ADVERTISING_INTERVAL_NONCONN_MIN (0x00A0)
#define LL_ADVERTISING_INTERVAL_MAX         (0x4000)

/**
 * @brief       Enumerates the various link layer advertising types
 */
enum ll_advertising_type {
    LL_ADVERTISING_TYPE_CONNECTABLE_UNDIRECTED              = 0,
    LL_ADVERTISING_TYPE_CONNECTABLE_DIRECTED_LOW_DUTY,
    LL_ADVERTISING_TYPE_CONNECTABLE_DIRECTED_HIGH_DUTY,
    LL_ADVERTISING_TYPE_NONCONNECTABLE_UNDIRECTED,
    LL_ADVERTISING_TYPE_SCANNABLE_UNDIRECTED
};

/**
 * @brief       Defines the structure of the link layer advertising parameters
 * @details     The advertising parameters have ranges and may be unsused
 *              depending on the selected type of advertising.
 *              For more information, read: Bluetooth Core Specification
 *              Version 4.1 [Vol 2], 7.8.5 - LE Set Advertising Parameters
 *              Command.
 */
struct ll_advertising_params {
    uint32_t interval;
    enum ll_advertising_type type;
    struct bdaddr direct_addr;
    uint32_t channel_map;
};

/**
 * @brief       Sets the link layer advertising data
 * @details     The provided advertising data will be used to build the
 *              advertising packets. The advertising data may only be set
 *              when the link layer is in LL_STATE_STANDBY state.
 * @param[in]   data
 *              Pointer to the input data. If NULL, the input data is considered
 *              empty, regardless of the provided size.
 * @param[in]   size
 *              Size in bytes of the input advertising data. If the input
 *              size is more than LL_ADVERTISING_DATA_SIZE, it is truncated.
 */
void ll_set_advertising_data(const uint8_t *data, size_t size);

/**
 * @brief       Sets the link layer scan response data
 * @details     The provided scan response data will be used to build the
 *              SCAN_RSP packets. The scan response data may only be set when
 *              the link layer is in LL_STATE_STANDBY state.
 * @param[in]   data
 *              Pointer to the input data. If NULL, the input data is considered
 *              empty, regardless of the provided size.
 * @param[in]   size
 *              Size in bytes of the input scan response data. If the input
 *              size is more than LL_SCAN_RESPONSE_DATA_SIZE, it is truncated.
 */
void ll_set_scan_response_data(const uint8_t *data, size_t size);

/**
 * @brief       Sets the advertising parameters
 * @details     The input advertising parameters are validated following the
 *              Bluetooth Core Specification Version 4.1 [Vol 2] 7.8.5 - LE
 *              Set Advertising Parameters Command.
 * @param[in]   params
 *              The new advertising parameters.
 * @returns     True if the input advertising parameters were accepted as
 *              valid or false, otherwise.
 */
bool ll_set_advertising_params(struct ll_advertising_params params);

/**
 * @brief       Gets the current advertising parameters
 * @returns     The current advertising parameters.
 */
struct ll_advertising_params ll_get_advertising_params(void);

/**
 * @brief       Sets the advertise enable state
 * @param[in]   enable
 *              If true, advertising is enabled, otherwise, advertising is
 *              disabled.
 * @returns     Whether the operation was successful.
 */
bool ll_set_advertise_enable(bool enable);

/* -------------------------------------------------------------------------- */
/*      Scanning                                                              */
/* -------------------------------------------------------------------------- */

/**
 * @brief       Holds the scan interval limits
 * @details     The scan interval is the time between two consecutive scan
 *              windows. Each value is interpreted as multiple of 625 us
 *              (0.625 ms).
 */
#define LL_SCAN_INTERVAL_MIN    (0x0004)
#define LL_SCAN_INTERVAL_MAX    (0x4000)

/**
 * @brief       Holds the scan window limits
 * @details     The scan window is the time that the radio will spend open to
 *              receiving advertising packets. Each value is interpreted as
 *              multiple of 625 us (0.625 ms).
 */
#define LL_SCAN_WINDOW_MIN      (0x0004)
#define LL_SCAN_WINDOW_MAX      (0x4000)

/**
 * @brief       Enumerates the various advertising report types
 */
enum ll_advertising_report_type {
    LL_ADVERTISING_REPORT_TYPE_CONNECTABLE_UNDIRECTED       = 0,
    LL_ADVERTISING_REPORT_TYPE_CONNECTABLE_DIRECTED,
    LL_ADVERTISING_REPORT_TYPE_SCANNABLE_UNDIRECTED,
    LL_ADVERTISING_REPORT_TYPE_NONCONNECTABLE_UNDIRECTED,
    LL_ADVERTISING_REPORT_TYPE_SCAN_RESPONSE
};

/**
 * @brief       Defines the structure of an advertising report
 */
struct ll_advertising_report {
    enum ll_advertising_report_type type;
    struct bdaddr addr;
    uint8_t data[LL_ADVERTISING_DATA_SIZE];
    size_t size;
    int32_t rssi;
};

/**
 * @brief       Enumerates the various scan types
 */
enum ll_scan_type {
    LL_SCAN_TYPE_PASSIVE    = 0,
    LL_SCAN_TYPE_ACTIVE
};

/**
 * @brief       Defines the structure of the scan parameters
 * @details     The scan parameters have ranges that must be respected.
 *              For more information, read: Bluetooth Core Specification
 *              Version 4.1 [Vol 2], 7.8.10 - LE Set Scan Parameters Command.
 */
struct ll_scan_params {
    enum ll_scan_type type;
    uint32_t interval;
    uint32_t window;
    uint32_t channel_map;
};

/**
 * @brief       Sets the scan parameters
 * @details     The input scan parameters are validated following the Bluetooth
 *              Core Specification Version 4.1 [Vol 2], 7.8.10 - LE Set Scan
 *              Parameters Command.
 * @param[in]   params
 *              The new scan parameters.
 * @returns     True if the input scan parameters were accepted as valid or
 *              false, otherwise.
 */
bool ll_set_scan_params(struct ll_scan_params params);

/**
 * @brief       Gets the current scan parameters
 * @returns     The current scan parameters.
 */
struct ll_scan_params ll_get_scan_params(void);

/**
 * @brief       Sets the scan enable state
 * @param[in]   enable
 *              If true, scanning is enabled, otherwise, scanning is disabled.
 * @returns     Whether the operation was successful.
 */
bool ll_set_scan_enable(bool enable);

/**
 * @brief       Sets the handler for advertising reports
 * @param[in]   handler
 *              The handler to be invoked for each generated advertising
 *              report.
 */
void ll_set_advertising_report_handler(
        void (*handler)(struct ll_advertising_report));

/**
 * @brief       Prototype for the SWI0's interrupt service routine
 * @details     Serves for documentation purposes only. The SWI0's interrupt
 *              service routine runs on DANDELION_IRQ_PRIORITY_LOW.
 */
void isr_swi0(void);


/* -------------------------------------------------------------------------- */
/*      Data Channel Hop Sequence                                             */
/* -------------------------------------------------------------------------- */

/**
 * @brief       Useful macros for handling the data channel map
 */
#define LL_DATA_CHANNEL_MAP_ALL     ((uint64_t) 0x1FFFFFFFFFULL)
#define LL_DATA_CHANNEL_MAP_BIT(n)  ((uint64_t) 0x1ULL << (n))

/**
 * @brief       Configures the data channel hopping sequence
 * @param[in]   map
 *              The channel map of enabled data channels. In Bluetooth LE, there
 *              are 37 available channels, indexed from 0-36. In the provided
 *              channel map the least significant bit (bit #0) corresponds to
 *              channel 0, while the most significant bit (bit #36) corresponds
 *              to channel 36. At least two data channels must be enabled,
 *              otherwise the previous channel map is kept.
 * @param[in]   hop
 *              The hop value. Per specification, a value between 5 and 16.
 */
bool ll_configure_data_channel_hopping(uint64_t map, uint32_t hop);

/**
 * @brief       Sets the next data channel
 * @details     The next data channel is a function of the current data channel,
 *              the hop value and the used data channels.
 */
void ll_set_next_data_channel(void);

/**
 * @brief       Retrieves the current data channel index
 * @returns     The current data channel index.
 */
uint32_t ll_get_data_channel_index(void);


/* -------------------------------------------------------------------------- */
/*      Initiating                                                            */
/* -------------------------------------------------------------------------- */

/**
 * @brief       Definition of the minimum and maximum values for the connection
 *              interval parameter
 * @details     All values are considered multiples of 1.25 ms in the range of
 *              7.5 ms to 4 s.
 */
#define LL_CONN_INTERVAL_MIN        (0x0006)
#define LL_CONN_INTERVAL_MAX        (0x0C80)

/**
 * @brief       Defines the minimum and maximum values for the slave connection
 *              latency parameter
 * @details     All values are interpreted as number of events (each event
 *              being triggered on each connection interval). Values are between
 *              0 and 499.
 */
#define LL_CONN_LATENCY_MIN         (0x0000)
#define LL_CONN_LATENCY_MAX         (0x01F3)

/**
 * @brief       Defines the minimum and maximum values for the supervision
 *              timeout parameter
 * @details     The supervision timeout is used in order detect lost
 *              connections. All values are multiples of 10 ms in the range
 *              of 100 ms to 32 s.
 */
#define LL_SUPERVISION_TIMEOUT_MIN  (0x000A)
#define LL_SUPERVISION_TIMEOUT_MAX  (0x0C80)

/**
 * @brief       Enumerates the various connection parameters
 * @details     The connection parameters are used by the initiating state in
 *              order to build a connect request to a peer device.
 */
struct ll_connection_params {
    uint32_t scan_interval;
    uint32_t scan_window;
    struct bdaddr peer_addr;
    uint32_t conn_interval;
    uint32_t conn_latency;
    uint32_t supervision_timeout;
    uint32_t channel_map;
};

/**
 * @brief       Sets the host channel classification
 * @details     Allows the host to set a classification for the data channels
 *              to be used during a connection based on its "local" information.
 * @param[in]   channel_map
 *              The channel map of enabled data channels. In Bluetooth LE, there
 *              are 37 available channels, indexed from 0-36. In the provided
 *              channel map the least significant bit (bit #0) corresponds to
 *              channel 0, while the most significant bit (bit #36) corresponds
 *              to channel 36. At least one data channel must be enabled,
 *              otherwise the previous channel map is kept.
 */
void ll_set_host_channel_classification(uint64_t channel_map);

/**
 * @brief       Creates a connection to a peer device
 * @param[in]   params
 *              The desired connection parameters.
 *              The connection parameters follow certain rules and ranges, which
 *              may be consulted in the Bluetooth Core Specification Version 4.1
 *              [Vol 2] - LE Create Connection Command. Specifically, the
 *              scan_* parameters follow the same rules as those defined in the
 *              scanning state.
 * @returns     True if the request was acknowledged as valid and the link layer
 *              entered the initiating state searching for the peer device or
 *              false if the parameters are invalid and no action was taken.
 */
bool ll_create_connection(struct ll_connection_params params);

/**
 * @brief       Cancels an on-going connection creation to a peer device
 * @details     Allows the host to cancel a connection that has not yet been
 *              established.
 * @returns     True if the connection creation could be cancelled or false,
 *              otherwise.
 */
bool ll_create_connection_cancel(void);


/* -------------------------------------------------------------------------- */
/*      Connection                                                            */
/* -------------------------------------------------------------------------- */

/* Defines link layer clock & ISR parameters */
#define LL_CLOCK_SCA        (LL_PDU_SCA_031_TO_050_PPM)
#define LL_CLOCK_SCA_PPM    (50)
#define LL_ISR_LATENCY      (30)

/**
 * @brief       Enumerates the various connection events
 * @details     Connection events are generated by the link layer in order to
 *              inform the host of the current connection status.
 */
enum ll_connection_event {
    LL_CONNECTION_EVENT_CONNECTION_CREATED              = 0,
    LL_CONNECTION_EVENT_CONNECTION_ESTABLISHED,
    LL_CONNECTION_EVENT_CONNECTION_LOST,
    LL_CONNECTION_EVENT_CONNECTION_TERMINATED_OK,
    LL_CONNECTION_EVENT_CONNECTION_TERMINATED_ERROR,
    LL_CONNECTION_EVENT_DATA_SENT,
    LL_CONNECTION_EVENT_DATA_RECEIVED
};

/**
 * @brief       Enumerates the various link layer connection roles
 */
enum ll_connection_role {
    LL_CONNECTION_ROLE_NONE     = -1,
    LL_CONNECTION_ROLE_MASTER   =  0,
    LL_CONNECTION_ROLE_SLAVE
};

/**
 * @brief       Handles the transition into the connection state
 * @details     The transition into the connection state should only be
 *              performed by the link layer advertising and initiating states.
 *              When transitioning into connection state, the link layer timer
 *              must still be running.
 * @param[in]   pdu
 *              A valid connect request PDU which may be either the request
 *              that was sent to or received from a peer device.
 * @param[in]   t_reference
 *              The reference time at which the connect request was sent or
 *              received. This value will be used for establishing the
 *              connection and as such, must be as accurate as possible.
 */
void ll_connect(struct ll_pdu_adv pdu, uint32_t t_reference);

/**
 * @brief       Retrieves the current connection role
 * @returns     The current connection role.
 */
enum ll_connection_role ll_connection_get_role(void);

/**
 * @brief       Retrieves the Bluetooth device address of the peer device that
 *              we're connected to.
 * @details     This value is only available after a created connection.
 * @return      The device address of the peer.
 */
struct bdaddr ll_connection_peer_addr(void);

/**
 * @brief       Sets the connection event handler
 * @details     The connection event handler is used to inform the host of
 *              the current connection status.
 * @param[in]   handler
 *              Pointer to handler function.
 */
void ll_set_connection_event_handler(void (*handler)(enum ll_connection_event));

/**
 * @brief       Retrieves a pointer to the transmit buffer
 * @details     The transmit buffer shall be write-only from the host's
 *              perspective. The link layer shall generate the
 *              LL_CONNECTION_EVENT_DATA_SENT event for each trasmitted
 *              packet.
 * @returns     A pointer to the transmit buffer.
 */
struct ll_buffer *ll_get_connection_tx_buffer(void);

/**
 * @brief       Retrieves a pointer to the receive buffer
 * @details     The receive buffer shall be read-only from the host's
 *              perspective. The link layer shall generate the
 *              LL_CONNECTION_EVENT_DATA_RECEIVED event for each received
 *              packet.
 * @returns     A pointer to the receive buffer.
 */
struct ll_buffer *ll_get_connection_rx_buffer(void);

/**
 * @brief       Prototype for the SWI1's interrupt service routine
 * @details     Serves for documentation purposes only. The SWI1's interrupt
 *              service routine runs on DANDELION_IRQ_PRIORITY_LOW.
 */
void isr_swi1(void);

#endif /* BLE_LL_H */
