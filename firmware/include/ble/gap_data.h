/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     ble
 * @{
 *
 * @file        gap_data.h
 * @brief       GAP (Generic Access Profile) Data Types
 * @details     Implements a set of enumerations, structures and functions
 *              which are useful for dealing with GAP Data Types.
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#ifndef BLE_LL_GAP_DATA_H
#define BLE_LL_GAP_DATA_H

/* Standard C library */
#include <stdint.h>

/* Bluetooth stack */
#include "ble/bdaddr.h"
#include "ble/ll_pdu.h"

/**
 * @brief       Holds the maximum size for GAP Data
 * @details     The maximum data size is a function of the maximum payload size
 *              for advertising channels, excluding the size of the Bluetooth
 *              access address, i.e. the number of free octets for application
 *              use.
 */
#define GAP_DATA_SIZE   (LL_PDU_ADV_PAYLOAD_SIZE - BDADDR_LEN)

/**
 * @brief       Enumerates the various GAP Data Types
 * @details     See the list of Bluetooth assigned numbers at:
 *              https://www.bluetooth.org/en-us/specification/assigned-numbers.
 */
enum gap_data_type {
    GAP_DATA_TYPE_FLAGS                                         = 0x01,
    GAP_DATA_TYPE_INCOMPLETE_LIST_OF_16BIT_SERVICE_CLASS_UUIDS  = 0x02,
    GAP_DATA_TYPE_COMPLETE_LIST_OF_16BIT_SERVICE_CLASS_UUIDS    = 0x03,
    GAP_DATA_TYPE_INCOMPLETE_LIST_OF_32BIT_SERVICE_CLASS_UUIDS  = 0x04,
    GAP_DATA_TYPE_COMPLETE_LIST_OF_32BIT_SERVICE_CLASS_UUIDS    = 0x05,
    GAP_DATA_TYPE_INCOMPLETE_LIST_OF_128BIT_SERVICE_CLASS_UUIDS = 0x06,
    GAP_DATA_TYPE_COMPLETE_LIST_OF_128BIT_SERVICE_CLASS_UUIDS   = 0x07,
    GAP_DATA_TYPE_SHORTENED_LOCAL_NAME                          = 0x08,
    GAP_DATA_TYPE_COMPLETE_LOCAL_NAME                           = 0x09,
    GAP_DATA_TYPE_TX_POWER_LEVEL                                = 0x0A,
    GAP_DATA_TYPE_CLASS_OF_DEVICE                               = 0x0D,
    GAP_DATA_TYPE_SIMPLE_PAIRING_HASH_C                         = 0x0E,
    ​GAP_DATA_TYPE_SIMPLE_PAIRING_HASH_C192                      = 0x0E,
    GAP_DATA_TYPE_SIMPLE_PAIRING_RANDOMIZER_R                   = 0x0F,
    GAP_DATA_TYPE_SIMPLE_PAIRING_RANDOMIZER_R192                = 0x0F,
    GAP_DATA_TYPE_DEVICE_ID                                     = 0x10,
    GAP_DATA_TYPE_SECURITY_MANAGER_TK_VALUE                     = 0x10,
    GAP_DATA_TYPE_SECURITY_MANAGER_OUT_OF_BAND_FLAGS            = 0x11,
    GAP_DATA_TYPE_SLAVE_CONNECTION_INTERVAL_RANGE               = 0x12,
    GAP_DATA_TYPE_LIST_OF_16BIT_SERVICE_SOLICITATION_UUIDS      = 0x14,
    GAP_DATA_TYPE_LIST_OF_32BIT_SERVICE_SOLICITATION_UUIDS      = 0x1F,
    GAP_DATA_TYPE_LIST_OF_128BIT_SERVICE_SOLICITATION_UUIDS     = 0x15,
    GAP_DATA_TYPE_SERVICE_DATA                                  = 0x16,
    GAP_DATA_TYPE_SERVICE_DATA_16BIT_UUID                       = 0x16,
    ​GAP_DATA_TYPE_SERVICE_DATA_32BIT_UUID                       = 0x20,
    GAP_DATA_TYPE_SERVICE_DATA_128BIT_UUID                      = 0x21,
    ​GAP_DATA_TYPE_LE_SECURE_CONNECTIONS_CONFIRMATION_VALUE      = 0x22,
    ​​​GAP_DATA_TYPE_LE_SECURE_CONNECTIONS_RANDOM_VALUE            = 0x23,
    GAP_DATA_TYPE_PUBLIC_TARGET_ADDRESS                         = 0x17,
    GAP_DATA_TYPE_RANDOM_TARGET_ADDRESS                         = 0x18,
    GAP_DATA_TYPE_APPEARANCE                                    = 0x19,
    GAP_DATA_TYPE_ADVERTISING_INTERVAL                          = 0x1A,
    ​GAP_DATA_TYPE_LE_BLUETOOTH_DEVICE_ADDRESS                   = 0x1B,
    ​GAP_DATA_TYPE_LE_ROLE                                       = 0x1C,
    ​GAP_DATA_TYPE_SIMPLE_PAIRING_HASH_C256                      = 0x1D,
    ​GAP_DATA_TYPE_SIMPLE_PAIRING_RANDOMIZER_R256                = 0x1E,
    ​​GAP_DATA_TYPE_3D_INFORMATION_DATA                           = 0x3D,
    GAP_DATA_TYPE_MANUFACTURER_SPECIFIC_DATA                    = 0xFF
};

/**
 * @brief       Defines the structure of GAP Data
 */
struct __attribute__((packed)) gap_data {
    uint8_t length;                     /* Length of this Data */
    uint8_t type;                       /* Type of Data */
    uint8_t data[GAP_DATA_SIZE - 2];    /* Data */
};

#endif /* BLE_LL_GAP_DATA_H */
