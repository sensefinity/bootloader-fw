/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     ble
 * @{
 *
 * @file        random.h
 * @brief       Implements a pool of random numbers
 * @details     This implementation uses the underlying random number generator.
 *              The reason why we're using a pool is because the underlying
 *              random number generator takes a lot of time to generate a new
 *              number.
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#ifndef BLE_RANDOM_H
#define BLE_RANDOM_H

/* Standard C library */
#include <stdint.h>

/* Platform */
#include "nrf51.h"
#include "nrf51_bitfields.h"

/**
 * @brief       Defines the size of the internal random number pool
 */
#define RANDOM_POOL_SIZE (128)

/**
 * @brief       Initializes the random number generator and the internal pool
 */
void random_init(void);

/**
 * @brief       Retrieves the next random number
 * @details     The random number is obtained from the internal pool.
 * @returns     The next random number.
 */
uint8_t random_next(void);

/**
 * @brief       Generates a new random number
 * @details     The new random number is obtained from the underlying random
 *              number generator.
 * @returns     The next random number.
 */
uint8_t random_generate(void);

/**
 * @brief       Refreshes the internal pool of random numbers
 * @details     The application should call this function periodically in order
 *              to refresh the internal pool.
 */
void random_refresh(void);

#endif /* BLE_RANDOM_H */
