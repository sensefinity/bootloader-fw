/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     ble
 * @{
 *
 * @file        l2cap.h
 * @brief       Implements the Logical Link Control and Adaptation Protocol
 *              (L2CAP)
 * @details     L2CAP supports higher-level protocol multiplexing, packet
 *              segmentation and reassembly. L2CAP is based around the concept
 *              of 'channels'. Each one of the endpoints of an L2CAP channel is
 *              referred to by a channel identifier (CID).
 *              Note that we won't support the L2CAP LE Signaling Channel just
 *              yet, since we won't be needing to use upper layers such as the
 *              ATT (Attribute Protocol) or SM (Security Manager).
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#ifndef BLE_L2CAP_H
#define BLE_L2CAP_H

/**
 * @brief       Defines the type of a L2CAP channel indentifier
 * @details     The channel identifier defines the target endpoint when sending
 *              or receiving data. The Bluetooth specification also defines the
 *              identifier namespace:
 *                  0x0000              Null Identifier
 *                  0x0001-0x0003       Reserved
 *                  0x0004              Attribute Protocol
 *                  0x0005              L2CAP LE Signaling Channel
 *                  0x0006              Security Manager Protocol
 *                  0x0007-0x001F       Reserved
 *                  0x0020-0x003E       Assigned Numbers
 *                  0x003F              Reserved
 *                  0x0040-0x007F       Dynamically Allocated
 *                  0x0080-0xFFFF       Reserved
 */
typedef uint16_t l2cap_cid_t;

/*
enum l2cap_event {
    L2CAP_EVENT_CHANNEL_OPEN    = 0,
    L2CAP_EVENT_CHANNEL_CLOSE,
    L2CAP_EVENT_SDU_SENT,
    L2CAP_EVENT_SDU_RECEIVED
};

void l2cap_register(l2cap_cid_t cid, void (*handler)(enum l2cap_event));
void l2cap_unregister(l2cap_cid_t cid);
void l2cap_send(l2cap_cid_t cid, const void *sdu, size_t size);
size_t l2cap_recv(l2cap_cid_t cid, void *sdu);
*/

/**
 * @brief       Defines the structure of a L2CAP header
 */
struct __attribute__((packed)) l2cap_header {
    uint16_t length;
    l2cap_cid_t channel_id;
};

#endif /* BLE_L2CAP_H */
