/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     ble
 * @{
 *
 * @file        ll_pdu.h
 * @brief       Defines link layer protocol data unit structures
 * @details     This definition is compliant with the Bluetooth Core
 *              Specification, Version 4.1 [Vol 6] Part B.
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#ifndef BLE_LL_PDU_H
#define BLE_LL_PDU_H

/* Standard C library */
#include <stdbool.h>
#include <stdint.h>

/* BLE stack */
#include "ble/bdaddr.h"


/* -------------------------------------------------------------------------- */
/*      Advertising Channel PDUs                                              */
/* -------------------------------------------------------------------------- */

/**
 * @brief       Defines the total payload size for advertising channel PDUs
 */
#define LL_PDU_ADV_PAYLOAD_SIZE     (37)

/**
 * @brief       Enumerates the various advertising channel PDU types
 */
enum ll_pdu_adv_type {
    LL_PDU_ADV_TYPE_ADV_IND         = 0,
    LL_PDU_ADV_TYPE_ADV_DIRECT_IND,
    LL_PDU_ADV_TYPE_ADV_NONCONN_IND,
    LL_PDU_ADV_TYPE_SCAN_REQ,
    LL_PDU_ADV_TYPE_SCAN_RSP,
    LL_PDU_ADV_TYPE_CONNECT_REQ,
    LL_PDU_ADV_TYPE_ADV_SCAN_IND
};

/**
 * @brief       Defines the common structure of advertising channel PDUs
 */
struct __attribute__((packed)) ll_pdu_adv {
    /* Header */
    uint32_t pdu_type               : 4;        /* PDU Type */
    uint32_t                        : 2;        /* RFU */
    uint32_t tx_add                 : 1;        /* TxAdd */
    uint32_t rx_add                 : 1;        /* RxAdd */
    uint32_t length                 : 6;        /* Length */
    uint32_t                        : 2;        /* RFU */

    /* Payload */
    uint8_t payload[LL_PDU_ADV_PAYLOAD_SIZE];
};

/**
 * @brief       Defines the structure of the ADV_IND PDU payload
 */
struct __attribute__((packed)) ll_pdu_payload_adv_ind {
    uint8_t adv_a[BDADDR_LEN];
    uint8_t adv_data[LL_PDU_ADV_PAYLOAD_SIZE - BDADDR_LEN];
};

/**
 * @brief       Defines the structure of the ADV_DIRECT_IND PDU payload
 */
struct __attribute__((packed)) ll_pdu_payload_adv_direct_ind {
    uint8_t adv_a[BDADDR_LEN];
    uint8_t init_a[BDADDR_LEN];
};

/**
 * @brief       Defines the structure of the ADV_NONCONN_IND payload
 */
struct __attribute__((packed)) ll_pdu_payload_adv_nonconn_ind {
    uint8_t adv_a[BDADDR_LEN];
    uint8_t adv_data[LL_PDU_ADV_PAYLOAD_SIZE - BDADDR_LEN];
};

/**
 * @brief       Defines the structure of the ADV_SCAN_IND payload
 */
struct __attribute__((packed)) ll_pdu_payload_adv_scan_ind {
    uint8_t adv_a[BDADDR_LEN];
    uint8_t adv_data[LL_PDU_ADV_PAYLOAD_SIZE - BDADDR_LEN];
};

/**
 * @brief       Defines the structure of the SCAN_REQ payload
 */
struct __attribute__((packed)) ll_pdu_payload_scan_req {
    uint8_t scan_a[BDADDR_LEN];
    uint8_t adv_a[BDADDR_LEN];
};

/**
 * @brief       Defines the structure of the SCAN_RSP payload
 */
struct __attribute__((packed)) ll_pdu_payload_scan_rsp {
    uint8_t adv_a[BDADDR_LEN];
    uint8_t scan_rsp_data[LL_PDU_ADV_PAYLOAD_SIZE - BDADDR_LEN];
};

/**
 * @brief       Enumerates the various sleep clock accuracies
 */
enum ll_pdu_sca {
    LL_PDU_SCA_251_TO_500_PPM       = 0,
    LL_PDU_SCA_151_TO_250_PPM,
    LL_PDU_SCA_101_TO_150_PPM,
    LL_PDU_SCA_076_TO_100_PPM,
    LL_PDU_SCA_051_TO_075_PPM,
    LL_PDU_SCA_031_TO_050_PPM,
    LL_PDU_SCA_021_TO_030_PPM,
    LL_PDU_SCA_000_TO_020_PPM
};

/**
 * @brief       Defines the structure of the CONNECT_REQ payload
 */
struct __attribute__((packed)) ll_pdu_payload_connect_req {
    uint8_t init_a[BDADDR_LEN];
    uint8_t adv_a[BDADDR_LEN];

    /* LLData */
    uint32_t aa;
    uint32_t crc_init               : 24;
    uint32_t win_size               : 8;
    uint32_t win_offset             : 16;
    uint32_t interval               : 16;
    uint32_t latency                : 16;
    uint32_t timeout                : 16;
    uint64_t ch_m                   : 40;
    uint32_t hop                    : 5;
    uint32_t sca                    : 3;
};


/* -------------------------------------------------------------------------- */
/*      Data Channel PDUs                                                     */
/* -------------------------------------------------------------------------- */

/**
 * @brief       Defines the total payload size for data channel PDUs
 */
#define LL_PDU_DATA_PAYLOAD_SIZE    (27)

/**
 * @brief       Holds the size of the MIC (Message Integrity Check)
 * @details     The MIC field shall not be included in the following cases:
 *                  * Unencrypted link layer connections;
 *                  * Encrypted link layer connections with zero-length payload;
 */
#define LL_PDU_DATA_MIC_SIZE        (4)

/**
 * @brief       Enumerates the various data channel LLID types
 * @details     The LLID indicates whether the packet is a data PDU or a control
 *              PDU.
 */
enum ll_pdu_data_llid {
    LL_PDU_DATA_LLID_RESERVED       = 0,        /* Unused */
    LL_PDU_DATA_LLID_CONTINUATION,              /* Continuation or Empty PDU */
    LL_PDU_DATA_LLID_START,                     /* Start or Unfragmented */
    LL_PDU_DATA_LLID_CONTROL                    /* Control PDU */
};

/**
 * @brief       Defines the common structure of data channel PDUs
 */
struct __attribute__((packed)) ll_pdu_data {
    /* Header */
    uint32_t llid           : 2;        /* LLID */
    uint32_t nesn           : 1;        /* NESN (Next Expected SN) */
    uint32_t sn             : 1;        /* SN (Sequence Number) */
    uint32_t md             : 1;        /* MD (More Data) */
    uint32_t                : 3;        /* RFU */
    uint32_t length         : 5;        /* Length of Paylaod + MIC */
    uint32_t                : 3;        /* RFU */

    /* Payload */
    uint8_t payload[LL_PDU_DATA_PAYLOAD_SIZE + LL_PDU_DATA_MIC_SIZE];
};

/**
 * @brief       Enumerates the various control PDU Opcodes
 */
enum ll_pdu_control_opcode {
    LL_PDU_CONTROL_OPCODE_CONNECTION_UPDATE_REQ     = 0,
    LL_PDU_CONTROL_OPCODE_CHANNEL_MAP_REQ,
    LL_PDU_CONTROL_OPCODE_TERMINATE_IND,
    LL_PDU_CONTROL_OPCODE_ENC_REQ,
    LL_PDU_CONTROL_OPCODE_ENC_RSP,
    LL_PDU_CONTROL_OPCODE_START_ENC_REQ,
    LL_PDU_CONTROL_OPCODE_START_ENC_RSP,
    LL_PDU_CONTROL_OPCODE_UNKNOWN_RSP,
    LL_PDU_CONTROL_OPCODE_FEATURE_REQ,
    LL_PDU_CONTROL_OPCODE_FEATURE_RSP,
    LL_PDU_CONTROL_OPCODE_PAUSE_ENC_REQ,
    LL_PDU_CONTROL_OPCODE_PAUSE_ENC_RSP,
    LL_PDU_CONTROL_OPCODE_VERSION_ID,
    LL_PDU_CONTROL_OPCODE_REJECT_IND,
    LL_PDU_CONTROL_OPCODE_SLAVE_FEATURE_REQ,
    LL_PDU_CONTROL_OPCODE_CONNECTION_PARAM_REQ,
    LL_PDU_CONTROL_OPCODE_CONNECTION_PARAM_RSP,
    LL_PDU_CONTROL_OPCODE_REJECT_IND_EXT,
    LL_PDU_CONTROL_OPCODE_PING_REQ,
    LL_PDU_CONTROL_OPCODE_PING_RSP
};

/**
 * @brief       Defines the common structure for all control PDUs
 */
struct __attribute__((packed)) ll_pdu_payload_control {
    uint8_t opcode;
    uint8_t ctr_data[LL_PDU_DATA_PAYLOAD_SIZE - 1];
};

/**
 * TODO: Structures for each control PDU payload
 */

#endif /* BLE_LL_PDU_H */
