/**
 * Copyright (c) 2016 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @file        columbus.h
 * @author      Rui Pires <rui.pires@sensefinity.com>
 * @brief       Utils to create/manage Columbus messages
 */
#ifndef UTIL_COLUMBUS
#define UTIL_COLUMBUS

/* C standard library */
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#define COLUMUBUS_MEASURE_DATA_SIZE_OFFSET      2

enum _columbus_measure_type {
    COLUMBUS_MEASURE_TEMPERATURE        = 1,
    COLUMBUS_MEASURE_MOISTURE           = 2,
    COLUMBUS_MEASURE_PRESSURE           = 3,
    COLUMBUS_MEASURE_LUMINOSITY         = 4,
    COLUMBUS_MEASURE_RAIN_PROB          = 5,
    COLUMBUS_MEASURE_BATTERY_LEVEL      = 6,
    COLUMBUS_MEASURE_BATTERY_VOLTGE     = 7,
    COLUMBUS_MEASURE_NTC_RESISTANCE     = 8,
    COLUMBUS_MEASURE_BW_OUT             = 9,
    COLUMBUS_MEASURE_BW_IN              = 10,
    COLUMBUS_MEASURE_BW                 = 11,
    COLUMBUS_MEASURE_EXTERNAL_POWER     = 12,
    COLUMBUS_MEASURE_RSSI               = 13,
    COLUMBUS_MEASURE_BIT_ERROR_CODE     = 14,
    COLUMBUS_MEASURE_GRINDER            = 15,
    COLUMBUS_MEASURE_COFFEE             = 16,
    COLUMBUS_MEASURE_PROVISION          = 17,
    COLUMBUS_MEASURE_SWITCH             = 18,
    COLUMBUS_MEASURE_ADC                = 19,
    COLUMBUS_MEASURE_PRESSURE_BAR       = 20,
    COLUMBUS_MEASURE_PROXIMITY          = 21,
    COLUMBUS_MEASURE_FEELS_LIKE_TMP     = 22,
    COLUMBUS_MEASURE_HUMIDITY           = 23,
    COLUMBUS_MEASURE_SKY_CONDITION      = 24
};

enum _columbus_measure_value_width {
    MEASURE_VALUE_2_BYTE  = 0x00,
    MEASURE_VALUE_4_BYTE  = 0x40,
    MEASURE_VALUE_8_BYTE  = 0x80,
    MEASURE_VALUE_16_BYTE = 0xC0
};

enum _columbus_measure_value_format_2byte {
    MEASURE_FORMAT_2_BYTE_12_0 = 0x00,
    MEASURE_FORMAT_2_BYTE_4_8  = 0x10,
    MEASURE_FORMAT_2_BYTE_0_12 = 0x20
};

enum _columbus_measure_value_format_4byte {
    MEASURE_FORMAT_4_BYTE_27_0  = 0x00,
    MEASURE_FORMAT_4_BYTE_24_3  = 0x08,
    MEASURE_FORMAT_4_BYTE_11_16 = 0x10,
    MEASURE_FORMAT_4_BYTE_3_24  = 0x18,
    MEASURE_FORMAT_4_BYTE_0_27  = 0x20
};

enum _columbus_measure_value_format_8byte {
    MEASURE_FORMAT_8_BYTE_59_0   = 0x00,
    MEASURE_FORMAT_8_BYTE_51_8   = 0x08,
    MEASURE_FORMAT_8_BYTE_43_16  = 0x10,
    MEASURE_FORMAT_8_BYTE_35_24  = 0x18,
    MEASURE_FORMAT_8_BYTE_27_32  = 0x20,
    MEASURE_FORMAT_8_BYTE_19_40  = 0x28,
    MEASURE_FORMAT_8_BYTE_11_48  = 0x30,
    MEASURE_FORMAT_8_BYTE_2_57   = 0x38
};


 /** Columbus message HEADER
 * 0x01,                                               Version
 * 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,     Device Serial
 * 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,     Timestamp
 * 0x06,                                               Message Type (Measurement)
 * 0x00, 0x00, 0x00, 0x06,                             Data Size
 * ...
 **/

/* Columbus message maximum size */
#define COLUMBUS_MESSAGE_MAX_SIZE                          (100)

/* Columbus message fields */
#define COLUMBUS_MESSAGE_VERSION                            (0)
#define COLUMBUS_MESSAGE_DEVICE_SERIAL                      (1)
#define COLUMBUS_MESSAGE_TIMESTAMP                          (9)
#define COLUMBUS_MESSAGE_TYPE                               (17)
#define COLUMBUS_MESSAGE_DATA_SIZE                          (18)

#define COLUMBUS_SUB_HEADER_SIZE                          	(22)



#define COLUMBUS_MESSAGE_DIRECTION							(18)
#define COLUMBUS_UPLINK_MESSAGE							    (0)
#define COLUMBUS_DOWNLINK_DIRECTION							(1)

/* Size definitions */
#define COLUMBUS_HEADER_SIZE                                (22)
#define COLUMBUS_HELLO_DATA_SIZE                            (11)


/* Columbus messages types */
#define COLUMBUS_HELLO_TYPE									(1)
#define COLUMBUS_MEASUREMENT_TYPE							(6)
#define COLUMBUS_PING_TYPE									(2)

#define COLUMBUS_CONFIGURATION_TYPE							(40)

#define COLUMBUS_MEASUREMENT_MESSAGE_SIZE                   (28)

/**
 * @brief       Sets the version of a Columbus message.
 * @param       message
 *              Pointer to the Columbus message.
 * @param       version
 */
void columbus_set_message_version(uint8_t *message, uint8_t version);

/**
 * @brief       Get the version of a Columbus message.
 * @param       message
 *              Pointer to the Columbus message.
 * @return      The version number.
 */
uint8_t columbus_get_message_version(uint8_t *message);

/**
 * @brief       Sets the serial number on a Columbus message.
 * @param       message
 *              Pointer to the Columbus message.
 * @param       serial
 *              The serial number to set.
 */
void columbus_set_serial(uint8_t *message, uint64_t serial);

/**
 * @brief       Retrieves the serial number of a Columbus message.
 * @param       message
 *              Pointer to the message.
 * @return      The serial number.
 */
uint64_t columbus_get_serial(const uint8_t *message);

/**
 * @brief       Sets the timestamp on a Columbus message.
 * @param       message
 *              Pointer to the Columbus message.
 * @param       timestamp
 *              The timestamp to set.
 */
void columbus_set_timestamp(uint8_t *message, uint64_t timestamp);

/**
 * @brief       Obtains the timestamp of a Columbus message.
 * @param       message
 *              Pointer to the Columbus message.
 * @return      The timestamp of the Columbus message.
 */
 uint64_t columbus_get_timestamp(const uint8_t *message);

 /**
  * @brief       Sets the type of a Columbus message.
  * @param       message
  *              Pointer to the Columbus message.
  * @param       type
  */
 void columbus_set_message_type(uint8_t *message, uint8_t type);

 uint8_t columbus_get_message_type(uint8_t *message);

/**
 *
 *
 * @brief       Sets the size of the data portion on a Columbus message.
 * @param       message
 *              Pointer to the Columbus message.
 * @param       size
 *              The size to be set.
 */
 void columbus_set_data_size(uint8_t *message, uint32_t size);

 /* Message direction */
 void columbus_set_message_direction (uint8_t *message, uint8_t dir);

 uint8_t columbus_get_message_direction (uint8_t *message);
 /*****************************************************************************/
 /* Measurement messages specific functions and definitions */
  /*****************************************************************************/

 /** Measurement message Prototype
 * 0x00,                                               Version
 * 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,     Device Serial
 * 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,     Timestamp
 * 0x06,                                               Message Type (Measurement)
 * 0x00, 0x00, 0x00, 0x06,                             Data Size
 * 0x00,                                               Measurement Sensor
 * 0x00,                                               Measurement Type (
 * 0x00, 0x00, 0x00, 0x00                              Measurement Value (float)
 **/

 /* Measurement message specific  fields */
 #define COLUMBUS_MESSAGE_MEASUREMENT_SENSOR                 (22)
 #define COLUMBUS_MESSAGE_MEASUREMENT_TYPE                   (23)
 #define COLUMBUS_MESSAGE_MEASUREMENT_VALUE                  (24)


#define COLUMUBUS_MEASURE_PAYLOAD_DATA_SIZE					(6)

 /**
  * @brief       Sets the measurement sensor of the Columbus message.
  * @param       uint8_t value
  *              The value to be set.
  */
 void columbus_set_measurement_sensor(uint8_t *message, uint8_t value);

 /**
  * @brief       Sets the measurement type of the Columbus message.
  * @param       uint8_t value
  *              The value to be set.
  */
 void columbus_set_measurement_type(uint8_t *message, uint8_t value);

 /**
  * @brief       Sets the measurement value on the Columbus message.
  * @param       uint32_t value
  *              The value to be set.
  */
 void columbus_set_measurement_value(uint8_t *message, uint32_t value);


 /**
  * @brief       Sets the measurement value with the specified width on the Columbus message.
  * @param       uint32_t value
  *              The value to be set.
  * @param       uint32_t value
  *              The value to be set.
  */
 void columbus_set_measurement_value_width(uint8_t *message, void * value, uint8_t width);

 /**
  * @brief       Takes an integer and fractional part and transforms into columbus value format.
  * @param       void * res
  *              A pointer to the result variable, written with the specified width.
  * @param       void * integer_part
  *              The integer part of the value to be formatted.
  * @param       void * fractional_part
  *              The fractional part of the value to be formatted.
  * @param       size_t width
  *              The width of the integer and fractional values.
  */
 void columbus_format_measurement_value(void * res, void * integer_part, void * fractional_part, uint8_t width, uint8_t format);



 /*****************************************************************************/
 /* Ping messages specific functions and definitions */
 /*****************************************************************************/

 /*
  * static uint8_t _columbus_ping[] = {
    0x01,                                               Message Version
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,     Serial
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,     Timestamp
    0x02,                                               Message Type (Ping-2)
    0x00, 0x00, 0x00, 0x01,                             Data Size
    0x00                                                Signal/
};*/

 #define COLUMBUS_MESSAGE_PING_SIGNAL		               	(22)
 #define COLUMBUS_MESSAGE_PING_SIZE		               		(23)
 #define COLUMBUS_MESSAGE_PING__DATA_SIZE		            (1)


 void columbus_ping_set_signal(uint8_t *message,int8_t signal);


#endif
