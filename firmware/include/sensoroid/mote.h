/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     sensoroid
 * @{
 *
 * @file        mote.h
 * @brief       Implements the Mote profile for Sensoroid devices.
 * @details     The Mote profile describes a multi-purpose device for which
 *              multiple services may be active at a time. Such services include
 *              mesh support, positioning and proximity.
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#ifndef SENSOROID_MOTE_H
#define SENSOROID_MOTE_H

/* C standard library */
#include <stdint.h>

/* Peripherals */
#include "periph/pin_name.h"
#include "sensoroid/config.h"

/* Sensoroid library */
#include "sensoroid/sensoroid.h"

/* Defines flags for the various mote services */
#define MOTE_SERVICE_NONE             (0x00)
#define MOTE_SERVICE_MESH             (0x01)
#define MOTE_SERVICE_POSITION         (0x02)
#define MOTE_SERVICE_PROXIMITY        (0x04)
#define MOTE_SERVICE_BATTERY          (0x08)
#define MOTE_SERVICE_PING             (0x10)
#define MOTE_SERVICE_NETWORK_STATUS   (0x20)

/* Useful definitions for the battery service */
#define VBAT_POWER              (P0_05)
#define VBAT_IN                 (AIN5)
#define VBAT_SCALE              (2.0F)
#define VBAT_SETTLING_US        (1000)

/**
 * @brief       Initializes this device as a Mote.
 * @param       flags
 *              The flags which hold the enabled services.
 */
void sensoroid_mote_init(uint32_t flags, struct properties *_properties);

#endif /* SENSOROID_MOTE_H */
