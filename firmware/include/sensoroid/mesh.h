/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     sensoroid
 * @{
 *
 * @file        mesh.h
 * @brief       Implements a simple mesh routing protocol
 *
 * @author      Rui Pires
 *
 * @}
 */
#ifndef SENSOROID_MESH_H
#define SENSOROID_MESH_H

/* C standard library */
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

/* BLE stack */
#include "ble/bdaddr.h"

/**
 * @brief       Max Capacity for the mesh routing table
 */
#define MESH_TABLE_CAPACITY     (5)

/**
 * @brief       Value periodicaly decreased to the entries life_time in the
 *              mesh table
 */
#define TIMER_DECREASE_COUNT    (1)

/* TODO define better weights and best threshold */
#define MESH_HOP_COUNT_WEIGHT   (1)     /*[1,4]*/
#define MESH_BATTERY_WEIGHT     (0)     /*[0,4]*/
#define MESH_RSSI_THRESHOLD     (-85)   /*[0,1]*/

#define MAX_METRIC              (65535)

#define NODE_FAILURE_MAX        (15)

#define GATEWAY_RESET_THRESHOLD (25)

#define SEQUENCE_NUMBER_THRESHOLD_ACCEPTANCE (6)
/**
 * @brief       Defines the structure of a mesh entry in the routing table
 */

 /**
  * @brief       Defines the structure of a mesh entry in the routing table
  */
struct mesh_entry{
    uint64_t        gateway_serial;         /* Address of gateway */
    uint64_t        node_serial;            /* Address of peer node */
    uint16_t        sequence_number;        /* sequence number of last update*/

    int             rssi;                   /* RSSI  of the link*/
    uint16_t        metric;                 /* In basic mode is the number of hops to gateway */
    int             life_time;              /* Entry life time */
    struct bdaddr   addr;
};


void mesh_init(uint32_t flags,uint64_t mesh_init_lt);

/**
 * @brief       Update the mesh routing table when a new routing message is received
 * @param[in]   mesh_entry
 *              A pointer to the entry received
 */
void mesh_new_routing_message(struct mesh_entry *mesh_entry);

/**
 * @brief       Update the mesh routing table when receive a new advertisement
 * @param[in]   node_serial pointer to the node
 *              rssi felt
 */
void mesh_announce(uint64_t node_serial, int rssi);

/**
 * @brief       Decrease mesh entries life time, and remove old entries.
 *
 */
void mesh_decrease_life_time();

/**
 * @brief       Select the best next hop to send messages, and its my routing status to be difunded.
 * @param[out]  Pointer to the best mesh entrie
 */
struct mesh_entry *mesh_next_hop_select();

/**
 * @brief       Signals a node failure, will remove their table entry
 */
void mesh_node_failure(void);

/**
 * @brief       Verify if there are entries in the routing table
 */
bool mesh_has_conectivity();

/**
 * @brief       Reset routing
 */
void mesh_reset();


/**
 * @brief       Get a specific table entry of the mesh table
 */
struct mesh_entry *mesh_get_entry(int index);

/**
 * @brief       Get the number of valid entries in the mesh_table
 */
int mesh_table_count();


int8_t mesh_next_hop_get_signal(void);

/*
  DEBUG
*/


void mesh_table_print();


#endif /* SENSOROID_MESH_H */
