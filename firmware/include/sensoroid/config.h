/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     sensoroid
 * @{
 *
 * @file        config.h
 * @brief       Handles Sensoroid configuration.
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

 /* C standard library */
 #include <ctype.h>
 #include <stdbool.h>
 #include <stddef.h>
 #include <stdint.h>
 #include <stdlib.h>
 #include <stdio.h>
 #include <string.h>

 /* Sensoroid library */
 #include "sensoroid/sensoroid.h"


#ifndef SENSOROID_CONFIG_H
#define SENSOROID_CONFIG_H



//#define GATEWAY_TEST        1
#define COLD_TEST           1
#define DEFAULT_SERIAL		 1

/**
 * @brief       Defines the common structure for services.
 */
struct service {
    bool enabled;
    uint64_t interval;
};

/**
 * @brief       Enumerates the various interfaces (gateway only).
 */
enum interface {
    INTERFACE_UNDEFINED         = -1,
    INTERFACE_BUTTERFINGER      =  0,
    INTERFACE_ETHERNET
};


/**
 * @brief       Defines the device configuration properties.
 */
struct properties {
    enum sensoroid_type type;
    //uint64_t serial;
    enum interface interface;
    struct service mesh;
    struct service battery;
    struct service temperature;
    struct service ping;
    struct service position;
    struct service irds;
    struct service network;
    struct service wine;
    struct service grinder;
    struct service barometer;
    struct service accelerometer;
    struct service orientation;
    struct service motion;
    struct service transient;
    struct service humidity;

    /* Gateway-only! */
    struct service positioning;
};


int readline(const char *input, char *buffer);

/**
 * @brief       Configures this Sensoroid device using the configuration
 *              present in the configuration block.
 */
void sensoroid_configure(void);

/**
 * @brief       Looping function for the Sensoroid configuration code.
 * @details     Since this code handles the enabling of apps, this loop is
 *              required to be called for each time the processor wakes up.
 */
void sensoroid_configure_loop(uint64_t t_now);

/**
 * @brief       Global getter for the Sensoroid serial number.
 * @return      The serial number of this Sensoroid device.
 */
uint64_t sensoroid_get_serial(void);

/**
 * @brief       Retrieves a pointer to the current Sensoroid configuration.
 * @return      A pointer the the string representing the current Sensoroid
 *              configuration or NULL if no configuration found.
 */
const char *sensoroid_get_configuration(void);

/**
 * @brief       Patches the internal Sensoroid configuration.
 * @details     Patching the internal configuration may result in either lines
 *              being modified or appended. Patching is only applied after a
 *              system reset, which must be done externally.
 * @param       data
 *              The data to patch the configuration with. The data may be a
 *              single line or a collection of lines.
 */
bool sensoroid_patch_configuration(const char *data);

#endif /* SENSOROID_CONFIG_H */
