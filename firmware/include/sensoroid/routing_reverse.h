/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     sensoroid
 * @{
 *
 * @file        routing.h
 * @brief       Implements a the reverse routing protocol
 *
 * @author      Rui Pires
 *
 * @}
 */
#ifndef SENSOROID_ROUTING_H
#define SENSOROID_ROUTING_H

/* C standard library */
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

/* BLE stack */
#include "ble/bdaddr.h"

#include "mesh.h"
#include "reverse_msg_table.h"


/**
 * @brief       Max Capacity for the reverse routing table
 */
#define ROUTING_REVERSE_TABLE_CAPACITY           (5)

/**
 * @brief       Value of life_time for new entries in the table
 */
#define ROUTING_REVERSE_LIFE_TIME_INIT           (5)

/**
 * @brief       Defines the structure of a routing entry in the reverse routing table
 */
struct routing_reverse_entry{
    uint64_t        origin_node_serial;            /* Origin node Address */
    struct bdaddr   intermediate_node_address;     /* Intermediate node ble address */
    int             life_time;                     /* Entry life time */
};


/**
 * @brief       Compare two ble addresses
 * @param[in]   struct bdaddr *address1, struct bdaddr *address2
 * @param[out]  boolean false if addresses are equal.
 */
bool _memcmp_ble_addr(struct bdaddr *address1, struct bdaddr *address2);

/**
 * @brief       Update the reverse routing table when a new routing message is received.
 * @param[in]   routing_reverse_entry
 *              A pointer to the entry received
 * @param[out]  boolean false cannot save message
 */
bool routing_reverse_new_message(struct routing_reverse_entry *routing_reverse_entry);

/**
 * @brief       Decrease routing entries life time, and remove old entries.
 */
void routing_reverse_decrease_life_time();

/**
 * @brief       Select the next Intermediate hop to send messages for
 *              a specific origin node.
 * @param[in]   uint64_t origin_node_serial
 * @param[out]  Pointer respective entrie
 */
struct bdaddr *routing_reverse_next_hop_select(uint64_t origin_node_serial);

/**
 * @brief       Signals a node failure.
 * @param[in]   uint64_t origin_node_serial
 */
void routing_reverse_node_failure(struct bdaddr node_address);

/**
 * @brief       Search for reverse messages to be forward by a intermediate node
 * @param[in]   struct bdaddr *node_address, intermediate node ble address
 * @param[out]  int, index of the older message to be forward by the specific intermediate node
 */
int routing_reverse_search_msg(struct bdaddr *node_address);

/**
 * @brief       Reset routing reverse table
 */
void routing_reverse_reset();

/**
 * @brief       Update reverse message remaining time to live (TTL)
 * @param[in]   uint8_t *msg, Columbus message to be updated
 */
void routing_reverse_update_out_timestamp(uint8_t *msg);

/*
  DEBUG
*/
void routing_reverse_table_print();


#endif /* SENSOROID_ROUTING_H */
