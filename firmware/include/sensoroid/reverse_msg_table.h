/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     sensoroid
 * @{
 *
 * @file        reverse_msg_table.h
 * @brief       Implements a table to save messages for the reverse communication
 *
 * @author      Rui Pires
 *
 * @}
 */

#ifndef REVERSE_MSG_TABLE_H
#define REVERSE_MSG_TABLE_H

/* C standard library */
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

/**
 * @brief       Holds the maximum number of elements in the table.
 */
#define RMT_ELEMENTS      (5)

/**
 * @brief       Defines the maximum size for each element.
 */
#define RMT_MAX_ELEMENT_SIZE  (100)

/**
 * @brief       Defines the structure for a each element.
 */
struct msg_element {
    uint8_t     msg[RMT_MAX_ELEMENT_SIZE];
    uint64_t    timestamp;
    size_t      size;
};

/**
 * @brief       Compare the destination node serial in a message with other serial.
 * @param[in]   uint8_t *msg, uint64_t serial2
 * @param[out]  Returns true if serials are equals.
 */
bool _cmp_serial(uint8_t *msg, uint64_t serial2);

/**
 * @brief       Insert new message in the table
 * @param[in]   uint8_t *msg, uint16_t size.
 * @param[out]  true if message was saved
 */
bool rmt_insert(uint8_t *msg, uint16_t size);

/**
 * @brief       Get a specific message by the index.
 * @param[in]   int index
 * @param[out]  pointer to the message
 */
struct msg_element *rmt_get_message(int index);

/**
 * @brief       Search for, the older message in the table, to a specific serial node.
 * @param[in]   int index.
 * @param[out]  pointer to the message
 */
uint8_t *rmt_search_msg(uint64_t origin_node_serial);

/**
 * @brief       Clear a specific table entry
 * @param[in]   int index.
 */
void rmt_clear_index(int index);


/**
 * @brief       Check messages Time To Live
 */
void rmt_check_ttl();

/**
 * @debug
 */

void rmt_print();

#endif /* REVERSE_MSG_TABLE_H */
