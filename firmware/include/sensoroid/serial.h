/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     sensoroid
 * @{
 *
 * @file        serial.h
 * @brief       Handles Sensoroid serial page.
 *
 * @author      Rui Pires
 *
 * @}
 */

 /* C standard library */
 #include <ctype.h>
 #include <stdbool.h>
 #include <stddef.h>
 #include <stdint.h>
 #include <stdlib.h>
 #include <stdio.h>
 #include <string.h>

#ifndef SENSOROID_SERIAL_H_
#define SENSOROID_SERIAL_H_



/**
 * @brief       Read this Sensoroid device serial number that must be present
 *             in the serial block, and update the device serial.
 */
void sensoroid_read_serial(void);

/* @brief 		Return the device serial present in the respective block, 0 if some error occurred */
uint64_t sensoroid_get_serial(void);

#endif /* SENSOROID_SERIAL_H_ */
