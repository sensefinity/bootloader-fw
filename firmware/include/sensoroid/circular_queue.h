/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     sensoroid
 * @{
 *
 * @file        circular_queue.h
 * @brief       Circular buffer code, to save any type of data.
 * 				One byte of the total size is sacrificed to distinguish between an
 * 				empty and  an full queue.
 *
 *  			Start pointer, pointer to Write;
 *  			End pointer, pointer to Read;
 *  						[ | | | | | | | | ]
 *  			- S = E queue is Empty
 *  			- S+1 = E queue is full
 *
 * @author      Rui Pires
 *
 * @}
 */

#ifndef SENSOROID_CIRCULAR_QUEUE_H
#define SENSOROID_CIRCULAR_QUEUE_H

/* Standard C library */
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "sensoroid/c_queue.h"

/*
	INIT
*/

void circular_queue_init(circular_queue_t * me, size_t size, uint8_t * circular_queue_pointer);

/*
	AUX
*/
void circular_queue_reset_pointers(circular_queue_t * me);
/*
	OPS
*/

bool circular_queue_reserve(circular_queue_t * me, size_t size);

bool circular_queue_copy_and_commit_data(circular_queue_t * me , uint8_t * data, size_t size);

bool circular_queue_read_and_delete(circular_queue_t * me , uint8_t * copy_buffer, size_t size);

bool circular_queue_read(circular_queue_t * me , uint8_t * copy_buffer, size_t size);

bool circular_queue_delete(circular_queue_t * me , size_t size);

void circular_queue_discard(circular_queue_t * me);

void* circular_queue_peek(circular_queue_t * me);

/*
    STATS
*/

size_t circular_queue_capacity(circular_queue_t * me);

size_t circular_queue_free_space(circular_queue_t * me);

size_t circular_queue_space_used(circular_queue_t * me);

bool circular_queue_is_full(circular_queue_t * me);

bool circular_queue_is_empty(circular_queue_t * me);


/*
	DEBUG
*/

/*
void circular_queue_print_stats(circular_queue_t * me);
void circular_queue_print(circular_queue_t * me);
*/
#endif  /* SENSOROID_CIRCULAR_QUEUE_H */

