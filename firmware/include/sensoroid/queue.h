/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     sensoroid
 * @{
 *
 * @file        queue.h
 * @brief       Implements a queue for shared Sensoroid communication.
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#ifndef SENSOROID_QUEUE_H
#define SENSOROID_QUEUE_H

/* C standard library */
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

/* Utilities */
#include "util/atomic.h"

/* Columbus */

#include "sensoroid/columbus.h"

/**
 * @brief       Holds the maximum number of elements in the queue.
 */
#define QUEUE_MAX_ELEMENTS      (20)


/**
 * @brief       Defines the structure for a queue element.
 */
struct queue_element {
    size_t      size;
    uint8_t     data[COLUMBUS_MESSAGE_MAX_SIZE];
};

struct queue_element *queue_reserve(void);
bool queue_reserved(void);
void queue_commit(void);
void queue_release(void);
struct queue_element *queue_peek(void);
struct queue_element *queue_peek_ahead(size_t offset);
void queue_delete(void);
bool queue_empty(void);
bool queue_full(void);
size_t queue_length(void);

#endif /* SENSOROID_QUEUE_H */
