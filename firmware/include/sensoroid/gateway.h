/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     sensoroid
 * @{
 *
 * @file        gateway.h
 * @brief       Implements the Gateway profile for Sensoroid devices.
 *
 * @author      Tiago Tom�s <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#ifndef SENSOROID_GATEWAY_H
#define SENSOROID_GATEWAY_H

#include "sensoroid/config.h"

/**
 * @brief       Initializes this device as a Gateway.
 */
void sensoroid_gateway_init();

/**
 * @brief       Resumes gateway operation if it was interrupted by an external
 *              application.
 */
void sensoroid_gateway_resume(void);

 /**
  * @brief       Update the connectivity state of the gateway interface.
  * @details     Call this function, when the connectivity state has changed.
  * @param[in]   state
  *              A boolean indicating the new interface connectivity state,
  *              true if have connectivity, false otherwise.
  */
void sensoroid_gateway_update_connectivity(bool state);


/**
  * @brief       Try dispatch columbus messages to the cloud, manage offered up-links
  */
void sensoroid_gateway_loop(void);


void gateway_uplink_ack_received(uint8_t seq_number);

#endif /* SENSOROID_GATEWAY_H */
