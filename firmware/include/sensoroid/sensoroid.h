/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     sensoroid
 * @{
 *
 * @file        sensoroid.h
 * @brief       Includes all Sensoroid header files.
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#ifndef SENSOROID_H
#define SENSOROID_H

/* Sensoroid library */
#include "sensoroid/common.h"
#include "sensoroid/config.h"
#include "sensoroid/serial.h"
#include "sensoroid/mesh.h"
#include "sensoroid/mote.h"
#include "sensoroid/queue.h"
#include "sensoroid/reverse_msg_table.h"
#include "sensoroid/routing_reverse.h"

#endif /* SENSOROID_H */
