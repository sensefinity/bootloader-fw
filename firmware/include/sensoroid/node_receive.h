/*
 * node_receive.h
 *
 *  Created on: 12/09/2016
 *      Author: Ruy__
 */

#ifndef NODE_RECEIVE_H_
#define NODE_RECEIVE_H_

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

bool node_receive_is_msg_to_me(uint8_t * gama_msg);

void node_receive_new_message(uint8_t * msg_pointer);

#endif /* NODE_RECEIVE_H_ */
