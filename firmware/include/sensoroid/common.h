/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     sensoroid
 * @{
 *
 * @file        common.h
 * @brief       Holds common definitions for the Sensoroid device.
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#ifndef SENSOROID_COMMON_H
#define SENSOROID_COMMON_H

/* C standard library */
#include <stdint.h>

/**
 * @brief       Holds the BLE L2CAP assigned number for Sensoroids.
 */
#define SENSOROID_L2CAP_ASSIGNED_NUMBER     (0x003E)

/**
 * @brief       Enumerates the various Sensoroid types.
 */
enum sensoroid_type {
    SENSOROID_TYPE_UNDEFINED    = -1,
    SENSOROID_TYPE_GATEWAY      =  0,
    SENSOROID_TYPE_BEACON,
    SENSOROID_TYPE_MOTE
};

/**
 * @brief       Defines the structure for the Sensoroid data.
 * @details     This data is shared between nodes.
 */
struct __attribute__((packed)) sensoroid_data {
    uint8_t     type;               /* Type defined in the sensoroid_type enum */
    uint64_t    serial_node;        /* Serial number of this device */
    uint64_t    serial_gateway;     /* Serial number of the gateway device */
    uint16_t    seqno;              /* The current sequence number */
    uint16_t    metric;             /* A metric for routing */
};

#endif /* SENSOROID_COMMON_H */
