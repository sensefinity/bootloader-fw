/*
 * fw_reassemble_app.h
 *
 *  Created on: 09/09/2016
 *      Author: Ruy__
 */

#ifndef SENSOROID_FW_REASSEMBLE_APP_H_
#define SENSOROID_FW_REASSEMBLE_APP_H_

/* C standard library */
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


void fw_reassemble_app_new_message(uint8_t *message);

#endif /* SENSOROID_FW_REASSEMBLE_APP_H_ */
