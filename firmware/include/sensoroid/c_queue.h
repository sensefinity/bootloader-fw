/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     sensoroid
 * @{
 *
 * @file        c_queue.h
 * @brief		Use of the circular queue to save messages.
 * 				First byte of the data saved indicates the next message size of the data
 * 				this way we can save any type of messages(columbus or expansion protocol)
 *
 *
 * @author      Rui Pires
 *
 * @}
 */

#ifndef SENSOROID_C_QUEUE_H
#define SENSOROID_C_QUEUE_H

/* Standard C library */
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


typedef struct {
    size_t size;
    bool reserved;
    size_t p_start, p_end;
    uint8_t *circular_queue_pointer;
} circular_queue_t;



/*
	INIT
*/

void c_queue_init(circular_queue_t * me, size_t size, uint8_t * circular_queue_pointer);

/*
	OPS
*/

bool c_queue_reserve(circular_queue_t * me, size_t size);

bool c_queue_copy_and_commit_data(circular_queue_t * me , uint8_t * data, size_t size);

bool c_queue_read(circular_queue_t * me , uint8_t * copy_buffer, size_t size);

bool c_queue_delete(circular_queue_t * me , size_t size);

void c_queue_discard(circular_queue_t * me);

uint8_t c_queue_peek(circular_queue_t * me);

uint8_t* c_queue_message_peek(circular_queue_t * me, size_t msg_size);

/*
    STATS
*/

size_t c_queue_capacity(circular_queue_t * me);

size_t c_queue_free_space(circular_queue_t * me);

size_t c_queue_space_used(circular_queue_t * me);

bool c_queue_is_full(circular_queue_t * me);

bool c_queue_is_empty(circular_queue_t * me);


/*
	DEBUG
*/

/*
void c_queue_print_stats(circular_queue_t * me);
void c_queue_print(circular_queue_t * me);
*/
#endif  /* SENSOROID_CIRCULAR_QUEUE_H */
