/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     app
 * @{
 *
 * @file        clkout.h
 * @brief       Implements an application that sends the HFCLK directly to
 *              a GPIO pin.
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#ifndef APP_CLKOUT_H
#define APP_CLKOUT_H

/* Peripherals */
#include "periph/pin_name.h"

/* Definitions for the CLKOUT application */
#define CLKOUT_PIN              (P0_00)
#define CLKOUT_PRESCALER        (0)
#define CLKOUT_COMPARE          (1)
#define CLKOUT_GPIOTE_CHANNEL   (0)
#define CLKOUT_PPI_CHANNEL      (10)

/**
 * @brief       Initializes the CLKOUT application.
 */
void clkout_setup(void);

/**
 * @brief       Looping function designed to be called for each time that the
 *              processor wakes up from sleep mode.
 */
void clkout_loop(void);


#endif /* APP_CLKOUT_H */
