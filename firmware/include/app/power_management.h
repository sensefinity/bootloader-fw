/**
 * Copyright (c) 2016 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     app
 * @{
 *
 * @file        power_management.h
 * @       		Implements the power management app
 *
 * @author      Jo�o Ambr�sio <joao.ambrosio@sensefinity.com>
 *
 * @}
 */

#ifndef APP_POWER_MANAGEMENT_H
#define APP_POWER_MANAGEMENT_H

/* Standard C library */
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

/* Drivers */
#include "expansion/expansion_apps.h"
#include "dandelion.h"

#include "app/telco_power_management.h"

#include "sensoroid/c_queue.h"
#include "sensoroid/circular_queue.h"
#include "expansion/expansion.h"
/********************************** Definitions ***********************************/

typedef enum _power_states {
	PREPARE_POWER_OFF = 		(0),
	POWER_OFF = 				(1),
	POWERED_OFF = 				(2),
	POWER_ON = 					(3),
	POWERED_ON  = 				(4),
} Power_States;

/********************************** Prototypes ***********************************/

/**
 * @brief       Initializes the power management app
 */
void power_management_setup(void);

/**
 * @brief       Updates the power management app
 */
void power_management_loop(void);

void power_management_set_state(Power_States power_state);

Power_States power_management_get_state(void);

#endif /* APP_POWER_MANAGEMENT_H */
