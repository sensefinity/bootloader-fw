/**
 * Copyright (c) 2016 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     app
 * @{
 *
 * @file        user.h
 * @brief       Implements the user interaction with Dandelion
 *
 * @author      Jo�o Ambr�sio <joao.ambrosio@sensefinity.com>
 *
 * @}
 */

#ifndef APP_USER_H
#define APP_USER_H

/* C standard library */
#include <stdbool.h>
#include <assert.h>
#include <stddef.h>

/* Peripherals */
#include "periph/gpio.h"

/* Utilities */
#include "util/delay.h"
#include "util/debug.h"

/* API */
#include "api/sched.h"

/* Applications */
#include "app/led.h"
#include "app/button.h"
#include "app/power_management.h"

/* Definitions */
#define DET_PIN						(P0_24)

#define POWER_PRESSING_TIME			(1000)
#define LED_SLEEP_TIMEOUT			(30000)

typedef enum _user_states {
	USER_POWERED_OFF  = 			(0),
	USER_POWERING_ON  = 			(1),
	USER_POWERED_ON   = 			(2),
	USER_POWERING_OFF = 			(3)
} User_States;

/* Prototypes */
void user_setup(void);
void user_loop(void);

bool is_led_but_connected(void);

void user_set_state(User_States state);

#endif /* APP_USER_H */
