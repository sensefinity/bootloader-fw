/**
 * Copyright (c) 2016 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     app
 * @{
 *
 * @file        orientation.h
 * @brief       Implements the orientation app
 *
 * @author      João Ambrósio <joao.ambrosio@sensefinity.com>
 *
 * @}
 */

#ifndef APP_ORIENTATION_H
#define APP_ORIENTATION_H

/* Standard C library */
#include <stdint.h>

/* Peripherals */
#include "app/accelerometer.h"
#include "sensors/MMA8652.h"

/* Utilities */
#include "util/delay.h"
#include "util/debug.h"

/* API */
#include "api/sched.h"

/* Prototypes */
void orientation_setup(uint64_t interval);
void orientation_loop(void);

#endif /* APP_ORIENTATION_H */
