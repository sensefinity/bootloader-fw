/**
 * Copyright (c) 2016 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     app
 * @{
 *
 * @file        motion.h
 * @brief       Implements the motion app
 *
 * @author      João Ambrósio <joao.ambrosio@sensefinity.com>
 *
 * @}
 */

#ifndef APP_MOTION_H
#define APP_MOTION_H

/* Standard C library */
#include <stdint.h>

/* Peripherals */
#include "app/accelerometer.h"
#include "sensors/MMA8652.h"

/* Utilities */
#include "util/delay.h"
#include "util/debug.h"

/* API */
#include "api/sched.h"

/* Prototypes */
void motion_setup(bool freefall_enabled, bool x_enabled, bool y_enabled, bool z_enabled, uint8_t motion_threshold, uint8_t count_threshold);
void motion_loop(void);

#endif /* APP_MOTION_H */
