/**
 * Copyright (c) 2016 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     app
 * @{
 *
 * @file        accelerometer.h
 * @brief       Implements the accelerometer app
 *
 * @author      João Ambrósio <joao.ambrosio@sensefinity.com>
 *
 * @}
 */

#ifndef APP_ACCELEROMETER_H
#define APP_ACCELEROMETER_H

/* Standard C library */
#include <stdint.h>

/* Peripherals */
#include "sensors/MMA8652.h"

/* Sensoroid library */
#include "sensoroid/sensoroid.h"

/* Utilities */
#include "util/delay.h"
#include "util/debug.h"

/* API */
#include "api/sched.h"

/* Definitions */
#define MMA8652_INT_1_PIN				       (P0_04)
#define MMA8652_INT_2_PIN				       (P0_03)

/* Prototypes */
void accelerometer_setup(void);
void accelerometer_loop(void);

/**
 * @brief       Configure the accelerometer interrupt
 * @param       pin_name
 *              The pin name that is connected to one of the interrupt pins on the sensor. Possible values are:
 *              - MMA8652_INT_1_PIN;
 *              - MMA8652_INT_2_PIN.
 *              handler
 *              The task that will run each time the interrupt is triggered.
 */
void accelerometer_configure_interrupt(uint8_t pin_name, void (*handler)(void));

#endif /* APP_ACCELEROMETER_H */
