/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     app
 * @{
 *
 * @file        humidity.h
 * @brief       Implements the base humidity App
 *
 * @author      Rui Pires rui.pires@sensefinity.com>
 *
 * @}
 */

#ifndef HUMIDITY_APP_H
#define HUMIDITY_APP_H

#include <stdint.h>


void humidity_loop(uint64_t t_now, uint64_t interval);

#endif /* HUMIDITY_APP_H */
