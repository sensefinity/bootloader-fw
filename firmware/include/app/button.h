/**
 * Copyright (c) 2016 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     app
 * @{
 *
 * @file        led.h
 * @brief       Implements the led utilities
 *
 * @author      Jo�o Ambr�sio <joao.ambrosio@sensefinity.com>
 *
 * @}
 */

#ifndef APP_BUTTON_H
#define APP_BUTTON_H

/* C standard library */
#include <stdbool.h>
#include <assert.h>
#include <stddef.h>

/* Utilities */
#include "util/delay.h"
#include "util/debug.h"

/* API */
#include "api/sched.h"

/* Definitions */
#define BUTTON_PIN						(P0_29)

/* Prototypes */

/**
 * @brief     Initializes the button pin
 */
void button_init(void);

void button_attach_press_function(void (*handler)(void));

void button_attach_unpress_function(void (*handler)(void));

bool is_button_pressed(void);

bool button_toggled(void);

void set_button_toggled(bool button_toggled);

int64_t get_button_pressed_time(void);

uint64_t get_button_pressed_timestamp(void);

uint64_t get_button_unpressed_timestamp(void);

#endif /* APP_BUTTON_H */
