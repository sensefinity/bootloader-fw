/*
 * ping.h
 *
 *  Created on: 24/08/2016
 *      Author: Ruy__
 */

#ifndef PING_H_
#define PING_H_


#include <stdint.h>


void ping_loop(uint64_t t_now, uint64_t interval);

#endif /* PING_H_ */
