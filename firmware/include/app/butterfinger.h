/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     app
 * @{
 *
 * @file        butterfinger.h
 * @brief       Implements a simple application that deals with the
 *              communication with a Butterfinger device, through a simple
 *              protocol.
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#ifndef APP_BUTTERFINGER_H
#define APP_BUTTERFINGER_H

/* Peripherals */
#include "periph/pin_name.h"

#define BUTTERFINGER_TXD        (P0_00)     /* White */
#define BUTTERFINGER_RXD        (P0_01)     /* Green */

/**
 * @brief       Initializes the communication with the Butterfinger device.
 */
void butterfinger_init(void);

/**
 * @brief       Function designed to be run in a loop for each time the
 *              processor wakes up.
 */
void butterfinger_loop(void);

#endif /* APP_BUTTERFINGER_H */
