/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     app
 * @{
 *
 * @file        wine.h
 * @brief       Implements an application that performs readings from the wine
 *              board "POF Interrogator".
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#ifndef APP_WINE_H
#define APP_WINE_H

/* Peripherals */
#include "periph/pin_name.h"

/* Definitions for the used pins */
#define POF_TXD     (P0_01)
#define POF_RXD     (P0_02)
#define POF_BAUD    (115200)

/* Prototypes */
void wine_init(void);
void wine_loop(void);

#endif /* APP_WINE_H */
