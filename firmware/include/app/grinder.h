/**
 * Copyright (c) 2016 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     app
 * @{
 *
 * @file        grinder.h
 * @brief       Implements the grinder app
 *
 * @author      João Ambrósio <joao.ambrosio@sensefinity.com>
 *
 * @}
 */

#ifndef APP_GRINDER_H
#define APP_GRINDER_H

/* Peripherals */
#include "periph/pin_name.h"
#include <stdint.h>

/* Definitions for the used pins */
#define GRINDER_POWER         (P0_02)

/* Timings in ms */
#define POWEROFF_DETECT_DELAY 500

/* Prototypes */
void grinder_init(void);
void grinder_loop(void);

#endif /* APP_GRINDER_H */
