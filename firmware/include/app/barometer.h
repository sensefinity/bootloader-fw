/**
 * Copyright (c) 2016 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     app
 * @{
 *
 * @file        barometer.h
 * @brief       Implements the barometer app
 *
 * @author      João Ambrósio <joao.ambrosio@sensefinity.com>
 *
 * @}
 */

#ifndef APP_BAROMETER_H
#define APP_BAROMETER_H

/* Standard C library */
#include <stdint.h>

/* Peripherals */
#include "sensors/MPL3115A2.h"

/* Utilities */
#include "util/delay.h"
#include "util/debug.h"

/* API */
#include "api/sched.h"

/* Prototypes */
void barometer_setup(uint64_t interval);
void barometer_loop(void);

#endif /* APP_BAROMETER_H */
