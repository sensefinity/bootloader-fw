/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     app
 * @{
 *
 * @file        positioning.h
 * @brief       Coordinates the operation of the "Gateway" profile in order
 *              to add an external positioning service.
 *
 * @author      Tiago Tom�s <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#ifndef APP_POSITIONING_H
#define APP_POSITIONING_H

/* C standard library */
#include <stdint.h>

/* Prototypes */
void positioning_init(uint64_t interval);
void positioning_loop(void);

#endif /* APP_POSITIONING_H */
