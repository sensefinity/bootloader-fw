/*
 * telco_power_management.h
 *
 *  Created on: 22/09/2016
 *      Author: Ruy__
 */

#ifndef INCLUDE_APP_TELCO_POWER_MANAGEMENT_H_
#define INCLUDE_APP_TELCO_POWER_MANAGEMENT_H_


#include <stdint.h>

void telco_power_management_order(uint8_t order);

#endif /* INCLUDE_APP_TELCO_POWER_MANAGEMENT_H_ */
