/**
 * Copyright (c) 2016 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     app
 * @{
 *
 * @file        led.h
 * @brief       Implements the led utilities
 *
 * @author      Jo�o Ambr�sio <joao.ambrosio@sensefinity.com>
 *
 * @}
 */

#ifndef APP_LED_H
#define APP_LED_H

/* C standard library */
#include <stdbool.h>
#include <assert.h>
#include <stddef.h>

/* Peripherals */
#include "periph/pwm.h"

/* Utilities */
#include "util/delay.h"
#include "util/debug.h"

/* API */
#include "api/sched.h"

/* Definitions */
#define LED_PIN						(P0_25)

#define PWM_UPDATE_FREQ				(30) 	//In ms
#define BRIGHTNESS_INCREMENT		(5)

/* Prototypes */

/**
 * @brief     Initializes the led pin
 */
void led_init(void);

/**
 * @brief     Turns on led
 */
void led_on(void);

/**
 * @brief     Turns off led
 */
void led_off(void);

bool led_status(void);

/**
 * @brief     Toggle led
 */
void led_toogle(void);

/**
 * @brief     Enable blinking
 * @param	  blinking_period
 * 			  Period of the blinking in milliseconds.
 */
void led_enable_blink(uint64_t blinking_period_ms);

void led_enable_blink_with_duration(uint64_t blinking_period_ms,
		uint64_t blinking_duration_ms);

void led_enable_blink_with_blinking_number(uint64_t blinking_period_ms,
		uint8_t number_of_blinks);

/**
 * @brief     Disable blinking
 */
void led_disable_blink(void);

void led_enable_breath(void);

void led_enable_breath_with_duration(uint64_t breathing_duration_ms);

void led_fade_in(void);

void led_fade_out(void);

void led_disable_fade(void);

bool is_led_busy(void);

#endif /* APP_LED_H */
