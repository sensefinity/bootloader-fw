/**
 * Copyright (c) 2016 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     app
 * @{
 *
 * @file        transient.h
 * @brief       Implements the transient app
 *
 * @author      João Ambrósio <joao.ambrosio@sensefinity.com>
 *
 * @}
 */

#ifndef APP_TRANSIENT_H
#define APP_TRANSIENT_H

/* Standard C library */
#include <stdint.h>

/* Peripherals */
#include "app/accelerometer.h"
#include "sensors/MMA8652.h"

/* Utilities */
#include "util/delay.h"
#include "util/debug.h"

/* API */
#include "api/sched.h"

/* Prototypes */
void transient_setup(uint8_t hp_cutoff_frequency, bool x_enabled, bool y_enabled, bool z_enabled, bool bypass_hp_filter_enabled, uint8_t transient_threshold, uint8_t count_threshold);
void transient_loop(void);

#endif /* APP_TRANSIENT_H */
