/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     app
 * @{
 *
 * @file        temperature.h
 * @brief       Implements the base temperature App
 *
 * @author      Rui Pires rui.pires@sensefinity.com>
 *
 * @}
 */

#ifndef TEMPERATURE_APP_H
#define TEMPERATURE_APP_H

#include <stdint.h>



void temperature_loop(uint64_t t_now, uint64_t interval);

#endif /* TEMPERATURE_APP_H */
