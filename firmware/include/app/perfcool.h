/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     app
 * @{
 *
 * @file        perfcool.h
 * @brief       Implements a simple "App" that implements the Perfect Cool
 *              use case on top of the Sensoroid library.
 * @details     This "App" is designed to work directly with the LMT85LP
 *              temperature sensor. For low power operation, the VDD signal
 *              is directly connected to a GPIO pin, which allows us to turn
 *              the sensor on and off.
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#ifndef APP_PERFCOOL_H
#define APP_PERFCOOL_H

/* C standard library */
#include <assert.h>
#include <math.h>
#include <stdint.h>
#include <string.h>

/* API */
#include "api/sched.h"
#include "api/sysclk.h"

/* Peripherals */
#include "periph/adc.h"
#include "periph/gpio.h"
#include "periph/pin_name.h"

/* Sensoroid library */
#include "sensoroid/sensoroid.h"

/* Utilities */
#include "util/delay.h"

/* Defines the pins for the temperature sensor */
#define LMT85LP_VDD         (P0_00)
#define LMT85LP_OUT         (AIN2)

/* Defines the interval at which to take measurements in milliseconds */
#define PERFCOOL_INTERVAL   (300000)

/**
 * @brief       Sets up the Perfect Cool application.
 */
void perfcool_setup(void);

/**
 * @brief       Implements the Perfect Cool looping function to be called each
 *              time the processor wakes up.
 */
void perfcool_loop(void);

#endif /* APP_PERFCOOL_H */
