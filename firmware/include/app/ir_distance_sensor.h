/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     app
 * @{
 *
 * @file        ir_distance_sensor.h
 * @brief       Implements an App that reads the infrared distance sensor
 *
 * @author      Rui Pires
 *
 * @}
 */

#ifndef APP_IRDS_H
#define APP_IRDS_H

/* Peripherals */
#include "periph/pin_name.h"
#include <stdint.h>


/* Pins */
#define IRDS_VCC         (P0_00)
#define IRDS_OUT         (AIN2)

/* Columbus messages irds fields contents */
#define COLUMBUS_IRDS_MEASUREMENT_DATA_SIZE                         (6)
#define COLUMBUS_IRDS_MEASUREMENT_SENSOR                            (0x01)
#define COLUMBUS_IRDS_MEASUREMENT_TYPE                              (0x80)

/**
 * @brief       Initializes the IRDS application.
 */
void irds_setup(uint64_t interval);

/**
 * @brief       Looping function designed to be called for each time that the
 *              processor wakes up from sleep mode.
 */
void irds_loop(void);

#endif /* APP_IRDS_H */
