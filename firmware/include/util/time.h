#ifndef __TIME_H__
#define __TIME_H__

#define SECONDS_TO_MILLISECONDS(x)      ((x)*(1000))
#define SECONDS_TO_MICROSECONDS(x)      ((x)*(1000000))

#define MILLISECONDS_TO_SECONDS(x)      ((x)*(0.001))
#define MILLISECONDS_TO_MICROSECONDS(x) ((x)*(1000))

#define MICROSECONDS_TO_SECONDS(x)      ((x)*(0.000001))
#define MICROSECONDS_TO_MILLISECONDS(x) ((x)*(0.001))

#endif
