/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @file        delay.h
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 * @date        5 Oct 2015
 * @brief       Platform-specific delay functions.
 * @see         http://www.sensefinity.com
 */
#ifndef UTIL_DELAY_H
#define UTIL_DELAY_H

/* C standard library */
#include <stdint.h>

/**
 * @brief       Delays execution for a time in milliseconds.
 * @param       millis
 *              Number of milliseconds to delay.
 */
void delay_ms(uint32_t millis);

/**
 * @brief       Delays execution for a time in microseconds.
 * @param       micros
 *              Number of microseconds to delay.
 */
void delay_us(uint32_t micros);

#endif /* UTIL_DELAY_H */
