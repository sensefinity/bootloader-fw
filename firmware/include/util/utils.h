/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup
 * @{
 *
 * @file        utils.h
 * @brief       Util functions
 *
 * @}
 */

 #ifndef UTILS_H
 #define UTILS_H

/* C standard library */
#include <stdint.h>
#include <stddef.h>

int cmp_64_with_array(uint64_t u64, uint8_t *array);

uint64_t get_serial_from_array(uint8_t * array);

void save_uint64_t_to_array(uint8_t *message, uint64_t u64);

uint32_t get_uint32_from_array(uint8_t * array);

void save_uint32_t_to_array(uint8_t * message, uint32_t u32);

uint16_t get_uint16_from_array(uint8_t * array);

uint64_t get_uint64_from_string(const char *value);

#endif  /* UTILS_H */
