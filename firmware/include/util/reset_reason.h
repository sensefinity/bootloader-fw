#ifndef __RESET_REASON_H__
#define __RESET_REASON_H__

#include <stdlib.h>

void reset_reason_set(unsigned int reason);

char *reset_reason_get_text(size_t *len);

#endif
