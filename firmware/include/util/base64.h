/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     util
 * @{
 *
 * @file        base64.h
 * @brief       Implements a simple to use Base64 encoder
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#ifndef UTIL_BASE64_H
#define UTIL_BASE64_H

/* C standard library */
#include <stdlib.h>
#include <string.h>

/**
 * @brief       Helper macro that computes the length of the resulting Base64
 *              string when the data to be encoded has length (x)
 */
#define BASE64_LENGTH(x) (((x) * 4) / 3)

/**
 * @brief       Encodes a sequence of bytes with the provided length as a
 *              Base64 string
 * @param[in]   data
 *              Pointer to the data to be encoded.
 * @param[in]   length
 *              The length of the data to be encoded, in number of bytes.
 * @param[in]   output
 *              Pointer to an output array of characters where the resulting
 *              Base64 string is written. The resulting string is
 *              NULL-terminated. The size of the output buffer shall be at
 *              least (BASE64_LENGTH(length) + 1).
 */
void base64_encode(const void *data, size_t length, char *output);

#endif /* UTIL_BASE64_H */
