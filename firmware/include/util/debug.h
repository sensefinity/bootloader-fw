/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     util
 * @{
 *
 * @file        debug.h
 * @brief       Definition of debug printing tasks, conditional on
 *              debug specific builds.
 *
 * @author      Carlos Filipe Costa <tiago.tomas@sensefinity.com>
 *
 * @}
 */
#ifndef __DEBUG_H__
#define __DEBUG_H__

#ifdef DEBUG
#include <stdio.h>

/* Segger RTT library */
#include "SEGGER_RTT.h"

/**
 * @brief       Prints into the debug screen. Uses printf interface.
 * @param[in]   format
 *              String following printf format specification.
 * @returns     Number of characters printed.
 */
#define dprintf(format, ...) SEGGER_RTT_printf(0, format, ##__VA_ARGS__)

/**
 * @brief       Prints into the debug screen. Uses puts interface.
 * @param[in]   string
 *              String to print.
 * @returns     Non-negative number if successful, ERRNO otherwise.
 */
#define dputs(string) SEGGER_RTT_WriteString(0, string "\r\n")

/**
 * @brief       Prints a new character to the terminal.
 */
#define dputchar(c) SEGGER_RTT_printf(0, "%c", c)

#else

/**
 * @brief       Does nothing.
 */
#define dprintf(format, ...)

/**
 * @brief       Does nothing.
 */
#define dputs(string)

/**
 * @brief       Does nothing.
 */
#define dputchar(c)

#endif /* DEBUG */

#endif /*__DEBUG_H__*/
