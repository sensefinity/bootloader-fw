/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup
 * @{
 *
 * @file        crc_ops.h
 * @brief       Use CRC driver to compute and verify messages crc.
 *              Using a 16-bit CRC the CCITT which is now widely used.
 *              Polynomial : x^16 + x^12 + x^5 + 1
 *
 * @}
 */

 #ifndef CRC_OPS_H
 #define CRC_OPS_H

 /* C standard library */
 #include <stdbool.h>
 #include <stddef.h>
 #include <stdint.h>


/*
 * Validates a crc, computes the corresponding crc to the messageSource and
 * compare with the crc received (crc_cmp), size indicates the message lenght.
 * Must be called after the crc inicialization (init_crc)
 */
bool check_crc(uint8_t * messageSource, size_t size, uint16_t crc_cmp);

/*
 *  CRC Calculation, computes the corresponding crc to the messageSource for
 *  the indicated the message lenght (size).
 */
uint16_t compute_crc(uint8_t * messageSource, size_t size);

 #endif /* CRC_OPS_H */
