/*
 * write_fw.h
 *
 *  Created on: 27/09/2016
 *      Author: Carlos Filipe Costa <carlos.costa@sensefinity.com>
 */

#ifndef __WRITE_FW_H__
#define __WRITE_FW_H__

#include <stdint.h>
#include <stddef.h>

/**
 * @brief   Write received firmware package into flash memory;
 * @param   offset
 *          Offset of the data relative to the beginning of the firmware image.
 * @param   message
 *          Firmware data to copy.
 * @param   data_size
 *          Size of the data to write
 * @returns Size written to flash, -1 if address is illegal.
 **/
int write_fw(uint32_t offset, uint8_t *message, size_t data_size);

/**
 * @brief   Signals all firmware is copied, prepares firmware update process.
 * @param   checksum
 *          Checksum to compare with copied data's one.
 * @param   size
 *          Size of checksum variable.
 **/
void fw_done(uint8_t* checksum, int size);

#endif /* __WRITE_FW_H__ */
