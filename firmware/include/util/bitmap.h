/*
 * bitmap.h
 *
 *  Created on: 12/09/2016
 *      Author: Ruy__
 */

#ifndef BITMAP_H_
#define BITMAP_H_



/* C standard library */
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define BIT_WORD              (8)

void bitmap_clean(uint8_t * bitmap, size_t size);

//void bitmap_complete(uint8_t * bitmap, size_t size);

bool bitmap_is_value_present(uint8_t * bitmap, uint16_t segment_number);

void bitmap_update(uint8_t * bitmap, uint16_t segment_number);

bool bitmap_is_complete(uint8_t * bitmap, uint16_t segment_count);

void bip_buffer_print(uint8_t * bitmap,size_t size);


#endif /* BITMAP_H_ */
