/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     api
 * @{
 *
 * @file        sched.h
 * @brief       Simple to use general-purpose task scheduler
 * @details     This task scheduler uses RTC1 for scheduling tasks using the
 *              lowest power possible.
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#ifndef API_SCHED_H
#define API_SCHED_H

/* Standard C library */
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

/* Dandelion library */
#include "dandelion.h"

/* Platform */
#include "nrf51.h"
#include "nrf51_bitfields.h"

/* Utilities */
#include "util/atomic.h"

/**
 * @brief       Holds the minimum and maximum milliseconds that may be
 *              scheduled on the current implementation
 */
#define SCHED_TASK_MS_MIN       ((uint64_t) (10))
#define SCHED_TASK_MS_MAX       ((uint64_t) ((1 << 24) - 1))

/**
 * @brief       Defines the type of a scheduler identifier
 * @details     A scheduler identifier is used for uniquely identifying a
 *              running task.
 */
typedef int32_t sched_id_t;

/**
 * @brief       Initializes the task scheduler and the underlying hardware
 *              for operation
 */
void sched_init(void);

/**
 * @brief       Schedules a timeout task
 * @details     A timeout task is a single-shot task. Upon returning from this
 *              function, the task is considered running if the generated
 *              identifier is valid.
 * @param[in]   action
 *              The action to be invoked after the timeout.
 * @param[in]   millis
 *              The number of milliseconds to wait until timeout.
 * @returns     The generated identifier for the provided task. If it is a
 *              negative number, there was an error scheduling the task,
 *              otherwise the task is running.
 */
sched_id_t sched_timeout(void (*action)(void), uint64_t millis);

/**
 * @brief       Schedules an interval task
 * @details     An interval task is a periodic task. Upon returining from this
 *              function, the task is considered running if the generated
 *              identifier is valid.
 * @param[in]   action
 *              The action to be invoked periodically.
 * @param[in]   millis
 *              The number of milliseconds that define the task period.
 * @returns     The generated identifier for the provided task. If it is a
 *              negative number, there was an error scheduling the task,
 *              otherwise the task is running.
 */
sched_id_t sched_interval(void (*action)(void), uint64_t millis);

/**
 * @brief       Cancels a running task
 * @param[in]   The identifier of the task to be cancelled.
 */
void sched_cancel(sched_id_t id);

/**
 * @brief       Reads the current value of the internal scheduling timer.
 * @returns     The current value of the internal scheduling timer.
 */
uint64_t sched_read(void);

/**
 * @brief       Actively waits for the specified time in milliseconds
 * @param[in]   millis
 *              The time in milliseconds to wait.
 */
void sched_wait(uint64_t millis);

#endif /* PERIPH_SCHED_H */
