/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     api
 * @{
 *
 * @file        sysclk.h
 * @brief       Implements a general system clock.
 * @details     The system clock is implemented using the RTC0.
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#ifndef API_SYSCLK_H
#define API_SYSCLK_H

/* C standard library */
#include <stdint.h>

/**
 * @brief       Initializes the system clock.
 * @details     This function must not be called by application code. Upon the
 *              invocation of the main function, the system clock is ready.
 */
void sysclk_init(void);

/**
 * @brief       Reads the current system clock value in milliseconds.
 * @returns     The current system clock value in milliseconds.
 */
uint64_t sysclk_get(void);

/**
 * @brief       Sets the current system clock value in milliseconds.
 * @param[in]   t_now
 *              The current time in milliseconds.
 */
void sysclk_set(uint64_t t_now);

#endif /* API_SYSCLK_H */
