/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @file        gpio.h
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 * @date        6 Oct 2015
 * @brief       Interface for handling GPIO pins.
 * @see         http://www.sensefinity.com
 */
#ifndef PERIPH_GPIO_H
#define PERIPH_GPIO_H

/* C standard library */
#include <stdbool.h>
#include <assert.h>
#include <stddef.h>

/* Peripherals */
#include "periph/pin_name.h"

/* nRF51 */
#include "nrf51.h"
#include "nrf51_bitfields.h"

/* Utilities */
#include "util/delay.h"
#include "util/debug.h"

/**
 * @brief       Enumerates the GPIOTE interrupts.
 */
enum gpiote_interrupt {
	GPIOTE_INT_0 = 0,
	GPIOTE_INT_1 = 1,
	GPIOTE_INT_2 = 2,
	GPIOTE_INT_3 = 3,
	GPIOTE_INT_PORT = 4
};

/**
 * @brief       Enumerates the GPIO pull modes for input pins.
 */
enum gpio_pull {
	GPIO_PULL_DISABLED = GPIO_PIN_CNF_PULL_Disabled, /**< No pull. */
	GPIO_PULL_UP = GPIO_PIN_CNF_PULL_Pullup, /**< Connected pull-up resistor. */
	GPIO_PULL_DOWN = GPIO_PIN_CNF_PULL_Pulldown, /**< Connected pull-down resistor. */
	GPIO_PULL_DEFAULT = GPIO_PULL_DISABLED /**< Default value. */
};

/**
 * @brief       Enumerates the GPIO sense modes.
 */
enum gpio_sense {
	GPIO_SENSE_DISABLED 	= (GPIO_PIN_CNF_SENSE_Disabled),
	GPIO_SENSE_HIGH 		= (GPIO_PIN_CNF_SENSE_High),
	GPIO_SENSE_LOW 			= (GPIO_PIN_CNF_SENSE_Low),
	GPIO_SENSE_DEFAULT		= (GPIO_SENSE_DISABLED)
};

enum pin_polarity {
	PIN_POLARITY_DISABLED 	= (0),
	PIN_POLARITY_HIGH 		= (1),
	PIN_POLARITY_LOW 		= (2),
	PIN_POLARITY_TOGGLE 	= (3),
	PIN_POLARITY_DEFAULT 	= (PIN_POLARITY_DISABLED)
};

/**
 * @brief       Initializes a GPIO pin for output.
 * @param       name
 *              Name of the pin as defined in the pin_name enum.
 */
void gpio_output_init(enum pin_name name);

/**
 * @brief       Sets a GPIO pin by writing a logic '1'.
 * @details     This function will only work if the pin has been previously
 *              configured for output.
 * @param       name
 *              Name of the pin as defined in the pin_name enum.
 */
void gpio_output_set(enum pin_name name);

/**
 * @brief       Clears a GPIO pin by writing a logic '0'.
 * @details     This function will only work if the pin has been previously
 *              configured for output.
 * @param       name
 *              Name of the pin as defined in the pin_name enum.
 */
void gpio_output_clear(enum pin_name name);

/**
 * @brief       Toggles the current value of a GPIO pin.
 * @details     This function will only work if the pin has been previously
 *              configured for output.
 * @param       name
 *              Name of the pin as defined in the pin_name enum.
 */
void gpio_output_toggle(enum pin_name name);

/**
 * @brief       Writes a boolean value to a GPIO pin.
 * @details     This function will only work if the pin has been previously
 *              configured for output.
 * @param       name
 *              Name of the pin as defined in the pin_name enum.
 * @param       value
 *              The boolean value. If true, a logic '1' is written, otherwise,
 *              a logic '0' is written.
 */
void gpio_output_write(enum pin_name name, bool value);

/**
 * @brief       Reads the current state of a GPIO pin.
 * @details     This function will only work if the pin has been previously
 *              configured for output.
 * @param       name
 *              Name of the pin as defined in the pin_name enum.
 * @return      A boolean value indicating the current state of the pin. If
 *              the pin is set (logic '1'), true is returned, otherwise, false
 *              is returned.
 */
bool gpio_output_read(enum pin_name name);

/**
 * @brief       Initializes a GPIO pin for input.
 * @param       name
 *              Name of the pin as defined in the pin_name enum.
 * @param       pull
 *              Pull mode as defined in the gpio_pull enum.
 */
void gpio_input_init(enum pin_name name, enum gpio_pull pull);

/**
 * @brief       Reads the current value of the GPIO input.
 * @details     This function will only work if the pin has been previously
 *              configured for input.
 * @param       name
 *              Name of the pin as defined in the pin_name enum.
 * @return      The current value of the input. If a logic '1' is present,
 *              true is returned, otherwise, false is returned.
 */
bool gpio_input_read(enum pin_name name);

/**
 * @brief       Configure a GPIOTE event.
 * @details     This function associates a event with a given pin, i.e. when a certain condition is
 *              verified the defined event will be verified.
 * @param       pin_name
 *              The pin name to be configured.
 *              gpiote_interrupt_pin
 *              The GPIOTE interrupt pin that will be associated with the pin. Possible values:
 *              - GPIOTE_INT_0;
 *              - GPIOTE_INT_1;
 *              - GPIOTE_INT_2;
 *              - GPIOTE_INT_3.
 *              polarity
 *              The polarity that must be verified in order to execute the event. Possible values:
 *              - GPIOTE_CONFIG_POLARITY_LoToHi;
 *              - GPIOTE_CONFIG_POLARITY_HiToLo;
 *              - GPIOTE_CONFIG_POLARITY_Toggle-
 */
void gpiote_set_event(uint8_t pin_name, uint8_t gpiote_interrupt_pin,
		uint8_t polarity);

void gpio_disable_channel(uint8_t gpiote_interrupt_pin);

void gpio_enable_channel(uint8_t gpiote_interrupt_pin);

/**
 * @brief       Associate a function with an event
 * @details     The associated function will be executed everytime the event of the given GPIOTE interrupt
 *              is verified.
 * @param       gpiote_interrupt_pin
 *              The GPIOTE interrupt pin that will be associated with the pin. Possible values:
 *              - GPIOTE_INT_0;
 *              - GPIOTE_INT_1;
 *              - GPIOTE_INT_2;
 *              - GPIOTE_INT_3.
 *              handler
 *              The function to be attached with the event.
 */
void gpiote_attach_event_function(void (*handler)(void),
		enum gpiote_interrupt gpiote_interrupt_pin);

void gpiote_detach_event_function(enum gpiote_interrupt gpiote_interrupt_pin);

void gpio_detach_pin_function(enum pin_name pin_number);

void gpio_attach_pin_function(void (*handler)(void), enum pin_name pin_number,
		enum pin_polarity pin_sense);

#endif /* PERIPH_GPIO_H */
