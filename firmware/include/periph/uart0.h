/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @file        uart0.h
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 * @date        8 Oct 2015
 * @brief       Interface for controlling UART0 peripheral. The layout of the
 *              UART frame is fixed and consists of 1 start bit, 8 data bits,
 *              1 optional parity bit and 1 stop bit.
 * @todo        Implement hardware-supported flow control.
 * @see         http://www.sensefinity.com
 */
#ifndef PERIPH_UART0_H
#define PERIPH_UART0_H

/* C standard library */
#include <assert.h>
#include <stdbool.h>
#include <stddef.h>

/* nRF51 */
#include "nrf51.h"
#include "nrf51_bitfields.h"

/* Peripherals */
#include "periph/clock.h"
#include "periph/gpio.h"
#include "periph/pin_name.h"

/* Dandelion library */
#include "dandelion.h"

/* Defines default values for the UART0 peripheral configuration */
#define UART0_DEFAULT_BAUDRATE  (9600)
#define UART0_DEFAULT_PARITY    (false)

/* Defines the UART0 interrupt priority */
#define UART0_PRIORITY          (DANDELION_IRQ_PRIORITY_LOW)

/**
 * @brief       Initializes and enables the UART.
 * @details     The UART peripheral shall be initialized using the default
 *              values defined in the UART0_DEFAULT_* macros. Calling this
 *              function on an already initialized UART will first disable
 *              it so that a new initialization can be performed.
 * @param       txd
 *              The name of the pin to be used for the TX signal, as defined
 *              in the pin_name enum. A value of NC (Not Connected) will cause
 *              an assertion fail.
 * @param       rxd
 *              The name of the pin to be used for the RX signal, as defined
 *              in the pin_name enum. A value of NC (Not Connected) will cause
 *              an assertion fail.
 */
void uart0_init(enum pin_name txd, enum pin_name rxd);

/**
 * @brief       Configures the UART baud rate.
 * @details     The baud rate defines the time of each symbol, in this case,
 *              a the time of each bit. It can be used interchangeably as the
 *              bit rate.
 * @param       baudrate
 *              The new baud rate.
 *              No checking will be perfomed on the baud rate, as such, it is
 *              the user's responsibility to properly defined it.
 */
void uart0_baud(int baudrate);

/**
 * @brief       Configures the UART parity bit.
 * @details     The parity bit is a crude form of error detection, which is
 *              included in the UART frame before the stop bit. The parity bit
 *              is set if the number of logic '1's in the data is odd.
 * @param       included
 *              A boolean value indicating whether to include a parity bit
 *              in the data frame or not.
 */
void uart0_parity(bool included);

/**
 * @brief       Disables the UART.
 * @details     Disabling the UART stops any ongoing transactions and detaches
 *              any previously attached interrupts.
 */
void uart0_disable(void);

/**
 * @brief       Tests whether the UART has new data to be read.
 * @return      A boolean value indicating if new data is available to be read.
 */
bool uart0_readable(void);

/**
 * @brief       Reads the next character from the UART.
 * @details     This function will block until a new character is available to
 *              be read. If non-blocking operation is needed, test the UART's
 *              readability using uart0_readable().
 * @return      The next character from the UART.
 */
int uart0_read(void);

/**
 * @brief       Tests whether the UART is ready to transmit new data.
 * @return      A boolean value indicating if new data can be written.
 */
bool uart0_writable(void);

/**
 * @brief       Writes a new character to the UART.
 * @details     This function will block until the UART is ready to transmit
 *              new data. If non-blocking operation is needed, test the UART's
 *              writability using uart0_writable().
 * @param       ch
 *              The character to be written.
 *              Only the 8 least significant bits will be transmitted.
 */
void uart0_write(int ch);

/**
 * @brief       Attaches an interrupt handler to the UART peripheral.
 * @details     Attaching an interrupt handler to the UART allows event-based
 *              code to be written. Once the interrupt handler is ivoked, the
 *              user is required to determine what caused an interruption, by
 *              testing the UART's readability or writability using the
 *              uart0_readable() and/or uart0_writable() respectively. The
 *              interrupt is only cleared once a read or write operation is
 *              performed.
 * @param       handler
 *              Pointer to the function to be invoked for each UART event.
 *              Using a value of NULL will cause an asssertion fail.
 */
void uart0_attach(void (*handler)(void));

/**
 * @brief       Detaches a previously attached interrupt handler.
 * @details     UART0 interrupts are effectively disabled this way.
 */
void uart0_detach(void);

/**
 * @brief       Tests whether the UART is currently enabled.
 * @return      A boolean value indicating if the UART is currently enabled.
 */
static inline bool _uart0_enabled(void) {
    return NRF_UART0->ENABLE
            == (UART_ENABLE_ENABLE_Enabled << UART_ENABLE_ENABLE_Pos);
}

#endif /* PERIPH_UART0_H */
