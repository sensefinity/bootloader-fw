/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     periph
 * @{
 *
 * @file        i2c.h
 * @brief       Implements a simple to use driver for the I2C peripheral.
 *              Special care must be taken when using I2C. If SPI-master
 *              peripheral is in use, the TWI (Two-wire Interface - I2C)
 *              can not be used at the same time because they have shared
 *              IDs in the instantiation table.
 *
 * @author      João Ambrósio <joao.ambrosio@sensefinity.com>
 *
 * @}
 */

#ifndef PERIPH_I2C_H
#define PERIPH_I2C_H

/* Standard C library */
#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

/* Peripherals */
#include "periph/gpio.h"
#include "periph/pin_name.h"

/* Platform */
#include "nrf51.h"
#include "nrf51_bitfields.h"

/* Dandelion library */
#include "dandelion.h"

/**
 * @brief       Enumerates the supported I2C clock frequencies
 */
enum i2c_frequency {
    I2C_FREQUENCY_K100  = 0,    /* 100 kHz clock */
    I2C_FREQUENCY_K250,         /* 250 kHz clock */
    I2C_FREQUENCY_K400,         /* 400 kHz clock */
};

/**
 * @brief       Defines the structure for the I2C configuration parameters
 */
struct I2C_params {
    /* I2C frequency. Possible values:
       - I2C_FREQUENCY_K100;
       - I2C_FREQUENCY_K250;
       - I2C_FREQUENCY_K400. */
    enum i2c_frequency  frequency;
    enum pin_name       scl;        /* Clock line pin */
    enum pin_name       sda;        /* Data line pin */
};

#define I2C_PPI_CHANNEL         (0)

/* Defines the maximum cycles to wait for a RXDREADY and TXDREADY event */
#define MAX_TIMEOUT_LOOPS       (20000UL)

/* Time to wait when pin states are changed. For fast-mode the delay can be
   zero and for standard-mode 4 us delay is sufficient */
#define I2C_DELAY               (4UL)

/**
 * @brief       Initializes and enables the I2C peripheral with the provided
 *              configuration parameters
 * @param[in]   params
 *              The I2C configuration parameters.
 */
bool i2c_init(struct I2C_params params);

/**
 * @brief       Disables the I2C peripheral
 * @details     Effectively, puts the I2C peripheral into a low-power state
 *              where communication is no longer supported. The I2C peripheral
 *              can be re-initialized using i2c_init().
 */
bool i2c_shutdown(void);

/**
 * @brief       Sets the I2C address of the peripheral module
 */
bool i2c_set_address(uint8_t address);

/**
 * @brief       Writes to the I2C peripheral
 * @param[in]   data
 *              The data to be written.
 *              data_length
 *              The size of the data to be transmitted.
 *              issue_stop_condition
 *              Indicates if the stop bit will be send in the transmission end.
 * @returns     Indicates if the write process was made with success or not.
 */
bool i2c_write(uint8_t *data, uint8_t data_length, bool issue_stop_condition);

/**
 * @brief       Reads from the I2C peripheral
 * @param[in]   data
 *              The read will be done to this structure.
 *              data_length
 *              The size of the data to be read.
 * @returns     Indicates if the read process was made with success or not.
 */
bool i2c_read(uint8_t *data, uint8_t data_length);

#endif /* PERIPH_I2C_H */
