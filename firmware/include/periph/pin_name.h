/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @file        pin_name.h
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 * @date        6 Oct 2015
 * @brief       Enumerates the system pins.
 * @see         http://www.sensefinity.com
 */
#ifndef PERIPH_PIN_NAME_H
#define PERIPH_PIN_NAME_H

/* Useful definitions for computing masks */
#define PIN_NAME_MASK(name) (1 << (name))

/**
 * @brief       Enumeates the pin names.
 * @note        This enumeration won't prefix the names the PIN_NAME_* as usual
 *              due to the fact that the names will probably always be unique.
 */
enum pin_name {
    /* General-purpose Input/Output */
    NC      = -1,
    P0_00   =  0,
    P0_01   =  1,
    P0_02   =  2,
    P0_03   =  3,
    P0_04   =  4,
    P0_05   =  5,
    P0_06   =  6,
    P0_07   =  7,
    P0_08   =  8,
    P0_09   =  9,
    P0_10   = 10,
    P0_11   = 11,
    P0_12   = 12,
    P0_13   = 13,
    P0_14   = 14,
    P0_15   = 15,
    P0_16   = 16,
    P0_17   = 17,
    P0_18   = 18,
    P0_19   = 19,
    P0_20   = 20,
    P0_21   = 21,
    P0_22   = 22,
    P0_23   = 23,
    P0_24   = 24,
    P0_25   = 25,
    P0_26   = 26,
    P0_27   = 27,
    P0_28   = 28,
    P0_29   = 29,
    P0_30   = 30,

    /* Analog input pins */
    AIN0    = P0_26,
    AIN1    = P0_27,
    AIN2    = P0_01,
    AIN3    = P0_02,
    AIN4    = P0_03,
    AIN5    = P0_04,
    AIN6    = P0_05,
    AIN7    = P0_06,

    /* ADC/LPCOMP input reference pins */
    AREF0   = P0_00,
    AREF1   = P0_06
};

#endif /* PERIPH_PIN_NAME_H */
