/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     periph
 * @{
 *
 * @file        pwm.h
 * @brief       Implements PWM driver.
 *
 * @author      Jo�o Ambr�sio <joao.ambrosio@sensefinity.com>
 *
 * @}
 */

#ifndef PERIPH_PWM_H
#define PERIPH_PWM_H

/* Standard C library */
#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

/* Utilities */
#include "util/delay.h"
#include "util/debug.h"

/* Peripherals */
#include "periph/gpio.h"
#include "periph/pin_name.h"

/* Platform */
#include "nrf51.h"
#include "nrf51_bitfields.h"

/* Dandelion library */
#include "dandelion.h"

/* Public */
#define DUTY_CYCLE_PERIOD			(40) 	//In us
#define DUTY_CYCLE_SAMPLES			(255)

/* Prototypes */
void pwm_set(enum pin_name pin, uint8_t cycles_on);

void pwm_enable(void);

void pwm_disable(void);

#endif /* PERIPH_PWM_H */
