/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     periph
 * @{
 *
 * @file        ppi.h
 * @brief       Implements a simple to use driver for the Programmable
 *              Peripheral Interconnect.
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#ifndef PERIPH_PPI_H
#define PERIPH_PPI_H

/* Standard C library */
#include <assert.h>
#include <stdint.h>

/* Platform */
#include "nrf51.h"
#include "nrf51_bitfields.h"

/**
 * @def         PPI_AVAILABLE_CHANNELS
 * @brief       Holds the number of available PPI channels
 */
#define PPI_AVAILABLE_CHANNELS      (16)

/**
 * @typedef     ppi_channel_t
 * @brief       Defines the type of a PPI channel
 */
typedef uint32_t ppi_channel_t;

/**
 * @brief       Enables a PPI channel
 * @param[in]   channel
 *              The channel to be enabled. Must be a number between 0 and
 *              PPI_AVAILABLE_CHANNELS - 1.
 * @param[in]   eep
 *              Event-end-point. The address of the event that shall trigger
 *              the associated task.
 * @param[in]   tep
 *              Task-end-point. The address of the task that shall be triggered
 *              as a result of the associated event.
 */
void ppi_channel_enable(ppi_channel_t channel, uint32_t eep, uint32_t tep);

/**
 * @brief       Disables a previously enabled PPI channel
 * @param[in]   channel
 *              The channel to be disabled. Must be a number between 0 and
 *              PPI_AVAILABLE_CHANNELS - 1.
 */
void ppi_channel_disable(ppi_channel_t channel);

#endif /* PERIPH_PPI_H */
