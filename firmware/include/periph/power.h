/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     periph
 * @{
 *
 * @file        power.h
 * @brief       Power control functions
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#ifndef PERIPH_POWER_H
#define PERIPH_POWER_H

/* Platform */
#include "nrf51.h"
#include "nrf51_bitfields.h"

/**
 * @brief       Power initialization function
 * @details     Initializes the system power, making sure that all RAM blocks
 *              are turned on and enabling the external DC/DC converter.
 */
void power_init(void);

/**
 * @brief       Enables low power mode
 * @details     In low power mode, if the system goes to sleep, the time the
 *              system takes to wake up is variable. Although, it's the least
 *              power hungry mode.
 */
void power_set_low_power_mode(void);

/**
 * @brief       Enables constant latency mode
 * @details     In constant latency mode, if the system goes to sleep, the time
 *              the system takes to wake up is always constant. Although, it's
 *              more power hungry. This mode is useful if the system has battery
 *              charging functionality.
 */
void power_set_constant_latency_mode(void);

/**
 * @brief       Enter power off
 * @details     Makes the system enter power off mode. In power off, the system
 *              may only wake up from a reset signal or a GPIO interrupt.
 */
void power_off(void);

#endif /* PERIPH_POWER_H */
