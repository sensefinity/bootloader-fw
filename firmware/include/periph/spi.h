/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     periph
 * @{
 *
 * @file        spi.h
 * @brief       Implements a simple to use driver for the SPI peripheral for
 *              master-mode operation. Special care must be taken when using
 *              SPI. If SPI-master peripheral is in use, the TWI (Two-wire
 *              Interface - I2C) and SPI-slave peripheral can not be used at
 *              the same time because they have shared IDs in the instantiation
 *              table.
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#ifndef PERIPH_SPI_H
#define PERIPH_SPI_H

/* Standard C library */
#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

/* Peripherals */
#include "periph/gpio.h"
#include "periph/pin_name.h"

/* Platform */
#include "nrf51.h"
#include "nrf51_bitfields.h"

/* Dandelion library */
#include "dandelion.h"

/**
 * @brief       Enumerates the various SPI modes
 */
enum spi_mode {
    SPI_MODE_0          = 0,    /* CPOL = 0, CPHA = 0 */
    SPI_MODE_1,                 /* CPOL = 0, CPHA = 1 */
    SPI_MODE_2,                 /* CPOL = 1, CPHA = 0 */
    SPI_MODE_3                  /* CPOL = 1, CPHA = 1 */
};

/**
 * @brief       Enumerates the SPI bit-ordering schemes
 */
enum spi_order {
    SPI_ORDER_MSB_FIRST = 0,    /* Use MSB-first order */
    SPI_ORDER_LSB_FIRST         /* Use LSB-first order */
};

/**
 * @brief       Enumerates the supported SPI clock frequencies
 */
enum spi_frequency {
    SPI_FREQUENCY_125K  = 0,    /* 125 kHz clock */
    SPI_FREQUENCY_250K,         /* 250 kHz clock */
    SPI_FREQUENCY_500K,         /* 500 kHz clock */
    SPI_FREQUENCY_1M,           /*   1 MHz clock */
    SPI_FREQUENCY_2M,           /*   2 MHz clock */
    SPI_FREQUENCY_4M,           /*   4 MHz clock */
    SPI_FREQUENCY_8M            /*   8 MHz clock */
};

/**
 * @brief       Defines the structure for the SPI configuration parameters
 */
struct spi_params {
    enum spi_mode       mode;       /* SPI clock mode */
    enum spi_order      order;      /* SPI bit-ordering scheme */
    enum spi_frequency  frequency;  /* SPI frequency */
    enum pin_name       sclk;       /* Serial clock pin */
    enum pin_name       miso;       /* Master input, slave output pin */
    enum pin_name       mosi;       /* Master output, slave input pin */
};

/**
 * @brief       Initializes and enables the SPI peripheral with the provided
 *              configuration parameters
 * @param[in]   params
 *              The SPI configuration parameters.
 */
void spi_init(struct spi_params params);

/**
 * @brief       Disables the SPI peripheral
 * @details     Effectively, puts the SPI peripheral into a low-power state
 *              where communication is no longer supported. The SPI peripheral
 *              can be re-initialized using spi_init().
 */
void spi_shutdown(void);

/**
 * @brief       Writes and/or reads from the SPI peripheral
 * @param[in]   data
 *              The data to be written.
 * @returns     The data read from the slave device.
 */
uint8_t spi_write(uint8_t data);

#endif /* PERIPH_SPI_H */
