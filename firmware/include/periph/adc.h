/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     periph
 * @{
 *
 * @file        adc.h
 * @brief       Implements a simple to use ADC driver
 * @details     The underlying ADC peripheral allows the selection of various
 *              pre-scale values and reference voltages, although as a
 *              simplification we'll always consider that the analog input
 *              always has a 0 - VDD input voltage range.
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#ifndef PERIPH_ADC_H
#define PERIPH_ADC_H

/* Standard C library */
#include <assert.h>
#include <stdbool.h>
#include <stdint.h>

/* Platform */
#include "nrf51.h"
#include "nrf51_bitfields.h"

/* Peripherals */
#include "periph/pin_name.h"

/**
 * @brief       Defines the configured ADC resolution in number of bits
 */
#define ADC_RESOLUTION  (10)

/**
 * @brief       Holds the number of channels in the ADC
 */
#define ADC_CHANNELS    (8)

/**
 * @brief       Defines the ADC reference voltage
 */
#define ADC_REFERENCE   (3.6F)

/**
 * @brief       Reads one sample from the provided ADC channel
 * @param[in]   channel
 *              A number between 0 and (ADC_CHANNELS - 1).
 *              An ADC channel is mapped directly to a physical ADC pin, e.g.,
 *              ADC channel 0 corresponds to the AIN0 pin and channel 1
 *              corresponds to the AIN1 pin.
 * @returns     The raw ADC sample proportional to the supply voltage in range
 *              of 0 to (1 << ADC_RESOLUTION) - 1.
 */
uint16_t adc_sample(uint32_t channel);

/**
 * @brief       Converts a given pin name to the corresponding ADC channel.
 * @param       name
 *              The pin name to be converted.
 * @return      The corresponding ADC channel.
 */
uint32_t adc_pin_as_channel(enum pin_name name);

/**
 * @brief       Reads the system's supply voltage using the ADC.
 * @returns     The raw ADC sample proportional to the supply voltage in the
 *              range of 0 to (1 << ADC_RESOLUTION) - 1.
 */
uint16_t adc_supply(void);

#endif /* PERIPH_ADC_H */
