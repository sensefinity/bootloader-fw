/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     periph
 * @{
 *
 * @file        ficr.h
 * @brief       Easy to use abstraction to accessing the data provided by the
 *              Factory Information Configuration Register
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#ifndef PERIPH_FICR_H
#define PERIPH_FICR_H

/* Standard C library */
#include <stdint.h>
#include <string.h>

/* Platform */
#include "nrf51.h"
#include "nrf51_bitfields.h"

/* Bluetooth stack */
#include "ble/bdaddr.h"

/**
 * @brief       Returns the size in bytes of each flash page
 * @returns     The size in bytes of each flash page.
 */
size_t ficr_flash_page_size(void);

/**
 * @brief       Returns the count of flash pages
 * @returns     The count of flash pages.
 */
size_t ficr_flash_page_count(void);

/**
 * @brief       Returns the size in bytes of each ram block
 * @returns     The size in bytes of each ram block.
 */
size_t ficr_ram_block_size(void);

/**
 * @brief       Returns the count of ram blocks
 * @returns     The count of ram blocks.
 */
size_t ficr_ram_block_count(void);

/**
 * @brief       Returns the factory programmed Bluetooth device address
 * @returns     The Bluetooth device address.
 */
struct bdaddr ficr_device_address(void);

/**
 * @brief       Returns a 64-bit unique device identifier
 * @returns     The unique device identifier.
 */
uint64_t ficr_device_id(void);

#endif /* PERIPH_FICR_H */
