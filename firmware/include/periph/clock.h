/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     periph
 * @{
 *
 * @file        clock.h
 * @brief       System clock functions
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#ifndef PERIPH_CLOCK_H
#define PERIPH_CLOCK_H

/* Standard C library */
#include <stdbool.h>

/* Platform */
#include "nrf51.h"
#include "nrf51_bitfields.h"

/**
 * @brief       Definitions for the system clock frequencies
 */
#define CLOCK_HFCLK_FREQ (16000000)
#define CLOCK_LFCLK_FREQ (32768)

/**
 * @brief       Initializes the underlying clock controllers
 * @details     The high-frequency clock is initialized using the RC oscillator
 *              and the low-frequency clock is initialized to use the installed
 *              XTAL.
 */
void clock_init(void);

/**
 * @brief       Starts the high-frequency clock from the XTAL oscillator
 * @details     When more precision is needed, the HFCLK may be started from
 *              the XTAL oscillator, although, the system will consequently
 *              use more power in sleep mode, due to the XTAL's standby
 *              current.
 */
void clock_hfclk_start(void);

/**
 * @brief       Disables the high-frequency clock from the XTAL oscillator
 * @details     When low-power consumption is needed, the HFCLK may be stopped
 *              from the XTAL oscillator, although the clock source will
 *              be more imprecise.
 */
void clock_hfclk_stop(void);


#endif /* PERIPH_CLOCK_H */
