/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     application
 * @{
 *
 * @file        dandelion.h
 * @brief       Dandelion-specific board functions and definitions
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#ifndef DANDELION_H
#define DANDELION_H

/* C standard library */
#include <stdbool.h>

/* Bluetooth stack */
#include "ble/ll.h"

/* Peripherals */
#include "periph/clock.h"
#include "periph/ficr.h"
#include "periph/gpio.h"
#include "periph/pin_name.h"
#include "periph/power.h"

/* Utilities */
#include "util/delay.h"

/* Platform */
#include "nrf51.h"
#include "nrf51_bitfields.h"

/* API */
#include "api/sched.h"
#include "api/sysclk.h"

/**
 * @brief       Holds the pin where the LED is connected
 */
//#define DANDELION_LED_START              (P0_21)
//#define DANDELION_LED_STOP               (P0_23)
#define DANDELION_LED                       (P0_25)

/**
 * @brief       Holds the pin where the user button is connected
 */
//#define DANDELION_BUTTONS_START          (P0_24)
//#define DANDELION_BUTTONS_STOP           (P0_25)
#define DANDELION_BUTTON                    (P0_29)

/**
 * @brief       Holds the pin for detection of connected label
 */

/**
 * @brief       Dandelion interrupt priority definitions
 */
#define DANDELION_IRQ_PRIORITY_HIGHEST   0
#define DANDELION_IRQ_PRIORITY_HIGH      1
#define DANDELION_IRQ_PRIORITY_MEDIUM    2
#define DANDELION_IRQ_PRIORITY_LOW       3

/**
 * @brief       Enumeration of the Dandelion colors
 * @details     The colors have a bit configuration for BGR color.
 */
//enum dandelion_color {
//    DANDELION_COLOR_NONE                 = 0,
//    DANDELION_COLOR_BLUE,
//    DANDELION_COLOR_GREEN,
//    DANDELION_COLOR_CYAN,
//    DANDELION_COLOR_RED,
//    DANDELION_COLOR_PINK,
//    DANDELION_COLOR_YELLOW,
//    DANDELION_COLOR_WHITE
//};

/**
 * @brief       Dandelion initialization function
 * @details     Initializes the Dandelion board by initializing:
 *                  1. The XTAL high and low-frequency clocks;
 *                  2. The GPIO pins for the system LEDs and user buttons;
 *                  3. The Power mode, DC/DC converter, etc.;
 */
void dandelion_init(void);

/**
 * @brief       Gets the current dandelion color
 * @returns     The current dandelion color.
 */
//enum dandelion_color dandelion_get_color(void);

/**
 * @brief       Sets the dandelion led power state
 * @details     The dandelion power state is the power given to the feedback LED.
 * @param[in]   on
 *              non zero or true to power on.
 */
//void dandelion_set_color(enum dandelion_color color);
void dandelion_led_power(uint8_t on);

/**
 * @brief       Reads the current state for the dandelion user buttons
 * @returns     The mask containing the state of each individual pin. The least
 *              significant bit holds the state of the start button and the
 *              remaining bits hold the state of the other buttons. A binary
 *              value of '1' indicates that a button is currently pressed,
 *              otherwise it is not pressed.
 */
uint32_t dandelion_read_button_state(void);

/**
 * @brief       Kill function
 * @details     This function may be called anywhere where an unexpected
 *              exception has occurred. The red light of death, identified by
 *              the DANDELION_COLOR_RED color is blinked.
 */
void dandelion_die(void);

/**
 * @brief       Sleep function
 * @details     Puts the Dandelion to sleep, waiting for an interrupt or event.
 */
void dandelion_sleep(void);

/**
 * @brief       Forced Sleep function
 * @details     Forces the Dandelion into sleep, waiting for an interrupt or event.
 */
void dandelion_sleep_force(void);

/**
 * @brief       Deep sleep function
 * @details     Puts the Dandelion into deep sleep mode. The only way to wake
 *              up from this mode is using an external reset interrupt.
 */
void dandelion_deep_sleep(void);

#endif /* DANDELION_H */
