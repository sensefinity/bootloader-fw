#ifndef __SVC_H__
#define __SVC_H__

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

typedef enum _test_state_t{
  TEST_PROCEED,
  TEST_FAIL,
  TEST_OK
}test_state_t;

#define SVC_NEW_FW            (0x00)
#define SVC_TEST_QUERY        (0x01)
#define SVC_TEST_RESULT       (0x02)
#define SVC_WRITE_DATA        (0x03)
#define SVC_RESET             (0x04)

void sv_call_new_fw(void);

bool sv_call_is_test_required(void);

void sv_call_test_result(test_state_t result);

int sv_call_write_data(char *address, char* data, size_t length);

void sv_call_reset(void);
#endif
