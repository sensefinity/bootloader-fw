/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     dev
 * @{
 *
 * @file        at25sfxx1.h
 * @brief       Implements a device driver for Adesto's family of Flash Memories
 *              based on the AT25SFxx1 series.
 *
 * @author      Tiago Tomás <tiago.tomas@sensefinity.com>
 *
 * @}
 */

#ifndef DEV_AT25SFxx1_H
#define DEV_AT25SFxx1_H

/* C standard library */
#include <stddef.h>
#include <stdint.h>

/* Peripherals */
#include "periph/pin_name.h"

/* Pins for SPI interface */
#define AT25SFxx1_NCS   (P0_01)
#define AT25SFxx1_SO    (P0_02)
#define AT25SFxx1_SI    (P0_03)
#define AT25SFxx1_SCK   (P0_04)

/* Definitions for minimum block and page sizes */
#define AT25SFxx1_BLOCK (4096UL)
#define AT25SFxx1_PAGE  (256UL)

/* Prototypes */
void at25sfxx1_init(void);
void at25sfxx1_fini(void);
size_t at25sfxx1_sizeof(void);
void at25sfxx1_read(uint32_t addr, void *buf, size_t nbytes);
void at25sfxx1_write(uint32_t addr, const void *buf, size_t nbytes);
void at25sfxx1_erase(void);

#endif /* DEV_AT25SFxx1_H */
