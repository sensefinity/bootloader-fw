/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     expansion
 * @{
 *
 * @file        expansion_apps.h
 * @brief       specific expansion_apps
 *
 * @author
 *
 * @}
 */

#ifndef EXPANSION_APPS_H
#define EXPANSION_APPS_H

/* C standard library */
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "expansion/exp_packet.h"

/*
 * APPLICATIONS messages payload
 * [APP ID |MESSAGE TYPE| ...]
 * */


/* Application messages specific fields */
#define EXPANSION_APP_ID                                (EX_PROTOCOL_HEADER_SIZE)
#define EXPANSION_APP_MESSAGE_TYPE                      (EXPANSION_APP_ID + 1)


/* Measurement */
#define EXPANSION_MEASUREMENT_PERIOD                    (EXPANSION_APP_MESSAGE_TYPE + 1)
/* Measurement applications messages types */
#define GET_MEASURE                                     (1)
#define SET_MEASURE_PERIOD                              (2)

/* Power management */
#define EXPANSION_POWER_MANAGEMENT_ORDER                (EXPANSION_APP_MESSAGE_TYPE + 1)
#define ORDER											(1)

/* ORDER types */
#define TURN_OFF										(0)
#define TURN_ON											(1)


#define APP_PING_ORDER 									(1)

/* Aplication ID */
void expansion_apps_set_app_id(uint8_t * msg, uint8_t app_id);
uint8_t expansion_apps_get_app_id(uint8_t * msg);

/* Specific application fields */

/* Message type */
void expansion_apps_set_type(uint8_t * msg, uint8_t message_type);
uint8_t expansion_apps_get_type(uint8_t * msg);

/* MEASUREMENT */
void expansion_measurement_set_period(uint8_t * msg, uint64_t period);
uint64_t expansion_measurement_get_period(uint8_t * msg);

/* POWER MANAGEMENT */
void expansion_pm_set_order(uint8_t * msg, uint8_t period);
uint8_t expansion_pm_get_order(uint8_t * msg);

#endif /* EXPANSION_APPS_H */
