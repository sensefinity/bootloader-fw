/**
 * Copyright (c) 2016 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @file        expansion.h
 * @brief       Manage expansion protocol and services
 * @author      Rui Pires <rui.pires@sensefinity.com>
 */
#ifndef EXPANSION_H
#define EXPANSION_H

/* Circular Queue */
#include "sensoroid/c_queue.h"

/* Services look up period */
#define EXPANSION_SERVICES_LOOK_UP_PERIOD               (1800000)

/* Retry timeout based on connection status */
#define EXPANSION_RETRY_NORMAL_TIMEOUT					(1000)
#define EXPANSION_RETRY_NACK_TIMEOUT					(60000)
#define EXPANSION_RETRY_CONNECTION_FAIL_TIMEOUT			(1800000)

/* Do work on expansion messages received */
void dispatcher_expansion_rx(void);


/*
 *  Do expansion work, dispatch expansion received messages, send expansion messages
 *  present in the expansion_tx_queue and perform service look up if necessary
*/
void expansion_work(uint64_t t_now);

void expansion_init(void);

circular_queue_t * get_exp_tx_queue_pointer(void);
circular_queue_t * get_exp_rx_queue_pointer(void);
bool expansion_tx_status_ok(void);

#endif /* EXPANSION_H */
