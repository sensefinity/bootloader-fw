/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     sensoroid
 * @{
 *
 * @file        up-link.h
 * @brief       up-link app
 *
 * @author
 *
 * @}
 */

#ifndef UPLINK_H
#define UPLINK_H


/* C standard library */
#include <stdint.h>
#include <string.h>
#include <stddef.h>
#include <stdbool.h>

#include "expansion/exp_packet.h"

/* Message Format*/
#define UPLINK_MESSAGE_DIRECTION						(EX_PROTOCOL_HEADER_SIZE)
#define	UPLINK_MESSAGE_SEQ_NUMBER						(UPLINK_MESSAGE_DIRECTION + 1)
#define UPLINK_MESSAGE_COLUMBUS_INIT					(UPLINK_MESSAGE_SEQ_NUMBER + 1)

#define UPLINK_SUB_HEADER_SIZE							(2)

/* Message direction */
#define UPLINK_FORWARD							(0x55)
#define UPLINK_ACK								(0xAA)

/* TODO */
#define UPLINK_SEND_RETRY_PERIOD				(10000)


void uplink_set_direction(uint8_t * msg, uint8_t direction);
uint8_t uplink_get_direction(uint8_t * msg);

void uplink_set_seq_number(uint8_t * msg, uint8_t seq_numb);
uint8_t uplink_get_seq_number(uint8_t * msg);

void uplink_copy_columbus_message(uint8_t * msg, uint8_t * columbus ,size_t length);
bool uplink_is_ack(uint8_t *msg);
#endif /* UPLINK_H */
