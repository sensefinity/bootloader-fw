/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     sensoroid
 * @{
 *
 * @file        expansion_protocol.h
 * @brief       Implements the uart comunication protocol for comunnication
 *              with the expansion boards.
 *
 * @author      Rui Pires
 *
 * @}
 */

/* Peripherals */
#include "periph/pin_name.h"

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>

/* UART RX TX PINS OF BLUE PEN*/
//#define EXPANSION_UART_TXD          (P0_09)
//#define EXPANSION_UART_RXD          (P0_11)

/* UART RX TX PINS*/

#define EXPANSION_UART_TXD          (P0_11)
#define EXPANSION_UART_RXD          (P0_10)

#define EX_SYNC_PIN                 (P0_07)

/* Receive ACK timeout */
#define EX_PROTOCOL_ACK_RECEIVE_TIMEOUT                    (1000)

 /**
  * @brief       Setup communication with the first expansion board.
  */
 void expansion_protocol_init(void);

 /**
  * @brief       Function designed to be run in a loop for each time the
  *              processor wakes up.
  */
 void expansion_protocol_loop(void);

 bool expansion_protocol_send_message(uint8_t * msg, size_t msg_size);

 bool expansion_receive_ack(uint64_t timeout);



