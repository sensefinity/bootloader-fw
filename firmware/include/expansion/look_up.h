/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     sensoroid
 * @{
 *
 * @file        look_up.h
 * @brief       Look_up services
 *
 * @author
 *
 * @}
 */

#ifndef LOOK_UP_H
#define LOOK_UP_H



/* C standard library */
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

enum look_up_msg_type {
    look_up_query,
    look_up_response
};

#define LOOK_UP_MESSAGE_TYPE                        (EX_PROTOCOL_HEADER_SIZE)
#define LOOK_UP_NUNBER_OF_SERVICES                  (LOOK_UP_MESSAGE_TYPE + 1)
#define LOOK_UP_SERVICES_LIST_START                 (LOOK_UP_NUNBER_OF_SERVICES + 1)

bool service_look_up(uint8_t service);
void look_up_message_receive(uint8_t * msg);
bool services_look_up(uint8_t * list, size_t number_of_services);

#endif /* LOOK_UP_H */
