/**
 * Copyright (c) 2015 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     expansion
 * @{
 *
 * @file        expansion_definitions.h
 * @brief       specific expansion definitions shoud be on base and expansion boards.
 *
 * @}
 */

#ifndef EXPANSION_DEFINITIONS_H
#define EXPANSION_DEFINITIONS_H

#include "expansion/exp_packet.h"


/* Expansion message types */
#define EX_PROTOCOL_LOOK_UP_MESSAGE                        (1)
#define EX_PROTOCOL_COLUMBUS_MESSAGE                       (2)
#define EX_PROTOCOL_APP_MESSAGE                            (3)
#define EX_PROTOCOL_UPLINK_MESSAGE						   (4)

/* Expansion applications ids */
#define EX_PROTOCOL_APP_GSM                                 (1)
#define EX_PROTOCOL_APP_TEMPERATURE                         (2)
#define EX_PROTOCOL_APP_HUMIDITY                            (3)
#define EX_PROTOCOL_APP_POWER_MANAGEMENT                    (4)



static const  uint64_t broadcast_serial = 0x00;

#endif /* EXPANSION_DEFINITIONS_H */
