/**
 * Copyright (c) 2016 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @file        exp_packet.h
 * @brief       Utils to create/manage expansion protocol packets
 * @author      Rui Pires <rui.pires@sensefinity.com>
 */
#ifndef EXP_PACKET_H
#define EXP_PACKET_H

/* C standard library */
#include <stdint.h>
#include <stdbool.h>


/**
 * @brief       General expansion protocol header

static uint8_t _expansion_protocol_header[19] = {
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,    // Serial destination
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,    // Serial origin
    0x00,                                              // Reserved + ack
    0x00,                                              // Message size
    0x00                                               // Message type
};
*/

/* UART message header fields */
#define EX_PROTOCOL_HEADER_ORIGIN_SERIAL                   (0)
#define EX_PROTOCOL_HEADER_DST_SERIAL                      (8)
#define EX_PROTOCOL_HEADER_ACK_BYTE                        (16)
#define EX_PROTOCOL_HEADER_MSG_LENGTH                      (17)
#define EX_PROTOCOL_HEADER_MESSAGE_TYPE                    (18)

/* Sizes definitions */
#define EX_PROTOCOL_PREAMBLE_SIZE                          (2)
#define EX_PROTOCOL_HEADER_SIZE                            (19)
#define EX_PROTOCOL_CRC_SIZE                               (2)
#define EX_PROTOCOL_ACK_SIZE                               (1)

/**
* @brief       Defines the maximum size for each expansion  message
*/
#define EXP_PROTOCOL_MSG_MAX_SIZE  (100 + EX_PROTOCOL_HEADER_SIZE + 10)  /* Columbus MAX size + expansion header + Preamble crc*/

/* Masks */
#define EX_PROTOCOL_HEADER_ACK_MASK                        (0x01)
#define EX_PROTOCOL_HEADER_FROM_BASE_MASK                  (0x02)

/* Special characters */
#define EX_PROTOCOL_PREAMBLE_CHARACTER                     (0xAA)
#define ASCII_EOT                                          (0x04)


/* Set and get specific fields of the expansion packet */

/* Origin serial */
void exp_set_origin_serial(uint8_t *message, uint64_t serial);
uint64_t exp_get_origin_serial(uint8_t *message);

/* Destination serial */
void exp_set_dst_serial(uint8_t *message, uint64_t serial);
uint64_t exp_get_dst_serial(uint8_t *message);

/* Reserved Byte */
void exp_set_message_ack(uint8_t *message);
bool exp_is_ack_message(uint8_t * message);

void exp_set_message_from_base(uint8_t *message);
bool exp_is_message_from_base(uint8_t *message);

/* Size */
void exp_set_message_size(uint8_t *message, uint8_t size);
uint8_t exp_get_message_size(uint8_t *message);

/* App id */
void exp_set_message_type(uint8_t *message, uint8_t type);
uint8_t exp_get_message_app_id(uint8_t *message);

#endif /* EXP_PACKET_H */
