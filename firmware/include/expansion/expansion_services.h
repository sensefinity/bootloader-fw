/**
 * Copyright (c) 2016 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @file        expansion_services.h
 * @brief       Utils to create/manage expansion protocol packets
 * @author      Rui Pires <rui.pires@sensefinity.com>
 */
#ifndef EXPANSION_SERVICES_H
#define EXPANSION_SERVICES_H

/* C standard library */
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>



#define MAX_SERVICES                                        (10)


/**
 * @brief       Defines the common structure for services.
 */
struct expansion_service {
    uint8_t service_id;
    bool active;
    uint64_t serial;
    uint64_t last_look_up;
};


struct my_services {
    uint8_t list[MAX_SERVICES];
    uint8_t number_of_services;
};


bool expansion_service_is_enabled(uint8_t service);
bool activate_service(uint8_t service);
void run_services_look_up(void);

bool expansion_service_is_active(uint8_t service);
bool expansion_services_all_active(void);

void expansion_service_add(uint8_t service, uint64_t serial);
uint64_t expansion_services_get_serial(uint8_t service);

#endif /* EXPANSION_SERVICES_H */
