/**
 * Copyright (c) 2016 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     sensors
 * @{
 *
 * @file        MPL3115A2.h
 * @brief       MPL3115A2 drivers prototype
 *
 * @author      Tiago Nascimento <tiago.nascimento@sensefinity.com>
 *
 * @}
 */

#ifndef MPL3115A2_H
#define MPL3115A2_H

/* Standard C library */
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>
#include <stddef.h>

/* Peripherals */
#include "periph/i2c.h"
#include "periph/pin_name.h"

/* Utilities */
#include "util/debug.h"

#define MPL3115A2_ADDR						0x60   /* 7 bit address */

#define MPL3115A2_SCL             P0_00  /* PIN connected with the SLC I2C bus */
#define MPL3115A2_SDA             P0_01  /* PIN connected with the SDA I2C bus */

struct MPL3115A2_data {
	int32_t press_alt;       													/** < 20 bit pressure value in Pa or 20 bit altitude(meters) value in Q16.4 format */
	int16_t temperature;   													  /** < 12 bit temperature value in ºC */
	uint8_t status;         												  /** < Sensor status register value */
};

/**********************************************************************************************
                                  Registers List
***********************************************************************************************/

#define MPL3115A2_STATUS_REG												0x00 /* Alias for DR_STATUS or F_STATUS */
#define MPL3115A2_OUT_P_MSB_REG											0x01 /* bits [19:12] of 20 bit real-time pressure or altitude sample */
#define MPL3115A2_OUT_P_CSB_REG											0x02 /* bits [11:4] of 20 bit real-time pressure or altitude sample */
#define MPL3115A2_OUT_P_LSB_REG											0x03 /* bits [3:0] of 20 bit real-time pressure or altitude sample */
#define MPL3115A2_OUT_T_MSB_REG											0x04 /* bits [11:4] of 12 bit real-time temperature sample */
#define MPL3115A2_OUT_T_LSB_REG											0x05 /* bits [3:0] of 12 bit real-time temperature sample */
#define MPL3115A2_DR_STATUS_REG											0x06 /* Data Ready status information */
#define MPL3115A2_OUT_P_DELTA_MSB_REG								0x07 /* bits [19:12] of 20 bit pressure or altitude change data */
#define MPL3115A2_OUT_P_DELTA_CSB_REG								0x08 /* bits [11:4] of 20 bit pressure or altitude change data */
#define MPL3115A2_OUT_P_DELTA_LSB_REG								0x09 /* bits [3:0] of 20 bit pressure or altitude change data */
#define MPL3115A2_OUT_T_DELTA_MSB_REG								0x0A /* bits [11:4] of 12 bit temperature change data  */
#define MPL3115A2_OUT_T_DELTA_LSB_REG								0x0B /* bits [3:0] of 12 bit temperature change data */
#define MPL3115A2_WHO_AM_I_REG											0x0C /* Fixed Device ID number */
#define MPL3115A2_F_STATUS_REG											0x0D /* FIFO status */
#define MPL3115A2_F_DATA_REG												0x0E /* FIFO 8-bit data access */
#define MPL3115A2_F_SETUP_REG												0x0F /* FIFO setup */
#define MPL3115A2_TIME_DLY_REG											0x10 /* Time since FIFO overflow */
#define MPL3115A2_SYSMOD_REG												0x11 /* Current System mode */
#define MPL3115A2_INT_SOURCE_REG										0x12 /* Interrupt status */
#define MPL3115A2_PT_DATA_CFG_REG										0x13 /* Data event flag configuration */
#define MPL3115A2_BAR_IN_MSB_REG										0x14 /* bits [15:8] of 16 bit Barometric input for Altitude calculation */
#define MPL3115A2_BAR_IN_LSB_REG										0x15 /* bits [7:0] of 16 bit Barometric input for Altitude calculation */
#define MPL3115A2_P_TGT_MSB_REG											0x16 /* bits [15:8] of 16 bit Pressure or altitude target value */
#define MPL3115A2_P_TGT_LSB_REG											0x17 /* bits [7:0] of 16 bit Presure or altitude target value */
#define MPL3115A2_T_TGT_REG													0x18 /* Temperature target value */
#define MPL3115A2_P_WND_MSB_REG											0x19 /* bits [15:8] of 16 bit Pressure or altitude window value */
#define MPL3115A2_P_WND_LSB_REG											0x1A /* bits [7:0] of 16 bit Pressure or altitude window value */
#define MPL3115A2_T_WND_REG													0x1B /* Temperature window value */
#define MPL3115A2_P_MIN_MSB_REG											0x1C /* bits [19:12] of 20 bit Pressure or altitude minimum value */
#define MPL3115A2_P_MIN_CSB_REG											0x1D /* bits [11:4] of 20 bit Pressure or altitude minimum value */
#define MPL3115A2_P_MIN_LSB_REG											0x1E /* bits [3:0] of 20 bit Pressure or altitude minimum value */
#define MPL3115A2_T_MIN_MSB_REG											0x1F /* bits [15:8] of 16 bit Temperature minimum value */
#define MPL3115A2_T_MIN_LSB_REG											0x20 /* bits [7:0] of 16 bit Temperature minimum value */
#define MPL3115A2_P_MAX_MSB_REG											0x21 /* bits [19:12] of 20 bit Pressure or altitude maximum value  */
#define MPL3115A2_P_MAX_CSB_REG											0x22 /* bits [19:12] of 20 bit Pressure or altitude maximum value  */
#define MPL3115A2_P_MAX_LSB_REG											0x23 /* bits [19:12] of 20 bit Pressure or altitude maximum value  */
#define MPL3115A2_T_MAX_MSB_REG											0x24 /* bits [15:8] of 16 bit Temperature maximum value  */
#define MPL3115A2_T_MAX_LSB_REG											0x25 /* bits [7:0] of 16 bit Temperature maximum value  */
#define MPL3115A2_CTRL1_REG													0x26 /* Modes, Oversampling */
#define MPL3115A2_CTRL2_REG													0x27 /* Aquisition time step */
#define MPL3115A2_CTRL3_REG													0x28 /* Interrupt pin configuration */
#define MPL3115A2_CTRL4_REG													0x29 /* Interrupt enables */
#define MPL3115A2_CTRL5_REG													0x2A /* Interrupt output pin assignment */
#define MPL3115A2_OFF_P_REG													0x2B /* Pressure data offset */
#define MPL3115A2_OFF_T_REG													0x2C /* Temperature data offset */
#define MPL3115A2_OFF_H_REG													0x2D /* Altitude dta offset */


/**********************************************************************************************
                                  Bit masks and definitions
***********************************************************************************************/

/** Data Ready Status Register **/
#define MPL3115A2_DR_STATUS_PTOW_MASK								0x80
#define MPL3115A2_DR_STATUS_POW_MASK								0x40
#define MPL3115A2_DR_STATUS_TOW_MASK								0x20
#define MPL3115A2_DR_STATUS_PTDR_MASK								0x08
#define MPL3115A2_DR_STATUS_PDR_MASK								0x04
#define MPL3115A2_DR_STATUS_TDR_MASK								0x02

#define MPL3115A2_DR_STATUS_PTOW_SHIFT							7
#define MPL3115A2_DR_STATUS_POW_SHIFT								6
#define MPL3115A2_DR_STATUS_TOW_SHIFT								5
#define MPL3115A2_DR_STATUS_PTDR_SHIFT							3
#define MPL3115A2_DR_STATUS_PDR_SHIFT								2
#define MPL3115A2_DR_STATUS_TDR_SHIFT								1

/** Pressure Output Registers **/
#define MPL3115A2_OUT_P_LSB_MASK										0x0F
#define MPL3115A2_OUT_T_LSB_MASK										0x0F
#define MPL3115A2_OUT_P_LSB_SHIFT										4
#define MPL3115A2_OUT_P_TO_INT32(m,c,l)							((int32_t)((((int32_t)m)<<16)|(((uint32_t)c)<<8)|((uint32_t)l))>>MPL3115A2_OUT_SHIFT)
#define MPL3115A2_OUT_T_TO_INT16(m,l)								((int16_t)((((int16_t)m)<<8)|((uint16_t)l)))
#define MPL3115A2_OUT_H_TO_INT32(m,c,l)							((int32_t)((((int32_t)m)<<16)|(((uint32_t)c)<<8)|((uint32_t)l))>>MPL3115A2_OUT_SHIFT)

/** WHO_AM_I Register **/
#define MPL3115A2_WHO_AM_I													0xC4

/** FIFO status Register **/
#define MPL3115A2_F_OVF_MASK												0x80
#define MPL3115A2_F_WMRK_FLAG_MASK									0x40
#define MPL3115A2_F_CNT_MASK												0x3F

#define MPL3115A2_F_OVF_SHIFT												7
#define MPL3115A2_F_WMRK_FLAG_SHIFT									6
#define MPL3115A2_F_CNT_SHIFT												0

/** FIFO Data Register **/

/** FIFO Setup Register **/
#define MPL3115A2_F_SETUP_F_MODE_MASK								0xC0
#define MPL3115A2_F_SETUP_F_WMRK_MASK								0x3F

#define MPL3115A2_F_SETUP_F_MODE_SHIFT							6
#define MPL3115A2_F_SETUP_F_WMRK_SHIFT							0

#define MPL3115A2_F_SETUP_F_MODE_FIFO_DISABLED			0x00
#define MPL3115A2_F_SETUP_F_MODE_FIFO_OVERWRITE			0x40
#define MPL3115A2_F_SETUP_F_MODE_FIFO_NO_OVERWRITE	0x80

/** Time Delay Register **/
#define MPL3115A2_TIME_DLY_MASK											0xFF

/** System Mode Register **/
#define MPL3115A2_SYSMOD_SYSMOD_MASK								0x01
#define MPL3115A2_SYSMOD_SYSMOD_SHIFT								0

/** System Interrupt Status **/
#define MPL3115A2_INT_SOURCE_SRC_DRDY_MASK					0x80
#define MPL3115A2_INT_SOURCE_SRC_FIFO_MASK					0x40
#define MPL3115A2_INT_SOURCE_SRC_PW_MASK						0x20
#define MPL3115A2_INT_SOURCE_SRC_TW_MASK						0x10
#define MPL3115A2_INT_SOURCE_SRC_PTH_MASK						0x08
#define MPL3115A2_INT_SOURCE_SRC_TTH_MASK						0x04
#define MPL3115A2_INT_SOURCE_SRC_PCHG_MASK					0x02
#define MPL3115A2_INT_SOURCE_SRC_TCHG_MASK					0x01

#define MPL3115A2_INT_SOURCE_SRC_DRDY_SHIFT					7
#define MPL3115A2_INT_SOURCE_SRC_FIFO_SHIFT					6
#define MPL3115A2_INT_SOURCE_SRC_PW_SHIFT						5
#define MPL3115A2_INT_SOURCE_SRC_TW_SHIFT						4
#define MPL3115A2_INT_SOURCE_SRC_PTH_SHIFT					3
#define MPL3115A2_INT_SOURCE_SRC_TTH_SHIFT					2
#define MPL3115A2_INT_SOURCE_SRC_PCHG_SHIFT					1
#define MPL3115A2_INT_SOURCE_SRC_TCHG_SHIFT					0

/** Pressure and Temperature Data Configuration **/
#define MPL3115A2_PT_DATA_CFG_DREM_MASK							0x04
#define MPL3115A2_PT_DATA_CFG_PDEFE_MASK						0x02
#define MPL3115A2_PT_DATA_CFG_TDEFE_MASK						0x01

#define MPL3115A2_PT_DATA_CFG_DREM_SHIFT						2
#define MPL3115A2_PT_DATA_CFG_PDEFE_SHIFT						1
#define MPL3115A2_PT_DATA_CFG_TDEFE_SHIFT						0

/** Barometric Input for Altitude Calculations Register **/

/** Control 1 Register **/
#define MPL3115A2_CTRL1_ALT_MASK										0x80
#define MPL3115A2_CTRL1_RAW_MASK										0x40
#define MPL3115A2_CTRL1_OS_MASK											0x38
#define MPL3115A2_CTRL1_RST_MASK										0x04
#define MPL3115A2_CTRL1_OST_MASK										0x02
#define MPL3115A2_CTRL1_SBYB_MASK										0x01

#define MPL3115A2_CTRL1_ALT_SHIFT										7
#define MPL3115A2_CTRL1_RAW_SHIFT										6
#define MPL3115A2_CTRL1_OS_SHIFT										3
#define MPL3115A2_CTRL1_RST_SHIFT										2
#define MPL3115A2_CTRL1_OST_SHIFT										1
#define MPL3115A2_CTRL1_SBYB_SHIFT									0

#define MPL3115A2_CTRL1_OS_1												0x00
#define MPL3115A2_CTRL1_OS_2												0x08
#define MPL3115A2_CTRL1_OS_4												0x10
#define MPL3115A2_CTRL1_OS_8												0x18
#define MPL3115A2_CTRL1_OS_16												0x20
#define MPL3115A2_CTRL1_OS_32												0x28
#define MPL3115A2_CTRL1_OS_64												0x30
#define MPL3115A2_CTRL1_OS_128											0x38

/** Control 2 Register **/
#define MPL3115A2_CTRL2_LOAD_OUTPUT_MASK						0x20
#define MPL3115A2_CTRL2_ALARM_SEL_MASK							0x10
#define MPL3115A2_CTRL2_ST_MASK											0x0F

#define MPL3115A2_CTRL2_LOAD_OUTPUT_SHIFT						5
#define MPL3115A2_CTRL2_ALARM_SEL_SHIFT							4
#define MPL3115A2_CTRL2_ST_SHIFT										0

/** Control 3 Register **/
#define MPL3115A2_CTRL3_IPOL1_MASK									0x20
#define MPL3115A2_CTRL3_IPOL2_MASK									0x02
#define MPL3115A2_CTRL3_PP_OD1_MASK									0x10
#define MPL3115A2_CTRL3_PP_OD2_MASK									0x01

#define MPL3115A2_CTRL3_IPOL1_SHIFT									5
#define MPL3115A2_CTRL3_IPOL2_SHIFT									4
#define MPL3115A2_CTRL3_PP_OD1_SHIFT								2
#define MPL3115A2_CTRL3_PP_OD2_SHIFT								1

/** Control 4 Register **/
#define MPL3115A2_CTRL4_INT_EN_DRDY_MASK						0x80
#define MPL3115A2_CTRL4_INT_EN_FIFO_MASK						0x40
#define MPL3115A2_CTRL4_INT_EN_PW_MASK							0x20
#define MPL3115A2_CTRL4_INT_EN_TW_MASK							0x10
#define MPL3115A2_CTRL4_INT_EN_PTH_MASK							0x08
#define MPL3115A2_CTRL4_INT_EN_TTH_MASK							0x04
#define MPL3115A2_CTRL4_INT_EN_PCHG_MASK						0x02
#define MPL3115A2_CTRL4_INT_EN_TCHG_MASK						0x01

#define MPL3115A2_CTRL4_INT_EN_DRDY_SHIFT						7
#define MPL3115A2_CTRL4_INT_EN_FIFO_SHIFT						6
#define MPL3115A2_CTRL4_INT_EN_PW_SHIFT							5
#define MPL3115A2_CTRL4_INT_EN_TW_SHIFT							4
#define MPL3115A2_CTRL4_INT_EN_PTH_SHIFT						3
#define MPL3115A2_CTRL4_INT_EN_TTH_SHIFT						2
#define MPL3115A2_CTRL4_INT_EN_PCHG_SHIFT						1
#define MPL3115A2_CTRL4_INT_EN_TCHG_SHIFT						0

/** Control 5 Register **/
#define MPL3115A2_CTRL_INT_CFG_DRDY_MASK						0x80
#define MPL3115A2_CTRL_INT_CFG_FIFO_MASK						0x40
#define MPL3115A2_CTRL_INT_CFG_PW_MASK							0x20
#define MPL3115A2_CTRL_INT_CFG_TW_MASK							0x10
#define MPL3115A2_CTRL_INT_CFG_PTH_MASK							0x08
#define MPL3115A2_CTRL_INT_CFG_TTH_MASK							0x04
#define MPL3115A2_CTRL_INT_CFG_PCHG_MASK						0x02
#define MPL3115A2_CTRL_INT_CFG_TCHG_MASK						0x01


#define MPL3115A2_CTRL_INT_CFG_DRDY_SHIFT						7
#define MPL3115A2_CTRL_INT_CFG_FIFO_SHIFT						6
#define MPL3115A2_CTRL_INT_CFG_PW_SHIFT							5
#define MPL3115A2_CTRL_INT_CFG_TW_SHIFT							4
#define MPL3115A2_CTRL_INT_CFG_PTH_SHIFT						3
#define MPL3115A2_CTRL_INT_CFG_TTH_SHIFT						2
#define MPL3115A2_CTRL_INT_CFG_PCHG_SHIFT						1
#define MPL3115A2_CTRL_INT_CFG_TCHG_SHIFT						0

/* Prototypes */

/**
 * @brief       Test if the I2C bus is free and try to communicate with the sensor.
 * 				If everything is ok, reserve the I2C bus.
 * @details     This will initialize the I2C driver to communicate with the
 *				MPL3115A2 sensor. It will test if the MCU is getting the right
 *				answer from the sensor, testing this way the I2C communication.
 *				If the I2C isn't free it will return false.
 */
bool mpl3115a2_reserve_i2c_bus(void);

/**
 * @brief       Turn on the MPL3115A2 sensor
 * @details     This function will put the MMA8652 sensor in a standby mode by
 *				setting to 1 the SBYB (bit 0) of CTRL_REG_1.
 */
bool mpl3115a2_power_on(void);

/**
 * @brief       Put the MPL3115A2 in a standby mode
 * @details     This function will put the MMA8652 sensor in a standby mode by
 *				setting to 0 the SBYB (bit 0) of CTRL_REG_1.
 */
bool mpl3115a2_power_off(void);

/**
 * @brief       Put MPL3115A2 in barometer mode
 */
bool mpl3115a2_barometer_enable();

/**
 * @brief       Put MPL3115A2 in altimeter mode
 */
bool mpl3115a2_altimeter_enable();

/**
 * @brief       Enable or disable MPL3115A2 raw mode
 * @param		raw_enable
 * 				True for enable raw mode, false to disable it.
 */
bool mpl3115a2_set_raw(bool raw_enable);

/**
 * @brief       Set the output sample rate of the sensor
 * @details     Set the bit 3, 4 and 5 of the MPL3115A2_CTRL1 register.
 * @param[in]   output_rate
 *              The output sample rate. Possible values:
 *							- MPL3115A2_CTRL1_OS_1;
 *							- MPL3115A2_CTRL1_OS_2;
 *							- MPL3115A2_CTRL1_OS_4;
 *							- MPL3115A2_CTRL1_OS_8;
 *							- MPL3115A2_CTRL1_OS_16;
 *							- MPL3115A2_CTRL1_OS_32;
 *							- MPL3115A2_CTRL1_OS_64;
 *							- MPL3115A2_CTRL1_OS_128.
 */
bool mpl3115a2_set_output_rate(uint8_t output_rate);

/**
 * @brief       Check if there is new data available from the MPL3115a2 sensor
 * @details     If there is new data available it will set to 1 the status field
 * 							of the MPL3115A2 data structure.
 */
bool mpl3115a2_new_data_available(void);

/**
 * @brief       Read the new preassure data
 * @details     It will get the new preassure data from the MPL3115A2 sensor and
 *							update the pressure field of the MPL3115A2 data structure with this
 *							value.
 */
struct MPL3115A2_data mpl3115a2_read_preassure(void);

/**
 * @brief       Read the new temperature data
 * @details     It will get the new temperature data from the MPL3115A2 sensor and
 *							update the temperature field of the MPL3115A2 data structure with this
 *							value.
 */
struct MPL3115A2_data mpl3115a2_read_temperature(void);

/**
 * @brief       All reads in one read. Read the new temperature and preassure data
 * @details     It will get the new temperature and preassure data from the MPL3115A2 sensor
 *						  and update the temperature field and preassure field of the MPL3115A2 data
 *						  structure with these values respectivily.
 */
struct MPL3115A2_data mpl3115a2_read_data(void);

/**
 * @brief       Returns the MPL3115A2 data structure
 * @details     Returns the MPL3115A2 data structure that contains the last
 *							values for the pressure, temperature and status of the sensor.
 */
struct MPL3115A2_data mpl3115a2_get_data(void);

#endif /* MPL3115A2_H */
