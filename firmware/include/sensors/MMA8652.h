/**
 * Copyright (c) 2016 Sensefinity
 *
 * This file is subject to the Sensefinity private code license. See the file
 * LICENSE in the top level directory for more details.
 */

/**
 * @ingroup     sensors
 * @{
 *
 * @file        MMA8652.h
 * @brief       MMA8652 drivers prototype
 *
 * @author      Tiago Nascimento <tiago.nascimento@sensefinity.com>
 *
 * @}
 */

#ifndef MMA8652_H
#define MMA8652_H

/* Standard C library */
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>
#include <stddef.h>

/* Peripherals */
#include "periph/i2c.h"
#include "periph/pin_name.h"

/* Utilities */
#include "util/debug.h"

#define MMA8652_ADDR						0x1D   /* 7 bit address */

#define MMA8652_SCL             P0_00  /* PIN connected with the SLC I2C bus */
#define MMA8652_SDA             P0_01  /* PIN connected with the SDA I2C bus */

enum interrupt_pin {
  MMA8652_INT_1 = 0,
  MMA8652_INT_2 = 1,
};

struct MMA8652_xyz_data {
	int16_t x_raw;
	int16_t y_raw;
	int16_t z_raw;
	int16_t x_mg;
	int16_t y_mg;
	int16_t z_mg;
	bool status;
};

struct MMA8652_motion_data {
  bool x_motion;
  bool x_polarity;
  bool y_motion;
  bool y_polarity;
  bool z_motion;
  bool z_polarity;
	bool status;
};

struct MMA8652_transient_data {
  bool x_transient;
  bool x_polarity;
  bool y_transient;
  bool y_polarity;
  bool z_transient;
  bool z_polarity;
	bool status;
};

struct MMA8652_orientation_data {
  bool front;
  bool landscape_right;
  bool portrait_up;
	bool status;
};

/**********************************************************************************************
                                  Registers List
***********************************************************************************************/

#define MMA8652_STATUS_REG																0x00 		/* Real time status when FMODE = 0 */
#define MMA8652_F_STATUS_REG															0x00 		/* FIFO status when FMODE != 0 */
#define MMA8652_OUT_X_MSB_REG															0x01 		/* [7:0] are 8 MSB bits of 12 bit X axis sample */
#define MMA8652_OUT_X_LSB_REG															0x02 		/* [7:4] are 4 LSB bits of 12 bit X axis sample */
#define MMA8652_OUT_X_LSB_SHIFT														4
#define MMA8652_OUT_Y_MSB_REG															0x03 		/* [7:0] are 8 MSB bits of 12 bit Y axis sample */
#define MMA8652_OUT_Y_LSB_REG															0x04 		/* [7:4] are 4 LSB bits of 12 bit Y axis sample */
#define MMA8652_OUT_Y_LSB_SHIFT														4
#define MMA8652_OUT_Z_MSB_REG															0x05 		/* [7:0] are 8 MSB bits of 12 bit Z axis sample */
#define MMA8652_OUT_Z_LSB_REG															0x06 		/* [7:4] are 4 LSB bits of 12 bit Z axis sample */
#define MMA8652_OUT_Z_LSB_SHIFT														4

/* RESERVED 																							0x07 */
/* RESERVED 																							0x08 */

#define MMA8652_F_SETUP_REG																0x09 		/* FIFO setup */
#define MMA8652_TRIG_CFG_REG															0x0A 		/* Map of FIFO data capture events */
#define MMA8652_SYSMOD_REG																0x0B 		/* Current system mode */
#define MMA8652_INT_SOURCE_REG														0x0C 		/* Interrupt status */
#define MMA8652_WHO_AM_I_REG															0x0D 		/* Device ID (0x4A) */
#define MMA8652_XYZ_DATA_CFG_REG													0x0E 		/* Dynamic range settings */
#define MMA8652_HP_FILTER_CUTOFF_REG											0x0F 		/* High pass filter selection */

#define MMA8652_PL_STATUS_REG															0x10 		/* Portrait-landscape orientation status */
#define MMA8652_PL_CFG_REG																0x11 		/* Portrait-landscape configuration */
#define MMA8652_PL_COUNT_REG															0x12 		/* Portrait-landscape debounce counter */
#define MMA8652_PL_BF_ZCOMP_REG														0x13 		/* Back/Front, Z-lock Trip threshold */
#define MMA8652_P_L_THS_REG																0x14 		/* Portrait-landscape threshold and hysteresis */

#define MMA8652_FF_MT_CFG_REG															0x15 		/* Freefall-Motion functional block configuration */
#define MMA8652_FF_MT_SRC_REG															0x16 		/* Freefall-Motion event source */
#define MMA8652_FF_MT_THS_REG															0x17 		/* Freefall-Motion threshold */
#define MMA8652_FF_MT_COUNT_REG														0x18 		/* Freefall-Motion debounce counter */

/* RESERVED 																							0x19 */
/* RESERVED 																							0x1A */
/* RESERVED 																							0x1B */
/* RESERVED 																							0x1C */

#define MMA8652_TRANSIENT_CFG_REG													0x1D 		/* Transient functional block configuration */
#define MMA8652_TRANSIENT_SRC_REG													0x1E 		/* Transient event status register */
#define MMA8652_TRANSIENT_THS_REG													0x1F 		/* Transient event threshold */
#define MMA8652_TRANSIENT_COUNT_REG												0x20 		/* Transient debounce counter */

#define MMA8652_PULSE_CFG_REG															0x21 		/* Pulse enable configuration */
#define MMA8652_PULSE_SRC_REG															0x22 		/* Pulse detection source */
#define MMA8652_PULSE_THSX_REG														0x23 		/* X pulse threshold */
#define MMA8652_PULSE_THSY_REG														0x24 		/* Y pulse threshold */
#define MMA8652_PULSE_THSZ_REG														0x25 		/* Z pulse threshold */
#define MMA8652_PULSE_TMLT_REG														0x26 		/* Time limit for pulse */
#define MMA8652_PULSE_LTCY_REG														0x27 		/* Latency time for 2nd pulse */
#define MMA8652_PULSE_WIND_REG														0x28 		/* Window time for 2nd pulse */

#define MMA8652_ASLP_COUNT_REG														0x29 		/* Counter setting for Auto-SLEEP */

#define MMA8652_CTRL_1_REG																0x2A 		/* Data rate and mode setting */
#define MMA8652_CTRL_2_REG																0x2B	  /* Sleep enable, OverSampling modes, Reset, ST */
#define MMA8652_CTRL_3_REG																0x2C 		/* Wake from sleep, IPOL, PP_OD */
#define MMA8652_CTRL_4_REG																0x2D 		/* Interrupt enable register */
#define MMA8652_CTRL_5_REG																0x2E 		/* Interupt pin (INT1,INT2) map */

#define MMA8652_OFF_X_REG																	0x2F	  /* X-axis offset adjustment */
#define MMA8652_OFF_Y_REG																	0x30 		/* Y-axis offset adjustment */
#define MMA8652_OFF_Z_REG																	0x31 		/* Z-axis offset adjustment */

/**********************************************************************************************
                                  Bit masks and definitions
***********************************************************************************************/

/** Status Register **/
#define MMA8652_STATUS_ZYXOW_MASK													0x80
#define MMA8652_STATUS_ZYXOW_SHIFT												7
#define MMA8652_STATUS_ZOW_MASK														0x40
#define MMA8652_STATUS_ZOW_SHIFT													6
#define MMA8652_STATUS_YOW_MASK														0x20
#define MMA8652_STATUS_YOW_SHIFT													5
#define MMA8652_STATUS_XOW_MASK														0x10
#define MMA8652_STATUS_XOW_SHIFT													4
#define MMA8652_STATUS_ZYX_DR_MASK												0x08
#define MMA8652_STATUS_ZYX_DR_SHIFT												3
#define MMA8652_STATUS_ZDR_MASK														0x04
#define MMA8652_STATUS_ZDR_SHIFT													2
#define MMA8652_STATUS_YDR_MASK														0x02
#define MMA8652_STATUS_YDR_SHIFT													1
#define MMA8652_STATUS_XDR_MASK														0x01
#define MMA8652_STATUS_XDR_SHIFT													0

/** FIFO Status Register **/
#define MMA8652_F_STATUS_F_OVF_MASK												0x80
#define MMA8652_F_STATUS_F_OVF_SHIFT											7
#define MMA8652_F_STATUS_F_WMRK_FLAG_MASK									0x40
#define MMA8652_F_STATUS_F_WMRK_FLAG_SHIFT								6
#define MMA8652_F_STATUS_F_CNT5_MASK											0x20
#define MMA8652_F_STATUS_F_CNT5_SHIFT											5
#define MMA8652_F_STATUS_F_CNT4_MASK											0x10
#define MMA8652_F_STATUS_F_CNT4_SHIFT											4
#define MMA8652_F_STATUS_F_CNT3_MASK											0x08
#define MMA8652_F_STATUS_F_CNT3_SHIFT											3
#define MMA8652_F_STATUS_F_CNT2_MASK											0x04
#define MMA8652_F_STATUS_F_CNT2_SHIFT											2
#define MMA8652_F_STATUS_F_CNT1_MASK											0x02
#define MMA8652_F_STATUS_F_CNT1_SHIFT											1
#define MMA8652_F_STATUS_F_CNT0_MASK											0x01
#define MMA8652_F_STATUS_F_CNT0_SHIFT											0

/** OUT registers **/
#define MMA8652_OUT_SHIFT																	4
#define MMA8652_OUT_TO_INT16(m,l)													((int16_t)((((int16_t)m)<<8)|((uint16_t)l))>>MMA8652_OUT_SHIFT)

/** FIFO Setup Register **/
#define MMA8652_F_SETUP_F_MODE_MASK												0xC0
#define MMA8652_F_SETUP_F_MODE_SHIFT											6
#define MMA8652_F_SETUP_F_MODE_00													0x00
#define MMA8652_F_SETUP_F_MODE_01													0x40
#define MMA8652_F_SETUP_F_MODE_10													0x80
#define MMA8652_F_SETUP_F_MODE_11													0xC0
#define MMA8652_F_SETUP_F_WMRK_MASK												0x3F
#define MMA8652_F_SETUP_F_WMRK_SHIFT											0

/** Trigger configuration register **/
#define MMA8652_TRIG_CFG_TRIG_TRANS_MASK									0x20
#define MMA8652_TRIG_CFG_TRIG_TRANS_SHIFT									5
#define MMA8652_TRIG_CFG_TRIG_LNDPRT_MASK									0x10
#define MMA8652_TRIG_CFG_TRIG_LNDPRT_SHIFT								4
#define MMA8652_TRIG_CFG_TRIG_PULSE_MASK									0x08
#define MMA8652_TRIG_CFG_TRIG_PULSE_SHIFT									3
#define MMA8652_TRIG_CFG_TRIG_FF_MT_MASK									0x04
#define MMA8652_TRIG_CFG_TRIG_FF_MT_SHIFT									2

/** System Mode Registers **/
#define MMA8652_SYSMOD_FGERR_MASK													0x80
#define MMA8652_SYSMOD_FGERR_SHIFT												7
#define MMA8652_SYSMOD_FGT_MASK														0x7C
#define MMA8652_SYSMOD_FGT_SHIFT													2
#define MMA8652_SYSMOD_SYSMOD_MASK												0x03
#define MMA8652_SYSMOD_SYSMOD_SHIFT												0
#define MMA8652_SYSMOD_SYSMOD_STANBY											0x00
#define MMA8652_SYSMOD_SYSMOD_WAKE												0x01
#define MMA8652_SYSMOD_SYSMOD_SLEEP												0x20

/** System Interrupt Status Register **/
#define MMA8652_INT_SOURCE_SRC_ASLP_MASK									0x80
#define MMA8652_INT_SOURCE_SRC_ASLP_SHIFT									7
#define MMA8652_INT_SOURCE_SRC_FIFO_MASK									0x40
#define MMA8652_INT_SOURCE_SRC_FIFO_SHIFT									6
#define MMA8652_INT_SOURCE_SRC_TRANS_MASK									0x20
#define MMA8652_INT_SOURCE_SRC_TRANS_SHIFT								5
#define MMA8652_INT_SOURCE_SRC_LNDPRT_MASK								0x10
#define MMA8652_INT_SOURCE_SRC_LNDPRT_SHIFT								4
#define MMA8652_INT_SOURCE_SRC_PULSE_MASK									0x08
#define MMA8652_INT_SOURCE_SRC_PULSE_SHIFT								3
#define MMA8652_INT_SOURCE_SRC_FF_MT_MASK									0x04
#define MMA8652_INT_SOURCE_SRC_FF_MT_SHIFT								2
#define MMA8652_INT_SOURCE_SRC_DRDY_MASK									0x01
#define MMA8652_INT_SOURCE_SRC_DRDY_SHIFT									0

/** Device ID Register **/
#define MMA8652_WHO_AM_I																	0x4A

/** XYZ Data Configuration Register **/
#define MMA8652_XYZ_CFG_HPF_OUT_MASK											0x10
#define MMA8652_XYZ_CFG_HPF_OUT_SHIFT											4
#define MMA8652_XYZ_CFG_FS_MASK														0x03
#define MMA8652_XYZ_CFG_FS_SHIFT													0
#define MMA8652_XYZ_CFG_FS_2G															0x00
#define MMA8652_XYZ_CFG_FS_4G															0x01
#define MMA8652_XYZ_CFG_FS_8G															0x02

/** High Pass Filter Register **/
#define MMA8652_HP_FILTER_CUTOFF_PULSE_HPF_BY_MASK				0x20
#define MMA8652_HP_FILTER_CUTOFF_PULSE_HPF_BY_SHIFT				5
#define MMA8652_HP_FILTER_CUTOFF_PULSE_HPF_EN_MASK				0x10
#define MMA8652_HP_FILTER_CUTOFF_PULSE_HPF_EN_SHIFT				4
#define MMA8652_HP_FILTER_CUTOFF_SEL_MASK									0x03
#define MMA8652_HP_FILTER_CUTOFF_SEL_SHIFT								0
#define MMA8652_HP_FILTER_CUTOFF_SEL_00										0x00
#define MMA8652_HP_FILTER_CUTOFF_SEL_01										0x01
#define MMA8652_HP_FILTER_CUTOFF_SEL_10										0x02
#define MMA8652_HP_FILTER_CUTOFF_SEL_11										0x03
/* Actual frequency depends on device configurations */

/** Portrait Landscape Status Register **/
#define MMA8652_PL_STATUS_NEWLP_MASK											0x80
#define MMA8652_PL_STATUS_NEWLP_SHIFT											7
#define MMA8652_PL_STATUS_LO_MASK													0x40
#define MMA8652_PL_STATUS_LO_SHIFT												6
#define MMA8652_PL_STATUS_LAPO_MASK												0x06
#define MMA8652_PL_STATUS_LAPO_SHIFT											1
#define MMA8652_PL_STATUS_LAPO_PORTRAIT_UP								0x00
#define MMA8652_PL_STATUS_LAPO_PORTRAIT_DOWN							0x02
#define MMA8652_PL_STATUS_LAPO_LANDSCAPE_RIGHT						0x04
#define MMA8652_PL_STATUS_LAPO_LANDSCAPE_LEFT							0x06
#define MMA8652_PL_STATUS_BAFRO_MASK											0x01
#define MMA8652_PL_STATUS_BAFRO_SHIFT											0

/** Portrait Landscape Configuration Register **/
#define MMA8652_PL_CFG_DBCNTM_MASK												0x80
#define MMA8652_PL_CFG_DBCNTM_SHIFT												7
#define MMA8652_PL_CFG_PL_EN_MASK													0x40
#define MMA8652_PL_CFG_PL_EN_SHIFT												6

/** Portrait Landscape Debounce Register **/
#define MMA8652_PL_COUNT_DBNCE_MASK												0xFF
#define MMA8652_PL_COUNT_DBNCE_SHIFT											0

/** Back Front and Z Compensation Register **/
#define MMA8652_PL_BF_COMP_BKFR_MASK											0xC0
#define MMA8652_PL_BF_COMP_BKFR_SHIFT											6
#define MMA8652_PL_BF_COMP_BKFR_65												0xC0
#define MMA8652_PL_BF_COMP_BKFR_70												0x80
#define MMA8652_PL_BF_COMP_BKFR_75												0x40
#define MMA8652_PL_BF_COMP_BKFR_80												0x00
#define MMA8652_PL_BF_COMP_ZLOCK_MASK											0x07
#define MMA8652_PL_BF_COMP_ZLOCK_SHIFT										0
#define MMA8652_PL_BF_COMP_ZLOCK_14												0x00
#define MMA8652_PL_BF_COMP_ZLOCK_18												0x01
#define MMA8652_PL_BF_COMP_ZLOCK_21												0x02
#define MMA8652_PL_BF_COMP_ZLOCK_25												0x03
#define MMA8652_PL_BF_COMP_ZLOCK_29												0x04
#define MMA8652_PL_BF_COMP_ZLOCK_33												0x05
#define MMA8652_PL_BF_COMP_ZLOCK_37												0x06
#define MMA8652_PL_BF_COMP_ZLOCK_42												0x07

/** Portrait Landscape Threshold and Hysteresis Register **/
#define MMA8652_P_L_THS_REG_P_L_THS_MASK									0xF8
#define MMA8652_P_L_THS_REG_P_L_THS_SHIFT									3
#define MMA8652_P_L_THS_REG_HYS_MASK											0x07
#define MMA8652_P_L_THS_REG_HYS_SHIFT											0
#define MMA8652_P_L_THS_REG_P_L_THS_15										0x38
#define MMA8652_P_L_THS_REG_P_L_THS_20										0x48
#define MMA8652_P_L_THS_REG_P_L_THS_30										0x60
#define MMA8652_P_L_THS_REG_P_L_THS_35										0x68
#define MMA8652_P_L_THS_REG_P_L_THS_40										0x78
#define MMA8652_P_L_THS_REG_P_L_THS_45										0x80
#define MMA8652_P_L_THS_REG_P_L_THS_55										0x98
#define MMA8652_P_L_THS_REG_P_L_THS_60										0xA0
#define MMA8652_P_L_THS_REG_P_L_THS_70										0xB8
#define MMA8652_P_L_THS_REG_P_L_THS_75										0xD8
#define MMA8652_P_L_THS_REG_HYS_0													0x00
#define MMA8652_P_L_THS_REG_HYS_4													0x01
#define MMA8652_P_L_THS_REG_HYS_7													0x02
#define MMA8652_P_L_THS_REG_HYS_11												0x03
#define MMA8652_P_L_THS_REG_HYS_14												0x04
#define MMA8652_P_L_THS_REG_HYS_17												0x05
#define MMA8652_P_L_THS_REG_HYS_21												0x06
#define MMA8652_P_L_THS_REG_HYS_24												0x07

/** Freefall Motion Configuration Register **/
#define MMA8652_FF_MT_CFG_ELE_MASK												0x80
#define MMA8652_FF_MT_CFG_ELE_SHIFT												7
#define MMA8652_FF_MT_CFG_OAE_MASK												0x40
#define MMA8652_FF_MT_CFG_OAE_SHIFT												6
#define MMA8652_FF_MT_CFG_ZEFE_MASK												0x20
#define MMA8652_FF_MT_CFG_ZEFE_SHIFT											5
#define MMA8652_FF_MT_CFG_YEFE_MASK												0x10
#define MMA8652_FF_MT_CFG_YEFE_SHIFT											4
#define MMA8652_FF_MT_CFG_XEFE_MASK												0x08
#define MMA8652_FF_MT_CFG_XEFE_SHIFT											3

/** Freefall Motion Source Register **/
#define MMA8652_FF_MT_SRC_EA_MASK													0x80
#define MMA8652_FF_MT_SRC_EA_SHIFT												7
#define MMA8652_FF_MT_SRC_ZHE_MASK												0x20
#define MMA8652_FF_MT_SRC_ZHE_SHIFT												5
#define MMA8652_FF_MT_SRC_ZHP_MASK												0x10
#define MMA8652_FF_MT_SRC_ZHP_SHIFT												4
#define MMA8652_FF_MT_SRC_YHE_MASK												0x08
#define MMA8652_FF_MT_SRC_YHE_SHIFT												3
#define MMA8652_FF_MT_SRC_YHP_MASK												0x04
#define MMA8652_FF_MT_SRC_YHP_SHIFT												2
#define MMA8652_FF_MT_SRC_XHE_MASK												0x02
#define MMA8652_FF_MT_SRC_XHE_SHIFT												1
#define MMA8652_FF_MT_SRC_XHP_MASK												0x01
#define MMA8652_FF_MT_SRC_XHP_SHIFT												0

/** Freefall Motion Threashold Register **/
#define MMA8652_FF_MT_THS_DBCNTM_MASK											0x80
#define MMA8652_FF_MT_THS_DBCNTM_SHIFT										7
#define MMA8652_FF_MT_THS_THS_MASK												0x7F
#define MMA8652_FF_MT_THS_THS_SHIFT												0

/** Freefall Motion Debounce Register **/
#define MMA8652_FF_MT_COUNT_D_MASK												0xFF
#define MMA8652_FF_MT_COUNT_D_SHIFT												0

/** Transient Configuration Register **/
#define MMA8652_TRANSIENT_CFG_ELE_MASK										0x10
#define MMA8652_TRANSIENT_CFG_ELE_SHIFT										4
#define MMA8652_TRANSIENT_CFG_ZTEFE_MASK									0x08
#define MMA8652_TRANSIENT_CFG_ZTEFE_SHIFT									3
#define MMA8652_TRANSIENT_CFG_YTEFE_MASK									0x04
#define MMA8652_TRANSIENT_CFG_YTEFE_SHIFT									2
#define MMA8652_TRANSIENT_CFG_XTEFE_MASK									0x02
#define MMA8652_TRANSIENT_CFG_XTEFE_SHIFT									1
#define MMA8652_TRANSIENT_CFG_HPF_BYP_MASK								0x01
#define MMA8652_TRANSIENT_CFG_HPF_BYP_SHIFT								0

/** Transient Source Register **/
#define MMA8652_TRANSIENT_SRC_EA_MASK											0x40
#define MMA8652_TRANSIENT_SRC_EA_SHIFT										6
#define MMA8652_TRANSIENT_SRC_ZTRANSE_MASK								0x20
#define MMA8652_TRANSIENT_SRC_ZTRANSE_SHIFT								5
#define MMA8652_TRANSIENT_SRC_Z_TRANS_POL_MASK						0x10
#define MMA8652_TRANSIENT_SRC_Z_TRANS_POL_SHIFT						4
#define MMA8652_TRANSIENT_SRC_YTRANSE_MASK								0x08
#define MMA8652_TRANSIENT_SRC_YTRANSE_SHIFT								3
#define MMA8652_TRANSIENT_SRC_Y_TRANS_POL_MASK						0x04
#define MMA8652_TRANSIENT_SRC_Y_TRANS_POL_SHIFT						2
#define MMA8652_TRANSIENT_SRC_XTRANSE_MASK								0x02
#define MMA8652_TRANSIENT_SRC_XTRANSE_SHIFT								1
#define MMA8652_TRANSIENT_SRC_X_TRANS_POL_MASK						0x01
#define MMA8652_TRANSIENT_SRC_X_TRANS_POL_SHIFT						0

/** Transient Threshold Register **/
#define MMA8652_TRANSIENT_THS_DBCNTM_MASK									0x80
#define MMA8652_TRANSIENT_THS_DBCNTM_SHIFT								7
#define MMA8652_TRANSIENT_THS_THS_MASK										0x7F
#define MMA8652_TRANSIENT_THS_THS_SHIFT										0

/** Transient Count Register **/
#define MMA8652_TRANSIENT_COUNT_MASK											0xFF

/** Pulse Configuration Register **/
#define MMA8652_PULSE_CFG_DPA_MASK												0x80
#define MMA8652_PULSE_CFG_DPA_SHIFT												7
#define MMA8652_PULSE_CFG_ELE_MASK												0x40
#define MMA8652_PULSE_CFG_ELE_SHIFT												6
#define MMA8652_PULSE_CFG_ZDPEFE_MASK											0x20
#define MMA8652_PULSE_CFG_ZDPEFE_SHIFT										5
#define MMA8652_PULSE_CFG_ZSPEFE_MASK											0x10
#define MMA8652_PULSE_CFG_ZSPEFE_SHIFT										4
#define MMA8652_PULSE_CFG_YDPEFE_MASK											0x08
#define MMA8652_PULSE_CFG_YDPEFE_SHIFT										3
#define MMA8652_PULSE_CFG_YSPEFE_MASK											0x04
#define MMA8652_PULSE_CFG_YSPEFE_SHIFT										2
#define MMA8652_PULSE_CFG_XDPEFE_MASK											0x02
#define MMA8652_PULSE_CFG_XDPEFE_SHIFT										1
#define MMA8652_PULSE_CFG_XSPEFE_MASK											0x01
#define MMA8652_PULSE_CFG_XSPEFE_SHIFT										0

/** Pulse Source Register **/
#define MMA8652_PULSE_SRC_EA_MASK													0x80
#define MMA8652_PULSE_SRC_EA_SHIFT												7
#define MMA8652_PULSE_SRC_AxZ_MASK												0x40
#define MMA8652_PULSE_SRC_AxZ_SHIFT												6
#define MMA8652_PULSE_SRC_AxY_MASK												0x20
#define MMA8652_PULSE_SRC_AxY_SHIFT												5
#define MMA8652_PULSE_SRC_AxX_MASK												0x10
#define MMA8652_PULSE_SRC_AxX_SHIFT												4
#define MMA8652_PULSE_SRC_DPE_MASK												0x08
#define MMA8652_PULSE_SRC_DPE_SHIFT												3
#define MMA8652_PULSE_SRC_POLZ_MASK												0x04
#define MMA8652_PULSE_SRC_POLZ_SHIFT											2
#define MMA8652_PULSE_SRC_POLY_MASK												0x02
#define MMA8652_PULSE_SRC_POLY_SHIFT											1
#define MMA8652_PULSE_SRC_POLX_MASK												0x01
#define MMA8652_PULSE_SRC_POLX_SHIFT											0

/** Pulse Threshold for X, Y and Z Registers **/
#define MMA8652_PULSE_THSX_THSX_MASK											0x7F
#define MMA8652_PULSE_THSY_THSY_MASK											0x7F
#define MMA8652_PULSE_THSZ_THSZ_MASK											0x7F

/** Pulse Time Window 1 Register **/
#define MMA8652_PULSE_TMLT_TMLT_MASK											0xFF

/** Pulse Latency Timer Register **/
#define MMA8652_PULSE_LTCY_LTCY_MASK											0xFF

/** Second Pulse Time Window Register **/
#define MMA8652_PULSE_WIND_WIND_MASK											0xFF

/** Auto-Wake Sleep Detection Register **/
#define MMA8652_ASLP_COUNT_D_MASK													0xFF

/** System Control 1 Register **/
#define MMA8652_CTRL1_ASLP_RATE_MASK											0xC0
#define MMA8652_CTRL1_ASLP_RATE_SHIFT											6
#define MMA8652_CTRL1_ASLP_50Hz														0x00
#define MMA8652_CTRL1_ASLP_12_5_Hz												0x40
#define MMA8652_CTRL1_ASLP_6_25_Hz												0x80
#define MMA8652_CTRL1_ASLP_1_56_Hz												0xC0
#define MMA8652_CTRL1_DR_MASK															0x38
#define MMA8652_CTRL1_DR_SHIFT														3
#define MMA8652_CTRL1_DR_800_Hz														0x00
#define MMA8652_CTRL1_DR_400_Hz														0x08
#define MMA8652_CTRL1_DR_200_Hz														0x10
#define MMA8652_CTRL1_DR_100_Hz														0x18
#define MMA8652_CTRL1_DR_50_Hz														0x20
#define MMA8652_CTRL1_DR_12_5_Hz													0x28
#define MMA8652_CTRL1_DR_6_25_Hz													0x30
#define MMA8652_CTRL1_DR_1_56_Hz													0x38
#define MMA8652_CTRL1_F_READ_MASK													0x02
#define MMA8652_CTRL1_F_READ_SHIFT												1
#define MMA8652_CTRL1_ACTIVE_MASK													0x01
#define MMA8652_CTRL1_ACTIVE_SHIFT												0

/** System Control 2 Register **/
#define MMA8652_CTRL2_ST_MASK															0x80
#define MMA8652_CTRL2_ST_SHIFT														7
#define MMA8652_CTRL2_RST_MASK														0x40
#define MMA8652_CTRL2_RST_SHIFT														6
#define MMA8652_CTRL2_SMODS_MASK													0x18
#define MMA8652_CTRL2_SMODS_SHIFT													3
#define MMA8652_CTRL2_SMODS_NORMAL												0x00
#define MMA8652_CTRL2_SMODS_L_N_L_PWR											0x08
#define MMA8652_CTRL2_SMODS_H_RES													0x10
#define MMA8652_CTRL2_SMODS_L_PWR													0x18
#define MMA8652_CTRL2_SLPE_MASK														0x04
#define MMA8652_CTRL2_SLPE_SHIFT													2
#define MMA8652_CTRL2_MODS_MASK														0x03
#define MMA8652_CTRL2_MODS_SHIFT													0
#define MMA8652_CTRL2_MODS_NORMAL													0x00
#define MMA8652_CTRL2_MODS_L_N_L_PWR											0x01
#define MMA8652_CTRL2_MODS_H_RES													0x02
#define MMA8652_CTRL2_MODS_L_PWR													0x03

/** System Control 3 Register **/
#define MMA8652_CTRL3_FIFO_GATE_MASK											0x80
#define MMA8652_CTRL3_FIFO_GATE_SHIFT											7
#define MMA8652_CTRL3_WAKE_TRANS_MASK											0x40
#define MMA8652_CTRL3_WAKE_TRANS_SHIFT										6
#define MMA8652_CTRL3_WAKE_LNDPRT_MASK										0x20
#define MMA8652_CTRL3_WAKE_LNDPRT_SHIFT										5
#define MMA8652_CTRL3_WAKE_PULSE_MASK											0x10
#define MMA8652_CTRL3_WAKE_PULSE_SHIFT										4
#define MMA8652_CTRL3_WAKE_FF_MT_MASK											0x08
#define MMA8652_CTRL3_WAKE_FF_MT_SHIFT										3
#define MMA8652_CTRL3_IPOL_MASK														0x02
#define MMA8652_CTRL3_IPOL_SHIFT													1
#define MMA8652_CTRL3_PP_OD_MASK													0x01
#define MMA8652_CTRL3_PP_OD_SHIFT													0

/** System Control 4 Register **/
#define MMA8652_CTRL4_INT_EN_ASLP_MASK										0x80
#define MMA8652_CTRL4_INT_EN_ASLP_SHIFT										7
#define MMA8652_CTRL4_INT_EN_FIFO_MASK										0x40
#define MMA8652_CTRL4_INT_EN_FIFO_SHIFT										6
#define MMA8652_CTRL4_INT_EN_TRANS_MASK										0x20
#define MMA8652_CTRL4_INT_EN_TRANS_SHIFT									5
#define MMA8652_CTRL4_INT_EN_LNDPRT_MASK									0x10
#define MMA8652_CTRL4_INT_EN_LNDPRT_SHIFT									4
#define MMA8652_CTRL4_INT_EN_PULSE_MASK										0x08
#define MMA8652_CTRL4_INT_EN_PULSE_SHIFT									3
#define MMA8652_CTRL4_INT_EN_FF_MT_MASK										0x04
#define MMA8652_CTRL4_INT_EN_FF_MT_SHIFT									2
#define MMA8652_CTRL4_INT_EN_DRDY_MASK										0x01
#define MMA8652_CTRL4_INT_EN_DRDY_SHIFT										0

/** System Control 5 Register **/
#define MMA8652_CTRL5_INT_CFG_ASLP_MASK										0x80
#define MMA8652_CTRL5_INT_CFG_ASLP_SHIFT									7
#define MMA8652_CTRL5_INT_CFG_FIFO_MASK										0x40
#define MMA8652_CTRL5_INT_CFG_FIFO_SHIFT									6
#define MMA8652_CTRL5_INT_CFG_TRANS_MASK									0x20
#define MMA8652_CTRL5_INT_CFG_TRANS_SHIFT									5
#define MMA8652_CTRL5_INT_CFG_LNDPRT_MASK									0x10
#define MMA8652_CTRL5_INT_CFG_LNDPRT_SHIFT								4
#define MMA8652_CTRL5_INT_CFG_PULSE_MASK									0x08
#define MMA8652_CTRL5_INT_CFG_PULSE_SHIFT									3
#define MMA8652_CTRL5_INT_CFG_FF_MT_MASK									0x04
#define MMA8652_CTRL5_INT_CFG_FF_MT_SHIFT									2
#define MMA8652_CTRL5_INT_CFG_DRDY_MASK										0x01
#define MMA8652_CTRL5_INT_CFG_DRDY_SHIFT									0

/** Data Calibration Registers **/
#define MMA8652_OFF_X_OFF_X_MASK													0xFF
#define MMA8652_OFF_Y_OFF_Y_MASK													0xFF
#define MMA8652_OFF_Z_OFF_Z_MASK													0xFF

/* Prototypes */

/* -------------------------------------------------------------------------- */
/*          MMA8652 Configurations                                            */
/* -------------------------------------------------------------------------- */

/**
 * @brief       Test if the I2C bus is free and try to communicate with the sensor.
 * 				If everything is ok, reserve the I2C bus.
 * @details     This will initialize the I2C driver to communicate with the
 *				MMA8652 sensor. It will test if the MCU is getting the right
 *				answer from the sensor, testing this way the I2C communication.
 *				If the I2C isn't free it will return false.
 */
bool mma8652_reserve_i2c_bus(void);

/**
 * @brief       Turn on the MMA8652 sensor
 * @details     This function will put the MMA8652 sensor in a standby mode by
 *				setting to 1 the SBYB (bit 0) of CTRL_REG_1.
 */
bool mma8652_power_on(void);

/**
 * @brief       Put the MMA8652 in a standby mode
 * @details     This function will put the MMA8652 sensor in a standby mode by
 *				setting to 0 the SBYB (bit 0) of CTRL_REG_1.
 */
bool mma8652_power_off(void);

/**
 * @brief       Set the MMA8652 sensor dynamic range
 * @details     This function will set the dynamic range of the sensor by first clear
 *							bit 0 and 1 of the XYZ_DATA_CFG_REG with the XYZ_CFG_FS mask and next
 								setting these bits for the desired dynamic range.
 * @param       dynamic_range
 *              The desired dynamic range. Possible values:
 *							- MMA8652_XYZ_CFG_FS_2G;
 *							- MMA8652_XYZ_CFG_FS_4G;
 *							- MMA8652_XYZ_CFG_FS_8G.
 */
bool mma8652_set_dynamic_range(uint8_t dynamic_range);

/**
 * @brief       Set the MMA8652 active data rate
 * @details     This function will set the active data rate of the sensor by first clear
 *							bit 3, 4 and 5 of the CTRL_REG_1 with the MMA8652_CTRL1_DR mask and next
 								setting these bits for the desired data rate.
 * @param       active_data_rate
 *              The desired data rate. Possible values:
 *							- MMA8652_CTRL1_DR_800_Hz;
 *							- MMA8652_CTRL1_DR_400_Hz;
 *							- MMA8652_CTRL1_DR_200_Hz;
 *							- MMA8652_CTRL1_DR_100_Hz;
 *							- MMA8652_CTRL1_DR_50_Hz;
 *							- MMA8652_CTRL1_DR_12_5_Hz;
 *							- MMA8652_CTRL1_DR_6_25_Hz;
 *							- MMA8652_CTRL1_DR_1_56_Hz.
 */
bool mma8652_set_active_data_rate(uint8_t active_data_rate);

/**
 * @brief       Set the MMA8652 oversampling mode in active mode
 * @details     This function will set the oversampling in active mode of the sensor by first clear
 *							bit 0 and 1 of the CTRL_REG_2 with the MMA8652_CTRL2_MODS mask and next
 								setting these bits for the desired oversampling in active mode.
 * @param       oversampling_mode
 *              The desired oversampling mode. Possible values:
 *							- MMA8652_CTRL2_MODS_NORMAL (Normal active mode);
 *							- MMA8652_CTRL2_MODS_L_N_L_PWR (Low noise + power active mode);
 *							- MMA8652_CTRL2_MODS_H_RES (High-resolution active mode);
 *							- MMA8652_CTRL2_MODS_L_PWR (Low-power active mode).
 */
bool mma8652_set_oversampling_active_mode(uint8_t oversampling_active_mode);

/**
 * @brief       Set the high-pass filter status
 * @param       high_pass_status
 *              True to turn on the high-pass filter, false to turn it off.
 */
bool mma8652_set_hp_filter(bool enable_hp_filter);

/**
 * @brief       Set the cutoff frequency of the filter
 * @details     This function will set the cutoff frequency of the filter of
 *							the sensor by first clear the bit 0 and 1 of the MMA8652_HP_FILTER_CUTOFF
 *							with the MMA8652_HP_FILTER_CUTOFF_SEL mask and next setting these bits
 * 							for the desired cutoff frequency.
 * @param       hp_cutoff_frequency
 *              The desired cutoff frequency. Possible values:
 *							- MMA8652_HP_FILTER_CUTOFF_SEL_00;
 *							- MMA8652_HP_FILTER_CUTOFF_SEL_01;
 *							- MMA8652_HP_FILTER_CUTOFF_SEL_10;
 *							- MMA8652_HP_FILTER_CUTOFF_SEL_11.
 *              Note that the filter cutoff options change based on the data rate
 *              selected. Check the datasheet for more informations.
 */
bool mma8652_set_hp_cutoff_frequency(uint8_t hp_cutoff_frequency);

/**
 * @brief       Set the MMA8652 auto-sleep mode
 * @details     This function will set the auto-sleep mode of the sensor by setting the bit
 *              2 of the CTRL_REG_2 with the MMA8652_CTRL2_SLPE.
 * @param       auto_sleep_on
 *              True to auto-sleep on, false to auto-sleep off.
 */
void mma8652_set_auto_sleep_mode(bool auto_sleep_on);

/**
 * @brief       Set the MMA8652 sleep data rate
 * @details     This function will set the sleep data rate of the sensor by first clear
 *							bit 6 and 7 of the CTRL_REG_1 with the MMA8652_CTRL1_ASLP_RATE mask and next
 								setting these bits for the desired data rate.
 * @param       sleep_data_rate
 *              The desired data rate. Possible values:
 *							- MMA8652_CTRL1_ASLP_50Hz;
 *							- MMA8652_CTRL1_ASLP_12_5_Hz;
 *							- MMA8652_CTRL1_ASLP_6_25_Hz;
 *							- MMA8652_CTRL1_ASLP_1_56_Hz.
 */
bool mma8652_set_sleep_data_rate(uint8_t sleep_data_rate);

/**
 * @brief       Set the MMA8652 oversampling in sleep mode
 * @details     This function will set the oversampling in sleep mode of the sensor by first clear
 *							bit 3 and 4 of the CTRL_REG_2 with the MMA8652_CTRL2_SMODS mask and next
 								setting these bits for the desired oversampling in sleep mode.
 * @param       oversampling_mode
 *              The desired oversampling mode. Possible values:
 *							- MMA8652_CTRL2_SMODS_NORMAL (Normal sleep mode);
 *							- MMA8652_CTRL2_SMODS_L_N_L_PWR (Low noise + power sleep mode);
 *							- MMA8652_CTRL2_SMODS_H_RES (High-resolution sleep mode);
 *							- MMA8652_CTRL2_SMODS_L_PWR (Low-power sleep mode).
 */
bool mma8652_set_oversampling_sleep_mode(uint8_t oversampling_sleep_mode);

/**
 * @brief       Set the read mode of the MMA8652 sensor
 * @details     This function will set the read mode of the sensor to fast read mode
 *							ot to the normal read mode by setting the bit 1 of the CTRL_REG_1.
 * @param       fast_read
 *              True to change to fast read mode and to normal read mode otherwise.
 */
bool mma8652_set_fast_read(bool fast_read);

/**
 * @brief       Check if there is new xyz axis data available from the MMA8652 sensor
 * @details     If there is new xyz axis data available it will set to 1 the status field
 * 							of the MMA8652 data structure.
 */
bool mma8652_new_xyz_data_available(void);

/**
 * @brief       Read the new xyz axis data
 * @details     It will get the new xyz axis info from the MMA8652 sensor and update
 *							the x, y and z field of the MMA8652 data structure with these values.
 */
struct MMA8652_xyz_data mma8652_read_xyz_data(void);

/**
 * @brief       Returns the MMA8652 data structure
 * @details     Returns the MMA8652 xyz axis data structure that contains the last values for
 *							the x, y and z axis as well as the status flag of the sensor.
 */
struct MMA8652_xyz_data mma8652_get_xyz_data(void);

/* -------------------------------------------------------------------------- */
/*          MMA8652 Specific Behaviours                                       */
/* -------------------------------------------------------------------------- */

/**
 * @brief       Clear all the interrupt vectors
 */
bool mma8652_clear_interrupts();

/**
 * @brief       Set a specific interrupt
 * @details     This function will configure a specific interrupt to take place in the
 *              interrupt 1 pin or interrupt 2 pin.
 * @param       interrupt_id
 *              The interrupt to be configured. Possible values are:
 *              - MMA8652_CTRL4_INT_EN_DRDY_MASK (New data received);
 *              - MMA8652_CTRL4_INT_EN_FF_MT_MASK (Motion/freefall. Can wake up the sensor from sleep);
 *              - MMA8652_CTRL4_INT_EN_PULSE_MASK (Tap detected. Can wake up the sensor from sleep);
 *              - MMA8652_CTRL4_INT_EN_LNDPRT_MASK (Orientation. Can wake up the sensor from sleep);
 *              - MMA8652_CTRL4_INT_EN_TRANS_MASK (Transient. Can wake up the sensor from sleep);
 *              - MMA8652_CTRL4_INT_EN_FIFO_MASK (FIFO);
 *              - MMA8652_CTRL4_INT_EN_ASLP_MASK (Auto-sleep).
 *              interrupt_pin
 *              Specifies the interrupt pin: Possible values are:
 *              - MMA8652_INT_1;
 *              - MMA8652_INT_2.
 *              wake_up
 *              Tells the sensor to wake up from sleep. Only make sense when the interrupt id is
 *              MMA8652_CTRL4_INT_EN_FF_MT_MASK, MMA8652_CTRL4_INT_EN_PULSE_MASK, MMA8652_CTRL4_INT_EN_LNDPRT_MASK
 *              or MMA8652_CTRL4_INT_EN_TRANS_MASK.
 */
bool mma8652_set_interrupt(uint8_t interrupt_id, enum interrupt_pin interrupt_pin, bool wake_up);

/**
 * @brief       Configure the freefall/motion profile of the sensor
 * @param       profile_type
 *              Specifies the profile between freefall and motion profile. Possible values:
 *              - ~MMA8652_FF_MT_CFG_OAE_MASK (freefall profile);
 *              - MMA8652_FF_MT_CFG_OAE_MASK (motion profile).
 *              x_enabled
 *              Indicates if the x will be monitorized. Possible values are:
 *              - MMA8652_FF_MT_CFG_XEFE_MASK;
 *              - ~MMA8652_FF_MT_CFG_XEFE_MASK.
 *              y_enabled
 *              Indicates if the y will be monitorized. Possible values are:
 *              - MMA8652_FF_MT_CFG_YEFE_MASK;
 *              - ~MMA8652_FF_MT_CFG_YEFE_MASK.
 *              z_enabled
 *              Indicates if the z will be monitorized. Possible values are:
 *              - MMA8652_FF_MT_CFG_ZEFE_MASK;
 *              - ~MMA8652_FF_MT_CFG_ZEFE_MASK.
 *              motion_threshold
 *              Specifies the threshold value for the freefall/motion detection.
 *              count_threshold
 *              Specifies the count value which after that the interrupt will be launched, i.e. after count value
 *              verified thresholds the interrupt will be launched.
 */
bool mma8652_set_freefall_motion_profile(uint8_t profile_type, uint8_t x_enabled, uint8_t y_enabled, uint8_t z_enabled, uint8_t motion_threshold, uint8_t count_threshold);

/**
 * @brief       Enables or disables freefall/motion profile
 * @param       enable
 *              True = enable it, false = disable it.
 */
bool mma8652_enable_freefall_motion_profile(bool enable);

/**
 * @brief       Reads the freefall/motion data
 * @details     Reads the MMA8652_FF_MT_SRC register that contains the freefall/motion
 *              info. Next update the global variable _mma8652_motion_data with this information.
 */
struct MMA8652_motion_data mma8652_read_freefall_motion_data();

/**
 * @brief       Returns the MMA8652 xyz axis data structure
 */
struct MMA8652_motion_data mma8652_get_motion_data(void);

/**
 * @brief       Configure the transient profile of the sensor
 * @param       x_enabled
 *              Indicates if the x will be monitorized. Possible values are:
 *              - MMA8652_TRANSIENT_CFG_XEFE_MASK;
 *              - ~MMA8652_TRANSIENT_CFG_XEFE_MASK.
 *              y_enabled
 *              Indicates if the y will be monitorized. Possible values are:
 *              - MMA8652_TRANSIENT_CFG_YEFE_MASK;
 *              - ~MMA8652_TRANSIENT_CFG_YEFE_MASK.
 *              z_enabled
 *              Indicates if the z will be monitorized. Possible values are:
 *              - MMA8652_TRANSIENT_CFG_ZEFE_MASK;
 *              - ~MMA8652_TRANSIENT_CFG_ZEFE_MASK.
 *              bypass_hp_filter_enabled
 *              Indicates if the high-pass will be bypassed. In the bypass case this transient profile
 *              will have the same bahaviour as a the freefall/motion profile.
 *              - MMA8652_TRANSIENT_CFG_HPF_BYP_MASK (bypass disabled);
 *              - ~MMA8652_TRANSIENT_CFG_HPF_BYP_MASK (bypass enabled).
 *              transient_threshold
 *              Specifies the threshold value for the transient detection.
 *              count_threshold
 *              Specifies the count value which after that the interrupt will be launched, i.e. after count value
 *              verified thresholds the interrupt will be launched.
 */
bool mma8652_set_transient_profile(uint8_t x_enabled, uint8_t y_enabled, uint8_t z_enabled, uint8_t bypass_hp_filter_enabled, uint8_t transient_threshold, uint8_t count_threshold);

/**
 * @brief       Enables or desables transient profile
 * @param       enable
 *              True for enable it, false disable it.
 */
bool mma8652_enable_transient_profile(bool enable);

/**
 * @brief       Reads the transient data
 * @details     Reads the MMA8652_TRANSIENT_SRC register that contains the transient
 *              info. Next update the global variable _mma8652_transient_data with this information.
 */
struct MMA8652_transient_data mma8652_read_transient_data();

/**
 * @brief       Returns the MMA8652 transient data structure
 */
struct MMA8652_transient_data mma8652_get_transient_data(void);

/**
 * @brief       Enables or desables orientation detection
 * @param       enable
 *              True for enable it, false disable it.
 */
bool mma8652_enable_orientation_detection(bool enable);

/**
 * @brief       Check if there is a new orientation profile
 */
bool mma8652_new_orientation_data_available(void);

/**
 * @brief       Reads the new orientation profile from the sensor
 */
struct MMA8652_orientation_data mma8652_read_orientation_data(void);

/**
 * @brief       Returns the MMA8652 orientation data structure
 */
struct MMA8652_orientation_data mma8652_get_orientation_data(void);

#endif /* MMA8652_H */
